import React, { Component } from 'react'
import { Text, View, Image, ImageBackground, ScrollView, TouchableOpacity, TextInput, FlatList, RefreshControl, ToastAndroid } from 'react-native'
import { blue, light, softGrey } from '../utils/Color'
import { xs, vs } from '../utils/Responsive'
import { f, c, b, p } from '../utils/StyleHelper'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
import FastImage from 'react-native-fast-image'
import LinearGradient from 'react-native-linear-gradient'
import Products from './ProductFavorite'
import Share from 'react-native-share'
import AsyncStorage from '@react-native-async-storage/async-storage'
import axios from 'axios'
import { API_LOCAL, API_URL } from '@env'

const url = 'https://awesome.contents.com/';
const title = 'Awesome Contents';
const message = 'Please check this out.';
const icon = 'data:<data_type>/<file_extension>;base64,<base64_data>';
const options = Platform.select({
  ios: {
    activityItemSources: [
      {
        // For sharing url with custom title.
        placeholderItem: { type: 'url', content: url },
        item: {
          default: { type: 'url', content: url },
        },
        subject: {
          default: title,
        },
        linkMetadata: { originalUrl: url, url, title },
      },
      {
        // For sharing text.
        placeholderItem: { type: 'text', content: message },
        item: {
          default: { type: 'text', content: message },
          message: null, // Specify no text to share via Messages app.
        },
        linkMetadata: {
          // For showing app icon on share preview.
          title: message,
        },
      },
      {
        // For using custom icon instead of default text icon at share preview when sharing with message.
        placeholderItem: {
          type: 'url',
          content: icon,
        },
        item: {
          default: {
            type: 'text',
            content: `${message} ${url}`,
          },
        },
        linkMetadata: {
          title: message,
          icon: icon,
        },
      },
    ],
  },
  default: {
    title,
    subject: title,
    message: `${message} ${url}`,
  },
});

export default class Profile extends Component {
	constructor(props) {
		super(props);
		this.state = {
            loading: true,
            check_id_login: null,
			nama: 'loading...',
            count: '',
            saldo: '',
			edit: false,
			hidden_myOrder: true,
            hidden_recentlyView: false,
            hidden_myFavorite: false,
            hidden_myCoupon: false,
            hidden_referal: false,
            hidden_touch: false,
            touch: 'myOrder',
            jenis_kelamin: 'Laki-laki',
            date: new Date(),
            timeActive: false,
            age_now: '2000-07-18',
            status: [
                {jenis_status: 'Belum dibayar', image: require('../asset/clock.png')},
				{jenis_status: 'Selesai', image: require('../asset/belumbayar_icon.png')},
                {jenis_status: 'Kadaluarsa', image: require('../asset/clock.png')}
			],
            all_challenges: null,
            clicked: 0,
            semua: undefined,
            belum_dibayar: {"276":{"status":"pending payment","tgl_pesanan":"2021-09-30T03:46:40.000000Z","total_pesanan":"5199000","total_qty":"2","store":{"Xiaomi Official Store":{"logo_toko":"93_toko.png","item":[{"id":"28","nama":"Xiaomi Mi 11 Lite (8GB+128GB)","image":"28_1.jpg","harga":"4099000","quantity":"1","rating":""}]},"Nike":{"logo_toko":"94_toko.jpg","item":[{"id":"31","nama":"Sepatu Sneakers Air Force 1 '07 AN20 White Black","image":"31_1.jpg","harga":"1100000","quantity":"1","rating":""}]}}}},
            kadaluarsa: {
                "0":{id: 0},
                "1":{id: 1},
                "2":{id: 2}
            },
            redeem_order: [
                {id: 1, items:[{ id: 1, nama: 'Sarung Tangan', image: require('../asset/sarung_tangan.png'), harga: '120000', quantity: '1', rating: '5' , giftcard: true},{ id: 1, nama: 'Sarung Tangan', image: require('../asset/sarung_tangan.png'), harga: '120000', quantity: '1', rating: '5' , giftcard: true},{ id: 1, nama: 'Sarung Tangan', image: require('../asset/sarung_tangan.png'), harga: '120000', quantity: '1', rating: '5' , giftcard: true},{ id: 1, nama: 'Sarung Tangan', image: require('../asset/sarung_tangan.png'), harga: '120000', quantity: '1', rating: '5' , giftcard: true},{ id: 1, nama: 'Sarung Tangan', image: require('../asset/sarung_tangan.png'), harga: '120000', quantity: '1', rating: '5' , giftcard: true},{ id: 1, nama: 'Sarung Tangan', image: require('../asset/sarung_tangan.png'), harga: '120000', quantity: '1', rating: '5' , giftcard: true}], logo_toko: require('../asset/logo_apotek.png'), nama_toko: 'Apotek Sehati', no_pesanan: '123456789', tgl_pesan: '19/08/2021', total_pesanan: '120000', status: 'Sudah redeem'},
                {id: 2, items:[{ id: 1, nama: 'Sarung Tangan', image: require('../asset/sarung_tangan.png'), harga: '120000', quantity: '1', rating: null, giftcard: false}], logo_toko: require('../asset/logo_apotek.png'), nama_toko: 'Apotek Sehati', no_pesanan: '5', tgl_pesan: '19/08/2021', total_pesanan: '120000', status: 'Sudah redeem'}
            ],
            beli_lagi: [
                {id: 1, nama: 'Nike Air Max', harga: '1200000', image: require('../asset/sepatu.jpg')},
                {id: 2, nama: 'Nike Air Max', harga: '1200000', image: require('../asset/sepatu.jpg')},
                {id: 3, nama: 'Nike Air Max', harga: '1200000', image: require('../asset/sepatu.jpg')}
            ],
            my_favorite: [{id: 1, nama: 'Black Sarung Tangan', image: require('../asset/sarung_tangan.png'), harga: '60000', lokasi: 'Semarang', rating: '5', ulasan: '10'}],
            promoCode: [
                {id: '1', jenis_voucher: 'DISKON PRODUK', code: 'GPLUS10K', desc: 'Potongan harga 10RB', for: 'Pengguna Baru', exp: 'Hingga 31 Desember 2021'},
                {id: '2', jenis_voucher: 'DISKON PRODUK', code: 'GPLUS10K', desc: 'Potongan harga 10RB', for: 'Pengguna Baru', exp: 'Hingga 31 Desember 2021'},
                {id: '3', jenis_voucher: 'DISKON PRODUK', code: 'GPLUS10K', desc: 'Potongan harga 10RB', for: 'Pengguna Baru', exp: 'Hingga 31 Desember 2021'}
            ],
		}
	}

    async componentDidMount() {
        const id_login = await AsyncStorage.getItem('@id_login')
        this.setState({ check_id_login: id_login })

        if (this.state.check_id_login === null) {
            const guest = await AsyncStorage.getItem('id_login_guest')

            axios.get(API_URL+'/main/shopping/cart', {params: {
                id_login: guest
            }})
            .then(res => {
                this.setState({
                    count: res.data.data.length,
                    loading: false
                })
            })
        } else {
            axios.get(API_URL+'/main/shopping/order/history/belum_dibayar', {params:{
                id_login: id_login
            }})
            .then(res => {
                this.setState({belum_dibayar: res.data.data})
                console.log("ORDER BELUM DIBAYAR ==> ", JSON.stringify(res.data.data))
            })

            axios.get(API_URL+'/main/get_user_by_id_login', {params: {
                id_login: id_login
            }})
            .then(result => {
                // console.log(result.data.data)
                this.setState({
                    nama: result.data.data.nama,
                    saldo: result.data.data.balance,
                    loading: false
                })
            })

            axios.get(API_URL+'/main/shopping/cart', {params: {
                id_login: id_login
            }})
            .then(res => {
                this.setState({
                    count: res.data.data.length,
                    loading: false
                })
            })

            axios.get(API_URL+'/main/shopping/favorit',{params:{
                id_login: id_login
            }})
            .then(res => {
                this.setState({my_favorite: res.data.data})
                // return console.log("FAVORIT ==> ", res.data.data[0].liked)
            })

            /* Order History Semua */
            axios.get(API_URL+'/main/shopping/order/history',{params:{
                id_login: id_login
            }})
            .then(res => {
                /*
                    {
                    id: 1,
                    items:[{
                        id: 1,
                        nama: 'Sarung Tangan',
                        image: require('../asset/sarung_tangan.png'),
                        harga: '120000',
                        quantity: '1',
                        rating: '5' ,
                        giftcard: true
                    }],
                    logo_toko: require('../asset/logo_apotek.png'),
                    nama_apotek: 'Apotek Sehati',
                    no_pesanan: '123456789',
                    tgl_pesan: '19/08/2021',
                    total_pesanan: '120000',
                    status: 'Belum dikirim'} 
                 */
                return console.warn("order history semua ==> ", res.data.data)
                // this.setState({my_favorite: res.data.data})
            })
        }

        // axios.get('https://api.healthplus.co/main/shopping/order/pending_payment_order', {params: {
        //     id_login: '118205008143923829902'
        // }})
        // .then(res => {
        //     // console.log(res.data.data)
        //     this.setState({
        //         loading: false,
        //         pending: res.data.data
        //     })
        // })
        // .catch(e => {
        //     console.log('Pending History => '+e)
        // })

        // axios.get('https://api.healthplus.co/main/shopping/order/delivering_order', {params: {
        //     id_login: '118205008143923829902'
        // }})
        // .then(res => {
        //     // console.log(res.data.data)
        //     this.setState({
        //         loading: false,
        //         delivering: res.data.data
        //     })
        // })
        // .catch(e => {
        //     console.log('Pending History => '+e)
        // })

        // axios.get('https://api.healthplus.co/main/shopping/order/finished_order', {params: {
        //     id_login: '118205008143923829902'
        // }})
        // .then(res => {
        //     // console.log(res.data.data)
        //     this.setState({
        //         loading: false,
        //         finished: res.data.data
        //     })
        // })
        // .catch(e => {
        //     console.log('Pending History => '+e)
        // })

        this.calculate_age(this.state.date)
    }

	async editBtn() {
        const id_login = await AsyncStorage.getItem('@id_login')
        if (this.state.edit === false) {
            this.setState({ edit: true })
        } else {
            // this.setState({ loading: true })
            // axios.get(API_URL+'/main/user/update', {params: {
            //     id_login: id_login,
            //     nama: this.state.nama
            // }})
            // .then(res => {
            //     console.log(res.data)
            // })
            this.setState({
                edit: false,
                loading: false
            })
            ToastAndroid.show('function is still under development', ToastAndroid.SHORT)
        }
    }

    calculate_age = (dob1) => {
        var today = new Date();
        var birthDate = new Date(dob1);
        var age_now = today.getFullYear() - birthDate.getFullYear();
        var m = today.getMonth() - birthDate.getMonth();
        if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) 
        {
            age_now--;
        }
        console.log(age_now);
        this.setState({ age_now: age_now })
        return age_now;
    }

    async swicth(jenis_status, index) {
		await this.setState({
            clicked: index,
            loading: false,
            all_challenges: jenis_status
        })
        // console.log(this.state.clicked)
	}

    format(number) {
        if (number) {
            var rupiah = "";
            var numberrev = number
                .toString()
                .split("")
                .reverse()
                .join("");
            for (var i = 0; i < numberrev.length; i++)
                if (i % 3 == 0) rupiah += numberrev.substr(i, 3) + ".";
            return (
                "Rp " +
                rupiah
                .split("", rupiah.length - 1)
                .reverse()
                .join("")
            );
        } else {
            return (
                "Rp "+number
            );
        }
    }

    render() {
        return (
            <>
                <ScrollView showsVerticalScrollIndicator={false} style={{ backgroundColor: '#181B22' }} refreshControl={<RefreshControl onRefresh={() => this.componentDidMount()} refreshing={this.state.loading} />}>
                    <View style={{ width: '100%', alignItems: 'center', backgroundColor: '#181B22' }}>
                    	{this.state.hidden_touch === true &&  (
                            <>
                                <View style={{ width: '100%', alignItems: 'center', backgroundColor: '#121212' }}>
                                    <View style={[ b.mt4, b.mb4, p.row, { width: wp('90%') }]}>
                                        <View style={{ width: wp('45%') }}>
                                            <FastImage source={require('../asset/giftplus.png')} style={{ width: xs(120), height: xs(32) }} />
                                        </View>
                                        <View style={{ width: wp('45%'), flexDirection: 'row-reverse' }}>
                                            <TouchableOpacity onPress={() => this.props.navigation.push('Drawer')}>
                                                <Image source={require('../asset/menu.png')} style={{ width: xs(22), height: xs(22), tintColor: light }} />
                                            </TouchableOpacity>
                                            <TouchableOpacity onPress={() => this.props.navigation.push('Notification')} style={[ b.mr2 ]}>
                                                <Image source={require('../asset/notification.png')} style={{ width: xs(22), height: xs(22), tintColor: light }} />
                                            </TouchableOpacity>
                                            <TouchableOpacity onPress={() => this.props.navigation.push('Cart')} style={[ b.mr2 ]}>
                                                <Image source={require('../asset/cart.png')} style={{ width: xs(19), height: xs(20), tintColor: light }} />
                                                {this.state.count !== 0 && (
                                                    <View style={{ width: xs(12), height: xs(12), borderRadius: xs(12), backgroundColor: 'darkorange', position: 'absolute', right: 0, alignItems: 'center', justifyContent: 'center' }}>
                                                        <Text style={[ c.light, f._10 ]}>{this.state.count}</Text>
                                                    </View>
                                                )}
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                </View>
                                {this.state.check_id_login === null ?
                                    <TouchableOpacity onPress={() => this.props.navigation.push('CreateAccount')} style={[ b.mt2, { width: wp('90%'), height: hp('7%'), alignItems: 'center', justifyContent: 'center', borderWidth: 1, borderColor: blue, borderRadius: xs(5) }]}>
                                        <Text style={[ c.light, { fontFamily: 'Roboto-Bold' }]}>Login/Register</Text>
                                    </TouchableOpacity>
                                    :
                                    <View style={[ b.mt2, { width: wp('90%'), flexDirection: 'row', alignItems: 'center' }]}>
                                        <View style={{ width: xs(60), height: xs(60), alignItems: 'center', justifyContent: 'center', borderRadius: xs(60), backgroundColor: 'grey' }}>
                                            <Image source={require('../asset/user.png')} style={{ width: xs(55), height: xs(55), tintColor: softGrey }} />
                                        </View>
                                        <ImageBackground source={require('../asset/group_1035.png')} style={[ b.ml3, { width: xs(250), height: xs(85) }]} imageStyle={[ b.roundedLow ]}>
                                            <View style={{ width: '100%', height: xs(80), alignItems: 'center' }}>
                                                <View style={[ b.mt1, { width: xs(220), flexDirection: 'row', alignItems: 'center' }]}>
                                                    <View style={{ width: xs(190) }}>
                                                    {this.state.edit === true ?
                                                        <TextInput
                                                            placeholder='Masukkan Nama Anda'
                                                            placeholderTextColor={light}
                                                            style={{ width: xs(160), height: xs(35), borderWidth: 1, borderColor: light, borderRadius: xs(5), color: light }}
                                                            value={this.state.nama}
                                                            onChangeText={(text) => this.setState({ nama: text })}
                                                        />
                                                        :
                                                        <Text style={[ c.light, { fontFamily: 'lineto-circular-pro-bold' }]}>{this.state.nama}</Text>
                                                    }
                                                    </View>
                                                    <View style={{ width: xs(30), alignItems: 'flex-end' }}>
                                                    <TouchableOpacity onPress={() => this.editBtn()}>
                                                        <Image source={this.state.edit === false ? require('../asset/edit.png') : require('../asset/checkmark.png')} style={{ width: xs(20), height: xs(20), tintColor: light }} />
                                                    </TouchableOpacity>
                                                    </View>
                                                </View>
                                                <View style={[ b.mt1, { width: '100%', height: xs(55), backgroundColor: blue, borderBottomLeftRadius: xs(5), borderBottomRightRadius: xs(5), flexDirection: 'row' }]}>
                                                    <View style={{ width: xs(150), alignItems: 'center' }}>
                                                        <View style={[ b.ml1, b.mt1, { flexDirection: 'row', alignItems: 'center' }]}>
                                                            <FastImage source={require('../asset/belumbayar_icon.png')} style={{ width: xs(20), height: xs(20) }} />
                                                            <Text style={[ b.ml1, c.light, { fontFamily: 'Roboto-Regular' }]}>Saldo Anda</Text>
                                                        </View>
                                                        <Text style={[ c.light, { fontFamily: 'Roboto-Bold' }]} numberOfLines={1}>{this.format(this.state.saldo)}</Text>
                                                    </View>
                                                    <View style={{ width: xs(100), alignItems: 'flex-end' }}>
                                                        <TouchableOpacity onPress={() => this.props.navigation.push('TopUp')} style={[ b.roundedLow, b.mt1, b.mr1, { width: xs(80), height: xs(30), backgroundColor: light, alignItems: 'center', justifyContent: 'center' }]}>
                                                            <Text style={[ c.blue, { fontFamily: 'Roboto-Bold' }]}>Isi Saldo</Text>
                                                        </TouchableOpacity>
                                                    </View>
                                                </View>
                                            </View>
                                        </ImageBackground>
                                    </View>
                                }

                                {this.state.check_id_login === null ?
                                    <>
                                        <View style={[ b.mt4, { width: wp('90%'), flexDirection: 'row', alignItems: 'center' }]}>
                                            <View style={{ width: wp('45%') }}>
                                                <Text style={[ c.light, { fontFamily: 'Roboto-Bold' }]}>Pesanan Saya</Text>
                                            </View>
                                            <View style={{ width: wp('45%'), alignItems: 'flex-end' }}>
                                                <TouchableOpacity>
                                                    <Text style={[ c.blue, { fontFamily: 'Roboto-Regular' }]}>Lihat Semua</Text>
                                                </TouchableOpacity>
                                            </View>
                                        </View>
                                        <View style={[ b.mt4, b.mb3, { width: wp('90%'), flexDirection: 'row' }]}>
                                            <TouchableOpacity onPress={() => ToastAndroid.show('You have to login first', ToastAndroid.SHORT)} style={{ width: wp('20%'), alignItems: 'center', marginRight: wp('4%') }}>
                                                <View style={{ width: wp('15%'), height: wp('15%'), backgroundColor: blue, borderRadius: wp('2%'), alignItems: 'center', justifyContent: 'center' }}>
                                                    <Image source={require('../asset/belumbayar_icon.png')} style={{ width: xs(30), height: xs(30) }} />
                                                </View>
                                                <Text style={[ c.light, f._12, { textAlign: 'center' }]}>Belum dibayar</Text>
                                            </TouchableOpacity>
                                            <TouchableOpacity onPress={() => ToastAndroid.show('You have to login first', ToastAndroid.SHORT)} style={{ width: wp('20%'), alignItems: 'center', marginRight: wp('4%') }}>
                                                <View style={{ width: wp('15%'), height: wp('15%'), backgroundColor: blue, borderRadius: wp('2%'), alignItems: 'center', justifyContent: 'center' }}>
                                                    <Image source={require('../asset/giftcardmenu_icon.png')} style={{ width: xs(30), height: xs(30) }} />
                                                </View>
                                                <Text style={[ c.light, f._12, { textAlign: 'center' }]}>Belum dikirim</Text>
                                            </TouchableOpacity>
                                            <TouchableOpacity onPress={() => ToastAndroid.show('You have to login first', ToastAndroid.SHORT)} style={{ width: wp('20%'), alignItems: 'center', marginRight: wp('4%') }}>
                                                <View style={{ width: wp('15%'), height: wp('15%'), backgroundColor: blue, borderRadius: wp('2%'), alignItems: 'center', justifyContent: 'center' }}>
                                                    <Image source={require('../asset/clock.png')} style={{ width: xs(30), height: xs(30), tintColor: light }} />
                                                </View>
                                                <Text style={[ c.light, f._12, { textAlign: 'center' }]}>Tertunda</Text>
                                            </TouchableOpacity>
                                            <TouchableOpacity onPress={() => ToastAndroid.show('You have to login first', ToastAndroid.SHORT)} style={{ width: wp('20%'), alignItems: 'center', marginRight: wp('4%') }}>
                                                <View style={{ width: wp('15%'), height: wp('15%'), backgroundColor: blue, borderRadius: wp('2%'), alignItems: 'center', justifyContent: 'center' }}>
                                                    <Image source={require('../asset/order_icon.png')} style={{ width: xs(32), height: xs(30) }} />
                                                </View>
                                                <Text style={[ c.light, f._12, { textAlign: 'center' }]}>Redeem Order</Text>
                                            </TouchableOpacity>
                                        </View>
                                    </>
                                    :
                                    <>
                                        <View style={[ b.mt4, { width: wp('90%'), flexDirection: 'row', alignItems: 'center' }]}>
                                            <View style={{ width: wp('45%') }}>
                                                <Text style={[ c.light, { fontFamily: 'Roboto-Bold' }]}>Pesanan Saya</Text>
                                            </View>
                                            <View style={{ width: wp('45%'), alignItems: 'flex-end' }}>
                                                <TouchableOpacity onPress={() => this.setState({ hidden_myOrder: true, hidden_touch: false, touch: 'myOrder' })}>
                                                    <Text style={[ c.blue, { fontFamily: 'Roboto-Regular' }]}>Lihat Semua</Text>
                                                </TouchableOpacity>
                                            </View>
                                        </View>
                                        <View style={[ b.mt4, b.mb3, { width: wp('90%'), flexDirection: 'row' }]}>
                                            <TouchableOpacity onPress={() => this.setState({ hidden_myOrder: true, hidden_touch: false, touch: 'myOrder' })} style={{ width: wp('20%'), alignItems: 'center', marginRight: wp('3.5%') }}>
                                                <View style={{ width: wp('15%'), height: wp('15%'), backgroundColor: blue, borderRadius: wp('2%'), alignItems: 'center', justifyContent: 'center' }}>
                                                    <Image source={require('../asset/clock.png')} style={{ width: xs(30), height: xs(30), tintColor: light }} />
                                                </View>
                                                <Text style={[ c.light, f._12, { textAlign: 'center' }]}>Belum dibayar</Text>
                                            </TouchableOpacity>
                                            <TouchableOpacity onPress={() => this.setState({ hidden_myOrder: true, hidden_touch: false, touch: 'myOrder' })} style={{ width: wp('20%'), alignItems: 'center', marginRight: wp('3.5%') }}>
                                                <View style={{ width: wp('15%'), height: wp('15%'), backgroundColor: blue, borderRadius: wp('2%'), alignItems: 'center', justifyContent: 'center' }}>
                                                    <Image source={require('../asset/belumbayar_icon.png')} style={{ width: xs(30), height: xs(30) }} />
                                                </View>
                                                <Text style={[ c.light, f._12, { textAlign: 'center' }]}>Selesai</Text>
                                            </TouchableOpacity>
                                            <TouchableOpacity onPress={() => this.setState({ hidden_myOrder: true, hidden_touch: false, touch: 'myOrder' })} style={{ width: wp('20%'), alignItems: 'center', marginRight: wp('3.5%') }}>
                                                <View style={{ width: wp('15%'), height: wp('15%'), backgroundColor: blue, borderRadius: wp('2%'), alignItems: 'center', justifyContent: 'center' }}>
                                                    <Image source={require('../asset/clock.png')} style={{ width: xs(30), height: xs(30), tintColor: light }} />
                                                </View>
                                                <Text style={[ c.light, f._12, { textAlign: 'center' }]}>Kadaluarsa</Text>
                                            </TouchableOpacity>
                                        </View>

                                        <View style={{ width: '100%', height: xs(10), backgroundColor: '#121212' }} />

                                        <View style={[ b.mt2, { width: wp('90%'), flexDirection: 'row', alignItems: 'center' }]}>
                                            <View style={{ width: wp('45%'), flexDirection: 'row', alignItems: 'center' }}>
                                                <Image source={require('../asset/basket.png')} style={{ width: xs(25), height: xs(25), tintColor: blue }} />
                                                <Text style={[ c.blue, b.ml2, { fontFamily: 'Roboto-Regular' }]}>Beli lagi</Text>
                                            </View>
                                            <View style={{ width: wp('45%'), alignItems: 'flex-end' }}>
                                                <TouchableOpacity onPress={() => ToastAndroid.show('still not implemented', ToastAndroid.SHORT)}>
                                                    <Text style={[ c.blue, { fontFamily: 'Roboto-Regular' }]}>Lihat Semua</Text>
                                                </TouchableOpacity>
                                            </View>
                                        </View>
                                        <View style={[ b.mt2, { width: wp('90%') }]}>
                                            <FlatList
                                                style={[ b.mb4 ]}
                                                horizontal={true}
                                                data={this.state.beli_lagi}
                                                keyExtractor={item => item.id}
                                                renderItem={({ item }) => (
                                                    <TouchableOpacity style={{ width: wp('28%'), borderWidth: 1, borderRadius: xs(5), borderColor: blue, marginRight: wp('2%') }}>
                                                        <Image source={item.image} style={{ width: '100%', height: xs(90), borderTopLeftRadius: xs(5), borderTopRightRadius: xs(5) }} />
                                                        <Text style={[ c.light, f._12, { fontFamily: 'Roboto-Regular', marginLeft: xs(4), marginTop: xs(4) }]}>{item.nama}</Text>
                                                        <Text style={[ c.light, f._12, { fontFamily: 'Roboto-Regular', marginLeft: xs(4), marginBottom: xs(4) }]}>{this.format(item.harga)}</Text>
                                                    </TouchableOpacity>
                                                )}
                                            />
                                        </View>

                                        <View style={{ width: '100%', height: xs(10), backgroundColor: '#121212' }} />
                                        <TouchableOpacity onPress={() => this.props.navigation.push('JoinSeller')} style={[ b.mt2, { width: wp('90%'), flexDirection: 'row', alignItems: 'center' }]}>
                                            <View style={{ width: wp('45%'), flexDirection: 'row', alignItems: 'center' }}>
                                                <Image source={require('../asset/icongabungjadiseller.png')} style={{ width: xs(25), height: xs(25), tintColor: blue }} />
                                                <Text style={[ b.ml2, c.blue, { fontFamily: 'Roboto-Regular' }]}>Mulai Menjual</Text>
                                            </View>
                                            <View style={{ width: wp('45%'), alignItems: 'flex-end' }}>
                                                <Text style={[ c.light, { fontFamily: 'Roboto-Regular' }]}>Daftar Gratis</Text>
                                            </View>
                                        </TouchableOpacity>
                                        <View style={[ b.mt2, b.mb2, { width: '100%', borderBottomWidth: 1, borderBottomColor: light }]} />
                                        <TouchableOpacity onPress={() => this.setState({ hidden_myCoupon: true, hidden_touch: false, touch: 'Coupon' })} style={{ width: wp('90%'), flexDirection: 'row', alignItems: 'center' }}>
                                            <View style={{ width: wp('45%'), flexDirection: 'row', alignItems: 'center' }}>
                                                <Image source={require('../asset/promocodekecil.png')} style={{ width: xs(25), height: xs(15), tintColor: blue }} />
                                                <Text style={[ c.blue, b.ml2, { fontFamily: 'Roboto-Regular' }]}>Kupon saya</Text>
                                            </View>
                                            <View style={{ width: wp('45%'), alignItems: 'flex-end' }}>
                                                <Text style={[ c.light, { fontFamily: 'Roboto-Regular' }]}>3 Kupon</Text>
                                            </View>
                                        </TouchableOpacity>
                                        <View style={[ b.mt2, b.mb2, { width: '100%', borderBottomWidth: 1, borderBottomColor: light }]} />
                                        <TouchableOpacity onPress={() => ToastAndroid.show('there is still problems during implementation', ToastAndroid.SHORT)} style={{ width: wp('90%'), flexDirection: 'row', alignItems: 'center' }}>
                                            <View style={{ width: wp('45%'), flexDirection: 'row', alignItems: 'center' }}>
                                                <Image source={require('../asset/love.png')} style={{ width: xs(25), height: xs(25), tintColor: blue }} />
                                                <Text style={[ c.blue, b.ml2, { fontFamily: 'Roboto-Regular' }]}>Favorit saya</Text>
                                            </View>
                                            <View style={{ width: wp('45%'), alignItems: 'flex-end' }}>
                                                <Text style={[ c.light, { fontFamily: 'Roboto-Regular' }]}>1 Suka</Text>
                                            </View>
                                        </TouchableOpacity>
                                        <View style={[ b.mt2, b.mb2, { width: '100%', borderBottomWidth: 1, borderBottomColor: light }]} />
                                        <TouchableOpacity onPress={() => ToastAndroid.show('there is still problems during implementation', ToastAndroid.SHORT)} style={{ width: wp('90%'), flexDirection: 'row', alignItems: 'center' }}>
                                            <View style={{ width: wp('45%'), flexDirection: 'row', alignItems: 'center' }}>
                                                <View style={{ width: xs(20), height: xs(20), backgroundColor: light, borderRadius: xs(25) }} />
                                                <Image source={require('../asset/clocked.png')} style={{ width: xs(25), height: xs(25), tintColor: blue, position: 'absolute', marginLeft: xs(-2.5) }} />
                                                <Text style={[ c.blue, b.ml2, { fontFamily: 'Roboto-Regular' }]}>Baru dilihat</Text>
                                            </View>
                                            <View style={{ width: wp('45%'), alignItems: 'flex-end' }}>
                                            </View>
                                        </TouchableOpacity>
                                        <View style={[ b.mt2, b.mb2, { width: '100%', borderBottomWidth: 1, borderBottomColor: light }]} />
                                        <TouchableOpacity onPress={() => this.setState({ hidden_referal: true, hidden_touch: false, touch: 'Referal' })} style={[ b.mb2, { width: wp('90%'), flexDirection: 'row', alignItems: 'center' }]}>
                                            <View style={{ width: wp('45%'), flexDirection: 'row', alignItems: 'center' }}>
                                                <Image source={require('../asset/love.png')} style={{ width: xs(25), height: xs(25), tintColor: blue }} />
                                                <Text style={[ c.blue, b.ml2, { fontFamily: 'Roboto-Regular' }]}>Undang teman</Text>
                                            </View>
                                            <View style={{ width: wp('45%'), alignItems: 'flex-end' }}>
                                            </View>
                                        </TouchableOpacity>
                                    </>
                                }
                            </>
                        )}

                        {this.state.hidden_myOrder === true && this.state.hidden_touch === false && this.state.touch === 'myOrder' && (
                            <View style={{ width: '100%', backgroundColor: '#181B22' }}>
                                <View style={{ width: '100%', alignItems: 'center' }}>
                                    <View style={{ width: wp('90%'), flexDirection: 'row', alignItems: 'center', marginTop: wp('4%'), marginBottom: wp('2%') }}>
                                        <TouchableOpacity onPress={() => this.setState({ hidden_myOrder: false, hidden_touch: true })}>
                                            <Image source={require('../asset/left.png')} style={{ width: wp('8%'), height: wp('8%') }} />
                                        </TouchableOpacity>
                                        <Text style={[ c.light, f._16, { marginLeft: wp('2%') }]}>Pesanan Saya</Text>
                                    </View>
                                </View>
                                <View style={{ width: '100%', height: wp('22%'), marginTop: wp('2%'), borderTopWidth: 2, borderTopColor: light, alignItems: 'center', backgroundColor: '#181B22' }}>
                                    <FlatList
                                        data={this.state.status}
                                        horizontal={true}
                                        keyExtractor={item => item.jenis_status}
                                        renderItem={({item, index}) => (
                                            (this.state.clicked === index ?
                                                <View style={{ width: wp('20%'), alignItems: 'center', backgroundColor: blue }}>
                                                    <TouchableOpacity onPress={() => this.swicth(item.jenis_status, index)} style={{ alignItems: 'center' }}>
                                                        <Image source={item.image} style={[ b.mt2, { width: wp('7%'), height: wp('7%'), tintColor: light }]} />
                                                        <Text style={[ b.ml2, b.mr2, b.mt1, b.mb1, c.light, f._12, p.textCenter ]}>{item.jenis_status}</Text>
                                                    </TouchableOpacity>
                                                </View>
                                                :
                                                <View style={{ width: wp('20%'), alignItems: 'center' }}>
                                                    <TouchableOpacity onPress={() => this.swicth(item.jenis_status, index)} style={{ alignItems: 'center' }}>
                                                        <Image source={item.image} style={[ b.mt2, { width: wp('7%'), height: wp('7%'), tintColor: light }]} />
                                                        <Text style={[ b.ml2, b.mr2, b.mt1, b.mb1, c.light, f._12, p.textCenter ]}>{item.jenis_status}</Text>
                                                    </TouchableOpacity>
                                                </View>
                                            )
                                        )}
                                    />
                                </View>
                                <ScrollView style={{ backgroundColor: '#121212' }} refreshControl={<RefreshControl onRefresh={() => this.componentDidMount()} refreshing={this.state.loading} />}>
                                    {this.state.clicked === 0 && (
                                        (this.state.belum_dibayar !== undefined ?
                                            <FlatList
                                                data={this.state.belum_dibayar}
                                                renderItem={({ item: order }) => (
                                                    <TouchableOpacity onPress={() => this.props.navigation.push('OrderDetail'/*, {gift: item.giftcard}*/)} style={{ width: '100%', backgroundColor: '#121212' }}>
                                                        <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }} colors={[ '#181C21', '#252F38', '#252F38' ]} style={[ b.mb2, { width: '100%', alignItems: 'center' }]}>
                                                            <View style={[ b.mt3, { width: wp('90%'), flexDirection: 'row', alignItems: 'center' }]}>
                                                                <View style={{ width: wp('45%') }}>
                                                                    <Text style={[ c.light, f._12, { fontFamily: 'Roboto-Regular' }]}>No Pesanan : {order['no_pesanan']}</Text>
                                                                </View>
                                                            </View>

                                                            <View style={[ b.mt2, b.mb2, { width: '100%', borderBottomWidth: 1, borderBottomColor: light }]} />
                                                            
                                                            <View style={{ width: '100%' }}>
                                                                <FlatList
                                                                    data={order['store']}
                                                                    renderItem={ ({ item: store }) => (
                                                                        <>
                                                                            <View style={{ width: '100%', alignItems: 'center' }}>
                                                                                <View style={{ width: wp('90%'), flexDirection: 'row', alignItems: 'center' }}>
                                                                                    <View style={{ width: wp('60%'), flexDirection: 'row', alignItems: 'center' }}>
                                                                                        <View style={[ p.center, { width: wp('8%'), height: wp('8%'), borderRadius: wp('5%'), backgroundColor: light }]}>
                                                                                            <Image source={{ uri: API_URL+'/public/user/'+store['logo_toko'] }} style={{ width: wp('5%'), height: wp('5%') }} />
                                                                                        </View>
                                                                                        <View style={{ width: wp('37%') }}>
                                                                                            <Text style={[ c.light, { marginLeft: wp('2%'), fontFamily: 'Roboto-Bold' }]}>{store['nama_toko']}</Text>
                                                                                        </View>
                                                                                    </View>
                                                                                    <View style={{ width: wp('30%') }}>
                                                                                    </View>
                                                                                </View>
                                                                            </View>
                                                                            <FlatList
                                                                                data={store['item']}
                                                                                renderItem={ ({ item: itemValue }) => {
                                                                                    console.log(itemValue)
                                                                                    return <View style={{ width: '100%' }}>
                                                                                            <View style={{ width: '100%', alignItems: 'center' }}>
                                                                                                <View style={{ width: wp('90%'), flexDirection: 'row', marginTop: wp('4%') }}>
                                                                                                    <View style={{ width: wp('20%') }}>
                                                                                                        <View style={{ width: '100%', height: wp('20%'), backgroundColor: light, borderRadius: wp('2%'), alignItems: 'center', justifyContent: 'center' }}>
                                                                                                            <Image source={{ uri: API_URL+'/public/product/'+itemValue['image'] }} style={{ width: xs(65), height: xs(65) }} />
                                                                                                        </View>
                                                                                                    </View>
                                                                                                    <View style={{ width: wp('45%') }}>
                                                                                                        <Text style={[ c.light, b.ml1, { fontFamily: 'Roboto-Bold' }]} numberOfLines={2}>{itemValue['nama']}</Text>
                                                                                                    </View>
                                                                                                    <View style={{ width: wp('25%'), alignItems: 'flex-end' }}>
                                                                                                        <Text style={[ c.light, { fontFamily: 'Roboto-Regular', position: 'absolute', bottom: vs(20) }]}>x{itemValue['quantity']}</Text>
                                                                                                        <Text style={[ c.light, { fontFamily: 'Roboto-Regular', position: 'absolute', bottom: 0 }]}>{this.format(itemValue['harga'])}</Text>
                                                                                                    </View>
                                                                                                </View>
                                                                                            </View>

                                                                                            <View style={{ width: '100%', borderBottomWidth: 1, borderBottomColor: light, marginTop: wp('3%'), marginBottom: wp('3%') }} />

                                                                                            <View style={{ width: '100%', alignItems: 'center' }}>
                                                                                                <View style={{ width: wp('90%'), flexDirection: 'row', alignItems: 'center' }}>
                                                                                                    <View style={{ width: wp('30%') }}>
                                                                                                        <Text style={[ c.light, { fontFamily: 'Roboto-Regular' }]}>{itemValue['quantity']} produk</Text>
                                                                                                    </View>
                                                                                                    <View style={{ width: wp('60%'), alignItems: 'flex-end' }}>
                                                                                                        <Text style={[ c.light, { fontFamily: 'Roboto-Regular' }]}>Total Pesanan: {this.format(itemValue['harga'])}</Text>
                                                                                                    </View>
                                                                                                </View>
                                                                                                <View style={{ width: wp('90%'), borderBottomWidth: 1, borderBottomColor: light, marginTop: wp('3%'), marginBottom: wp('4%') }} />
                                                                                            </View>
                                                                                        </View>
                                                                                }}
                                                                            />
                                                                        </>
                                                                    )}
                                                                />
                                                            </View>

                                                        </LinearGradient>
                                                    </TouchableOpacity>
                                                )}
                                            />
                                            :
                                            <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }} colors={[ '#181C21', '#252F38', '#252F38' ]} style={{ width: '100%', height: hp('66.8%'), alignItems: 'center' }}>
                                                <FastImage source={require('../asset/cart.png')} style={{ width: xs(40), height: xs(40), marginTop: xs(150) }} />
                                                <Text style={[ c.light, b.mt2, { textAlign: 'center' }]}>Belum ada pesanan</Text>
                                            </LinearGradient>
                                        )
                                    )}

                                    {this.state.all_challenges === 'Selesai' && (
                                        (this.state.selesai !== undefined ?
                                            <FlatList
                                                data={this.state.selesai}
                                                keyExtractor={item => item.no_pesanan}
                                                renderItem={({ item }) => (
                                                    <View style={{ width: '100%', backgroundColor: '#121212' }}>
                                                        <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }} colors={[ '#181C21', '#252F38', '#252F38' ]} style={[ b.mb2, { width: '100%', alignItems: 'center' }]}>
                                                            <View style={[ b.mt3, { width: wp('90%'), flexDirection: 'row', alignItems: 'center' }]}>
                                                                <View style={{ width: wp('45%') }}>
                                                                    <Text style={[ c.light, f._12, { fontFamily: 'Roboto-Regular' }]}>No Pesanan : {item.no_pesanan}</Text>
                                                                </View>
                                                                <View style={{ width: wp('45%'), alignItems: 'flex-end' }}>
                                                                    <Text style={[ c.light, f._12, { fontFamily: 'Roboto-Regular' }]}>Tgl Pesan {item.tgl_pesan}</Text>
                                                                </View>
                                                            </View>

                                                            <View style={[ b.mt2, b.mb2, { width: '100%', borderBottomWidth: 1, borderBottomColor: light }]} />

                                                            <View style={{ width: wp('90%'), flexDirection: 'row', alignItems: 'center' }}>
                                                                <View style={{ width: wp('45%'), flexDirection: 'row', alignItems: 'center' }}>
                                                                    <View style={[ p.center, { width: wp('10%'), height: wp('10%'), borderRadius: wp('5%'), backgroundColor: light }]}>
                                                                        <Image source={item.logo_toko} style={{ width: wp('6%'), height: wp('6%') }} />
                                                                    </View>
                                                                    <Text style={[ c.light, f._16, { marginLeft: wp('2%'), fontFamily: 'Roboto-Bold' }]}>{item.nama_toko}</Text>
                                                                </View>
                                                                <View style={{ width: wp('45%'), alignItems: 'flex-end' }}>
                                                                    <Text style={{ fontFamily: 'Roboto-Regular', color: 'darkorange' }}>{item.status}</Text>
                                                                </View>
                                                            </View>
                                                            <FlatList
                                                                data={item.items}
                                                                renderItem={({ item }) => (
                                                                    <>
                                                                        <View style={{ width: wp('90%'), flexDirection: 'row', marginTop: wp('4%') }}>
                                                                            <View style={{ width: wp('25%') }}>
                                                                                <View style={{ width: '100%', height: wp('25%'), backgroundColor: light, borderRadius: wp('2%'), alignItems: 'center', justifyContent: 'center' }}>
                                                                                    <Image source={item.image} style={{ width: xs(80), height: xs(80) }} />
                                                                                </View>
                                                                            </View>
                                                                            <View style={{ width: wp('45%') }}>
                                                                                <Text style={[ c.light, { fontFamily: 'lineto-circular-pro-bold', marginLeft: wp('2%') }]}>{item.nama}</Text>
                                                                            </View>
                                                                            <View style={{ width: wp('20%'), alignItems: 'flex-end' }}>
                                                                                <Text style={[ c.light, { marginTop: wp('15%') }]}>x{item.quantity}</Text>
                                                                                <Text style={[ c.light ]}>{this.format(item.harga)}</Text>
                                                                            </View>
                                                                        </View>
                                                                        <View style={{ width: '100%', borderBottomWidth: 1, borderBottomColor: light, marginTop: wp('3%') }} />
                                                                    </>
                                                                )}
                                                            />

                                                            <View style={{ width: wp('90%'), marginTop: wp('2%'), marginBottom: wp('2%'), flexDirection: 'row', alignItems: 'center' }}>
                                                                <View style={{ width: wp('40%') }}>
                                                                    <Text style={[ c.light ]}>{item.items.length} produk</Text>
                                                                </View>
                                                                <View style={{ width: wp('50%'), alignItems: 'flex-end' }}>
                                                                    <Text style={[ c.light ]}>Total Pesanan: {this.format(item.total_pesanan)}</Text>
                                                                </View>
                                                            </View>
                                                            <View style={[ b.mb2, { width: wp('90%'), borderBottomWidth: 1, borderBottomColor: light }]} />
                                                        </LinearGradient>
                                                    </View>
                                                )}
                                            />
                                            :
                                            <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }} colors={[ '#181C21', '#252F38', '#252F38' ]} style={{ width: '100%', height: hp('66.8%'), alignItems: 'center' }}>
                                                <FastImage source={require('../asset/cart.png')} style={{ width: xs(40), height: xs(40), marginTop: xs(150) }} />
                                                <Text style={[ c.light, b.mt2, { textAlign: 'center' }]}>Belum ada pesanan</Text>
                                            </LinearGradient>
                                        )
                                    )}

                                    {this.state.all_challenges === 'Kadaluarsa' && (
                                        (this.state.kadaluarsa !== undefined ?
                                            <FlatList
                                                data={this.state.kadaluarsa}
                                                renderItem={({ key, item }) => (
                                                    <Text style={[ c.light, b.mt2, { textAlign: 'center' }]}>Belum ada pesanan</Text>
                                                )}
                                            />
                                            :
                                            <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }} colors={[ '#181C21', '#252F38', '#252F38' ]} style={{ width: '100%', height: hp('66.8%'), alignItems: 'center' }}>
                                                <FastImage source={require('../asset/cart.png')} style={{ width: xs(40), height: xs(40), marginTop: xs(150) }} />
                                                <Text style={[ c.light, b.mt2, { textAlign: 'center' }]}>Belum ada pesanan</Text>
                                            </LinearGradient>
                                        )
                                    )}

                                    {this.state.all_challenges === 'Tertunda' && (
                                        (this.state.tertunda !== undefined ?
                                            <FlatList
                                                data={this.state.tertunda}
                                                keyExtractor={item => item.id}
                                                renderItem={({ item }) => (
                                                    <View style={{ width: '100%', backgroundColor: '#121212' }}>
                                                        <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }} colors={[ '#181C21', '#252F38', '#252F38' ]} style={[ b.mb2, { width: '100%', alignItems: 'center' }]}>
                                                            <View style={[ b.mt3, { width: wp('90%'), flexDirection: 'row', alignItems: 'center' }]}>
                                                                <View style={{ width: wp('45%') }}>
                                                                    <Text style={[ c.light, f._12, { fontFamily: 'Roboto-Regular' }]}>No Pesanan : {item.no_pesanan}</Text>
                                                                </View>
                                                                <View style={{ width: wp('45%'), alignItems: 'flex-end' }}>
                                                                    <Text style={[ c.light, f._12, { fontFamily: 'Roboto-Regular' }]}>Tgl Pesan {item.tgl_pesan}</Text>
                                                                </View>
                                                            </View>

                                                            <View style={[ b.mt2, b.mb2, { width: '100%', borderBottomWidth: 1, borderBottomColor: light }]} />

                                                            <View style={{ width: wp('90%'), flexDirection: 'row', alignItems: 'center' }}>
                                                                <View style={{ width: wp('45%'), flexDirection: 'row', alignItems: 'center' }}>
                                                                    <View style={[ p.center, { width: wp('10%'), height: wp('10%'), borderRadius: wp('5%'), backgroundColor: light }]}>
                                                                        <Image source={item.logo_toko} style={{ width: wp('6%'), height: wp('6%') }} />
                                                                    </View>
                                                                    <Text style={[ c.light, f._16, { marginLeft: wp('2%'), fontFamily: 'Roboto-Bold' }]}>{item.nama_toko}</Text>
                                                                </View>
                                                                <View style={{ width: wp('45%'), alignItems: 'flex-end' }}>
                                                                    <Text style={{ fontFamily: 'Roboto-Regular', color: 'darkorange' }}>{item.status}</Text>
                                                                </View>
                                                            </View>
                                                            <FlatList
                                                                data={item.items}
                                                                renderItem={({ item }) => (
                                                                    <>
                                                                        <View style={{ width: wp('90%'), flexDirection: 'row', marginTop: wp('4%') }}>
                                                                            <View style={{ width: wp('25%') }}>
                                                                                <View style={{ width: '100%', height: wp('25%'), backgroundColor: light, borderRadius: wp('2%'), alignItems: 'center', justifyContent: 'center' }}>
                                                                                    <Image source={item.image} style={{ width: xs(80), height: xs(80) }} />
                                                                                </View>
                                                                            </View>
                                                                            <View style={{ width: wp('45%') }}>
                                                                                <Text style={[ c.light, { fontFamily: 'lineto-circular-pro-bold', marginLeft: wp('2%') }]}>{item.nama}</Text>
                                                                            </View>
                                                                            <View style={{ width: wp('20%'), alignItems: 'flex-end' }}>
                                                                                <Text style={[ c.light, { marginTop: wp('15%') }]}>x{item.quantity}</Text>
                                                                                <Text style={[ c.light ]}>{this.format(item.harga)}</Text>
                                                                            </View>
                                                                        </View>
                                                                        <View style={{ width: '100%', borderBottomWidth: 1, borderBottomColor: light, marginTop: wp('3%') }} />
                                                                    </>
                                                                )}
                                                            />

                                                            <View style={{ width: wp('90%'), marginTop: wp('2%'), marginBottom: wp('2%'), flexDirection: 'row', alignItems: 'center' }}>
                                                                <View style={{ width: wp('40%') }}>
                                                                    <Text style={[ c.light ]}>{item.items.length} produk</Text>
                                                                </View>
                                                                <View style={{ width: wp('50%'), alignItems: 'flex-end' }}>
                                                                    <Text style={[ c.light ]}>Total Pesanan: {this.format(item.total_pesanan)}</Text>
                                                                </View>
                                                            </View>
                                                            <View style={[ b.mb2, { width: wp('90%'), borderBottomWidth: 1, borderBottomColor: light }]} />
                                                        </LinearGradient>
                                                    </View>
                                                )}
                                            />
                                            :
                                            <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }} colors={[ '#181C21', '#252F38', '#252F38' ]} style={{ width: '100%', height: hp('66.8%'), alignItems: 'center' }}>
                                                <FastImage source={require('../asset/cart.png')} style={{ width: xs(40), height: xs(40), marginTop: xs(150) }} />
                                                <Text style={[ c.light, b.mt2, { textAlign: 'center' }]}>Belum ada pesanan</Text>
                                            </LinearGradient>
                                        )
                                    )}

                                    {this.state.all_challenges === 'Redeem Order' && (
                                        (this.state.redeem_order !== undefined ?
                                            <FlatList
                                                data={this.state.redeem_order}
                                                keyExtractor={item => item.id}
                                                renderItem={({ item }) => (
                                                    <TouchableOpacity onPress={() => this.props.navigation.push('OrderDetail', {gift: item.giftcard})} style={{ width: '100%', backgroundColor: '#121212' }}>
                                                        <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }} colors={[ '#181C21', '#252F38', '#252F38' ]} style={[ b.mb2, { width: '100%', alignItems: 'center' }]}>
                                                            <View style={[ b.mt3, { width: wp('90%'), flexDirection: 'row', alignItems: 'center' }]}>
                                                                <View style={{ width: wp('45%') }}>
                                                                    <Text style={[ c.light, f._12, { fontFamily: 'Roboto-Regular' }]}>No Pesanan : {item.no_pesanan}</Text>
                                                                </View>
                                                                <View style={{ width: wp('45%'), alignItems: 'flex-end' }}>
                                                                    <Text style={[ c.light, f._12, { fontFamily: 'Roboto-Regular' }]}>Tgl Pesan {item.tgl_pesan}</Text>
                                                                </View>
                                                            </View>

                                                            <View style={[ b.mt2, b.mb2, { width: '100%', borderBottomWidth: 1, borderBottomColor: light }]} />

                                                            <View style={{ width: wp('90%'), flexDirection: 'row', alignItems: 'center' }}>
                                                                <View style={{ width: wp('45%'), flexDirection: 'row', alignItems: 'center' }}>
                                                                    <View style={[ p.center, { width: wp('10%'), height: wp('10%'), borderRadius: wp('5%'), backgroundColor: light }]}>
                                                                        <Image source={item.logo_toko} style={{ width: wp('6%'), height: wp('6%') }} />
                                                                    </View>
                                                                    <Text style={[ c.light, f._16, { marginLeft: wp('2%'), fontFamily: 'Roboto-Bold' }]}>{item.nama_toko}</Text>
                                                                </View>
                                                                <View style={{ width: wp('45%'), alignItems: 'flex-end' }}>
                                                                    <Text style={{ fontFamily: 'Roboto-Regular', color: 'darkorange' }}>{item.status}</Text>
                                                                </View>
                                                            </View>
                                                            <FlatList
                                                                data={item.items}
                                                                renderItem={({ item }) => (
                                                                    <>
                                                                        <View style={{ width: wp('90%'), flexDirection: 'row', marginTop: wp('4%') }}>
                                                                            <View style={{ width: wp('25%') }}>
                                                                                <View style={{ width: '100%', height: wp('25%'), backgroundColor: light, borderRadius: wp('2%'), alignItems: 'center', justifyContent: 'center' }}>
                                                                                    <Image source={item.image} style={{ width: xs(80), height: xs(80) }} />
                                                                                </View>
                                                                            </View>
                                                                            <View style={{ width: wp('45%') }}>
                                                                                <Text style={[ c.light, { fontFamily: 'lineto-circular-pro-bold', marginLeft: wp('2%') }]}>{item.nama}</Text>

                                                                                {item.rating === null && (
                                                                                    <TouchableOpacity onPress={() => this.props.navigation.push('AddRating')} style={{ position: 'absolute', left: wp('2%'), bottom: xs(5) }}>
                                                                                        <Text style={[ c.blue, { fontFamily: 'Roboto-Regular' }]}>Berikan ulasan</Text>
                                                                                    </TouchableOpacity>
                                                                                )}
                                                                                {item.rating === '5' && (
                                                                                    <View style={{ flexDirection: 'row', alignItems: 'center', position: 'absolute', left: wp('2%'), bottom: xs(5) }}>
                                                                                        <Image source={require('../asset/star.png')} style={{ width: wp('5%'), height: wp('5%'), tintColor: '#FE8829' }} />
                                                                                        <Image source={require('../asset/star.png')} style={{ width: wp('5%'), height: wp('5%'), tintColor: '#FE8829' }} />
                                                                                        <Image source={require('../asset/star.png')} style={{ width: wp('5%'), height: wp('5%'), tintColor: '#FE8829' }} />
                                                                                        <Image source={require('../asset/star.png')} style={{ width: wp('5%'), height: wp('5%'), tintColor: '#FE8829' }} />
                                                                                        <Image source={require('../asset/star.png')} style={{ width: wp('5%'), height: wp('5%'), tintColor: '#FE8829' }} />
                                                                                    </View>
                                                                                )}
                                                                                {item.rating === '4' && (
                                                                                    <View style={{ flexDirection: 'row', alignItems: 'center', position: 'absolute', left: wp('2%'), bottom: xs(5) }}>
                                                                                        <Image source={require('../asset/star.png')} style={{ width: wp('5%'), height: wp('5%'), tintColor: '#FE8829' }} />
                                                                                        <Image source={require('../asset/star.png')} style={{ width: wp('5%'), height: wp('5%'), tintColor: '#FE8829' }} />
                                                                                        <Image source={require('../asset/star.png')} style={{ width: wp('5%'), height: wp('5%'), tintColor: '#FE8829' }} />
                                                                                        <Image source={require('../asset/star.png')} style={{ width: wp('5%'), height: wp('5%'), tintColor: '#FE8829' }} />
                                                                                        <Image source={require('../asset/star.png')} style={{ width: wp('5%'), height: wp('5%'), tintColor: light }} />
                                                                                    </View>
                                                                                )}
                                                                                {item.rating === '3' && (
                                                                                    <View style={{ flexDirection: 'row', alignItems: 'center', position: 'absolute', left: wp('2%'), bottom: xs(5) }}>
                                                                                        <Image source={require('../asset/star.png')} style={{ width: wp('5%'), height: wp('5%'), tintColor: '#FE8829' }} />
                                                                                        <Image source={require('../asset/star.png')} style={{ width: wp('5%'), height: wp('5%'), tintColor: '#FE8829' }} />
                                                                                        <Image source={require('../asset/star.png')} style={{ width: wp('5%'), height: wp('5%'), tintColor: '#FE8829' }} />
                                                                                        <Image source={require('../asset/star.png')} style={{ width: wp('5%'), height: wp('5%'), tintColor: light }} />
                                                                                        <Image source={require('../asset/star.png')} style={{ width: wp('5%'), height: wp('5%'), tintColor: light }} />
                                                                                    </View>
                                                                                )}
                                                                                {item.rating === '2' && (
                                                                                    <View style={{ flexDirection: 'row', alignItems: 'center', position: 'absolute', left: wp('2%'), bottom: xs(5) }}>
                                                                                        <Image source={require('../asset/star.png')} style={{ width: wp('5%'), height: wp('5%'), tintColor: '#FE8829' }} />
                                                                                        <Image source={require('../asset/star.png')} style={{ width: wp('5%'), height: wp('5%'), tintColor: '#FE8829' }} />
                                                                                        <Image source={require('../asset/star.png')} style={{ width: wp('5%'), height: wp('5%'), tintColor: light }} />
                                                                                        <Image source={require('../asset/star.png')} style={{ width: wp('5%'), height: wp('5%'), tintColor: light }} />
                                                                                        <Image source={require('../asset/star.png')} style={{ width: wp('5%'), height: wp('5%'), tintColor: light }} />
                                                                                    </View>
                                                                                )}
                                                                                {item.rating === '1' && (
                                                                                    <View style={{ flexDirection: 'row', alignItems: 'center', position: 'absolute', left: wp('2%'), bottom: xs(5) }}>
                                                                                        <Image source={require('../asset/star.png')} style={{ width: wp('5%'), height: wp('5%'), tintColor: '#FE8829' }} />
                                                                                        <Image source={require('../asset/star.png')} style={{ width: wp('5%'), height: wp('5%'), tintColor: light }} />
                                                                                        <Image source={require('../asset/star.png')} style={{ width: wp('5%'), height: wp('5%'), tintColor: light }} />
                                                                                        <Image source={require('../asset/star.png')} style={{ width: wp('5%'), height: wp('5%'), tintColor: light }} />
                                                                                        <Image source={require('../asset/star.png')} style={{ width: wp('5%'), height: wp('5%'), tintColor: light }} />
                                                                                    </View>
                                                                                )}
                                                                            </View>
                                                                            <View style={{ width: wp('20%'), alignItems: 'flex-end' }}>
                                                                                <Text style={[ c.light, { marginTop: wp('15%') }]}>x{item.quantity}</Text>
                                                                                <Text style={[ c.light ]}>{this.format(item.harga)}</Text>
                                                                            </View>
                                                                        </View>
                                                                        <View style={{ width: '100%', borderBottomWidth: 1, borderBottomColor: light, marginTop: wp('3%') }} />
                                                                    </>
                                                                )}
                                                            />

                                                            <View style={{ width: wp('90%'), marginTop: wp('2%'), marginBottom: wp('2%'), flexDirection: 'row', alignItems: 'center' }}>
                                                                <View style={{ width: wp('40%') }}>
                                                                    <Text style={[ c.light ]}>{item.items.length} produk</Text>
                                                                </View>
                                                                <View style={{ width: wp('50%'), alignItems: 'flex-end' }}>
                                                                    <Text style={[ c.light ]}>Total Pesanan: {this.format(item.total_pesanan)}</Text>
                                                                </View>
                                                            </View>
                                                            <View style={[ b.mb2, { width: wp('90%'), borderBottomWidth: 1, borderBottomColor: light }]} />
                                                        </LinearGradient>
                                                    </TouchableOpacity>
                                                )}
                                            />
                                            :
                                            <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }} colors={[ '#181C21', '#252F38', '#252F38' ]} style={{ width: '100%', height: hp('66.8%'), alignItems: 'center' }}>
                                                <FastImage source={require('../asset/cart.png')} style={{ width: xs(40), height: xs(40), marginTop: xs(150) }} />
                                                <Text style={[ c.light, b.mt2, { textAlign: 'center' }]}>Belum ada pesanan</Text>
                                            </LinearGradient>
                                        )
                                    )}
                                </ScrollView>
                            </View>
                        )}

                        {this.state.hidden === true && this.state.hidden_touch === false && this.state.touch === 'akun' && (
                            <View style={{ width: '100%', alignItems: 'center', backgroundColor: '#181B22' }}>
                                {/* {this.state.timeActive === true && (
                                    <DateTimePicker
                                        mode='date'
                                        value={new Date()}
                                        maximumDate={new Date().setDate(new Date().getDate())}
                                        onChange={data => {
                                            // console.log(this.dateFix(new Date(data.nativeEvent.timestamp).getFullYear()+'-'+this.state.month[new Date(data.nativeEvent.timestamp).getMonth()].bulan+'-'+new Date(data.nativeEvent.timestamp).getDate()))
                                            if (data.type === 'dismissed') {
                                                this.setState({
                                                    timeActive: false
                                                })
                                            } else {
                                                this.setState({
                                                    date: this.dateFix(new Date(data.nativeEvent.timestamp).getFullYear()+'-'+this.state.month[new Date(data.nativeEvent.timestamp).getMonth()].bulan+'-'+new Date(data.nativeEvent.timestamp).getDate()),
                                                    timeActive: false
                                                })
                                                this.componentDidMount()
                                            }
                                        }}
                                    />
                                )} */}
                                <View style={{ width: wp('90%'), flexDirection: 'row', alignItems: 'center', marginTop: wp('2%'), marginBottom: wp('2%') }}>
                                    <TouchableOpacity onPress={() => this.setState({ hidden: false, hidden_touch: true })}>
                                        <Image source={require('../asset/left.png')} style={{ width: wp('8%'), height: wp('8%') }} />
                                    </TouchableOpacity>
                                    <Text style={[ c.light, f._16, { marginLeft: wp('2%') }]}>Pengaturan Akun</Text>
                                </View>
                                <View style={{ width: '100%', height: hp('15%'), flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                                    <TouchableOpacity onPress={() => this.setState({ timeActive: true })} style={{ alignItems: 'center' }}>
                                        <FastImage source={require('../asset/calendar_icon.png')} style={{ width: wp('7%'), height: wp('7%') }} />
                                        <Text style={[ c.light, { marginTop: wp('1%') }]}>{this.state.age_now} Tahun</Text>
                                    </TouchableOpacity>
                                    <View style={{ width: wp('10%'), borderBottomWidth: 1, borderBottomColor: light, transform: [{ rotate: '90deg' }], marginLeft: wp('12%'), marginRight: wp('12%') }} />
                                    <TouchableOpacity onPress={() => this.setState({ visible: true })} style={{ alignItems: 'center' }}>
                                        <FastImage source={require('../asset/gender_icon.png')} style={{ width: wp('7%'), height: wp('7%') }} />
                                        <Text style={[ c.light, { marginTop: wp('1%') }]}>{this.state.jenis_kelamin}</Text>
                                    </TouchableOpacity>
                                </View>

                                <View style={{ width: '100%', alignItems: 'center', backgroundColor: '#121212' }}>
                                    <View style={{ width: wp('90%'), marginTop: wp('4%'), marginBottom: wp('4%') }}>
                                        <Text style={[ c.blue ]}>Informasi Kontak</Text>
                                    </View>
                                </View>

                                <View style={{ width: '100%', alignItems: 'center' }}>
                                    <View style={{ width: wp('90%'), marginTop: wp('4%'), marginBottom: wp('6%') }}>
                                        <Text style={[ c.light ]}>Nomor Telepon</Text>
                                        <View style={{ width: wp('90%'), borderBottomWidth: 1, borderBottomColor: light }}>
                                            {this.state.edit === true ?
                                                <TextInput
                                                    placeholder='Masukkan nomor telp Anda'
                                                    placeholderTextColor={light}
                                                    style={{ width: '100%', borderBottomWidth: 1, borderBottomColor: light, color: light }}
                                                    value={this.state.nmr_telp}
                                                    onChangeText={(text) => this.setState({ nmr_telp: text })}
                                                />
                                                :
                                                <Text style={[ c.light ]}>+6287882973962</Text>
                                            }
                                        </View>

                                        <Text style={[ c.light, { marginTop: wp('4%') }]}>Email</Text>
                                        <View style={{ width: wp('90%'), borderBottomWidth: 1, borderBottomColor: light }}>
                                            {this.state.edit === true ?
                                                <TextInput
                                                    placeholder='Masukkan email Anda'
                                                    placeholderTextColor={light}
                                                    style={{ width: '100%', borderBottomWidth: 1, borderBottomColor: light, color: light }}
                                                    value={this.state.email}
                                                    onChangeText={(text) => this.setState({ email: text })}
                                                />
                                                :
                                                <Text style={[ c.light ]}>rizki@healthplus.co</Text>
                                            }
                                        </View>
                                    </View>
                                </View>
                            </View>
                        )}

                        {this.state.hidden_myFavorite === true && this.state.hidden_touch === false && this.state.touch === 'Favorite' && (
                            <>
                                <View style={{ width: '100%', alignItems: 'center', backgroundColor: '#121212' }}>
                                    <View style={[ b.mt4, b.mb4, { width: wp('90%'), flexDirection: 'row', alignItems: 'center' }]}>
                                        <View style={{ width: wp('15%') }}>
                                            <TouchableOpacity onPress={() => this.setState({ hidden_myFavorite: false, hidden_touch: true })}>
                                                <FastImage source={require('../asset/left.png')} style={{ width: xs(25), height: xs(25) }} />
                                            </TouchableOpacity>
                                        </View>
                                        <View style={{ width: wp('60%'), alignItems: 'center' }}>
                                            <Text style={[ c.light, f._16, { fontFamily: 'Roboto-Bold' }]}>Favorit Saya</Text>
                                        </View>
                                        <View style={{ width: wp('15%'), alignItems: 'flex-end' }}>
                                        </View>
                                    </View>
                                </View>

                                <ScrollView showsVerticalScrollIndicator={false} style={{ backgroundColor: '#181B22' }}>
                                    <View style={{ width: '100%', alignItems: 'center' }}>
                                        <View style={[ b.mt4, { width: wp('90%') }]}>
                                            <FlatList
                                                numColumns={2}
                                                data={this.state.my_favorite}
                                                keyExtractor={item => item.id}
                                                renderItem={({ item }) => (
                                                    <Products
                                                        gambar={item.image}
                                                        nama={item.nama}
                                                        harga={item.harga}
                                                        liked={item.liked}
                                                        ratings={item.rating}
                                                        action={() => this.props.navigation.push('ProductDetail', {id_product: item.id})}
                                                    />
                                                )}
                                            />
                                        </View>
                                    </View>
                                </ScrollView>
                            </>
                        )}

                        {this.state.hidden_recentlyView === true && this.state.hidden_touch === false && this.state.touch === 'Recently' && (
                            <>
                                <View style={{ width: '100%', alignItems: 'center', backgroundColor: '#121212' }}>
                                    <View style={[ b.mt4, b.mb4, { width: wp('90%'), flexDirection: 'row', alignItems: 'center' }]}>
                                        <View style={{ width: wp('15%') }}>
                                            <TouchableOpacity onPress={() => this.setState({ hidden_recentlyView: false, hidden_touch: true })}>
                                                <FastImage source={require('../asset/left.png')} style={{ width: xs(25), height: xs(25) }} />
                                            </TouchableOpacity>
                                        </View>
                                        <View style={{ width: wp('60%'), alignItems: 'center' }}>
                                            <Text style={[ c.light, f._16, { fontFamily: 'Roboto-Bold' }]}>Baru dilihat</Text>
                                        </View>
                                        <View style={{ width: wp('15%'), alignItems: 'flex-end' }}>
                                        </View>
                                    </View>
                                </View>

                                <ScrollView showsVerticalScrollIndicator={false} style={{ backgroundColor: '#181B22' }}>
                                    <View style={{ width: '100%', alignItems: 'center' }}>
                                        <View style={[ b.mt4, { width: wp('90%') }]}>
                                            <FlatList
                                                numColumns={2}
                                                data={this.state.my_favorite}
                                                keyExtractor={item => item.id}
                                                renderItem={({ item }) => (
                                                  <TouchableOpacity style={[ b.roundedLow, b.mr2, b.mb2, { width: xs(151), borderWidth: 1, borderColor: blue, backgroundColor: '#121212' }]}>
                                                    <View style={{ width: '100%', backgroundColor: light, alignItems: 'center', borderTopRightRadius: xs(5), borderTopLeftRadius: xs(5) }}>
                                                        <FastImage source={item.image} style={[ b.mt2, b.mb2, { width: xs(100), height: xs(100) }]} />
                                                    </View>

                                                    <Text style={[ c.light, b.ml1, { fontFamily: 'Roboto-Regular', marginTop: xs(3) }]} numberOfLines={2}>{item.nama}</Text>
                                                    {item.diskon !== undefined ?
                                                        <>
                                                        <Text style={[ c.light, b.ml1, { fontFamily: 'Roboto-Bold' }]}>{this.format(item.harga_diskon)}</Text>
                                                        <View style={[ b.mb1, b.mt1, b.ml1, { flexDirection: 'row', alignItems: 'center' }]}>
                                                            <View style={{ backgroundColor: '#FF8B12', borderRadius: xs(3) }}>
                                                                <Text style={[ c.light, f._10, { fontFamily: 'Roboto-Bold', marginTop: xs(1), marginBottom: xs(1), marginLeft: xs(2), marginRight: xs(2) }]}>{item.diskon}</Text>
                                                            </View>
                                                            <Text style={[ f._10, { fontFamily: 'Roboto-Regular', color: '#CCC', textDecorationLine: 'line-through', marginLeft: xs(3) }]}>{this.format(item.harga_awal)}</Text>
                                                        </View>
                                                        </>
                                                        :
                                                        <Text style={[ c.light, b.ml1, b.mb1, { fontFamily: 'Roboto-Bold' }]}>{this.format(item.harga)}</Text>
                                                    }
                                                    <View style={[ b.mb1, b.ml1, { flexDirection: 'row', alignItems: 'center' }]}>
                                                        <FastImage source={require('../asset/tokolokasi_icon.png')} style={{ width: xs(20), height: xs(17) }} />
                                                        <Text style={[ f._12, { fontFamily: 'Roboto-Regular', color: '#CCC', marginLeft: xs(3) }]}>Jakarta</Text>
                                                    </View>
                                                    <View style={[ b.mb2, b.ml1, { flexDirection: 'row', alignItems: 'center' }]}>
                                                        <Image source={require('../asset/star.png')} style={{ width: xs(15), height: xs(15), tintColor: 'darkorange' }} />
                                                        <Text style={[ f._12, { fontFamily: 'Roboto-Regular', color: 'darkorange', marginLeft: xs(3) }]}>{item.rating}.0</Text>
                                                        <View style={{ width: xs(12), borderBottomWidth: 1, borderBottomColor: 'darkorange', transform: [{ rotate: '90deg' }] }} />
                                                        {item.rating.length === 0 ?
                                                          <Text style={[ f._12, { fontFamily: 'Roboto-Regular', color: 'darkorange' }]}>0 Ulasan</Text>
                                                          :
                                                          <Text style={[ f._12, { fontFamily: 'Roboto-Regular', color: 'darkorange' }]}>{item.ulasan} Ulasan</Text>
                                                        }
                                                    </View>
                                                  </TouchableOpacity>
                                                )}
                                            />
                                        </View>
                                    </View>
                                </ScrollView>
                            </>
                        )}

                        {this.state.hidden_myCoupon === true && this.state.hidden_touch === false && this.state.touch === 'Coupon' && (
                            <>
                                <View style={{ width: '100%', alignItems: 'center', backgroundColor: '#121212' }}>
                                    <View style={[ b.mt4, b.mb4, { width: wp('90%'), flexDirection: 'row', alignItems: 'center' }]}>
                                        <View style={{ width: wp('15%') }}>
                                            <TouchableOpacity onPress={() => this.setState({ hidden_myCoupon: false, hidden_touch: true })}>
                                                <FastImage source={require('../asset/left.png')} style={{ width: xs(25), height: xs(25) }} />
                                            </TouchableOpacity>
                                        </View>
                                        <View style={{ width: wp('60%'), alignItems: 'center' }}>
                                            <Text style={[ c.light, f._16, { fontFamily: 'Roboto-Bold' }]}>Kupon Saya</Text>
                                        </View>
                                        <View style={{ width: wp('15%'), alignItems: 'flex-end' }}>
                                        </View>
                                    </View>
                                </View>

                                <ScrollView showsVerticalScrollIndicator={false} style={{ backgroundColor: '#181B22' }}>
                                    <View style={{ width: '100%', alignItems: 'center' }}>
                                        <View style={[ b.mt4, { width: wp('90%') }]}>
                                            <FlatList
                                                data={this.state.promoCode}
                                                keyExtractor={item => item.id}
                                                renderItem={({ item, index }) => (
                                                    <TouchableOpacity>
                                                        <FastImage source={require('../asset/couponpromo.png')} style={{ width: wp('90%'), height: xs(100), marginBottom: wp('4%'), flexDirection: 'row', alignItems: 'center' }}>
                                                            <View style={{ width: wp('27.5%'), height: '100%', alignItems: 'center', justifyContent: 'center' }}>
                                                                <View style={{ width: wp('20%') }}>
                                                                    <Text style={[ c.light, p.textCenter, f._16, { fontFamily: 'Roboto-Bold' }]}>{item.jenis_voucher}</Text>
                                                                </View>
                                                            </View>
                                                            <View style={{ width: wp('47%'), height: '100%' }}>
                                                                <Text style={[ c.blue, f._16, { marginTop: wp('2%'), marginLeft: wp('4%'), fontFamily: 'Roboto-Bold' }]}>{item.code}</Text>
                                                                <Text style={{ marginLeft: wp('4%'), marginTop: wp('1%'), fontFamily: 'Roboto-Bold' }}>{item.desc}</Text>
                                                                <View style={{ marginLeft: wp('4%'), borderWidth: 1, borderColor: blue, width: wp('32%'), alignItems: 'center', borderRadius: wp('2%') }}>
                                                                    <Text style={[ c.blue, { fontFamily: 'Roboto-Regular' }]}>{item.for}</Text>
                                                                </View>
                                                                <Text style={[ f._12, { marginLeft: wp('4%'), color: softGrey, marginTop: wp('1%'), fontFamily: 'Roboto-Regular' }]}>{item.exp}</Text>
                                                            </View>
                                                            <View style={{ width: wp('15.5%'), height: '100%' }}>
                                                                {this.state.checked === index ?
                                                                    <View style={{ width: wp('5%'), height: wp('5%'), backgroundColor: blue, borderRadius: wp('5%'), marginLeft: wp('6%'), marginTop: wp('2%') }}></View>
                                                                    :
                                                                    <View style={{ width: wp('5%'), height: wp('5%'), borderWidth: 1, borderColor: blue, borderRadius: wp('5%'), marginLeft: wp('6%'), marginTop: wp('2%') }}></View>
                                                                }
                                                                <TouchableOpacity onPress={() => this.props.navigation.push('DetailPromoCode')}>
                                                                    <Text style={[ c.blue, f._12, { marginLeft: wp('6%'), marginTop: wp('14%'), fontFamily: 'Roboto-Regular' }]}>S&K</Text>
                                                                </TouchableOpacity>
                                                            </View>
                                                        </FastImage>
                                                    </TouchableOpacity>
                                                )}
                                            />
                                        </View>

                                        <TouchableOpacity style={{ width: wp('90%'), height: hp('7%'), backgroundColor: blue, borderRadius: xs(5), alignItems: 'center', justifyContent: 'center' }}>
                                            <Text style={[ c.light, { fontFamily: 'Roboto-Bold' }]}>Gunakan Kode</Text>
                                        </TouchableOpacity>
                                    </View>
                                </ScrollView>
                            </>
                        )}

                        {this.state.hidden_referal === true && this.state.hidden_touch === false && this.state.touch === 'Referal' && (
                            <>
                                <View style={{ width: '100%', alignItems: 'center', backgroundColor: '#121212' }}>
                                    <View style={[ b.mt4, b.mb4, { width: wp('90%'), flexDirection: 'row', alignItems: 'center' }]}>
                                        <View style={{ width: wp('15%') }}>
                                            <TouchableOpacity onPress={() => this.setState({ hidden_referal: false, hidden_touch: true })}>
                                                <FastImage source={require('../asset/left.png')} style={{ width: xs(25), height: xs(25) }} />
                                            </TouchableOpacity>
                                        </View>
                                        <View style={{ width: wp('60%'), alignItems: 'center' }}>
                                            <Text style={[ c.light, f._16, { fontFamily: 'Roboto-Bold' }]}>Undang Teman</Text>
                                        </View>
                                        <View style={{ width: wp('15%'), alignItems: 'flex-end' }}>
                                        </View>
                                    </View>
                                </View>

                                <ScrollView showsVerticalScrollIndicator={false} style={{ backgroundColor: '#181B22' }}>
                                    <View style={{ width: '100%', alignItems: 'center' }}>
                                        <FastImage source={require('../asset/referal.png')} style={[ b.mt4, { width: xs(300), height: xs(153) }]} />
                                        <View style={[ b.mt2, { width: wp('90%') }]}>
                                            <Text style={[ c.light, p.textCenter, { fontFamily: 'Roboto-Bold' }]}>Undang Temanmu, Dapatkan hadiahnya</Text>

                                            <Text style={[ c.light, p.textCenter, b.mt2, { fontFamily: 'Roboto-Regular' }]}>Undang temanmu untuk bergabung dan dapatkan berbagai hadiah menarik untuk setiap teman yang bergabung dengan kode undanganmu</Text>
                                        </View>

                                        <Text style={[ c.light, { fontFamily: 'Roboto-Regular', marginTop: vs(50) }]}>KODE UNDANGAN ANDA</Text>
                                        <View style={[ b.mt1, { width: xs(150), backgroundColor: light, borderWidth: 1, borderColor: blue, alignItems: 'center' }]}>
                                            <Text style={[ b.mt2, c.blue, { fontFamily: 'Roboto-Bold' }]}>GF10HP</Text>
                                            <Text style={[ b.mt1, b.mb2, c.blue, f._12, { fontFamily: 'Roboto-Regular' }]}>Tab untuk salin</Text>
                                        </View>

                                        <TouchableOpacity onPress={() => Share.open(options)} style={[ b.mt4, { width: wp('90%'), height: hp('7%'), backgroundColor: blue, borderRadius: xs(5), flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }]}>
                                            <Image source={require('../asset/share.png')} style={{ width: xs(18), height: xs(20), tintColor: light }} />
                                            <Text style={[ c.light, b.ml1, { fontFamily: 'Roboto-Bold' }]}>Bagikan</Text>
                                        </TouchableOpacity>
                                    </View>
                                </ScrollView>
                            </>
                        )}
                    </View>
                </ScrollView>
            </>
        )
    }
}
