import React, { Component } from 'react'
import { View, Text, Image, TouchableOpacity, TextInput, ToastAndroid } from 'react-native'
import { blue, light, softGrey } from '../utils/Color'
import { xs, vs } from '../utils/Responsive'
import { f, c, b, p } from '../utils/StyleHelper'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
import FastImage from 'react-native-fast-image'
import AsyncStorage from '@react-native-async-storage/async-storage'
import axios from 'axios'
import { API_LOCAL, API_URL } from '@env'

export default class Help extends Component {
	constructor(props) {
		super(props);
		this.state = {
			search: ''
		}
	}

	render() {
		return (
			<View style={{ width: '100%', height: '100%', backgroundColor: '#181B22' }}>
				<View style={{ width: '100%', backgroundColor: '#121212', alignItems: 'center' }}>
					<View style={{ width: wp('90%'), height: hp('10%'), flexDirection: 'row', alignItems: 'center' }}>
						<View style={{ width: wp('15%') }}>
							<TouchableOpacity onPress={() => this.props.navigation.pop()}>
								<FastImage source={require('../asset/left.png')} style={{ width: xs(25), height: xs(25) }} />
							</TouchableOpacity>
						</View>
						<View style={{ width: wp('60%'), alignItems: 'center' }}>
							<Text style={[ c.light, f._16, { fontFamily: 'Roboto-Bold' }]}>Bantuan</Text>
						</View>
						<View style={{ width: wp('15%') }}>
						</View>
					</View>
				</View>

				<View style={{ width: '100%', alignItems: 'center' }}>
					<View style={[ b.mt2, { width: wp('90%') }]}>
						<Text style={[ c.light, f._18, { fontFamily: 'Roboto-Regular' }]}>Hi, ada yang bisa kami bantu?</Text>
					</View>

					<View style={[ b.mt3, { width: wp('90%'), height: hp('6.5%'), borderRadius: wp('5%'), backgroundColor: softGrey, flexDirection: 'row', alignItems: 'center' }]}>
						<TextInput
							style={[ c.light, { width: wp('62%'), height: '100%', marginLeft: wp('4%'), fontFamily: 'Roboto-Regular' }]}
							ref={ref => this.textInputRef = ref}
							placeholder='Ketik kata kunci'
							placeholderTextColor={light}
							returnKeyType='search'
							onChangeText={(text) => this.setState({ search: text })}
							// onSubmitEditing={() => this.nextScreen()}
							value={this.state.search}
						/>
						<View style={{ width: wp('20%'), flexDirection: 'row-reverse' }}>
							{this.state.search === '' ?
								<TouchableOpacity onPress={() => this.textInputRef.focus()}>
									<Image source={require('../asset/search.png')} style={{ width: wp('6%'), height: wp('6%'), tintColor: light, marginLeft: wp('4%') }} />
								</TouchableOpacity>
								:
								<TouchableOpacity onPress={() => this.setState({ search: '' })} style={{ marginLeft: wp('4%') }}>
									<Image source={require('../asset/cross.png')} style={{ width: wp('6%'), height: wp('6%'), tintColor: light }} />
								</TouchableOpacity>
							}
							{/* <TouchableOpacity>
								<Image source={require('../asset/camera.png')} style={{ width: wp('6%'), height: wp('6%'), tintColor: light }} />
							</TouchableOpacity> */}
						</View>
					</View>

					<View style={[ b.mt4, { width: wp('90%') }]}>
						<Text style={[ c.light, { fontFamily: 'Roboto-Bold' }]}>Yang sering ditanyakan</Text>
					</View>
					<View style={[ b.mt4, { width: '100%', alignItems: 'center' }]}>
						<View style={{ width: wp('90%') }}>
							<Text onPress={() => this.props.navigation.push('Laporan', {laporan: 'barcode'})} style={[ c.blue, { fontFamily: 'Roboto-Regular' }]}>Kode barcode expired</Text>
						</View>
						<View style={[ b.mt3, b.mb3, { width: '100%', borderBottomWidth: 1, borderBottomColor: softGrey }]} />

						<View style={{ width: wp('90%') }}>
							<Text onPress={() => this.props.navigation.push('Laporan', {laporan: 'produk'})} style={[ c.blue, { fontFamily: 'Roboto-Regular' }]}>Produk bermasalah</Text>
						</View>
						<View style={[ b.mt3, b.mb3, { width: '100%', borderBottomWidth: 1, borderBottomColor: softGrey }]} />

						<View style={{ width: wp('90%') }}>
							<Text onPress={() => this.props.navigation.push('Laporan', {laporan: 'penjual'})} style={[ c.blue, { fontFamily: 'Roboto-Regular' }]}>Laporkan penjual</Text>
						</View>
						<View style={[ b.mt3, { width: '100%', borderBottomWidth: 1, borderBottomColor: softGrey }]} />
					</View>

					<View style={[ b.mt4, { width: wp('90%') }]}>
						<Text style={[ c.light, { fontFamily: 'Roboto-Bold' }]}>Hubungi kami</Text>
					</View>
					<TouchableOpacity onPress={() => ToastAndroid.show('not implemented yet', ToastAndroid.SHORT)} style={[ b.mt3, { width: wp('90%'), flexDirection: 'row', alignItems: 'center' }]}>
						<FastImage source={require('../asset/floatingbuttonapotekcs.png')} style={{ width: xs(30), height: xs(30) }} />
						<Text style={[ c.light, b.ml2, { fontFamily: 'Roboto-Regular' }]}>Chat dengan GiftPlus</Text>
					</TouchableOpacity>
					<TouchableOpacity onPress={() => ToastAndroid.show('not implemented yet', ToastAndroid.SHORT)} style={[ b.mt3, { width: wp('90%'), flexDirection: 'row', alignItems: 'center' }]}>
						<FastImage source={require('../asset/floatingbuttonapotekcs.png')} style={{ width: xs(30), height: xs(30) }} />
						<Text style={[ c.light, b.ml2, { fontFamily: 'Roboto-Regular' }]}>Kirim Email ke GiftPlus</Text>
					</TouchableOpacity>
				</View>
			</View>
		)
	}
}