import React, { Component } from 'react'
import { View, Text, Image, TouchableOpacity, TextInput, FlatList, ScrollView, ToastAndroid } from 'react-native'
import { blue, light, softGrey } from '../utils/Color'
import { xs, vs } from '../utils/Responsive'
import { f, c, b, p } from '../utils/StyleHelper'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
import FastImage from 'react-native-fast-image'
import AsyncStorage from '@react-native-async-storage/async-storage'
import axios from 'axios'
import { API_LOCAL, API_URL } from '@env'

export default class Laporan extends Component {
	constructor(props) {
		super(props);
		this.state = {
			laporan: '',
			product: [
				{id: 1, nama: 'Sarung Tangan', image: require('../asset/sarung_tangan.png'), jumlah: '1'}
			],
			produkBermasalah: '',
			laporPenjual: ''
		}
	}

	componentDidMount() {
		this.setState({ laporan: this.props.navigation.getParam('laporan') })
	}

	render() {
		return (
			<View style={{ width: '100%', height: '100%', backgroundColor: '#181B22' }}>
				{this.state.laporan === 'barcode' && (
					<>
						<ScrollView showsVerticalScrollIndicator={false}>
							<View style={{ width: '100%', backgroundColor: '#121212', alignItems: 'center' }}>
								<View style={{ width: wp('90%'), height: hp('10%'), flexDirection: 'row', alignItems: 'center' }}>
									<View style={{ width: wp('15%') }}>
										<TouchableOpacity onPress={() => this.props.navigation.pop()}>
											<FastImage source={require('../asset/left.png')} style={{ width: xs(25), height: xs(25) }} />
										</TouchableOpacity>
									</View>
									<View style={{ width: wp('60%'), alignItems: 'center' }}>
										<Text style={[ c.light, f._16, { fontFamily: 'Roboto-Bold' }]}>Kode Barcode</Text>
									</View>
									<View style={{ width: wp('15%') }}>
									</View>
								</View>
							</View>

							<View style={{ width: '100%', alignItems: 'center' }}>
								<View style={[ b.mt3, { width: wp('95%') }]}>
									<Text style={[ c.light, f._18, { fontFamily: 'Roboto-Regular' }]}>Produk di dalam Barcode</Text>
								</View>
							</View>
							<FlatList
								style={[ b.mt3 ]}
								showsVerticalScrollIndicator={false}
								data={this.state.product}
								keyExtractor={item => item.id}
								renderItem={({ item }) => (
									<View style={{ width: '100%', alignItems: 'center' }}>
										<View style={{ width: wp('95%'), flexDirection: 'row' }}>
											<View style={{ width: wp('20%') }}>
												<View style={{ width: '100%', height: xs(70), backgroundColor: light, alignItems: 'center', justifyContent: 'center' }}>
													<FastImage source={item.image} style={{ width: xs(60), height: xs(60) }} />
												</View>
											</View>
											<View style={{ width: wp('65%') }}>
												<Text style={[ c.light, b.ml2, { fontFamily: 'Roboto-Regular' }]} numberOfLines={2}>{item.nama}</Text>
											</View>
											<View style={{ width: wp('10%'), alignItems: 'flex-end' }}>
												<Text style={[ c.light, { fontFamily: 'Roboto-Regular', marginTop: vs(25) }]}>x{item.jumlah}</Text>
											</View>
										</View>

										<View style={[ b.mt2, b.mb2, { width: '100%', borderBottomWidth: 1, borderBottomColor: softGrey }]} />
									</View>
								)}
							/>
							<View style={[ b.mt4, { width: '100%', alignItems: 'center' }]}>
								<FastImage source={require('../asset/giftcard1.png')} style={{ width: xs(325), height: xs(230), alignItems: 'center' }}>
									<Text style={[ c.blue, { fontFamily: 'Roboto-Regular', marginTop: vs(30) }]}>Hi Michael</Text>
									<Text style={[ c.blue, f._18, { fontFamily: 'Roboto-Regular' }]}>Hi Michael</Text>
									<View style={{ width: xs(180) }}>
										<Text style={[ c.blue, p.textCenter, { fontFamily: 'Roboto-Regular' }]}>Semoga panjang umur dan sehat selalu</Text>
									</View>

									<Image source={require('../asset/qrCode.png')} style={{ width: xs(100), height: xs(100), tintColor: '#000', position: 'absolute', bottom: vs(5) }} />
								</FastImage>
							</View>
						</ScrollView>

						<View style={{ width: '100%', alignItems: 'center' }}>
							<TouchableOpacity onPress={() => this.props.navigation.navigate('OrderDetail')} style={[ b.mt4, b.mb4, b.roundedLow, { width: wp('90%'), height: hp('7%'), backgroundColor: blue, alignItems: 'center', justifyContent: 'center' }]}>
								<Text style={[ c.light, { fontFamily: 'Roboto-Bold' }]}>Ajukan Barcode baru</Text>
							</TouchableOpacity>
						</View>
					</>
				)}

				{this.state.laporan === 'produk' && (
					<>
						<ScrollView showsVerticalScrollIndicator={false}>
							<View style={{ width: '100%', backgroundColor: '#121212', alignItems: 'center' }}>
								<View style={{ width: wp('90%'), height: hp('10%'), flexDirection: 'row', alignItems: 'center' }}>
									<View style={{ width: wp('15%') }}>
										<TouchableOpacity onPress={() => this.props.navigation.pop()}>
											<FastImage source={require('../asset/left.png')} style={{ width: xs(25), height: xs(25) }} />
										</TouchableOpacity>
									</View>
									<View style={{ width: wp('60%'), alignItems: 'center' }}>
										<Text style={[ c.light, f._16, { fontFamily: 'Roboto-Bold' }]}>Produk Bermasalah</Text>
									</View>
									<View style={{ width: wp('15%') }}>
									</View>
								</View>
							</View>

							<FlatList
								style={[ b.mt3 ]}
								showsVerticalScrollIndicator={false}
								data={this.state.product}
								keyExtractor={item => item.id}
								renderItem={({ item }) => (
									<View style={{ width: '100%', alignItems: 'center' }}>
										<View style={{ width: wp('95%'), flexDirection: 'row' }}>
											<View style={{ width: wp('20%') }}>
												<View style={{ width: '100%', height: xs(70), backgroundColor: light, alignItems: 'center', justifyContent: 'center' }}>
													<FastImage source={item.image} style={{ width: xs(60), height: xs(60) }} />
												</View>
											</View>
											<View style={{ width: wp('65%') }}>
												<Text style={[ c.light, b.ml2, { fontFamily: 'Roboto-Regular' }]} numberOfLines={2}>{item.nama}</Text>
											</View>
											<View style={{ width: wp('10%'), alignItems: 'flex-end' }}>
												<Text style={[ c.light, { fontFamily: 'Roboto-Regular', marginTop: vs(25) }]}>x{item.jumlah}</Text>
											</View>
										</View>

										<View style={[ b.mt3, b.mb3, { width: '100%', borderBottomWidth: 1, borderBottomColor: softGrey }]} />
									</View>
								)}
							/>

							<View style={[ b.mt1, { width: '100%', alignItems: 'center' }]}>
								<View style={{ width: wp('90%') }}>
									<Text style={[ c.light, { fontFamily: 'Roboto-Regular' }]}>Tulis detail masalah</Text>
								</View>

								<View style={{ width: wp('90%') }}>
									<TextInput
										style={[ c.light, { width: '100%', maxHeight: hp('35%') }]}
										placeholder='Deskripsi produk'
										placeholderTextColor='#CCC'
										multiline={true}
										value={this.state.produkBermasalah}
										onChangeText={(text) => this.setState({ produkBermasalah: text })}
									/>
									<View style={{ width: '100%', borderBottomWidth: 1, borderBottomColor: '#CCC', marginTop: vs(-5) }} />
								</View>
							</View>
						</ScrollView>

						<View style={{ width: '100%', alignItems: 'center' }}>
							<TouchableOpacity onPress={() => ToastAndroid.show('not implemented yet', ToastAndroid.SHORT)} style={[ b.mt4, b.mb4, b.roundedLow, { width: wp('90%'), height: hp('7%'), backgroundColor: blue, alignItems: 'center', justifyContent: 'center' }]}>
								<Text style={[ c.light, { fontFamily: 'Roboto-Bold' }]}>Kirim</Text>
							</TouchableOpacity>
						</View>
					</>
				)}

				{this.state.laporan === 'penjual' && (
					<>
						<ScrollView showsVerticalScrollIndicator={false}>
							<View style={{ width: '100%', backgroundColor: '#121212', alignItems: 'center' }}>
								<View style={{ width: wp('90%'), height: hp('10%'), flexDirection: 'row', alignItems: 'center' }}>
									<View style={{ width: wp('15%') }}>
										<TouchableOpacity onPress={() => this.props.navigation.pop()}>
											<FastImage source={require('../asset/left.png')} style={{ width: xs(25), height: xs(25) }} />
										</TouchableOpacity>
									</View>
									<View style={{ width: wp('60%'), alignItems: 'center' }}>
										<Text style={[ c.light, f._16, { fontFamily: 'Roboto-Bold' }]}>Laporkan Penjual</Text>
									</View>
									<View style={{ width: wp('15%') }}>
									</View>
								</View>
							</View>

							<View style={[ b.mt3, { width: '100%', alignItems: 'center' }]}>
								<View style={{ width: wp('90%'), flexDirection: 'row' }}>
									<View style={{ width: wp('18%') }}>
										<View style={{ width: wp('15%'), height: wp('15%'), borderRadius: wp('15%'), backgroundColor: light, alignItems: 'center', justifyContent: 'center' }}>
											<FastImage source={require('../asset/logo_apotek.png')} style={{ width: xs(40), height: xs(40) }} />
										</View>
									</View>
									<View style={{ width: wp('62%') }}>
										<Text style={[ c.light, b.ml2, { fontFamily: 'Roboto-Bold' }]} numberOfLines={1}>Apotek Sehati</Text>
										<Text style={[ c.light, b.ml2, { fontFamily: 'Roboto-Regular' }]} numberOfLines={2}>Ruko Manyaran Semarang</Text>
									</View>
									<View style={{ width: wp('10%'), alignItems: 'flex-end' }}>
									</View>
								</View>

								<View style={[ b.mt3, b.mb3, { width: '100%', borderBottomWidth: 1, borderBottomColor: softGrey }]} />
							</View>

							<View style={[ b.mt1, { width: '100%', alignItems: 'center' }]}>
								<View style={{ width: wp('90%') }}>
									<Text style={[ c.light, { fontFamily: 'Roboto-Regular' }]}>Tulis detail laporan</Text>
								</View>

								<View style={{ width: wp('90%') }}>
									<TextInput
										style={[ c.light, { width: '100%', maxHeight: hp('35%') }]}
										placeholder='Deskripsi produk'
										placeholderTextColor='#CCC'
										multiline={true}
										value={this.state.laporPenjual}
										onChangeText={(text) => this.setState({ laporPenjual: text })}
									/>
									<View style={{ width: '100%', borderBottomWidth: 1, borderBottomColor: '#CCC', marginTop: vs(-5) }} />
								</View>
							</View>
						</ScrollView>

						<View style={{ width: '100%', alignItems: 'center' }}>
							<TouchableOpacity onPress={() => ToastAndroid.show('not implemented yet', ToastAndroid.SHORT)} style={[ b.mt4, b.mb4, b.roundedLow, { width: wp('90%'), height: hp('7%'), backgroundColor: blue, alignItems: 'center', justifyContent: 'center' }]}>
								<Text style={[ c.light, { fontFamily: 'Roboto-Bold' }]}>Kirim</Text>
							</TouchableOpacity>
						</View>
					</>
				)}
			</View>
		)
	}
}