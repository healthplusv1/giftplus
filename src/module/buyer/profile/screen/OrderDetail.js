import React, { Component } from 'react'
import { View, Text, Image, TouchableOpacity, ScrollView, FlatList } from 'react-native'
import { blue, light } from '../utils/Color'
import { xs, vs } from '../utils/Responsive'
import { f, c, b, p } from '../utils/StyleHelper'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
import FastImage from 'react-native-fast-image'
import LinearGradient from 'react-native-linear-gradient'
import AsyncStorage from '@react-native-async-storage/async-storage'
import axios from 'axios'
import { API_LOCAL, API_URL } from '@env'

export default class OrderDetail extends Component {
	constructor(props) {
		super(props);
		this.state = {
			products: [
	       {id: 1, nama: 'Sarung Tangan', image: require('../asset/sarung_tangan.png'), harga: '120000', jml_barang: '1', rating: '5', logo_apotek: require('../asset/logo_apotek.png'), nama_apotek: 'Apotek Sehati', no_pesanan: '123456789', tgl_pesan: '19/08/2021', total_pesanan: '120000'}
	    ],
	    giftCard: false
		}
	}

	componentDidMount() {
		console.log('giftcard: '+this.props.navigation.getParam('gift'))
		// this.setState({ giftCard: this.props.navigation.getParam('giftcard') })
	}

	format(number) {
        if (number) {
          var rupiah = "";
          var numberrev = number
            .toString()
            .split("")
            .reverse()
            .join("");
          for (var i = 0; i < numberrev.length; i++)
            if (i % 3 == 0) rupiah += numberrev.substr(i, 3) + ".";
          return (
            "Rp " +
            rupiah
              .split("", rupiah.length - 1)
              .reverse()
              .join("")
          );
        } else {
            return (
                "Rp "+number
            );
        }
    }

	render() {
		const { products } = this.state;
        let totalQuantity = 0;
        let totalPrice = 0;
        products.forEach((item) => {
            totalQuantity += item.jml_barang;
            totalPrice += item.jml_barang * item.harga/* - 10000*/;
        })
		return (
			<LinearGradient start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }} colors={[ '#181C21', '#252F38', '#252F38' ]} style={{ width: '100%', height: '100%' }}>
				<View style={{ width: '100%', backgroundColor: '#121212', alignItems: 'center' }}>
					<View style={{ width: wp('90%'), height: hp('10%'), flexDirection: 'row', alignItems: 'center' }}>
						<View style={{ width: wp('15%') }}>
							<TouchableOpacity onPress={() => this.props.navigation.pop()}>
								<FastImage source={require('../asset/left.png')} style={{ width: xs(25), height: xs(25) }} />
							</TouchableOpacity>
						</View>
						<View style={{ width: wp('60%'), alignItems: 'center' }}>
							<Text style={[ c.light, f._16, { fontFamily: 'Roboto-Bold' }]}>Detail Pembelian</Text>
						</View>
						<View style={{ width: wp('15%'), alignItems: 'flex-end' }}>
							<TouchableOpacity onPress={() => this.props.navigation.push('Help')}>
								<Image source={require('../asset/help.png')} style={{ width: xs(25), height: xs(25), tintColor: blue }} />
							</TouchableOpacity>
						</View>
					</View>
				</View>

				<ScrollView>
					<View style={{ width: '100%', alignItems: 'center' }}>
						<View style={[ b.mt4, b.mb2, { width: wp('90%'), flexDirection: 'row', alignItems: 'center' }]}>
							<View style={{ width: wp('45%') }}>
								<Text style={[ c.light ]}>No Pesanan</Text>
							</View>
							<View style={{ width: wp('45%'), alignItems: 'flex-end' }}>
								<Text style={[ c.light ]}>123456789</Text>
							</View>
						</View>
						<View style={{ width: wp('90%'), borderBottomWidth: 1, borderBottomColor: light }} />
						<View style={[ b.mt2, b.mb4, { width: wp('90%'), flexDirection: 'row', alignItems: 'center' }]}>
							<View style={{ width: wp('45%') }}>
								<Text style={[ c.light ]}>Waktu</Text>
							</View>
							<View style={{ width: wp('45%'), alignItems: 'flex-end' }}>
								<Text style={[ c.light ]}>04/21/2021, 14:00 WIB</Text>
							</View>
						</View>

						<View style={{ width: '100%', height: hp('2%'), backgroundColor: '#121212' }} />

						<View style={{ width: '100%' }}>
							<FlatList
								style={[ b.mt2 ]}
								data={this.state.products}
								keyExtractor={item => item.id}
								renderItem={({ item }) => (
									<View style={{ width: '100%', alignItems: 'center' }}>
										<View style={{ width: wp('90%'), flexDirection: 'row' }}>
											<View style={{ width: wp('15%') }}>
												<FastImage source={ item.image } style={{ width: '100%', height: wp('15%') }} />
											</View>
											<View style={{ width: wp('50%') }}>
												<Text style={[ b.ml2, c.light, { fontFamily: 'Roboto-Bold' }]} numberOfLines={2}>{item.nama}</Text>
												{this.state.promo !== '' && (
													<Text style={[ b.ml2, b.mt1, { color: 'darkorange', fontFamily: 'Roboto-Regular' }]}>Promo</Text>
												)}
											</View>
											<View style={{ width: wp('25%'), alignItems: 'flex-end' }}>
												<Text style={[ c.light, { fontFamily: 'Roboto-Regular' }]}>x{item.jml_barang}</Text>
												<Text style={[ c.light, { fontFamily: 'Roboto-Regular' }]} numberOfLines={1}>{this.format(totalPrice)}</Text>
												{this.state.promo !== '' && (
													<Text style={[ b.mt1, { color: 'darkorange', fontFamily: 'Roboto-Regular' }]}>-{this.format(10000)}</Text>
												)}
											</View>
										</View>
										<View style={[ b.mt1, b.mb2, { width: '100%', borderBottomWidth: 1, borderBottomColor: light }]} />
										<View style={{ width: wp('90%'), flexDirection: 'row', alignItems: 'center' }}>
											<View style={{ width: wp('30%') }}>
												<Text style={[ c.light ]}>{item.jml_barang} produk</Text>
											</View>
											<View style={{ width: wp('60%'), alignItems: 'flex-end' }}>
												<Text style={[ c.light ]}>Total Pesanan: {this.format(totalPrice)}</Text>
											</View>
										</View>
										<View style={[ b.mt2, b.mb2, { width: wp('90%'), borderBottomWidth: 1, borderBottomColor: light }]} />
									</View>
								)}
							/>
						</View>
					</View>
				</ScrollView>

				{/*<View style={{ width: '100%', alignItems: 'center', position: 'absolute', bottom: 0 }}>
									{this.state.giftCard === true ?
										<TouchableOpacity style={[ b.mt4, b.mb4, b.roundedLow, { width: wp('90%'), height: hp('7%'), backgroundColor: blue, alignItems: 'center', justifyContent: 'center' }]}>
											<Text style={[ c.light, { fontFamily: 'Roboto-Bold' }]}>Kirim Ulang</Text>
										</TouchableOpacity>
										:
										<TouchableOpacity onPress={() => this.props.navigation.navigate('Gift')} style={[ b.mt4, b.mb4, b.roundedLow, { width: wp('90%'), height: hp('7%'), backgroundColor: blue, alignItems: 'center', justifyContent: 'center' }]}>
											<Text style={[ c.light, { fontFamily: 'Roboto-Bold' }]}>Buat Giftcard</Text>
										</TouchableOpacity>
									}
								</View>*/}
			</LinearGradient>
		)
	}
}