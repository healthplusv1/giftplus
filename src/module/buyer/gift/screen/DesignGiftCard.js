import React, { Component } from 'react'
import { Text, View, Image, ScrollView, TouchableOpacity, TextInput, FlatList, ToastAndroid, RefreshControl } from 'react-native'
import { blue, light } from '../utils/Color'
import { xs, vs } from '../utils/Responsive'
import { f, c, b, p } from '../utils/StyleHelper'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
import FastImage from 'react-native-fast-image'
import LinearGradient from 'react-native-linear-gradient'
import ViewShot from 'react-native-view-shot'
import Share from 'react-native-share'
import axios from 'axios'
import AsyncStorage from '@react-native-async-storage/async-storage'
import { API_LOCAL, API_URL } from '@env'

export default class DesignGiftCard extends Component {
	constructor(props) {
		super(props);
		this.state = {
            loading: false,
            position: [
                {pos: 'Potrait', image: require('../asset/rectangleUp.png')},
                {pos: 'Landscape', image: require('../asset/rectangle.png')}
            ],
            clicked: 0,
            detail: null,
            id_gift: '',
            namaPenerima: '',
            ucapanSelamat: '',
            tulisPesan: '',
            qr: require('../asset/qrCode.png')
		}
	}

    removeNull(array) {
        return array.filter(x => x !== null)
    }

    componentDidMount() {
        this.setState({ loading: true })
        console.log(" image ==> ", Image.resolveAssetSource(require('../asset/giftcard1.png')))

        this.setState({
            id_gift: this.props.navigation.getParam('id_gift')
        })

        /* coming from ChooseProduct.js a.k.a after performing payment */
        this.setState({
            order_id: this.props.navigation.getParam('order_id'),
            seller_id: this.props.navigation.getParam('seller_id'),
            order_item: this.props.navigation.getParam('order_item')
        })

        console.log(this.props.navigation.getParam('order_id'))
        console.log(this.props.navigation.getParam('seller_id'))
        console.log(this.props.navigation.getParam('order_item'))

    }

    async insert_gift() {
        const id_login = await AsyncStorage.getItem('@id_login')

        this.setState({
            loading: true
        })
        const imageView = await this.viewShot.capture();
        // appending params by FormData
        const data = new FormData();
        data.append('card_image', {
            name: Math.floor((Math.random() * 999999999999999) + 1)+'.png',
            type: 'image/png',
            uri: imageView
        });
        data.append("order_id", this.props.navigation.getParam('order_id'));
        data.append("seller_id", this.props.navigation.getParam('seller_id'));
        data.append("id_login", id_login);
        for (let order_item_id in this.props.navigation.getParam('order_item')) {
            data.append("order_item["+order_item_id+"]", this.props.navigation.getParam('order_item')[order_item_id]);
        } // end appending params by FormData

        let res = await fetch(API_URL+'/main/shopping/order/gift_cards', {
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data'
            },
            body: data
        });

        let response = await res.json();
        console.log("THE FAKING RESPONSE ==> ", response.data.qr_code)
        this.setState({qr: {uri : API_URL+'/public/storage/gift_card/qr/'+response.data.qr_code+'.png', loading: false}})

        if (response.data.qr_code !== '') {
            ToastAndroid.show('Wait a second, still in progress', ToastAndroid.SHORT)
            setTimeout( async () => {
                const imageURL = await this.viewShot.capture();
                this.props.navigation.push('Recipient', {imageURI: imageURL})
            }, 5000);
        } else {
            console.log('Data tidak tersimpan')
        }
    }

    async switch(index, detail) {
        await this.setState({
            clicked: index,
            detail: detail
        })
    }

    render() {
        return (
            <View style={{ width: '100%', height: '100%', backgroundColor: '#181B22' }}>
                {this.state.loading === true ?
                    <RefreshControl onRefresh={() => this.componentDidMount()} refreshing={this.state.loading} />
                    :
                    null
                }
                <View style={{ width: '100%', alignItems: 'center', backgroundColor: '#121212' }}>
                	<View style={[ b.mt4, b.mb4, { width: wp('90%'), flexDirection: 'row', alignItems: 'center' }]}>
                		<View style={{ width: wp('20%') }}>
                            <TouchableOpacity onPress={() => this.props.navigation.pop()}>
                    			<FastImage source={require('../asset/left.png')} style={{ width: xs(30), height: xs(30) }} />
                            </TouchableOpacity>
                		</View>
                        <View style={{ width: wp('50%'), alignItems: 'center' }}>
                            <Text style={[ c.light, f.bold, f._16 ]}>Design giftcard</Text>
                        </View>
                		<View style={{ width: wp('20%'), flexDirection: 'row-reverse' }}>
                            <TouchableOpacity onPress={() => this.insert_gift()} style={[ b.roundedLow, { width: wp('9%'), height: hp('5%'), backgroundColor: blue, alignItems: 'center', justifyContent: 'center' }]}>
                                <Image source={require('../asset/email_send.png')} style={{ width: xs(20), height: xs(20), marginLeft: xs(-5), tintColor: light, transform: [{ rotate: '45deg' }] }} />
                            </TouchableOpacity>
                			<TouchableOpacity onPress={() => ToastAndroid.show('still not implemented', ToastAndroid.SHORT)} style={[ b.roundedLow, { width: wp('9%'), height: hp('5%'), backgroundColor: blue, alignItems: 'center', justifyContent: 'center', marginRight: wp('2%') }]}>
                				<Image source={require('../asset/save.png')} style={{ width: xs(20), height: xs(20), tintColor: light }} />
                			</TouchableOpacity>
                		</View>
                	</View>
                </View>

                <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }} colors={[ '#181C21', '#252F38', '#252F38' ]} style={{ width: '100%', alignItems: 'center' }}>
                    {/*<FlatList
                        horizontal={true}
                        data={this.state.position}
                        keyExtractor={item => item.pos}
                        renderItem={({ item, index }) => (
                            (this.state.clicked === index ?
                                <View style={[ b.ml4, { flexDirection: 'row', alignItems: 'center' }]}>
                                    <TouchableOpacity onPress={() => this.switch(index, item.pos)}>
                                        <Image source={this.state.clicked === index ? item.image : require('../asset/rectangleOutlineUp.png')} style={{ width: xs(15), height: xs(15), tintColor: blue }} />
                                    </TouchableOpacity>
                                    <Text style={[ c.blue, b.ml1 ]}>{item.pos}</Text>
                                </View>
                                :
                                <View style={[ b.ml4, { flexDirection: 'row', alignItems: 'center' }]}>
                                    <TouchableOpacity onPress={() => this.switch(index, item.pos)}>
                                        <Image source={this.state.clicked === index ? item.image : require('../asset/rectangleOutline.png')} style={{ width: xs(15), height: xs(15), tintColor: blue }} />
                                    </TouchableOpacity>
                                    <Text style={[ c.light, b.ml1 ]}>{item.pos}</Text>
                                </View>
                            )
                        )}
                    />

                    {this.state.clicked === 0 && (
                        <View style={[ b.mt4, { width: wp('80%'), height: hp('60%'), backgroundColor: light, alignItems: 'center', justifyContent: 'center' }]}>
                            <Image source={require('../asset/giftcardmenu_icon.png')} style={{ width: xs(35), height: xs(35), tintColor: '#CCC' }} />
                            <Text style={[ b.mt1, { color: '#CCC' }]}>Buat Giftcardmu</Text>
                        </View>
                    )}

                    {this.state.clicked === 1 && (
                        <View style={{ width: wp('95%'), height: hp('30%'), backgroundColor: light, alignItems: 'center', justifyContent: 'center', marginTop: xs(100) }}>
                            <Image source={require('../asset/giftcardmenu_icon.png')} style={{ width: xs(35), height: xs(35), tintColor: '#CCC' }} />
                            <Text style={[ b.mt1, { color: '#CCC' }]}>Buat Giftcardmu</Text>
                        </View>
                    )}*/}

                    {this.state.id_gift === 1 && (
                        <ViewShot style={{ marginTop: xs(70) }} ref={ref => this.viewShot = ref} options={{ format: 'jpg', quality: 1.0 }}>
                            <FastImage source={require('../asset/giftcard1.png')} style={{ width: xs(320), height: xs(220), alignItems: 'center' }}>
                                <Text style={{ color: '#1A69C6', fontFamily: 'Roboto-Regular', marginTop: vs(30) }}>{this.state.namaPenerima}</Text>
                                <Text style={[ b.mt1, f._24, { color: '#1A69C6', fontFamily: 'Brush Script Regular', marginLeft: vs(2), marginRight: vs(2) }]}>{this.state.ucapanSelamat}</Text>
                                <View style={{ width: xs(200) }}>
                                    <Text style={[ b.mt1, p.textCenter, { color: '#1A69C6', fontFamily: 'Roboto-Regular' }]}>{this.state.tulisPesan}</Text>
                                </View>

                                <View style={{ width: xs(80), height: xs(80), backgroundColor: light, alignItems: 'center', justifyContent: 'center', position: 'absolute', bottom: vs(5) }}>
                                    <FastImage source={this.state.qr} style={{ width: '100%', height: '100%' }} />
                                </View>
                            </FastImage>
                        </ViewShot>
                    )}
                    {this.state.id_gift === 2 && (
                        <ViewShot style={{ marginTop: xs(70) }} ref={ref => this.viewShot = ref} options={{ format: 'jpg', quality: 1.0 }}>
                            <FastImage source={require('../asset/giftcard2.png')} style={{ width: xs(320), height: xs(220), alignItems: 'center' }}>
                                <Text style={{ color: '1A69C6', fontFamily: 'Roboto-Regular', marginTop: vs(30) }}>{this.state.namaPenerima}</Text>
                                <Text style={[ b.mt1, f._24, { color: '1A69C6', fontFamily: 'Brush Script Regular' }]}>{this.state.ucapanSelamat}</Text>
                                <View style={{ width: xs(200) }}>
                                    <Text style={[ b.mt1, p.textCenter, { color: '1A69C6', fontFamily: 'Roboto-Regular' }]}>{this.state.tulisPesan}</Text>
                                </View>

                                <View style={{ width: xs(80), height: xs(80), backgroundColor: light, alignItems: 'center', justifyContent: 'center', position: 'absolute', bottom: vs(5) }}>
                                    <FastImage source={this.state.qr} style={{ width: '100%', height: '100%' }} />
                                </View>
                            </FastImage>
                        </ViewShot>
                    )}
                    {this.state.id_gift === 3 && (
                        <ViewShot style={{ marginTop: xs(70) }} ref={ref => this.viewShot = ref} options={{ format: 'jpg', quality: 1.0 }}>
                            <FastImage source={require('../asset/giftcard3.png')} style={{ width: xs(320), height: xs(220), alignItems: 'center' }}>
                                <Text style={{ color: '1A69C6', fontFamily: 'Roboto-Regular', marginTop: vs(30) }}>{this.state.namaPenerima}</Text>
                                <Text style={[ b.mt1, f._24, { color: '1A69C6', fontFamily: 'Brush Script Regular' }]}>{this.state.ucapanSelamat}</Text>
                                <View style={{ width: xs(200) }}>
                                    <Text style={[ b.mt1, p.textCenter, { color: '1A69C6', fontFamily: 'Roboto-Regular' }]}>{this.state.tulisPesan}</Text>
                                </View>

                                <View style={{ width: xs(80), height: xs(80), backgroundColor: light, alignItems: 'center', justifyContent: 'center', position: 'absolute', bottom: vs(5) }}>
                                    <FastImage source={this.state.qr} style={{ width: '100%', height: '100%' }} />
                                </View>
                            </FastImage>
                        </ViewShot>
                    )}

                    <View style={[ b.mt4, { width: wp('90%') }]}>
                        <Text style={[ c.light, { fontFamily: 'Roboto-Bold' }]}>Tulis Pesan</Text>

                        <View style={[ b.mt2, b.rounded, p.textCenter, { width: '100%', backgroundColor: light }]}>
                            <TextInput
                                style={[ b.ml1, { width: wp('88%') }]}
                                placeholder='Nama penerima'
                                value={this.state.namaPenerima}
                                onChangeText={(text) => this.setState({ namaPenerima: text })}
                            />
                        </View>
                        <View style={[ b.mt2, b.rounded, p.textCenter, { width: '100%', backgroundColor: light }]}>
                            <TextInput
                                style={[ b.ml1, { width: wp('88%') }]}
                                placeholder='Ucapan salam'
                                value={this.state.ucapanSelamat}
                                onChangeText={(text) => this.setState({ ucapanSelamat: text })}
                            />
                        </View>
                        <View style={[ b.mt2, b.rounded, p.textCenter, { width: '100%', backgroundColor: light, marginBottom: xs(25) }]}>
                            <TextInput
                                style={[ b.ml1, { width: wp('88%') }]}
                                placeholder='Tulis pesan'
                                value={this.state.tulisPesan}
                                onChangeText={(text) => this.setState({ tulisPesan: text })}
                            />
                        </View>
                    </View>
                </LinearGradient>

                {/*<View style={{ width: '100%', height: hp('8%'), backgroundColor: blue, position: 'absolute', bottom: 0, flexDirection: 'row' }}>
                    <View style={{ width: wp('20%'), height: '100%', alignItems: 'center', justifyContent: 'center' }}>
                        <TouchableOpacity style={{ alignItems: 'center' }}>
                            <FastImage source={require('../asset/template_icon.png')} style={{ width: xs(20), height: xs(20) }} />
                            <Text style={[ c.light, f._12 ]}>Template</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{ width: wp('20%'), height: '100%', alignItems: 'center', justifyContent: 'center' }}>
                        <TouchableOpacity style={{ alignItems: 'center' }}>
                            <FastImage source={require('../asset/teks_icon.png')} style={{ width: xs(22), height: xs(20) }} />
                            <Text style={[ c.light, f._12 ]}>Teks</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{ width: wp('20%'), height: '100%', alignItems: 'center', justifyContent: 'center' }}>
                        <TouchableOpacity style={{ alignItems: 'center' }}>
                            <FastImage source={require('../asset/ukuran_icon.png')} style={{ width: xs(20), height: xs(20) }} />
                            <Text style={[ c.light, f._12 ]}>Ukuran</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{ width: wp('20%'), height: '100%', alignItems: 'center', justifyContent: 'center' }}>
                        <TouchableOpacity style={{ alignItems: 'center' }}>
                            <FastImage source={require('../asset/warna_icon.png')} style={{ width: xs(20), height: xs(20) }} />
                            <Text style={[ c.light, f._12 ]}>Warna</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{ width: wp('20%'), height: '100%', alignItems: 'center', justifyContent: 'center' }}>
                        <TouchableOpacity style={{ alignItems: 'center' }}>
                            <FastImage source={require('../asset/gambar_icon.png')} style={{ width: xs(21), height: xs(20) }} />
                            <Text style={[ c.light, f._12 ]}>Gambar</Text>
                        </TouchableOpacity>
                    </View>
                </View>*/}
            </View>
        )
    }
}
