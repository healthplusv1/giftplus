import React, { Component } from 'react'
import { View, Text, Image, ScrollView, FlatList, TouchableOpacity } from 'react-native'
import { blue, light, softBlue } from '../utils/Color'
import { xs, vs } from '../utils/Responsive'
import { f, c, b, p } from '../utils/StyleHelper'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
import FastImage from 'react-native-fast-image'
import Share from 'react-native-share'
import DateTimePicker from '@react-native-community/datetimepicker'
import AsyncStorage from '@react-native-async-storage/async-storage'
import axios from 'axios'
import { API_LOCAL, API_URL } from '@env'

export default class GiftCardSchedule extends Component {
	constructor(props) {
		super(props);
		this.state = {
			schedule: [
				{daftar: 'Akan Datang'},
				{daftar: 'Sudah Lewat'}
			],
			clicked: 0,
			akanDatang: [
				{id: 1, schedule: '2021-08-30T23:07:32', image: require('../asset/giftcard1.png')},
				{id: 2, schedule: '2021-08-30T23:07:32', image: require('../asset/giftcard1.png')}
			],
			sudahLewat: [
				{id: 1, schedule: '2021-08-15T23:07:32', image: require('../asset/giftcard1.png'), status: 'Sudah terkirim'},
				{id: 2, schedule: '2021-08-15T23:07:32', image: require('../asset/giftcard1.png'), status: 'Belum terkirim'}
			],
			month:[
				{bulan: 'Jan'},
				{bulan: 'Feb'},
				{bulan: 'Mar'},
				{bulan: 'Apr'},
				{bulan: 'Mei'},
				{bulan: 'Juni'},
				{bulan: 'Juli'},
				{bulan: 'Agu'},
				{bulan: 'Sep'},
				{bulan: 'Okt'},
				{bulan: 'Nov'},
				{bulan: 'Des'}
			],
			default0: true,
			default1: true,
			kirim: false,
			kirimNanti: false,
			startDate: new Date().setDate(new Date().getDate()),
            startTime: new Date().setDate(new Date().getDate()),
            startDateActive: false,
            startTimeActive: false,
		}
	}

	async switch(index) {
		await this.setState({
			clicked: index
		})
	}

	day_left_to_begin(tanggal_mulai) {
		let tanggal_dari = new Date(tanggal_mulai)
		let tanggal_sekarang = new Date()
	
		// To calculate the time difference of two dates 
		var Difference_In_Time = tanggal_sekarang.getTime() - tanggal_dari.getTime(); 
		  
		// To calculate the no. of days between two dates 
		var Difference_In_Days = Difference_In_Time / (1000 * 3600 * 24);
	
		return Difference_In_Days.toFixed().toString().replace(/-/gi, '');
	}

	shareWhatsApp() {
        Share.shareSingle({
            title: 'Share via',
            message: 'some message',
            url: this.state.image_url,
            social: Share.Social.WHATSAPP,
        })
    }

    shareMessenger() {
        Share.shareSingle({
            title: 'Share via',
            message: 'some message',
            url: this.state.image_url,
            social: Share.Social.MESSENGER,
        })
    }

    shareTelegram() {
        Share.shareSingle({
            title: 'Share via',
            message: 'some message',
            url: this.state.image_url,
            social: Share.Social.TELEGRAM,
        })
    }

    shareEmail() {
        Share.shareSingle({
            title: 'Share via',
            message: 'some message',
            url: this.state.image_url,
            social: Share.Social.EMAIL,
        })
    }

    sharePesan() {
        Share.shareSingle({
            title: 'Share via',
            message: 'some message',
            url: this.state.image_url,
            social: Share.Social.SMS,
        })
    }

	render() {
		return (
			<View style={{ width: '100%', height: '100%', backgroundColor: '#181B22' }}>
				<View style={{ width: '100%', height: hp('8%') }}>
					<FlatList
						horizontal={true}
						data={this.state.schedule}
						keyExtractor={item => item.daftar}
						renderItem={({ item, index }) => (
							(this.state.clicked === index ?
                                <TouchableOpacity onPress={() => this.switch(index)} style={{ width: wp('50%'), height: '100%', backgroundColor: blue, borderBottomWidth: 4, borderBottomColor: softBlue, alignItems: 'center', justifyContent: 'center' }}>
                                    <Text style={[ c.light, { fontFamily: 'Roboto-Bold' }]}>{item.daftar}</Text>
                                </TouchableOpacity>
                                :
                                <TouchableOpacity onPress={() => this.switch(index)} style={{ width: wp('50%'), height: '100%', backgroundColor: '#121212', alignItems: 'center', justifyContent: 'center' }}>
                                    <Text style={[ c.light, { fontFamily: 'Roboto-Bold' }]}>{item.daftar}</Text>
                                </TouchableOpacity>
                            )
						)}
					/>
				</View>

				{this.state.clicked === 0 && (
					<ScrollView showsVerticalScrollIndicator={false}>
						{this.state.default0 === true && (
							<FlatList
								style={[ b.mt2 ]}
								showsVerticalScrollIndicator={false}
								data={this.state.akanDatang}
								keyExtractor={item => item.id}
								renderItem={({ item }) => (
									<View style={{ width: '100%', alignItems: 'center' }}>
										<View style={{ width: wp('90%'), flexDirection: 'row' }}>
											<View style={{ width: wp('45%') }}>
												<FastImage source={item.image} style={{ width: '100%', height: xs(110), alignItems: 'center' }}>
													<Text style={[ c.blue, f._10, b.mt3, { fontFamily: 'Roboto-Regular' }]}>Hi Michael</Text>
													<Text style={[ c.blue, f._12, { fontFamily: 'BrushScript' }]}>Selamat Ulang Tahun</Text>
													<View style={{ width: xs(100) }}>
														<Text style={[ c.blue, f._10, p.textCenter, { fontFamily: 'BrushScript' }]}>Semoga panjang umur dan sehat selalu</Text>
													</View>
													<Image source={require('../asset/qrCode.png')} style={{ width: xs(35), height: xs(35), tintColor: '#000', position: 'absolute', bottom: xs(5) }} />
												</FastImage>
											</View>
											<View style={{ width: wp('45%') }}>
												<Text style={[ c.light, b.ml2, { fontFamily: 'Roboto-Bold' }]}>{this.day_left_to_begin(item.schedule)} hari lagi</Text>
												<Text style={[ c.light, b.ml2, { fontFamily: 'Roboto-Regular' }]}>{new Date(Date.parse(item.schedule)).getDate()-1}  {this.state.month[new Date(Date.parse(item.schedule)).getMonth()].bulan} {new Date(Date.parse(item.schedule)).getFullYear()} {new Date(Date.parse(item.schedule)).getHours()}:{new Date(Date.parse(item.schedule)).getMinutes()}:{new Date(Date.parse(item.schedule)).getSeconds()}</Text>

												<TouchableOpacity onPress={() => this.setState({ kirim: true, default0: false })} style={[ b.ml2, b.roundedLow, { width: xs(125), height: xs(30), backgroundColor: blue, position: 'absolute', bottom: 0, alignItems: 'center', justifyContent: 'center' }]}>
													<Text style={[ c.light, { fontFamily: 'Roboto-Regular' }]}>Kirim sekarang</Text>
												</TouchableOpacity>
											</View>
										</View>

										<View style={[ b.mt2, b.mb2, { width: '100%', borderBottomWidth: 1, borderBottomColor: light }]} />
									</View>
								)}
							/>
						)}

						{this.state.kirim === true && (
							<View style={[ b.mt3, { width: '100%', height: hp('90%'), alignItems: 'center', backgroundColor: '#343E47', borderTopLeftRadius: xs(8), borderTopRightRadius: xs(8) }]}>
								<TouchableOpacity onPress={() => this.setState({ kirim: false, default0: true })} style={[ b.roundedLow, b.mt3, { width: xs(50), height: xs(5), backgroundColor: light }]} />

								<View style={[ b.mt4, { width: wp('90%') }]}>
									<Text style={[ c.light, f._16, { fontFamily: 'Roboto-Regular' }]}>Kirim dengan</Text>
								</View>

								<View style={[ b.mt4, { width: wp('90%'), flexDirection: 'row' }]}>
			                        <TouchableOpacity onPress={() => this.shareWhatsApp()} style={{ width: wp('22.5%'), alignItems: 'center' }}>
			                            <FastImage source={require('../asset/whatsapp.png')} style={{ width: xs(40), height: xs(40) }} />
			                            <Text style={[ p.textCenter, c.light, b.mt1 ]}>WhatsApp</Text>
			                        </TouchableOpacity>
			                        <TouchableOpacity onPress={() => ToastAndroid.show('the package still has problem to send by Facebook Messenger', ToastAndroid.SHORT)} style={{ width: wp('22.5%'), alignItems: 'center' }}>
			                            <FastImage source={require('../asset/facebookmessager.png')} style={{ width: xs(40), height: xs(40) }} />
			                            <Text style={[ p.textCenter, c.light, b.mt1 ]}>Facebook Messenger</Text>
			                        </TouchableOpacity>
			                        <TouchableOpacity onPress={() => this.shareTelegram()} style={{ width: wp('22.5%'), alignItems: 'center' }}>
			                            <FastImage source={require('../asset/telegram.png')} style={{ width: xs(40), height: xs(40) }} />
			                            <Text style={[ p.textCenter, c.light, b.mt1 ]}>Telegram</Text>
			                        </TouchableOpacity>
			                        <TouchableOpacity onPress={() => this.shareEmail()} style={{ width: wp('22.5%'), alignItems: 'center' }}>
			                            <FastImage source={require('../asset/email.png')} style={{ width: xs(40), height: xs(40) }} />
			                            <Text style={[ p.textCenter, c.light, b.mt1 ]}>Email</Text>
			                        </TouchableOpacity>
			                    </View>

			                    <View style={[ b.mt3, { width: wp('90%'), flexDirection: 'row' }]}>
			                        <TouchableOpacity onPress={() => ToastAndroid.show('the package still has problem to send by SMS', ToastAndroid.SHORT)} style={{ width: wp('22.5%'), alignItems: 'center' }}>
			                            <FastImage source={require('../asset/pesan.png')} style={{ width: xs(40), height: xs(40) }} />
			                            <Text style={[ p.textCenter, c.light, b.mt1 ]}>Pesan</Text>
			                        </TouchableOpacity>
			                    </View>
							</View>
						)}
					</ScrollView>
				)}

				{this.state.clicked === 1 && (
					<ScrollView showsVerticalScrollIndicator={false}>
						{this.state.default1 === true && (
							<FlatList
								style={[ b.mt2 ]}
								showsVerticalScrollIndicator={false}
								data={this.state.sudahLewat}
								keyExtractor={item => item.id}
								renderItem={({ item }) => (
									<View style={{ width: '100%', alignItems: 'center' }}>
										<View style={[ b.mb1, { width: wp('90%'), flexDirection: 'row', alignItems: 'center' }]}>
											<Image source={require('../asset/giftcardmenu_icon.png')} style={{ width: xs(15), height: xs(15) }} />
											<Text style={[ c.light, b.ml1, { fontFamily: 'Roboto-Regular' }]}>Status:</Text>
											{item.status === 'Sudah terkirim' ?
												<Text style={[ c.light, b.ml1, { fontFamily: 'Roboto-Regular' }]}>{item.status}</Text>
												:
												<Text style={[ b.ml1, { fontFamily: 'Roboto-Regular', color: 'darkorange' }]}>{item.status}</Text>
											}
										</View>
										<View style={{ width: wp('90%'), flexDirection: 'row' }}>
											<View style={{ width: wp('45%') }}>
												<FastImage source={item.image} style={{ width: '100%', height: xs(110), alignItems: 'center' }}>
													<Text style={[ c.blue, f._10, b.mt3, { fontFamily: 'Roboto-Regular' }]}>Hi Michael</Text>
													<Text style={[ c.blue, f._12, { fontFamily: 'BrushScript' }]}>Selamat Ulang Tahun</Text>
													<View style={{ width: xs(100) }}>
														<Text style={[ c.blue, f._10, p.textCenter, { fontFamily: 'BrushScript' }]}>Semoga panjang umur dan sehat selalu</Text>
													</View>
													<Image source={require('../asset/qrCode.png')} style={{ width: xs(35), height: xs(35), tintColor: '#000', position: 'absolute', bottom: xs(5) }} />
												</FastImage>
											</View>
											<View style={{ width: wp('45%') }}>
												<Text style={[ c.light, b.ml2, { fontFamily: 'Roboto-Bold', color: 'darkorange' }]}>Waktu sudah lewat</Text>
												<Text style={[ c.light, b.ml2, { fontFamily: 'Roboto-Regular' }]}>{new Date(Date.parse(item.schedule)).getDate()-1}  {this.state.month[new Date(Date.parse(item.schedule)).getMonth()].bulan} {new Date(Date.parse(item.schedule)).getFullYear()} {new Date(Date.parse(item.schedule)).getHours()}:{new Date(Date.parse(item.schedule)).getMinutes()}:{new Date(Date.parse(item.schedule)).getSeconds()}</Text>

												<TouchableOpacity onPress={() => this.setState({ kirim: true, kirimNanti: true, default1: false })} style={[ b.ml2, b.roundedLow, { width: xs(125), height: xs(30), backgroundColor: blue, alignItems: 'center', justifyContent: 'center', marginTop: vs(10) }]}>
													<Text style={[ c.light, { fontFamily: 'Roboto-Regular' }]}>Jadwal ulang</Text>
												</TouchableOpacity>

												<TouchableOpacity onPress={() => this.setState({ kirim: true, default1: false })} style={[ b.ml2, b.mt1, b.roundedLow, { width: xs(125), height: xs(30), backgroundColor: blue, alignItems: 'center', justifyContent: 'center' }]}>
													<Text style={[ c.light, { fontFamily: 'Roboto-Regular' }]}>Kirim ulang</Text>
												</TouchableOpacity>
											</View>
										</View>

										<View style={[ b.mt2, b.mb2, { width: '100%', borderBottomWidth: 1, borderBottomColor: light }]} />
									</View>
								)}
							/>
						)}

						{this.state.kirim === true && (
							<View style={[ b.mt3, { width: '100%', height: hp('90%'), alignItems: 'center', backgroundColor: '#343E47', borderTopLeftRadius: xs(8), borderTopRightRadius: xs(8) }]}>
								<TouchableOpacity onPress={() => this.setState({ kirim: false, kirimNanti: false, default1: true })} style={[ b.roundedLow, b.mt3, { width: xs(50), height: xs(5), backgroundColor: light }]} />

								{this.state.kirimNanti === true && (
									<>
										<View style={[ b.mt4, { width: wp('90%') }]}>
				                            <Text style={[ c.light, { fontFamily: 'Roboto-Regular' }]}>Kirim Nanti</Text>
										</View>

										<View style={[ b.mt3, { width: wp('90%'), flexDirection: 'row', alignItems: 'center' }]}>
				                            <TouchableOpacity onPress={() => this.setState({ startDateActive: true })} style={{ height: hp('5%'), backgroundColor: '#121212', borderWidth: 1, borderColor: blue, borderRadius: xs(5), flexDirection: 'row', alignItems: 'center' }}>
				                                <Text style={[ c.light, b.ml1, { fontFamily: 'Roboto-Regular' }]}>{typeof this.state.startDate === 'object' ? new Date(Date.parse(this.state.startDate)).getDate()+'/'+(new Date(Date.parse(this.state.startDate)).getMonth()+1)+'/'+new Date(Date.parse(this.state.startDate)).getFullYear() : new Date(this.state.startDate).getDate()+'/'+(new Date(this.state.startDate).getMonth()+1)+'/'+new Date(this.state.startDate).getFullYear()}</Text>
				                                <Image source={require('../asset/tanggal.png')} style={[ b.ml1, b.mr1, { width: xs(18), height: xs(18), tintColor: blue }]} />
				                            </TouchableOpacity>
				                            {this.state.startDateActive === true && (
				                                <DateTimePicker
				                                    mode="date"
				                                    value={new Date()}
				                                    minimumDate={new Date()}
				                                    onChange={data => {
				                                        if (data.type === 'dismissed') {
				                                            this.setState({
				                                                startDateActive: false
				                                            })
				                                        } else {
				                                            // console.log(data.nativeEvent.timestamp)
				                                            this.setState({
				                                                startDate: data.nativeEvent.timestamp,
				                                                startDateActive: false
				                                            })
				                                        }
				                                    }}
				                                />
				                            )}

				                            <Text style={[ c.light, b.ml2, b.mr2, { fontFamily: 'Roboto-Regular' }]}>-</Text>

				                            <TouchableOpacity onPress={() => this.setState({ startTimeActive: true })} style={{ height: hp('5%'), backgroundColor: '#121212', borderWidth: 1, borderColor: blue, borderRadius: xs(5), flexDirection: 'row', alignItems: 'center' }}>
				                                <Text style={[ c.light, b.ml1, { fontFamily: 'Roboto-Regular' }]}>{typeof this.state.startTime === 'object' ? new Date(Date.parse(this.state.startTime)).getHours()+':'+new Date(Date.parse(this.state.startTime)).getMinutes()+':'+new Date(Date.parse(this.state.startTime)).getSeconds() : new Date(this.state.startTime).getHours()+':'+new Date(this.state.startTime).getMinutes()+':'+new Date(this.state.startTime).getSeconds()}</Text>
				                                <Image source={require('../asset/estimated_icon.png')} style={[ b.ml2, b.mr1, { width: xs(18), height: xs(18), tintColor: blue }]} />
				                            </TouchableOpacity>
				                            {this.state.startTimeActive === true && (
				                                <DateTimePicker
				                                    mode="time"
				                                    value={new Date()}
				                                    onChange={data => {
				                                        if (data.type === 'dismissed') {
				                                            this.setState({
				                                                startTimeActive: false
				                                            })
				                                        } else {
				                                            this.setState({
				                                                startTime: data.nativeEvent.timestamp,
				                                                startTimeActive: false
				                                            })
				                                        }
				                                    }}
				                                />
				                            )}
				                        </View>
									</>
								)}

								<View style={[ b.mt4, { width: wp('90%') }]}>
									<Text style={[ c.light, f._16, { fontFamily: 'Roboto-Regular' }]}>Kirim dengan</Text>
								</View>

								<View style={[ b.mt4, { width: wp('90%'), flexDirection: 'row' }]}>
			                        <TouchableOpacity onPress={() => this.shareWhatsApp()} style={{ width: wp('22.5%'), alignItems: 'center' }}>
			                            <FastImage source={require('../asset/whatsapp.png')} style={{ width: xs(40), height: xs(40) }} />
			                            <Text style={[ p.textCenter, c.light, b.mt1 ]}>WhatsApp</Text>
			                        </TouchableOpacity>
			                        <TouchableOpacity onPress={() => ToastAndroid.show('the package still has problem to send by Facebook Messenger', ToastAndroid.SHORT)} style={{ width: wp('22.5%'), alignItems: 'center' }}>
			                            <FastImage source={require('../asset/facebookmessager.png')} style={{ width: xs(40), height: xs(40) }} />
			                            <Text style={[ p.textCenter, c.light, b.mt1 ]}>Facebook Messenger</Text>
			                        </TouchableOpacity>
			                        <TouchableOpacity onPress={() => this.shareTelegram()} style={{ width: wp('22.5%'), alignItems: 'center' }}>
			                            <FastImage source={require('../asset/telegram.png')} style={{ width: xs(40), height: xs(40) }} />
			                            <Text style={[ p.textCenter, c.light, b.mt1 ]}>Telegram</Text>
			                        </TouchableOpacity>
			                        <TouchableOpacity onPress={() => this.shareEmail()} style={{ width: wp('22.5%'), alignItems: 'center' }}>
			                            <FastImage source={require('../asset/email.png')} style={{ width: xs(40), height: xs(40) }} />
			                            <Text style={[ p.textCenter, c.light, b.mt1 ]}>Email</Text>
			                        </TouchableOpacity>
			                    </View>

			                    <View style={[ b.mt3, { width: wp('90%'), flexDirection: 'row' }]}>
			                        <TouchableOpacity onPress={() => ToastAndroid.show('the package still has problem to send by SMS', ToastAndroid.SHORT)} style={{ width: wp('22.5%'), alignItems: 'center' }}>
			                            <FastImage source={require('../asset/pesan.png')} style={{ width: xs(40), height: xs(40) }} />
			                            <Text style={[ p.textCenter, c.light, b.mt1 ]}>Pesan</Text>
			                        </TouchableOpacity>
			                    </View>
							</View>
						)}
					</ScrollView>
				)}
			</View>
		)
	}
}