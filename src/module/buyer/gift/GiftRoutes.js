import { createStackNavigator } from 'react-navigation-stack'
import GiftPage from './screen/Gift'

export const GiftStack = createStackNavigator({
	Gift: {
		screen: GiftPage,
		navigationOptions: {
            headerShown: false
        }
	}
})