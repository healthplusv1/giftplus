import { createStackNavigator } from 'react-navigation-stack'
import MallPage from './screen/Mall'
import AllProductMallPage from './screen/AllProductMall'
import ProductDetailMallPage from './screen/ProductDetailMall'
import BrandPilihanPage from './screen/BrandPilihan'

export const MallBuyerStack = createStackNavigator({
	Mall: {
		screen: MallPage,
		navigationOptions: {
            headerShown: false
        }
	},

	AllProductMall: {
		screen: AllProductMallPage,
		navigationOptions: {
			headerShown: false
		}
	},

	ProductDetailMall: {
		screen: ProductDetailMallPage,
		navigationOptions: {
			headerShown: false
		}
	},

	BrandPilihan: {
		screen: BrandPilihanPage,
		navigationOptions: {
			headerShown: false
		}
	}
})