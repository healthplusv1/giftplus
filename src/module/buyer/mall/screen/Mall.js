import React, { Component } from 'react'
import { View, Text, ScrollView, TouchableOpacity, Image, TextInput, RefreshControl, FlatList, TouchableWithoutFeedback, ToastAndroid } from 'react-native'
import { blue, light, softGrey } from '../utils/Color'
import { xs, vs } from '../utils/Responsive'
import { f, c, b, p } from '../utils/StyleHelper'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
import FastImage from 'react-native-fast-image'
import Swiper from 'react-native-swiper'
import AsyncStorage from '@react-native-async-storage/async-storage'
import axios from 'axios'
import { API_LOCAL, API_URL } from '@env'

export default class Mall extends Component {
	constructor(props) {
		super(props);
		this.state = {
			loading: false,
			count: '',
			search: '',
			brand_pilihan: [
				{id: 1, color: '#CC0000', deskripsi: 'Diskon hingga 50%', logo: require('../asset/Canon_logo.png'), image: require('../asset/canon.png')},
				{id: 2, color: '#999999', deskripsi: 'Diskon hingga 50%', logo: require('../asset/nike_logo.png'), image: require('../asset/sepatu.png')},
				{id: 3, color: '#1AB24D', deskripsi: 'Diskon hingga 50%', logo: require('../asset/parfum.png'), image: require('../asset/parfum.png')},
				{id: 4, color: '#08658C', deskripsi: 'Diskon hingga 50%', logo: require('../asset/hp_logo.png'), image: require('../asset/laptopHP.png')}
			],
			list_popular_products: [
          {id: 1, nama: 'Rolex GMT-MASTER II', gambar: require('../asset/sepatu.png'), harga_awal: '2000000', diskon: '40%', harga_diskon: '1200000'},
          {id: 2, nama: 'Rolex GMT-MASTER II', gambar: require('../asset/sepatu.png'), harga_awal: '2000000', diskon: '40%', harga_diskon: '1200000'},
          {id: 3, nama: 'Rolex GMT-MASTER II', gambar: require('../asset/sepatu.png'), harga_awal: '2000000', diskon: '40%', harga_diskon: '1200000'}
      ],
      list_recomendation: undefined,
		}
	}

	async componentDidMount() {
		this.setState({ loading: true })
		
		const id_login = await AsyncStorage.getItem('@id_login')

		if (id_login === null) {
			const guest = await AsyncStorage.getItem('id_login_guest')

			axios.get(API_URL+'/main/shopping/cart', {params: {
				id_login: guest
			}})
			.then(res => {
				this.setState({
					count: res.data.data.length,
					loading: false
				})
			})
		} else {
			axios.get(API_URL+'/main/shopping/cart', {params: {
				id_login: id_login
			}})
			.then(res => {
				this.setState({
					count: res.data.data.length,
					loading: false
				})
			})
		}

		axios.get(API_URL+'/main/shopping/produk')
		.then(result => {
			console.log(result.data.data[0].seller)
			this.setState({
				list_recomendation: result.data.data,
				loading: false
			})
		})
	}

	format(number) {
    if (number) {
      var rupiah = "";
      var numberrev = number
        .toString()
        .split("")
        .reverse()
        .join("");
      for (var i = 0; i < numberrev.length; i++)
        if (i % 3 == 0) rupiah += numberrev.substr(i, 3) + ".";
      return (
        "Rp " +
        rupiah
          .split("", rupiah.length - 1)
          .reverse()
          .join("")
      );
    } else {
        return (
            "Rp "+number
        );
    }
  }

  nextScreen() {
    axios.get(API_URL+'/main/shopping/search', {params: {
        keyword: this.state.search
    }})
    .then(res => {
        // console.log(this.state.search+' = '+JSON.stringify(res.data.data))
        this.props.navigation.push('AllProductMall', { keyword: this.state.search })
    })
  }

	render() {
		return (
			<ScrollView showsVerticalScrollIndicator={false} style={{ backgroundColor: '#181B22' }} refreshControl={<RefreshControl onRefresh={() => this.componentDidMount()} refreshing={this.state.loading} />}>
				<View style={{ width: '100%', backgroundColor: '#121212', alignItems: 'center' }}>
					<View style={[ b.mt4, b.mb4, { width: wp('90%'), flexDirection: 'row', alignItems: 'center' }]}>
						<View style={{ width: wp('45%') }}>
        			<FastImage source={require('../asset/giftplus.png')} style={{ width: xs(120), height: xs(32) }} />
        		</View>
        		<View style={{ width: wp('45%'), flexDirection: 'row-reverse' }}>
        			<TouchableOpacity onPress={() => this.props.navigation.push('Drawer')}>
        				<Image source={require('../asset/menu.png')} style={{ width: xs(22), height: xs(22), tintColor: light }} />
        			</TouchableOpacity>
        			<TouchableOpacity onPress={() => this.props.navigation.push('Notification')} style={[ b.mr2 ]}>
        				<Image source={require('../asset/notification.png')} style={{ width: xs(22), height: xs(22), tintColor: light }} />
        			</TouchableOpacity>
        			<TouchableOpacity onPress={() => this.props.navigation.push('Cart')} style={[ b.mr2 ]}>
        				<Image source={require('../asset/cart.png')} style={{ width: xs(19), height: xs(20), tintColor: light }} />
								{this.state.count !== 0 && (
									<View style={{ width: xs(12), height: xs(12), borderRadius: xs(12), backgroundColor: 'darkorange', position: 'absolute', right: 0, alignItems: 'center', justifyContent: 'center' }}>
                    <Text style={[ c.light, f._10 ]}>{this.state.count}</Text>
                  </View>
								)}
        			</TouchableOpacity>
        		</View>
					</View>
				</View>

				<View style={[ b.mt2, { width: '100%', alignItems: 'center' }]}>
					<View style={{ width: wp('90%'), height: hp('6.5%'), borderRadius: wp('5%'), backgroundColor: softGrey, flexDirection: 'row', alignItems: 'center' }}>
						<TextInput
							style={[ c.light, { width: wp('62%'), height: '100%', marginLeft: wp('4%'), fontFamily: 'Roboto-Regular' }]}
							ref={ref => this.textInputRef = ref}
							placeholder='Apa yang kamu cari'
							placeholderTextColor={light}
							returnKeyType='search'
							onChangeText={(text) => this.setState({ search: text })}
							onSubmitEditing={() => this.nextScreen()}
							value={this.state.search}
						/>
						<View style={{ width: wp('20%'), flexDirection: 'row-reverse' }}>
							{this.state.search === '' ?
								<TouchableOpacity onPress={() => this.textInputRef.focus()}>
									<Image source={require('../asset/search.png')} style={{ width: wp('6%'), height: wp('6%'), tintColor: light, marginLeft: wp('4%') }} />
								</TouchableOpacity>
								:
								<TouchableOpacity onPress={() => this.setState({ search: '' })} style={{ marginLeft: wp('4%') }}>
									<Image source={require('../asset/cross.png')} style={{ width: wp('6%'), height: wp('6%'), tintColor: light }} />
								</TouchableOpacity>
							}
							{/* <TouchableOpacity>
								<Image source={require('../asset/camera.png')} style={{ width: wp('6%'), height: wp('6%'), tintColor: light }} />
							</TouchableOpacity> */}
						</View>
					</View>

					<View style={[ b.mt4, { width: '100%' }]}>
						<Swiper
							height={120}
						>
							<FastImage source={require('../asset/banner.png')} style={{ width: '100%', height: '100%' }} />
						</Swiper>
					</View>

					<View style={[ b.mt4, { width: wp('90%'), flexDirection: 'row', alignItems: 'center' }]}>
						<View style={{ width: wp('45%') }}>
							<Text style={[ c.light, { fontFamily: 'Roboto-Bold' }]}>Brand Pilihan</Text>
						</View>
						<View style={{ width: wp('45%'), alignItems: 'flex-end' }}>
							<TouchableOpacity onPress={() => this.props.navigation.push('DaftarBrand')}>
								<Text style={[ c.blue, { fontFamily: 'Roboto-Regular' }]}>Lihat Semua</Text>
							</TouchableOpacity>
						</View>
					</View>

					<View style={[ b.mt4, { width: '100%' }]}>
						<FlatList
							numColumns={2}
							data={this.state.brand_pilihan}
							keyExtractor={item => item.id}
							renderItem={({ item }) => (
								<TouchableOpacity onPress={() => this.props.navigation.push('BrandPilihan')} style={{ width: wp('49%'), alignItems: 'center', backgroundColor: item.color, marginRight: wp('2%'), marginBottom: wp('2%') }}>
									<View style={{ width: wp('47%'), flexDirection: 'row', marginTop: wp('1%'), marginBottom: wp('1%') }}>
										<View style={{ width: wp('23.5%') }}>
											<View style={{ width: xs(35), height: xs(35), backgroundColor: light, borderRadius: xs(35), alignItems: 'center', justifyContent: 'center' }}>
												<FastImage source={item.logo} style={{ width: xs(25), height: xs(25) }} />
											</View>
											<Text style={[ c.light, { fontFamily: 'Roboto-Bold', marginTop: wp('2%') }]}>{item.deskripsi}</Text>
										</View>
										<View style={{ width: wp('23.5%'), alignItems: 'flex-end' }}>
											<FastImage source={item.image} style={{ width: xs(55), height: xs(55) }} />
										</View>
									</View>
								</TouchableOpacity>
							)}
						/>
					</View>

					<View style={[ b.mt4, { flexDirection: 'row', alignItems: 'center' }]}>
						<View style={{ width: wp('45%') }}>
							<Text style={[ c.light, { fontFamily: 'Roboto-Bold' }]}>Produk Populer</Text>
						</View>
						<View style={{ width: wp('45%'), alignItems: 'flex-end' }}>
							<TouchableOpacity onPress={() => this.props.navigation.push('AllProductMall')}>
								<Text style={[ c.blue, { fontFamily: 'Roboto-Regular' }]}>Lihat Semua</Text>
							</TouchableOpacity>
						</View>
					</View>
					<View style={[ b.mt3, { width: wp('90%') }]}>
						<FlatList
              horizontal={true}
              showsHorizontalScrollIndicator={false}
              data={this.state.list_recomendation}
              keyExtractor={item => item.id}
              renderItem={({ item }) => (
              	<TouchableOpacity onPress={() => this.props.navigation.push('ProductDetailMall', {id_product: item.id})} style={[ b.mr2, { width: xs(120), borderColor: blue, borderWidth: 1, borderRadius: xs(5) }]}>
        					<FastImage source={{ uri: API_URL+'/public/product/'+item.gambar.split(",",1) }} style={{ width: '100%', height: xs(125), borderTopLeftRadius: xs(5), borderTopRightRadius: xs(5) }} />
        					<View style={{ width: '100%', height: xs(55) }}>
	        					<Text style={[ c.light, b.mt1, b.ml1, f._12, { fontFamily: 'Roboto-Regular' }]} numberOfLines={2}>{item.nama}</Text>
	        					<Text style={[ c.light, b.ml1, f._12, { fontFamily: 'Roboto-Regular' }]}>{this.format(item.harga)}</Text>
        					</View>
        					<View style={[ p.row, b.ml1, b.mt1, b.mb2 ]}>
        						<Image source={require('../asset/tokolokasi_icon.png')} style={{ width: xs(15), height: xs(15) }} />
        						<Text style={[ b.ml1, c.light, f._12, { fontFamily: 'Roboto-Regular' }]}>Jakarta</Text>
        					</View>
        					
        				</TouchableOpacity>
              )}
            />
					</View>

					<View style={[ b.mt4, { width: wp('90%') }]}>
						<Text style={[ c.light, { fontFamily: 'Roboto-Bold' }]}>Rekomendasi</Text>
					</View>
					<View style={[ b.mt3, { width: wp('90%') }]}>
						<FlatList
                numColumns={2}
                data={this.state.list_recomendation}
                keyExtractor={item => item.id}
                renderItem={({ item }) => (
                  <TouchableOpacity onPress={() => this.props.navigation.push('ProductDetailMall', {id_product: item.id})} style={[ b.roundedLow, b.mr2, b.mb2, { width: xs(151), borderWidth: 1, borderColor: blue, backgroundColor: '#121212' }]}>
                    <FastImage source={{ uri: API_URL+'/public/product/'+item.gambar.split(",",1) }} style={{ width: '100%', height: xs(150), borderTopRightRadius: xs(5), borderTopLeftRadius: xs(5) }} />

                    <Text style={[ c.light, b.ml1, { fontFamily: 'Roboto-Regular', marginTop: xs(3) }]} numberOfLines={2}>{item.nama}</Text>
                    {item.diskon !== undefined ?
                    	<>
                        <Text style={[ c.light, b.ml1, { fontFamily: 'Roboto-Bold' }]}>{this.format(item.harga_diskon)}</Text>
                        <View style={[ b.mb1, b.mt1, b.ml1, { flexDirection: 'row', alignItems: 'center' }]}>
                            <View style={{ backgroundColor: '#FF8B12', borderRadius: xs(3) }}>
                                <Text style={[ c.light, f._10, { fontFamily: 'Roboto-Bold', marginTop: xs(1), marginBottom: xs(1), marginLeft: xs(2), marginRight: xs(2) }]}>{item.diskon}</Text>
                            </View>
                            <Text style={[ f._10, { fontFamily: 'Roboto-Regular', color: '#CCC', textDecorationLine: 'line-through', marginLeft: xs(3) }]}>{this.format(item.harga_awal)}</Text>
                        </View>
                    	</>
                    	:
                    	<Text style={[ c.light, b.ml1, b.mb1, { fontFamily: 'Roboto-Bold' }]}>{this.format(item.harga)}</Text>
                    }
                    <View style={[ b.mb1, b.ml1, { flexDirection: 'row', alignItems: 'center' }]}>
                        <FastImage source={require('../asset/tokolokasi_icon.png')} style={{ width: xs(20), height: xs(17) }} />
                        <Text style={[ f._12, { fontFamily: 'Roboto-Regular', color: '#CCC', marginLeft: xs(3) }]}>Jakarta</Text>
                    </View>
                    <View style={[ b.mb2, b.ml1, { flexDirection: 'row', alignItems: 'center' }]}>
                        <Image source={require('../asset/star.png')} style={{ width: xs(15), height: xs(15), tintColor: 'darkorange' }} />
                        <Text style={[ f._12, { fontFamily: 'Roboto-Regular', color: 'darkorange', marginLeft: xs(3) }]}>{item.ratings.length}.0</Text>
                        <View style={{ width: xs(12), borderBottomWidth: 1, borderBottomColor: 'darkorange', transform: [{ rotate: '90deg' }] }} />
                        {item.ratings.length === 0 ?
                          <Text style={[ f._12, { fontFamily: 'Roboto-Regular', color: 'darkorange' }]}>0 Ulasan</Text>
                          :
                          <Text style={[ f._12, { fontFamily: 'Roboto-Regular', color: 'darkorange' }]}>{item.ulasan} Ulasan</Text>
                        }
                    </View>
                  </TouchableOpacity>
                )}
            />
					</View>
				</View>
			</ScrollView>
		)
	}
}