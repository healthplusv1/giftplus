import React, { Component } from 'react'
import { View, Text, Image, ScrollView, TouchableOpacity, TextInput, RefreshControl, FlatList } from 'react-native'
import { blue, light, softGrey } from '../utils/Color'
import { xs, vs } from '../utils/Responsive'
import { f, c, b, p } from '../utils/StyleHelper'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
import FastImage from 'react-native-fast-image'
import LinearGradient from 'react-native-linear-gradient'
import AsyncStorage from '@react-native-async-storage/async-storage'
import axios from 'axios'
import { API_LOCAL, API_URL } from '@env'

export default class BrandPilihan extends Component {
	constructor(props) {
		super(props);
		this.state = {
			loading: false,
			count: '',
			search: '',
			produk_diskon: [
				{id: 1, image: require('../asset/canon.png'), harga: '15000000', diskon: '50%', harga_diskon: '7500000'},
				{id: 2, image: require('../asset/canon.png'), harga: '15000000', diskon: '50%', harga_diskon: '7500000'},
				{id: 3, image: require('../asset/canon.png'), harga: '15000000', diskon: '50%', harga_diskon: '7500000'},
			],
			produk_diskon_xiaomi: [
				{id: 1, image: require('../asset/poco.png'), harga: '5000000', diskon: '10%', harga_diskon: '4500000'},
				{id: 2, image: require('../asset/poco.png'), harga: '5000000', diskon: '10%', harga_diskon: '4500000'},
				{id: 3, image: require('../asset/poco.png'), harga: '5000000', diskon: '10%', harga_diskon: '4500000'},
			],
			all_products: [
				{id: 1, nama: 'Camera Canon Rebel T81', image: require('../asset/canon.png'), harga: '15000000', lokasi: 'Semarang', rating: '5', ulasan: '10'},
				{id: 2, nama: 'Camera Canon Rebel T81', image: require('../asset/canon.png'), harga: '15000000', lokasi: 'Semarang', rating: '5', ulasan: '10'},
				{id: 3, nama: 'Camera Canon Rebel T81', image: require('../asset/canon.png'), harga: '15000000', lokasi: 'Semarang', rating: '5', ulasan: '10'},
				{id: 4, nama: 'Camera Canon Rebel T81', image: require('../asset/canon.png'), harga: '15000000', lokasi: 'Semarang', rating: '5', ulasan: '10'}
			],
			all_products_xiaomi: [
				{id: 1, nama: 'Redmi Note 8', image: require('../asset/redmiNote8.jpg'), harga: '2500000', lokasi: 'Semarang', rating: '5', ulasan: '10'},
				{id: 2, nama: 'Redmi Note 8', image: require('../asset/redmiNote8.jpg'), harga: '2500000', lokasi: 'Semarang', rating: '5', ulasan: '10'},
				{id: 3, nama: 'Redmi Note 8', image: require('../asset/redmiNote8.jpg'), harga: '2500000', lokasi: 'Semarang', rating: '5', ulasan: '10'},
				{id: 4, nama: 'Redmi Note 8', image: require('../asset/redmiNote8.jpg'), harga: '2500000', lokasi: 'Semarang', rating: '5', ulasan: '10'}
			],
			kategori_pilihan: [
				{id: 1, name: 'Redmi', image: require('../asset/redmiNote8.jpg'), width: '100%', height: xs(100)},
				{id: 2, name: 'Poco', image: require('../asset/poco.png'), width: '100%', height: xs(100)},
				{id: 3, name: 'Aksesoris', image: require('../asset/handsfree.png'), width: xs(100), height: xs(100)},
			],
			kupon: [
        		{id: 1, background: require('../asset/kupon_mock.png'), diskon: '10%OFF', des: 'Min. Belanja Rp 100K', tgl_berlaku: '31 Jul - 31 Agu, 2021'},
        		{id: 2, background: require('../asset/kupon_mock.png'), diskon: '10%OFF', des: 'Min. Belanja Rp 100K', tgl_berlaku: '31 Jul - 31 Agu, 2021'}
        	],
			brand_name: ''
		}
	}

	async componentDidMount() {
		this.setState({ loading: true, brand_name: this.props.navigation.getParam('nama_brand') })
		
		const id_login = await AsyncStorage.getItem('@id_login')

		if (id_login === null) {
			const guest = await AsyncStorage.getItem('id_login_guest')

			axios.get(API_URL+'/main/shopping/cart', {params: {
				id_login: guest
			}})
			.then(res => {
				this.setState({
					count: res.data.data.length,
					loading: false
				})
			})
		} else {
			axios.get(API_URL+'/main/shopping/cart', {params: {
				id_login: id_login
			}})
			.then(res => {
				this.setState({
					count: res.data.data.length,
					loading: false
				})
			})
		}
	}

	format(number) {
	    if (number) {
	      var rupiah = "";
	      var numberrev = number
	        .toString()
	        .split("")
	        .reverse()
	        .join("");
	      for (var i = 0; i < numberrev.length; i++)
	        if (i % 3 == 0) rupiah += numberrev.substr(i, 3) + ".";
	      return (
	        "Rp " +
	        rupiah
	          .split("", rupiah.length - 1)
	          .reverse()
	          .join("")
	      );
	    } else {
	        return (
	            "Rp "+number
	        );
	    }
	}

	render() {
		return (
			<ScrollView showsVerticalScrollIndicator={false} refreshControl={<RefreshControl onRefresh={() => this.componentDidMount()} refreshing={this.state.loading} />}>
				<LinearGradient start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }} colors={[ '#181C21', '#252F38', '#252F38' ]} style={{ width: '100%' }}>
					<View style={{ width: '100%', alignItems: 'center', backgroundColor: '#121212' }}>
						<View style={[ b.mt4, b.mb4, { width: wp('90%'), flexDirection: 'row', alignItems: 'center' }]}>
							<View style={{ width: wp('45%') }}>
		        			<FastImage source={require('../asset/giftplus.png')} style={{ width: xs(120), height: xs(32) }} />
		        		</View>
		        		<View style={{ width: wp('45%'), flexDirection: 'row-reverse' }}>
		        			<TouchableOpacity onPress={() => this.props.navigation.push('Drawer')}>
		        				<Image source={require('../asset/menu.png')} style={{ width: xs(22), height: xs(22), tintColor: light }} />
		        			</TouchableOpacity>
		        			<TouchableOpacity onPress={() => this.props.navigation.push('Notification')} style={[ b.mr2 ]}>
		        				<Image source={require('../asset/notification.png')} style={{ width: xs(22), height: xs(22), tintColor: light }} />
		        			</TouchableOpacity>
		        			<TouchableOpacity onPress={() => this.props.navigation.push('Cart')} style={[ b.mr2 ]}>
		        				<Image source={require('../asset/cart.png')} style={{ width: xs(19), height: xs(20), tintColor: light }} />
								{this.state.count !== 0 && (
									<View style={{ width: xs(12), height: xs(12), borderRadius: xs(12), backgroundColor: 'darkorange', position: 'absolute', right: 0, alignItems: 'center', justifyContent: 'center' }}>
					                    <Text style={[ c.light, f._10 ]}>{this.state.count}</Text>
					                </View>
								)}
		        			</TouchableOpacity>
		        		</View>
						</View>
					</View>

					<View style={{ width: '100%', alignItems: 'center' }}>
						<View style={[ b.mt3, { width: wp('90%'), height: hp('6.5%'), borderRadius: wp('5%'), backgroundColor: softGrey, flexDirection: 'row', alignItems: 'center' }]}>
							<TextInput
								style={[ c.light, { width: wp('62%'), height: '100%', marginLeft: wp('4%'), fontFamily: 'Roboto-Regular' }]}
								ref={ref => this.textInputRef = ref}
								placeholder='Apa yang kamu cari'
								placeholderTextColor={light}
								returnKeyType='search'
								onChangeText={(text) => this.setState({ search: text })}
								onSubmitEditing={() => this.nextScreen()}
								value={this.state.search}
							/>
							<View style={{ width: wp('20%'), flexDirection: 'row-reverse' }}>
								{this.state.search === '' ?
									<TouchableOpacity onPress={() => this.textInputRef.focus()}>
										<Image source={require('../asset/search.png')} style={{ width: wp('6%'), height: wp('6%'), tintColor: light, marginLeft: wp('4%') }} />
									</TouchableOpacity>
									:
									<TouchableOpacity onPress={() => this.setState({ search: '' })} style={{ marginLeft: wp('4%') }}>
										<Image source={require('../asset/cross.png')} style={{ width: wp('6%'), height: wp('6%'), tintColor: light }} />
									</TouchableOpacity>
								}
								{/* <TouchableOpacity>
									<Image source={require('../asset/camera.png')} style={{ width: wp('6%'), height: wp('6%'), tintColor: light }} />
								</TouchableOpacity> */}
							</View>
						</View>

						{this.state.brand_name !== 'xiaomi' ?
							<>
								<FastImage source={require('../asset/bannerCamera.jpg')} style={[ b.mt4, { width: '100%', height: xs(180) }]} />
								<View style={{ flexDirection: 'row' }}>
									<TouchableOpacity style={{ width: wp('50%'), height: xs(40), borderWidth: 1, borderColor: blue, flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
										<Image source={require('../asset/phone.png')} style={{ width: xs(20), height: xs(20), tintColor: blue }} />
										<Text style={[ b.ml2, c.blue, { fontFamily: 'Roboto-Regular' }]}>Call</Text>
									</TouchableOpacity>
									<TouchableOpacity style={{ width: wp('50%'), height: xs(40), borderWidth: 1, borderColor: blue, flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
										<Image source={require('../asset/chatBubble.png')} style={{ width: xs(20), height: xs(20), transform: [{ rotateY: '180deg' }] }} />
										<Text style={[ b.ml2, c.blue, { fontFamily: 'Roboto-Regular' }]}>Chat</Text>
									</TouchableOpacity>
								</View>

								<View style={[ b.mt4, { width: wp('90%'), flexDirection: 'row', alignItems: 'center' }]}>
									<View style={{ width: wp('45%') }}>
										<Text style={[ c.light, { fontFamily: 'Roboto-Bold' }]}>Produk Diskon</Text>
									</View>
									<View style={{ width: wp('45%'), alignItems: 'flex-end' }}>
										<TouchableOpacity>
											<Text style={[ c.blue, { fontFamily: 'Roboto-Regular' }]}>Lihat Semua</Text>
										</TouchableOpacity>
									</View>
								</View>
								<View style={[ b.mt4, { width: wp('90%') }]}>
									<FlatList
										horizontal={true}
										data={this.state.produk_diskon}
										keyExtractor={item => item.id}
										renderItem={({ item }) => (
											<TouchableOpacity style={[ b.roundedLow, b.mr2, { width: wp('35%'), borderWidth: 1, borderColor: blue, backgroundColor: '#121212', alignItems: 'center' }]}>
								                <View style={{ width: '100%', alignItems: 'center', justifyContent: 'center', backgroundColor: light, borderTopRightRadius: xs(5), borderTopLeftRadius: xs(5) }}>
									                <FastImage source={item.image} style={[ b.mt2, b.mb2, { width: '100%', height: xs(100) }]} />
								                </View>

								                {item.diskon !== undefined ?
								                  	<>
										                <Text style={[ c.light, { fontFamily: 'Roboto-Bold', marginTop: xs(3) }]}>{this.format(item.harga_diskon)}</Text>
										                <View style={[ b.mb2, b.mt1, { flexDirection: 'row', alignItems: 'center' }]}>
										                    <View style={{ backgroundColor: '#FF8B12', borderRadius: xs(3) }}>
										                    	<Text style={[ c.light, f._10, { fontFamily: 'Roboto-Bold', marginTop: xs(1), marginBottom: xs(1), marginLeft: xs(2), marginRight: xs(2) }]}>{item.diskon}</Text>
										                    </View>
										                    <Text style={[ f._10, { fontFamily: 'Roboto-Regular', color: '#CCC', textDecorationLine: 'line-through', marginLeft: xs(3) }]}>{this.format(item.harga)}</Text>
										                </View>
								                  	</>
								                  	:
								                  	<Text style={[ c.light, b.mb2, { fontFamily: 'Roboto-Bold', marginTop: xs(3) }]}>{this.format(item.harga)}</Text>
								                }
							                </TouchableOpacity>
										)}
									/>
								</View>

								<View style={[ b.mt4, { width: wp('90%'), flexDirection: 'row', alignItems: 'center' }]}>
									<Text style={[ c.light, { fontFamily: 'Roboto-Bold' }]}>Semua Produk</Text>
								</View>
								<View style={[ b.mt4, { width: wp('90%') }]}>
									<FlatList
						                numColumns={2}
						                data={this.state.all_products}
						                keyExtractor={item => item.id}
						                renderItem={({ item }) => (
						                  <TouchableOpacity onPress={() => this.props.navigation.push('ProductDetailMall', {id_product: item.id})} style={[ b.roundedLow, b.mr2, b.mb2, { width: xs(151), borderWidth: 1, borderColor: blue, backgroundColor: '#121212' }]}>
						                    <View style={{ width: '100%', backgroundColor: light, alignItems: 'center', borderTopRightRadius: xs(5), borderTopLeftRadius: xs(5) }}>
							                    <FastImage source={item.image} style={[ b.mt2, b.mb2, { width: '100%', height: xs(100) }]} />
						                    </View>

						                    <Text style={[ c.light, b.ml1, { fontFamily: 'Roboto-Regular', marginTop: xs(3) }]} numberOfLines={2}>{item.nama}</Text>
						                    {item.diskon !== undefined ?
						                    	<>
						                        <Text style={[ c.light, b.ml1, { fontFamily: 'Roboto-Bold' }]}>{this.format(item.harga_diskon)}</Text>
						                        <View style={[ b.mb1, b.mt1, b.ml1, { flexDirection: 'row', alignItems: 'center' }]}>
						                            <View style={{ backgroundColor: '#FF8B12', borderRadius: xs(3) }}>
						                                <Text style={[ c.light, f._10, { fontFamily: 'Roboto-Bold', marginTop: xs(1), marginBottom: xs(1), marginLeft: xs(2), marginRight: xs(2) }]}>{item.diskon}</Text>
						                            </View>
						                            <Text style={[ f._10, { fontFamily: 'Roboto-Regular', color: '#CCC', textDecorationLine: 'line-through', marginLeft: xs(3) }]}>{this.format(item.harga_awal)}</Text>
						                        </View>
						                    	</>
						                    	:
						                    	<Text style={[ c.light, b.ml1, b.mb1, { fontFamily: 'Roboto-Bold' }]}>{this.format(item.harga)}</Text>
						                    }
						                    <View style={[ b.mb1, b.ml1, { flexDirection: 'row', alignItems: 'center' }]}>
						                        <FastImage source={require('../asset/tokolokasi_icon.png')} style={{ width: xs(20), height: xs(17) }} />
						                        <Text style={[ f._12, { fontFamily: 'Roboto-Regular', color: '#CCC', marginLeft: xs(3) }]}>Jakarta</Text>
						                    </View>
						                    <View style={[ b.mb2, b.ml1, { flexDirection: 'row', alignItems: 'center' }]}>
						                        <Image source={require('../asset/star.png')} style={{ width: xs(15), height: xs(15), tintColor: 'darkorange' }} />
						                        <Text style={[ f._12, { fontFamily: 'Roboto-Regular', color: 'darkorange', marginLeft: xs(3) }]}>{item.rating}.0</Text>
						                        <View style={{ width: xs(12), borderBottomWidth: 1, borderBottomColor: 'darkorange', transform: [{ rotate: '90deg' }] }} />
						                        {item.rating.length === 0 ?
						                          <Text style={[ f._12, { fontFamily: 'Roboto-Regular', color: 'darkorange' }]}>0 Ulasan</Text>
						                          :
						                          <Text style={[ f._12, { fontFamily: 'Roboto-Regular', color: 'darkorange' }]}>{item.ulasan} Ulasan</Text>
						                        }
						                    </View>
						                  </TouchableOpacity>
						                )}
						            />
								</View>
							</>
							:
							<>
								<FastImage source={require('../asset/supersale.png')} style={[ b.mt4, { width: '100%', height: xs(120) }]} />
								<View style={[ b.mt3, b.mb4, { width: wp('90%'), flexDirection: 'row' }]}>
									<View style={{ width: wp('20%') }}>
										<FastImage source={require('../asset/xiaomi_logo.png')} style={{ width: wp('18%'), height: wp('18%'), borderRadius: wp('18%') }} />
									</View>
									<View style={{ width: wp('55%') }}>
										<Text style={[ c.light, f._16, { fontFamily: 'Roboto-Bold' }]}>Xiaomi Store</Text>
										<Text style={[ c.light, { fontFamily: 'Roboto-Regular' }]}>Ruko Mataram Semarang</Text>
									</View>
									<View style={{ width: wp('15%'), flexDirection: 'row-reverse' }}>
										<TouchableOpacity>
											<Image source={require('../asset/chatBubble.png')} style={{ width: xs(20), height: xs(20), transform: [{ rotateY: '180deg' }] }} />
										</TouchableOpacity>
										<TouchableOpacity>
											<Image source={require('../asset/phone.png')} style={[ b.mr2, { width: xs(18), height: xs(18), tintColor: blue }]} />
										</TouchableOpacity>
									</View>
								</View>

								<View style={{ width: '100%', height: hp('2%'), backgroundColor: '#121212' }} />

								<View style={[ b.mt4, { width: wp('90%'), flexDirection: 'row', alignItems: 'center' }]}>
									<View style={{ width: wp('45%') }}>
										<Text style={[ c.light, { fontFamily: 'Roboto-Bold' }]}>Produk diskon</Text>
									</View>
									<View style={{ width: wp('45%'), alignItems: 'flex-end' }}>
										<TouchableOpacity>
											<Text style={[ c.blue, { fontFamily: 'Roboto-Regular' }]}>Lihat Semua</Text>
										</TouchableOpacity>
									</View>
								</View>
								<View style={[ b.mt2, { width: wp('90%') }]}>
									<FlatList
										horizontal={true}
										data={this.state.produk_diskon_xiaomi}
										keyExtractor={item => item.id}
										renderItem={({ item }) => (
											<TouchableOpacity style={[ b.roundedLow, b.mr2, { width: wp('35%'), borderWidth: 1, borderColor: blue, backgroundColor: '#121212', alignItems: 'center' }]}>
								                <View style={{ width: '100%', alignItems: 'center', justifyContent: 'center', backgroundColor: light, borderTopRightRadius: xs(5), borderTopLeftRadius: xs(5) }}>
									                <FastImage source={item.image} style={[ b.mt2, b.mb2, { width: '100%', height: xs(100) }]} />
								                </View>

								                {item.diskon !== undefined ?
								                  	<>
										                <Text style={[ c.light, { fontFamily: 'Roboto-Bold', marginTop: xs(3) }]}>{this.format(item.harga_diskon)}</Text>
										                <View style={[ b.mb2, b.mt1, { flexDirection: 'row', alignItems: 'center' }]}>
										                    <View style={{ backgroundColor: '#FF8B12', borderRadius: xs(3) }}>
										                    	<Text style={[ c.light, f._10, { fontFamily: 'Roboto-Bold', marginTop: xs(1), marginBottom: xs(1), marginLeft: xs(2), marginRight: xs(2) }]}>{item.diskon}</Text>
										                    </View>
										                    <Text style={[ f._10, { fontFamily: 'Roboto-Regular', color: '#CCC', textDecorationLine: 'line-through', marginLeft: xs(3) }]}>{this.format(item.harga)}</Text>
										                </View>
								                  	</>
								                  	:
								                  	<Text style={[ c.light, b.mb2, { fontFamily: 'Roboto-Bold', marginTop: xs(3) }]}>{this.format(item.harga)}</Text>
								                }
							                </TouchableOpacity>
										)}
									/>
								</View>

								<View style={[ b.mt4, { width: wp('90%'), flexDirection: 'row', alignItems: 'center' }]}>
									<Text style={[ c.light, { fontFamily: 'Roboto-Bold' }]}>Promo Diskon hari ini</Text>
								</View>
								<View style={[ b.mt2, { width: wp('90%') }]}>
									<FlatList
					            		horizontal={true}
					            		showsHorizontalScrollIndicator={false}
					            		data={this.state.kupon}
					            		keyExtractor={item => item.id}
					            		renderItem={({ item }) => (
					            			<FastImage source={item.background} style={[ b.mr2, { width: wp('43%'), height: xs(72), flexDirection: 'row' }]}>
					            				<View style={{ width: xs(44), alignItems: 'center', justifyContent: 'center' }}>
					            					<View style={{ width: xs(30), height: xs(30), borderRadius: xs(30), backgroundColor: light, alignItems: 'center', justifyContent: 'center' }}>
					            						<Image source={require('../asset/icongabungjadiseller.png')} style={{ width: xs(15), height: xs(15), tintColor: blue }} />
					            					</View>
				            						<Text style={[ c.light, f._12, { fontFamily: 'Roboto-Regular', marginTop: xs(2) }]}>Toko</Text>
					            				</View>

					            				<View style={{ width: xs(104), alignItems: 'center', justifyContent: 'center', marginLeft: xs(2) }}>
					            					<Text style={[ c.blue, f._16, { fontFamily: 'Roboto-Bold' }]}>{item.diskon}</Text>
					            					<Text style={[ c.blue, f._10, { fontFamily: 'Roboto-Bold', marginTop: xs(-1) }]}>{item.des}</Text>
					            					<Text style={[ c.blue, f._10, { fontFamily: 'Roboto-Bold', marginTop: xs(-2) }]}>{item.tgl_berlaku}</Text>

					            					<TouchableOpacity style={{ width: xs(55), height: xs(22), backgroundColor: blue, borderRadius: xs(50), alignItems: 'center', justifyContent: 'center', marginTop: xs(1), marginBottom: xs(2) }}>
					            						<Text style={[ c.light, f._12, { fontFamily: 'Roboto-Regular' }]}>Ambil</Text>
					            					</TouchableOpacity>
					            				</View>
					            			</FastImage>
					            		)}
					            	/>
					            </View>

								<View style={[ b.mt4, { width: wp('90%'), flexDirection: 'row', alignItems: 'center' }]}>
									<Text style={[ c.light, { fontFamily: 'Roboto-Bold' }]}>Kategori Pilihan</Text>
								</View>
								<View style={[ b.mt2, { width: wp('90%') }]}>
									<FlatList
										horizontal={true}
										data={this.state.kategori_pilihan}
										keyExtractor={item => item.id}
										renderItem={({ item }) => (
											<TouchableOpacity style={[ b.roundedLow, b.mr2, { width: wp('35%'), alignItems: 'center' }]}>
								                <View style={{ width: '100%', alignItems: 'center', justifyContent: 'center', backgroundColor: light, borderRadius: xs(5) }}>
									                <FastImage source={item.image} style={[ b.mt2, b.mb2, { width: item.width, height: item.height }]} />
								                </View>
								                <Text style={[ b.mt1, c.light, { fontFamily: 'Roboto-Regular' }]}>{item.name}</Text>
							                </TouchableOpacity>
										)}
									/>
								</View>

								<View style={[ b.mt4, { width: wp('90%'), flexDirection: 'row', alignItems: 'center' }]}>
									<Text style={[ c.light, { fontFamily: 'Roboto-Bold' }]}>Semua Produk</Text>
								</View>
								<View style={[ b.mt2, { width: wp('90%') }]}>
									<FlatList
						                numColumns={2}
						                data={this.state.all_products_xiaomi}
						                keyExtractor={item => item.id}
						                renderItem={({ item }) => (
						                  <TouchableOpacity onPress={() => this.props.navigation.push('ProductDetailMall', {id_product: item.id})} style={[ b.roundedLow, b.mr2, b.mb2, { width: xs(151), borderWidth: 1, borderColor: blue, backgroundColor: '#121212' }]}>
						                    <View style={{ width: '100%', backgroundColor: light, alignItems: 'center', borderTopRightRadius: xs(5), borderTopLeftRadius: xs(5) }}>
							                    <FastImage source={item.image} style={[ b.mt2, b.mb2, { width: '100%', height: xs(120) }]} />
						                    </View>

						                    <Text style={[ c.light, b.ml1, { fontFamily: 'Roboto-Regular', marginTop: xs(3) }]} numberOfLines={2}>{item.nama}</Text>
						                    {item.diskon !== undefined ?
						                    	<>
						                        <Text style={[ c.light, b.ml1, { fontFamily: 'Roboto-Bold' }]}>{this.format(item.harga_diskon)}</Text>
						                        <View style={[ b.mb1, b.mt1, b.ml1, { flexDirection: 'row', alignItems: 'center' }]}>
						                            <View style={{ backgroundColor: '#FF8B12', borderRadius: xs(3) }}>
						                                <Text style={[ c.light, f._10, { fontFamily: 'Roboto-Bold', marginTop: xs(1), marginBottom: xs(1), marginLeft: xs(2), marginRight: xs(2) }]}>{item.diskon}</Text>
						                            </View>
						                            <Text style={[ f._10, { fontFamily: 'Roboto-Regular', color: '#CCC', textDecorationLine: 'line-through', marginLeft: xs(3) }]}>{this.format(item.harga_awal)}</Text>
						                        </View>
						                    	</>
						                    	:
						                    	<Text style={[ c.light, b.ml1, b.mb1, { fontFamily: 'Roboto-Bold' }]}>{this.format(item.harga)}</Text>
						                    }
						                    <View style={[ b.mb1, b.ml1, { flexDirection: 'row', alignItems: 'center' }]}>
						                        <FastImage source={require('../asset/tokolokasi_icon.png')} style={{ width: xs(20), height: xs(17) }} />
						                        <Text style={[ f._12, { fontFamily: 'Roboto-Regular', color: '#CCC', marginLeft: xs(3) }]}>Jakarta</Text>
						                    </View>
						                    <View style={[ b.mb2, b.ml1, { flexDirection: 'row', alignItems: 'center' }]}>
						                        <Image source={require('../asset/star.png')} style={{ width: xs(15), height: xs(15), tintColor: 'darkorange' }} />
						                        <Text style={[ f._12, { fontFamily: 'Roboto-Regular', color: 'darkorange', marginLeft: xs(3) }]}>{item.rating}.0</Text>
						                        <View style={{ width: xs(12), borderBottomWidth: 1, borderBottomColor: 'darkorange', transform: [{ rotate: '90deg' }] }} />
						                        {item.rating.length === 0 ?
						                          <Text style={[ f._12, { fontFamily: 'Roboto-Regular', color: 'darkorange' }]}>0 Ulasan</Text>
						                          :
						                          <Text style={[ f._12, { fontFamily: 'Roboto-Regular', color: 'darkorange' }]}>{item.ulasan} Ulasan</Text>
						                        }
						                    </View>
						                  </TouchableOpacity>
						                )}
						            />
								</View>
							</>
						}
					</View>
				</LinearGradient>
			</ScrollView>
		)
	}
}