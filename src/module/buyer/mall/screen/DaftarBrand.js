import React, { Component } from 'react'
import { View, Text, Image, ScrollView, TouchableOpacity, TextInput, RefreshControl, FlatList } from 'react-native'
import { blue, light, softGrey } from '../utils/Color'
import { xs, vs } from '../utils/Responsive'
import { f, c, b, p } from '../utils/StyleHelper'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
import FastImage from 'react-native-fast-image'
import LinearGradient from 'react-native-linear-gradient'
import AsyncStorage from '@react-native-async-storage/async-storage'
import axios from 'axios'
import { API_LOCAL, API_URL } from '@env'

export default class DaftarBrand extends Component {
	constructor(props) {
		super(props);
		this.state = {
			loading: 'false',
			search: '',
			brand_populer: [
				{id: 1, image: require('../asset/ace.png')},
				{id: 2, image: require('../asset/acer.png')},
				{id: 3, image: require('../asset/lg.png')},
				{id: 4, image: require('../asset/lenovo.png')},
				{id: 5, image: require('../asset/panasonic.png')},
			],
			all_brand: [
				{id: 1, image: require('../asset/samsung2.png'), background: '#1428A0'},
				{id: 2, image: require('../asset/realme2.png'), background: '#FFC915'},
				{id: 3, image: require('../asset/vivo2.png'), background: '#415FFF'},
				{id: 4, image: require('../asset/nokia2.png'), background: '#004B99'},
				{id: 5, image: require('../asset/oppo2.png'), background: '#00A068'},
				{id: 6, image: require('../asset/sony2.png'), background: '#FFF'},
			]
		}
	}

	render() {
		return (
			<View style={{ width: '100%', height: '100%', backgroundColor: '#121212' }}>
				<View style={{ width: '100%', alignItems: 'center' }}>
					<View style={[ b.mt4, b.mb4, { width: wp('90%'), flexDirection: 'row', alignItems: 'center' }]}>
						<View style={{ width: wp('15%') }}>
							<TouchableOpacity onPress={() => this.props.navigation.pop()}>
								<FastImage source={require('../asset/left.png')} style={{ width: xs(25), height: xs(25) }} />
							</TouchableOpacity>
						</View>
						<View style={{ width: wp('60%'), alignItems: 'center' }}>
							<Text style={[ c.light, f._16, { fontFamily: 'Roboto-Bold' }]}>Daftar Brand</Text>
						</View>
						<View style={{ width: wp('15%') }}>
						</View>
					</View>
				</View>

				<ScrollView showsVerticalScrollIndicator={false} style={{ backgroundColor: '#181B22' }}>
					<LinearGradient start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }} colors={[ '#181C21', '#252F38', '#252F38' ]} style={{ width: '100%', alignItems: 'center' }}>
						<View style={[ b.mt3, { width: wp('90%'), height: hp('6.5%'), borderRadius: wp('5%'), backgroundColor: softGrey, flexDirection: 'row', alignItems: 'center' }]}>
							<TextInput
								style={[ c.light, { width: wp('62%'), height: '100%', marginLeft: wp('4%'), fontFamily: 'Roboto-Regular' }]}
								ref={ref => this.textInputRef = ref}
								placeholder='Cari nama brand'
								placeholderTextColor={light}
								returnKeyType='search'
								onChangeText={(text) => this.setState({ search: text })}
								// onSubmitEditing={() => this.nextScreen()}
								value={this.state.search}
							/>
							<View style={{ width: wp('20%'), flexDirection: 'row-reverse' }}>
								{this.state.search === '' ?
									<TouchableOpacity onPress={() => this.textInputRef.focus()}>
										<Image source={require('../asset/search.png')} style={{ width: wp('6%'), height: wp('6%'), tintColor: light, marginLeft: wp('4%') }} />
									</TouchableOpacity>
									:
									<TouchableOpacity onPress={() => this.setState({ search: '' })} style={{ marginLeft: wp('4%') }}>
										<Image source={require('../asset/cross.png')} style={{ width: wp('6%'), height: wp('6%'), tintColor: light }} />
									</TouchableOpacity>
								}
								{/* <TouchableOpacity>
									<Image source={require('../asset/camera.png')} style={{ width: wp('6%'), height: wp('6%'), tintColor: light }} />
								</TouchableOpacity> */}
							</View>
						</View>

						<View style={[ b.mt4, { width: wp('90%') }]}>
							<View style={{ flexDirection: 'row' }}>
								<View style={{ width: wp('60%'), height: hp('18%'), backgroundColor: '#FB6B0B', alignItems: 'center', justifyContent: 'center' }}>
									<FastImage source={require('../asset/xiaomi.png')} style={{ width: xs(200), height: xs(85) }} />
								</View>

								<View style={[ b.ml1 ]}>
									<View style={{ width: wp('29%'), height: hp('8.6%'), backgroundColor: '#004B99', alignItems: 'center', justifyContent: 'center' }}>
										<FastImage source={require('../asset/nokia.png')} style={{ width: xs(100), height: xs(50) }} />
									</View>
									<View style={[ b.mt1, { width: wp('29%'), height: hp('8.6%'), backgroundColor: '#00A068', alignItems: 'center', justifyContent: 'center' }]}>
										<FastImage source={require('../asset/oppo.png')} style={{ width: xs(100), height: xs(50) }} />
									</View>
								</View>
							</View>

							<View style={[ b.mt1, { flexDirection: 'row' }]}>
								<View style={[ b.mr1, { width: wp('21.5%'), height: hp('8.6%'), backgroundColor: '#1428A0', alignItems: 'center', justifyContent: 'center' }]}>
									<FastImage source={require('../asset/samsung.png')} style={{ width: xs(65), height: xs(40) }} />
								</View>
								<View style={[ b.mr1, { width: wp('21.5%'), height: hp('8.6%'), backgroundColor: '#FFC915', alignItems: 'center', justifyContent: 'center' }]}>
									<FastImage source={require('../asset/realme.png')} style={{ width: xs(65), height: xs(40) }} />
								</View>
								<View style={[ b.mr1, { width: wp('21.5%'), height: hp('8.6%'), backgroundColor: '#415FFF', alignItems: 'center', justifyContent: 'center' }]}>
									<FastImage source={require('../asset/vivo.png')} style={{ width: xs(65), height: xs(40) }} />
								</View>
								<View style={{ width: wp('21.5%'), height: hp('8.6%'), backgroundColor: light, alignItems: 'center', justifyContent: 'center' }}>
									<FastImage source={require('../asset/sony.png')} style={{ width: xs(65), height: xs(40) }} />
								</View>
							</View>

							<Text style={[ c.light, b.mt4, { fontFamily: 'Roboto-Bold' }]}>Brand populer</Text>
							<FlatList
								style={[ b.mt2 ]}
								horizontal={true}
								showsHorizontalScrollIndicator={false}
								data={this.state.brand_populer}
								keyExtractor={item => item.id}
								renderItem={({ item }) => (
									<FastImage source={item.image} style={[ b.mr1, { width: xs(65), height: xs(65) }]} />
								)}
							/>

							<FastImage source={require('../asset/supersale.png')} style={[ b.mt3, { width: '100%', height: xs(100) }]} />

							<Text style={[ c.light, b.mt4, { fontFamily: 'Roboto-Bold' }]}>Semua brand</Text>
							<FlatList
								style={[ b.mt2, b.mb4 ]}
								numColumns={3}
								data={this.state.all_brand}
								keyExtractor={item => item.id}
								renderItem={({ item }) => (
									<View style={[ b.mr1, b.mb1, { width: wp('29%'), height: hp('9%'), backgroundColor: item.background, alignItems: 'center', justifyContent: 'center' }]}>
										<FastImage source={item.image} style={{ width: xs(100), height: xs(50) }} />
									</View>
								)}
							/>
						</View>
					</LinearGradient>
				</ScrollView>
			</View>
		)
	}
}