import React, { Component } from 'react'
import { View, Text, TouchableOpacity, ScrollView, FlatList, Alert, ToastAndroid } from 'react-native'
import { blue, light } from '../utils/Color'
import { xs, vs } from '../utils/Responsive'
import { f, c, b, p } from '../utils/StyleHelper'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
import FastImage from 'react-native-fast-image'
import LinearGradient from 'react-native-linear-gradient'
import Dialog, { DialogContent } from 'react-native-popup-dialog'
import AsyncStorage from '@react-native-async-storage/async-storage'
import axios from 'axios'
import { API_LOCAL, API_URL } from '@env'

export default class PaymentWallet extends Component {
	constructor(props) {
		super(props);
		this.state = {
			loading: false,
			products: [],
			voucherCode: null,
			promo: ''
		}
	}

	async componentDidMount() {
		this.setState({ loading: true })

		const id_login = await AsyncStorage.getItem('@id_login')

		axios.get(API_URL+'/main/shopping/cart', {params: {
            id_login: id_login
        }})
        .then(res => {
            this.setState({
                loading: false,
                products: res.data.data
            })
        })
        .catch(err => {
            this.setState({
                loading: false
            })
        })
	}

	async selectingPayment() {
        const id_login = await AsyncStorage.getItem('@id_login')
        axios.get(API_URL+'/main/shopping/order/checkout', {params: {
            id_login: id_login,
            voucherCode: this.state.voucherCode
        }})
        .then(res => {
            console.log("SAVE ORDER LOG ==> ",res.data)
            if (res.data.status==='true') {
                if (res.data.status==='true') {
                    axios.post(API_URL+"/main/duitku/generate/"+res.data.order_id+"/WALLET/60",{params:{
                        amount: this.props.navigation.getParam('price')
                    }})
                    .then(res => {
                        console.log("PAYMENT LOG ==> ", res.data)
                        ToastAndroid.show('membayar menggunakan saldo berhasil', ToastAndroid.SHORT)
                        this.props.navigation.push('Gift')
                        // ToastAndroid.show('Halaman Selanjutnya (QR Code GiftCard) masih dalam proses pembangunan', ToastAndroid.LONG)
                    })
                }
            } else if (res.data.status==='keranjang belanja kosong') {
                Alert.alert('', 'Tidak ada barang dikeranjang belanja anda')
            } else if (res.data.status==='stok habis') {
                Alert.alert('', 'Salah satu produk dikeranjang telah kehabisan persediaan ')
            } else if (res.data.status==='voucher tidak ditemukan') {
                Alert.alert('', 'Voucher tidak ditemukan')
            } else if (res.data.status==='minimal pembelian tidak terpenuhi') {
                Alert.alert('', 'Minimal pembelian untuk menggunakan voucher ini belum terpenuhi')
            } else if (res.data.status==='voucher tidak valid') {
                Alert.alert('', 'Voucher sudah tidak berlaku')
            } else if (res.data.status==='voucher sudah pernah dipakai') {
                Alert.alert('', 'Voucher sudah pernah dipakai')
            } else if (res.data.status==='voucher telah mencapai batas pemakaian') {
                Alert.alert('', 'Voucher telah mencapai batas pemakaian')
            } else if (res.data.status==='produk tidak didukung') {
                Alert.alert('', 'Produk tidak didukung untuk menggunakan voucher ini')
            } else {
                Alert.alert('', 'Terjadi kesalahan, mohon coba kembali beberapa saat lagi')
            }
        });
    }

	format(number) {
        if (number) {
          var rupiah = "";
          var numberrev = number
            .toString()
            .split("")
            .reverse()
            .join("");
          for (var i = 0; i < numberrev.length; i++)
            if (i % 3 == 0) rupiah += numberrev.substr(i, 3) + ".";
          return (
            "Rp " +
            rupiah
              .split("", rupiah.length - 1)
              .reverse()
              .join("")
          );
        } else {
            return (
                "Rp "+number
            );
        }
    }

	render() {
		const { products } = this.state;
        let totalQuantity = 0;
        let totalPrice = 0;
        products.forEach((item) => {
            totalQuantity += item.jumlah;
            totalPrice += item.jumlah * item.harga/* - 10000*/;
        })
		return (
			<LinearGradient start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }} colors={[ '#181C21', '#252F38', '#252F38' ]} style={{ width: '100%', height: '100%' }}>
				<View style={{ width: '100%', alignItems: 'center', backgroundColor: '#121212' }}>
					<View style={[ b.mt4, b.mb4, { width: wp('90%'), flexDirection: 'row' }]}>
						<View style={{ width: wp('15%') }}>
							<TouchableOpacity onPress={() => this.props.navigation.pop()}>
								<FastImage source={require('../asset/left.png')} style={{ width: xs(25), height: xs(25) }} />
							</TouchableOpacity>
						</View>
						<View style={{ width: wp('60%'), alignItems: 'center' }}>
							<Text style={[ c.light, f._16, { fontFamily: 'Roboto-Bold' }]}>Pembayaran</Text>
						</View>
						<View style={{ width: wp('15%') }}>
						</View>
					</View>
				</View>

				<ScrollView showsVerticalScrollIndicator={false}>
					<View style={{ width: '100%', alignItems: 'center' }}>
						<View style={[ b.mt4, b.mb2, { width: wp('90%'), flexDirection: 'row', alignItems: 'center' }]}>
							<View style={{ width: wp('45%') }}>
								<Text style={[ c.light ]}>No Pesanan</Text>
							</View>
							<View style={{ width: wp('45%'), alignItems: 'flex-end' }}>
								<Text style={[ c.light ]}>123456789</Text>
							</View>
						</View>
						<View style={{ width: wp('90%'), borderBottomWidth: 1, borderBottomColor: light }} />
						<View style={[ b.mt2, b.mb4, { width: wp('90%'), flexDirection: 'row', alignItems: 'center' }]}>
							<View style={{ width: wp('45%') }}>
								<Text style={[ c.light ]}>Waktu</Text>
							</View>
							<View style={{ width: wp('45%'), alignItems: 'flex-end' }}>
								<Text style={[ c.light ]}>04/21/2021, 14:00 WIB</Text>
							</View>
						</View>

						<View style={{ width: '100%', height: hp('2%'), backgroundColor: '#121212' }} />

						<View style={{ width: '100%' }}>
							<FlatList
								style={[ b.mt2 ]}
								data={this.state.products}
								keyExtractor={item => item.id}
								renderItem={({ item }) => (
									<View style={{ width: '100%', alignItems: 'center' }}>
										<View style={{ width: wp('90%'), flexDirection: 'row' }}>
											<View style={{ width: wp('15%') }}>
												<FastImage source={{ uri: API_URL+'/public/product/'+item.gambar.split(",", 1) }} style={{ width: '100%', height: wp('15%') }} />
											</View>
											<View style={{ width: wp('50%') }}>
												<Text style={[ b.ml2, c.light, { fontFamily: 'Roboto-Bold' }]} numberOfLines={2}>{item.nama}</Text>
												{this.state.promo !== '' && (
													<Text style={[ b.ml2, b.mt1, { color: 'darkorange', fontFamily: 'Roboto-Regular' }]}>Promo</Text>
												)}
											</View>
											<View style={{ width: wp('25%'), alignItems: 'flex-end' }}>
												<Text style={[ c.light, { fontFamily: 'Roboto-Regular' }]}>x{item.jumlah}</Text>
												<Text style={[ c.light, { fontFamily: 'Roboto-Regular' }]} numberOfLines={1}>{this.format(totalPrice)}</Text>
												{this.state.promo !== '' && (
													<Text style={[ b.mt1, { color: 'darkorange', fontFamily: 'Roboto-Regular' }]}>-{this.format(10000)}</Text>
												)}
											</View>
										</View>
										<View style={[ b.mt1, b.mb2, { width: '100%', borderBottomWidth: 1, borderBottomColor: light }]} />
										<View style={{ width: wp('90%'), flexDirection: 'row', alignItems: 'center' }}>
											<View style={{ width: wp('30%') }}>
												<Text style={[ c.light ]}>{item.jumlah} produk</Text>
											</View>
											<View style={{ width: wp('60%'), alignItems: 'flex-end' }}>
												<Text style={[ c.light ]}>Total Pesanan: {this.format(totalPrice)}</Text>
											</View>
										</View>
										<View style={[ b.mt2, b.mb2, { width: wp('90%'), borderBottomWidth: 1, borderBottomColor: light }]} />
									</View>
								)}
							/>
						</View>
					</View>
				</ScrollView>

				<View style={{ width: '100%', position: 'relative', bottom: 0, alignItems: 'center', justifyContent: 'center' }}>
					<TouchableOpacity style={[ b.mt4, b.mb4, b.roundedLow, { width: wp('90%'), height: hp('7%'), backgroundColor: blue, alignItems: 'center', justifyContent: 'center' }]} onPress={() => this.selectingPayment()}>
						<Text style={[ c.light, f._16, { fontFamily: 'Roboto-Bold' }]}>Bayar</Text>
					</TouchableOpacity>
				</View>
			</LinearGradient>
		)
	}
}