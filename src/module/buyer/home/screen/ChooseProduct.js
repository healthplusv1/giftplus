import React, { Component } from 'react'
import { View, Text, Image, TouchableOpacity, ScrollView, FlatList, RefreshControl } from 'react-native'
import { blue, light } from '../utils/Color'
import { xs, vs } from '../utils/Responsive'
import { f, c, b, p } from '../utils/StyleHelper'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
import FastImage from 'react-native-fast-image'
import LinearGradient from 'react-native-linear-gradient'
import CheckBox from '@react-native-community/checkbox'
import SelectMultiple from 'react-native-select-multiple'
import AsyncStorage from '@react-native-async-storage/async-storage'
import axios from 'axios'
import { API_LOCAL, API_URL } from '@env'

export default class ChooseProduct extends Component {
	constructor(props) {
		super(props);
		this.state = {
			checked: false,
			selected_item: null,
			renderData: []
		}
	}

	async generateQRCode() {
        const id_login = await AsyncStorage.getItem('@id_login');

		const renderData = this.state.renderData;
		let order_item = {};
		for (let data of renderData) {
			if (data.selected===true) order_item[data.order_item_id]=data.quantity;
		}

		let params = {
            order_id: this.props.navigation.getParam('order_id'),
            seller_id: this.state.renderData[0].seller_id,
            order_item: order_item
        };
        console.log("PARAMS ==> ",params)
    	this.props.navigation.navigate('Gift', params);
    	// axios.post(API_URL+'/main/shopping/order/gift_cards', params)
     //    .then(res => {
     //    	console.log(res.data)

     //    	/* QR CODE SCAN RETURN QR DATA */
	    //     axios.get(API_URL+'/giftcard/'+res.data.data.qr_code, {params:{
	    //         id_login: id_login
	    //     }})
	    //     .then(res => {
	    //         return console.log("scan QR CODE url ==> ", res.data)
	    //     })
     //    })
	}

	async componentDidMount() {
        const id_login = await AsyncStorage.getItem('@id_login');
        console.log('current order_id ==> ', this.props.navigation.getParam('order_id'))

        /* GET ORDER ITEM TO CHOOSE THE PRODUCT */
        axios.get(API_URL+'/main/shopping/order/get_order_items/'+this.props.navigation.getParam('order_id'), {params:{
            id_login: id_login
        }})
        .then(res => {
        	console.log("get_order_items", res.data.data)
        	this.setState({renderData: res.data.data})
            // return console.log("testing QR CODE ==> ", res.data.data)
	        /* GENERATE THE QR CODE OF CHOOSED PRODUCTS */
	        // let order_item = {};
	        // order_item['197']='1';
	        // let params = {
	        //     id_login: id_login,
	        //     order_id: '188',
	        //     seller_id: '93',
	        //     order_item: order_item,
	        //     // card_image: 
	        // };

	        // axios.post(API_URL+'/main/shopping/order/gift_cards', params)
	        // .then(res => this.onPressHandler(null, true)     //     console.log("testing QR CODE ==> ", res)
	        // })
		})

        /* QR CODE SCAN RETURN QR DATA */
        // axios.get(API_URL+'/giftcard/283573d65a37fee22a3de2c013278463', {params:{
        //     id_login: id_login
        // }})
        // .then(res => {
        //     console.log("scan QR CODE url ==> ", res.data)
        // })



    }

	agreement() {
        if (this.state.checked === false) {
            this.setState({
                checked: true,
                checked_item: true
            })
        }else{
            this.setState({
                checked: false,
                checked_item: false
            })
        }
    }

    onPressHandler(id=null, select_all=false) {
		let renderData=[...this.state.renderData];
    	if (select_all) {
    		for(let data of renderData){
				data.selected=this.state.checked?false:true;
    		}
    		if (this.state.checked) {
	    		this.setState({checked: false})
	    		console.log('false')
    		} else {
	    		this.setState({checked: true})
	    		console.log('true')
    		}
    	} else {
			for(let data of renderData){
				if(data.order_item_id==id){
					data.selected=(data.selected==null)?true:!data.selected;
					break;
				}
			}
    	}
		this.setState({renderData});
	}

	format(number) {
        if (number) {
            var rupiah = "";
            var numberrev = number
                .toString()
                .split("")
                .reverse()
                .join("");
            for (var i = 0; i < numberrev.length; i++)
                if (i % 3 == 0) rupiah += numberrev.substr(i, 3) + ".";
            return (
                "Rp " +
                rupiah
                .split("", rupiah.length - 1)
                .reverse()
                .join("")
            );
        } else {
            return (
                "Rp "+number
            );
        }
    }

	render() {
		return (
			<LinearGradient start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }} colors={[ '#181C21', '#252F38', '#252F38' ]} style={{ width: '100%', height: '100%' }}>
				<View style={{ width: '100%', backgroundColor: '#121212', alignItems: 'center' }}>
					<View style={{ width: wp('90%'), height: hp('10%'), flexDirection: 'row', alignItems: 'center' }}>
						<View style={{ width: wp('15%') }}>
							<TouchableOpacity onPress={() => this.props.navigation.pop()}>
								<FastImage source={require('../asset/left.png')} style={{ width: xs(25), height: xs(25) }} />
							</TouchableOpacity>
						</View>
						<View style={{ width: wp('60%'), alignItems: 'center' }}>
							<Text style={[ c.light, f._16, { fontFamily: 'Roboto-Bold' }]}>Pilih Hadiah</Text>
						</View>
						<View style={{ width: wp('15%') }}>
						</View>
					</View>
				</View>

				<View style={{ width: '100%', backgroundColor: '#343E47', alignItems: 'center' }}>
					<View style={[ b.mt2, b.mb2, { width: wp('90%') }]}>
						<Text style={[ c.light, { fontFamily: 'Roboto-Regular' }]}>Kirim Barang</Text>
					</View>
				</View>

				<ScrollView>
					<View style={{ width: '100%' }}>
						<View style={[ b.mb2, { width: '100%', alignItems: 'center', borderBottomWidth: 1, borderBottomColor: light }]}>
							<View style={[ b.mt2, b.mb2, { width: wp('90%'), flexDirection: 'row-reverse', alignItems: 'center' }]}>
								<CheckBox
	                                style={{ zIndex: 1 }}
	                                tintColors={{ false: light, true: blue }}
	                                value={this.state.checked}
	                                onChange={() => this.onPressHandler(null, true)}
	                            />
	                            {this.state.checked === true && (
	                                <View style={{ width: xs(13), height: xs(13), backgroundColor: light, position: 'absolute', top: xs(8), left: xs(8) }} />
	                            )}
	                            <Text style={[ c.light, b.mr1, { fontFamily: 'Roboto-Regular' }]}>Pilih Semua</Text>
							</View>
						</View>

						<FlatList
							data={this.state.renderData}
							keyExtractor={item => item.order_item_id}
							renderItem={({ item }) => (
								<View style={{ width: '100%', alignItems: 'center' }}>
									<View style={{ width: wp('90%'), flexDirection: 'row' }}>
										<View style={{ width: wp('20%') }}>
											<FastImage source={{ uri: API_URL+'/public/product/'+item.gambar.split(",",1) }} style={{ width: wp('18%'), height: wp('18%'), borderRadius: xs(5) }} />
										</View>
										<View style={{ width: wp('60%') }}>
											<Text style={[ c.light, { fontFamily: 'Roboto-Bold' }]} numberOfLines={2}>{item.nama}</Text>
											<View style={{ width: '100%', position: 'absolute', bottom: 0 }}>
												<Text style={[ c.light, { fontFamily: 'Roboto-Regular' }]}>{this.format(item.harga)}</Text>
											</View>
										</View>
										<View style={{ width: wp('10%'), alignItems: 'flex-end' }}>
											<CheckBox
				                                style={{ zIndex: 1, marginTop: vs(18) }}
				                                tintColors={{ false: light, true: blue }}
				                                value={item.selected}
				                                onChange={() => this.onPressHandler(item.order_item_id)}
				                            />
				                            {item.selected === true && (
				                                <View style={{ width: xs(13), height: xs(13), backgroundColor: light, position: 'absolute', top: vs(27), left: vs(16) }} />
				                            )}
										</View>
									</View>
									<View style={[ b.mt2, b.mb2, { width: '100%', borderBottomWidth: 1, borderBottomColor: light }]} />
								</View>
							)}
						/>
					</View>
				</ScrollView>

				<View style={{ width: '100%', alignItems: 'center', position: 'relative', bottom: 0 }}>
					<TouchableOpacity onPress={() => this.generateQRCode()} style={[ b.rounded, b.mt4, b.mb4, { width: wp('85%'), height: hp('6.5%'), backgroundColor: blue, alignItems: 'center', justifyContent: 'center' }]}>
						<Text style={[ c.light, { fontFamily: 'Roboto-Regular' }]}>Kirim Giftcard</Text>
					</TouchableOpacity>
				</View>
			</LinearGradient>
		)
	}
}