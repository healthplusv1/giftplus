import React, { Component } from 'react'
import { View, Text, TouchableOpacity, Image, ScrollView, FlatList } from 'react-native'
import { blue, light, softGrey } from '../utils/Color'
import { xs, vs } from '../utils/Responsive'
import { f, c, b, p } from '../utils/StyleHelper'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
import FastImage from 'react-native-fast-image'
import AsyncStorage from '@react-native-async-storage/async-storage'
import axios from 'axios'
import { API_LOCAL, API_URL } from '@env'

export default class PromoCode extends Component {
	constructor(props) {
		super(props);
		this.state = {
			promoCode: [],
		}
	}

    async componentDidMount() {
    }

	render() {
		return (
			<View style={{ width: '100%', height: '100%', backgroundColor: '#121212' }}>
				<View style={{ width: '100%', alignItems: 'center' }}>
					<View style={[ b.mt4, b.mb4, { width: wp('90%'), flexDirection: 'row', alignItems: 'center' }]}>
						<View style={{ width: wp('15%') }}>
							<TouchableOpacity onPress={() => this.props.navigation.pop()}>
								<Image source={require('../asset/left.png')} style={{ width: wp('8%'), height: wp('8%') }} />
							</TouchableOpacity>
						</View>
						<View style={{ width: wp('60%'), alignItems: 'center' }}>
							<Text style={[ c.light, f._16, { fontFamily: 'Roboto-Bold' }]}>Pilih Kupon Promo</Text>
						</View>
						<View style={{ width: wp('15%') }}>
						</View>
					</View>
				</View>

				<ScrollView style={{ backgroundColor: '#181B22' }}>
					<View style={[ b.mt4, { width: '100%', alignItems: 'center' }]}>
						<FlatList
                            data={this.state.promoCode}
                            keyExtractor={item => item.id}
                            renderItem={({ item, index }) => (
                                <TouchableOpacity>
                                    <FastImage source={require('../asset/couponpromo.png')} style={{ width: wp('90%'), height: xs(100), marginBottom: wp('4%'), flexDirection: 'row', alignItems: 'center' }}>
                                        <View style={{ width: wp('27.5%'), height: '100%', alignItems: 'center', justifyContent: 'center' }}>
                                            <View style={{ width: wp('20%') }}>
                                                <Text style={[ c.light, p.textCenter, f._16, { fontFamily: 'Roboto-Bold' }]}>{item.jenis_voucher}</Text>
                                            </View>
                                        </View>
                                        <View style={{ width: wp('47%'), height: '100%' }}>
                                            <Text style={[ c.blue, f._16, { marginTop: wp('2%'), marginLeft: wp('4%'), fontFamily: 'Roboto-Bold' }]}>{item.code}</Text>
                                            <Text style={{ marginLeft: wp('4%'), marginTop: wp('1%'), fontFamily: 'Roboto-Bold' }}>{item.desc}</Text>
                                            <View style={{ marginLeft: wp('4%'), borderWidth: 1, borderColor: blue, width: wp('32%'), alignItems: 'center', borderRadius: wp('2%') }}>
                                                <Text style={[ c.blue, { fontFamily: 'Roboto-Regular' }]}>{item.for}</Text>
                                            </View>
                                            <Text style={[ f._12, { marginLeft: wp('4%'), color: softGrey, marginTop: wp('1%'), fontFamily: 'Roboto-Regular' }]}>{item.exp}</Text>
                                        </View>
                                        <View style={{ width: wp('15.5%'), height: '100%' }}>
                                            {this.state.checked === index ?
                                                <View style={{ width: wp('5%'), height: wp('5%'), backgroundColor: blue, borderRadius: wp('5%'), marginLeft: wp('6%'), marginTop: wp('2%') }}></View>
                                                :
                                                <View style={{ width: wp('5%'), height: wp('5%'), borderWidth: 1, borderColor: blue, borderRadius: wp('5%'), marginLeft: wp('6%'), marginTop: wp('2%') }}></View>
                                            }
                                            <TouchableOpacity onPress={() => this.props.navigation.push('DetailPromoCode')}>
                                                <Text style={[ c.blue, f._12, { marginLeft: wp('6%'), marginTop: wp('14%'), fontFamily: 'Roboto-Regular' }]}>S&K</Text>
                                            </TouchableOpacity>
                                        </View>
                                    </FastImage>
                                </TouchableOpacity>
                            )}
                        />
					</View>
				</ScrollView>
			</View>
		)
	}
}