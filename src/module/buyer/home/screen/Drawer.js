import React, { Component } from 'react'
import { Text, View, Image, ScrollView, TouchableOpacity, Linking, ToastAndroid } from 'react-native'
import { blue, light } from '../utils/Color'
import { xs, vs } from '../utils/Responsive'
import { f, c, b, p } from '../utils/StyleHelper'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
import FastImage from 'react-native-fast-image'
import Share from 'react-native-share'
import AsyncStorage from '@react-native-async-storage/async-storage'
import axios from 'axios'
import { API_LOCAL } from '@env'

const url = 'https://play.google.com/store/apps/details?id=com.healthplus';
const title = '';
const message = 'Silahkan download HealthPlus+';
const icon = 'data:<data_type>/<file_extension>;base64,<base64_data>';
const options = Platform.select({
	ios: {
		activityItemSources: [
		{ // For sharing url with custom title.
			placeholderItem: { type: 'url', content: url },
			item: {
			default: { type: 'url', content: url },
			},
			subject: {
			default: title,
			},
			linkMetadata: { originalUrl: url, url, title },
		},
		{ // For sharing text.
			placeholderItem: { type: 'text', content: message },
			item: {
			default: { type: 'text', content: message },
			message: null, // Specify no text to share via Messages app.
			},
			linkMetadata: { // For showing app icon on share preview.
			title: message
			},
		},
		{ // For using custom icon instead of default text icon at share preview when sharing with message.
			placeholderItem: {
			type: 'url',
			content: icon
			},
			item: {
			default: {
				type: 'text',
				content: `${message} ${url}`
			},
			},
			linkMetadata: {
			title: message,
			icon: icon
			}
		},
		],
	},
	default: {
		title,
		subject: title,
		message: `${message} ${url}`,
	},
});

export default class Drawer extends Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    }

    render() {
        return (
            <ScrollView style={{ backgroundColor: '#181B22' }} showsVerticalScrollIndicator={false}>
                <View style={{ width: '100%', alignItems: 'center', backgroundColor: '#121212' }}>
                    <View style={[ b.mt4, b.mb4, { width: wp('90%'), flexDirection: 'row', alignItems: 'center' }]}>
                        <View style={{ width: wp('15%') }}>
                            <TouchableOpacity onPress={() => this.props.navigation.pop()}>
                                <FastImage source={require('../asset/left.png')} style={{ width: xs(30), height: xs(30) }} />
                            </TouchableOpacity>
                        </View>
                        <View style={{ width: wp('60%'), alignItems: 'center' }}>
                            <Text style={[ c.light, f._16, f.bold ]}>Informasi</Text>
                        </View>
                        <View style={{ width: wp('15%') }}></View>
                    </View>
                </View>

                <View style={[ b.mt3, { width: '100%', alignItems: 'center' }]}>
					<TouchableOpacity onPress={() => this.props.navigation.push('SellerNotif')} style={{ width: wp('90%'), flexDirection: 'row', alignItems: 'center' }}>
						<View style={{ width: wp('10%') }}>
							<Image source={require('../asset/notification.png')} style={{ width: wp('7%'), height: wp('7%'), tintColor: blue }} />
						</View>
						<View style={{ width: wp('70%') }}>
							<Text style={[ f._16, b.ml3, b.mt1, c.light ]}>Pemberitahuan</Text>
						</View>
						<View style={{ width: wp('10%'), alignItems: 'flex-end' }}>
							<FastImage source={require('../asset/left.png')} style={{ width: xs(25), height: xs(25), transform: [{ rotateY: '180deg' }] }} />
						</View>
					</TouchableOpacity>
				</View>
				<View style={[ b.mt3, { borderBottomWidth: 1, borderBottomColor: '#999999' }]} />

                <View style={[ b.mt3, { width: '100%', alignItems: 'center' }]}>
					<TouchableOpacity onPress={() => this.props.navigation.push('JoinSeller')} style={{ width: wp('90%'), flexDirection: 'row', alignItems: 'center' }}>
						<View style={{ width: wp('10%') }}>
							<Image source={require('../asset/icongabungjadiseller.png')} style={{ width: wp('7%'), height: wp('7%'), tintColor: blue }} />
						</View>
						<View style={{ width: wp('70%') }}>
							<Text style={[ f._16, b.ml3, b.mt1, c.light ]}>Gabung jadi penjual</Text>
						</View>
						<View style={{ width: wp('10%'), alignItems: 'flex-end' }}>
							<FastImage source={require('../asset/left.png')} style={{ width: xs(25), height: xs(25), transform: [{ rotateY: '180deg' }] }} />
						</View>
					</TouchableOpacity>
				</View>
				<View style={[ b.mt3, { borderBottomWidth: 1, borderBottomColor: '#999999' }]} />

				<View style={[ b.mt3, { width: '100%', alignItems: 'center' }]}>
					<TouchableOpacity onPress={() => this.props.navigation.push('EdukasiPenjual')} style={{ width: wp('90%'), flexDirection: 'row', alignItems: 'center' }}>
						<View style={{ width: wp('10%') }}>
							<Image source={require('../asset/icongabungjadiseller.png')} style={{ width: wp('7%'), height: wp('7%'), tintColor: blue }} />
						</View>
						<View style={{ width: wp('70%') }}>
							<Text style={[ f._16, b.ml3, b.mt1, c.light ]}>Edukasi penjual</Text>
						</View>
						<View style={{ width: wp('10%'), alignItems: 'flex-end' }}>
							<FastImage source={require('../asset/left.png')} style={{ width: xs(25), height: xs(25), transform: [{ rotateY: '180deg' }] }} />
						</View>
					</TouchableOpacity>
				</View>
				<View style={[ b.mt3, { borderBottomWidth: 1, borderBottomColor: '#999999' }]} />

                <View style={[ b.mt3, { width: '100%', alignItems: 'center' }]}>
					<TouchableOpacity onPress={() => ToastAndroid.show('still not implemented', ToastAndroid.SHORT)} style={{ width: wp('90%'), flexDirection: 'row', alignItems: 'center' }}>
						<View style={{ width: wp('10%') }}>
							<Image source={require('../asset/terms.png')} style={{ width: wp('7%'), height: wp('7%') }} />
						</View>
						<View style={{ width: wp('70%') }}>
							<Text style={[ f._16, b.ml3, b.mt1, c.light ]}>Syarat & Ketentuan</Text>
						</View>
						<View style={{ width: wp('10%'), alignItems: 'flex-end' }}>
							<FastImage source={require('../asset/left.png')} style={{ width: xs(25), height: xs(25), transform: [{ rotateY: '180deg' }] }} />
						</View>
					</TouchableOpacity>
				</View>
				<View style={[ b.mt3, { borderBottomWidth: 1, borderBottomColor: '#999999' }]} />

                <View style={[ b.mt3, { width: '100%', alignItems: 'center' }]}>
					<TouchableOpacity onPress={() => ToastAndroid.show('still not implemented', ToastAndroid.SHORT)} style={{ width: wp('90%'), flexDirection: 'row', alignItems: 'center' }}>
						<View style={{ width: wp('10%') }}>
							<Image source={require('../asset/faq.png')} style={{ width: wp('7%'), height: wp('7%') }} />
						</View>
						<View style={{ width: wp('70%') }}>
							<Text style={[ f._16, b.ml3, b.mt1, c.light ]}>FAQ</Text>
						</View>
						<View style={{ width: wp('10%'), alignItems: 'flex-end' }}>
							<FastImage source={require('../asset/left.png')} style={{ width: xs(25), height: xs(25), transform: [{ rotateY: '180deg' }] }} />
						</View>
					</TouchableOpacity>
				</View>
				<View style={[ b.mt3, { borderBottomWidth: 1, borderBottomColor: '#999999' }]} />

                <View style={[ b.mt3, { width: '100%', alignItems: 'center' }]}>
					<TouchableOpacity onPress={() => ToastAndroid.show('still not implemented', ToastAndroid.SHORT)} style={{ width: wp('90%'), flexDirection: 'row', alignItems: 'center' }}>
						<View style={{ width: wp('10%') }}>
							<Image source={require('../asset/privacy.png')} style={{ width: wp('7%'), height: wp('7%') }} />
						</View>
						<View style={{ width: wp('70%') }}>
							<Text style={[ f._16, b.ml3, b.mt1, c.light ]}>Kebijakan Privasi</Text>
						</View>
						<View style={{ width: wp('10%'), alignItems: 'flex-end' }}>
							<FastImage source={require('../asset/left.png')} style={{ width: xs(25), height: xs(25), transform: [{ rotateY: '180deg' }] }} />
						</View>
					</TouchableOpacity>
				</View>
				<View style={[ b.mt3, { borderBottomWidth: 1, borderBottomColor: '#999999' }]} />

                <View style={[ b.mt3, { width: '100%', alignItems: 'center' }]}>
					<TouchableOpacity onPress={() => ToastAndroid.show('still not implemented', ToastAndroid.SHORT)} style={{ width: wp('90%'), flexDirection: 'row', alignItems: 'center' }}>
						<View style={{ width: wp('10%') }}>
							<Image source={require('../asset/aboutus.png')} style={{ width: wp('7%'), height: wp('7%') }} />
						</View>
						<View style={{ width: wp('70%') }}>
							<Text style={[ f._16, b.ml3, b.mt1, c.light ]}>Tentang Kami</Text>
						</View>
						<View style={{ width: wp('10%'), alignItems: 'flex-end' }}>
							<FastImage source={require('../asset/left.png')} style={{ width: xs(25), height: xs(25), transform: [{ rotateY: '180deg' }] }} />
						</View>
					</TouchableOpacity>
				</View>
				<View style={[ b.mt3, { borderBottomWidth: 1, borderBottomColor: '#999999' }]} />

                <View style={[ b.mt3, { width: '100%', alignItems: 'center' }]}>
					<TouchableOpacity onPress={() => ToastAndroid.show('still not implemented', ToastAndroid.SHORT)} style={{ width: wp('90%'), flexDirection: 'row', alignItems: 'center' }}>
						<View style={{ width: wp('10%') }}>
							<Image source={require('../asset/contactus.png')} style={{ width: wp('7%'), height: wp('7%') }} />
						</View>
						<View style={{ width: wp('70%') }}>
							<Text style={[ f._16, b.ml3, b.mt1, c.light ]}>Hubungi Kami</Text>
						</View>
						<View style={{ width: wp('10%'), alignItems: 'flex-end' }}>
							<FastImage source={require('../asset/left.png')} style={{ width: xs(25), height: xs(25), transform: [{ rotateY: '180deg' }] }} />
						</View>
					</TouchableOpacity>
				</View>
				<View style={[ b.mt3, { borderBottomWidth: 1, borderBottomColor: '#999999' }]} />

                <View style={[ b.mt3, { width: '100%', alignItems: 'center' }]}>
					<TouchableOpacity onPress={() => Share.open(options)} style={{ width: wp('90%'), flexDirection: 'row', alignItems: 'center' }}>
						<View style={{ width: wp('10%') }}>
							<Image source={require('../asset/share.png')} style={{ width: wp('7%'), height: wp('7%'), tintColor: blue }} />
						</View>
						<View style={{ width: wp('70%') }}>
							<Text style={[ f._16, b.ml3, b.mt1, c.light ]}>Share</Text>
						</View>
						<View style={{ width: wp('10%'), alignItems: 'flex-end' }}>
							<FastImage source={require('../asset/left.png')} style={{ width: xs(25), height: xs(25), transform: [{ rotateY: '180deg' }] }} />
						</View>
					</TouchableOpacity>
				</View>
				<View style={[ b.mt3, { borderBottomWidth: 1, borderBottomColor: '#999999' }]} />

				{/*<View style={{ width: '100%', alignItems: 'center', marginTop: vs(55) }}>
					<TouchableOpacity onPress={() => AsyncStorage.removeItem('@id_login')} style={[ b.roundedLow, { width: wp('90%'), height: hp('7%'), alignItems: 'center', justifyContent: 'center', borderWidth: 1, borderColor: blue }]}>
						<Text style={[ c.light, { fontFamily: 'Roboto-Bold' }]}>Log Out</Text>
					</TouchableOpacity>
				</View>*/}
            </ScrollView>
        )
    }
}
