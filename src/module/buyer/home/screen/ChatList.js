import React, { Component } from 'react'
import { View, Text, Image, TouchableOpacity, ScrollView, FlatList, TextInput } from 'react-native'
import { blue, light, softGrey } from '../utils/Color'
import { xs, vs } from '../utils/Responsive'
import { f, c, b, p } from '../utils/StyleHelper'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
import FastImage from 'react-native-fast-image'
import LinearGradient from 'react-native-linear-gradient'
import AsyncStorage from '@react-native-async-storage/async-storage'
import axios from 'axios'
import { API_LOCAL, API_URL } from '@env'

export default class ChatList extends Component {
	constructor(props) {
		super(props);
		this.state = {
			search: '',
			chatList: [
				{id: 1, nama_store: 'Canon Official Store', image: require('../asset/Canon_logo.png'), notif: '1', terakhir_chat: '08 Agt', frontChat: 'Hi Kak, terima kasih sudah berbelanja di toko kami dan semoga anda puas'},
				{id: 2, nama_store: 'Canon Official Store', image: require('../asset/Canon_logo.png'), notif: '1', terakhir_chat: '08 Agt', frontChat: 'Hi Kak, terima kasih sudah berbelanja di toko kami dan semoga anda puas'},
				{id: 3, nama_store: 'Canon Official Store', image: require('../asset/Canon_logo.png'), notif: '1', terakhir_chat: '08 Agt', frontChat: 'Hi Kak, terima kasih sudah berbelanja di toko kami dan semoga anda puas'},
				{id: 4, nama_store: 'Canon Official Store', image: require('../asset/Canon_logo.png'), notif: '1', terakhir_chat: '08 Agt', frontChat: 'Hi Kak, terima kasih sudah berbelanja di toko kami dan semoga anda puas'}
			]
		}
	}

	render() {
		return (
			<View style={{ width: '100%', height: '100%' }}>
				<View style={{ width: '100%', height: hp('10%'), backgroundColor: blue, alignItems: 'center' }}>
					<View style={[ b.mt4, b.mb4, { width: wp('90%'), flexDirection: 'row', alignItems: 'center' }]}>
						<View style={{ width: wp('15%') }}>
							<TouchableOpacity onPress={() => this.props.navigation.pop()}>
								<FastImage source={require('../asset/left.png')} style={{ width: xs(25), height: xs(25) }} />
							</TouchableOpacity>
						</View>
						<View style={{ width: wp('60%') }}>
							<Text style={[ c.light, f._16, { fontFamily: 'Roboto-Bold' }]}>Chat</Text>
						</View>
						<View style={{ width: wp('15%') }}>
						</View>
					</View>
				</View>

				<LinearGradient start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }} colors={[ '#181C21', '#252F38', '#252F38' ]} style={{ width: '100%', height: hp('86.5%') }}>
					<ScrollView showsVerticalScrollIndicator={false}>
						<View style={{ width: '100%', alignItems: 'center', borderBottomWidth: 1, borderBottomColor: light }}>
							<View style={[ b.mt4, b.mb4, { width: wp('90%'), height: hp('6.5%'), borderRadius: wp('5%'), backgroundColor: softGrey, flexDirection: 'row', alignItems: 'center' }]}>
								<TextInput
									style={[ c.light, { width: wp('62%'), height: '100%', marginLeft: wp('4%') }]}
									ref={ref => this.textInputRef = ref}
									placeholder='Cari chat'
									placeholderTextColor={light}
									returnKeyType='search'
									onChangeText={(text) => this.setState({ search: text })}
									onSubmitEditing={() => this.nextScreen()}
									value={this.state.search}
								/>
								<View style={{ width: wp('20%'), flexDirection: 'row-reverse' }}>
									{this.state.search === '' ?
										<TouchableOpacity onPress={() => this.textInputRef.focus()}>
											<Image source={require('../asset/search.png')} style={{ width: wp('6%'), height: wp('6%'), tintColor: light, marginLeft: wp('4%') }} />
										</TouchableOpacity>
										:
										<TouchableOpacity onPress={() => this.setState({ search: '' })} style={{ marginLeft: wp('4%') }}>
											<Image source={require('../asset/cross.png')} style={{ width: wp('6%'), height: wp('6%'), tintColor: light }} />
										</TouchableOpacity>
									}
									{/* <TouchableOpacity>
										<Image source={require('../asset/camera.png')} style={{ width: wp('6%'), height: wp('6%'), tintColor: light }} />
									</TouchableOpacity> */}
								</View>
							</View>
						</View>

						<View style={[ b.mt2, { width: '100%' }]}>
							<FlatList
								data={this.state.chatList}
								keyExtractor={item => item.id}
								renderItem={({ item }) => (
									<View style={{ width: '100%', alignItems: 'center' }}>
										<TouchableOpacity onPress={() => this.props.navigation.push('ChatDetail')} style={{ width: wp('90%'), flexDirection: 'row' }}>
											<View style={{ width: wp('20%') }}>
												<View style={{ width: wp('18%'), height: wp('18%'), borderRadius: wp('18%'), backgroundColor: light, alignItems: 'center', justifyContent: 'center' }}>
													<FastImage source={item.image} style={{ width: xs(45), height: xs(45) }} />
												</View>
											</View>
											<View style={{ width: wp('55%') }}>
												<Text style={[ c.light, { fontFamily: 'Roboto-Bold' }]}>{item.nama_store}</Text>
												<Text style={[ c.light, { fontFamily: 'Roboto-Regular' }]} numberOfLines={2}>{item.frontChat}</Text>
											</View>
											<View style={{ width: wp('15%'), alignItems: 'flex-end' }}>
												<Text style={[ c.light, f._12, { fontFamily: 'Roboto-Regular' }]}>{item.terakhir_chat}</Text>
												<View style={[ b.mt2, { width: xs(15), height: xs(15), backgroundColor: 'darkorange', borderRadius: xs(15), alignItems: 'center', justifyContent: 'center' }]}>
													<Text style={[ c.light, f._10, { fontFamily: 'Roboto-Regular' }]}>{item.notif}</Text>
												</View>
											</View>
										</TouchableOpacity>
										<View style={[ b.mt2, b.mb2, { width: '100%', borderBottomWidth: 1, borderBottomColor: light }]} />
									</View>
								)}
							/>
						</View>
					</ScrollView>
				</LinearGradient>
			</View>
		)
	}
}