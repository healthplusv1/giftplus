import React, { Component } from 'react'
import { Text, View, TouchableOpacity, TouchableHighlight, ScrollView, FlatList, RefreshControl, Image, ToastAndroid, Alert } from 'react-native'
import { xs, vs } from '../utils/Responsive'
import { blue, light } from '../utils/Color'
import { f, c, b, p } from '../utils/StyleHelper'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
import FastImage from 'react-native-fast-image'
import AsyncStorage from '@react-native-async-storage/async-storage'
import { NavigationEvents } from 'react-navigation'
import axios from 'axios'
import { API_LOCAL, API_URL } from '@env'

export default class Cart extends Component {
    constructor(props) {
        super(props);
        this.state = {
            cart: [],
            count: 0,
            loading: false,
            check: [],
            voucherCode: null,
            check_id_login: null,
            discount_type: null,
            discount_amount: null,
            totalPrice: 0,
            voucherStatus: 'voucher valid'
        }
    }

    async componentDidMount() {
        // AsyncStorage.setItem('@voucher', JSON.stringify({
        //     id:1,
        //     code: 'GP77BD',
        //     voucher_type: 'global',
        //     discount_type: 'percentage',
        //     minimum_amount: 10000,
        //     maximum_amount: 'unlimited',
        //     amount: 50,
        // }));

        /*
        percentage
        amount/100 * total price

        amount
        total price - amount
        if amount > maximum discount then amount = maximum discount

        */
        this.setState({ loading: true })
        const id_login = await AsyncStorage.getItem('@id_login')
        let voucher = await AsyncStorage.getItem('@voucher')
        voucher = JSON.parse(voucher);
        console.log("VOUCHER LOG ===> ", voucher)
        this.setState({ loading: true, check_id_login: id_login/*, voucherCode: voucher.code*/ })

        if (this.state.check_id_login === null) {
            const guest = await AsyncStorage.getItem('id_login_guest')

            axios.get(API_URL+'/main/shopping/cart', {params: {
                id_login: guest
            }})
            .then(res => {
                // console.log('guest ', res.data.data)
                this.setState({
                    count: res.data.data.length,
                    cart: res.data.data,
                    loading: false
                })

                let totalQuantity = 0;
                let totalPrice = 0;
                res.data.data.forEach((item) => {
                    totalQuantity += item.jumlah;
                    totalPrice += item.jumlah * item.harga/* - 10000*/;
                })

                if (voucher.code!==null) this.validateVoucher(id_login, voucher.code, totalPrice);
            })
        } else {
            axios.get(API_URL+'/main/shopping/cart', {params: {
                id_login: id_login
            }})
            .then(res => {
                this.setState({
                    loading: false,
                    count: res.data.data.length,
                    cart: res.data.data
                })

                let totalQuantity = 0;
                let totalPrice = 0;
                res.data.data.forEach((item) => {
                    totalQuantity += item.jumlah;
                    totalPrice += item.jumlah * item.harga/* - 10000*/;
                })

                if (voucher.code!==null) this.validateVoucher(id_login, voucher.code, totalPrice, voucher.id);
            })
            .catch(err => {
                this.setState({
                    loading: false
                })
            })
        }
    }

    validateVoucher(id_login, voucherCode, totalPrice, id) {
        axios.get(API_URL+"/main/shopping/vouchers/validate",{params:{
            id_login:id_login,
            voucherCode: voucherCode,
            amount: totalPrice
        }})
        .then(res => {
            // return console.log('validateVoucher ===> ', res.data)
            if (res.data.status==='voucher valid') {
                const voucher = res.data.data;
                this.setState({
                    discount_type: voucher.discount_type,
                    discount_amount: voucher.discount_amount
                })
                console.log("AFTER DISOUNT ===> ", this.cutNominal(totalPrice, voucher.discount_type, voucher.discount_amount, voucher.maximum_discount))
            }
        })
    }

    cutNominal(nominal, discount_type, discount_amount, maximum_discount){
        let amount = 0;
        let discount = 0;
        if (discount_type==='nominal') amount = (Number(nominal) - Number(discount_amount));

        if (discount_type==='percentage'){
            discount = (Number(discount_amount)/100) * Number(nominal);

            if (maximum_discount!=='unlimited') {
                if (Number(discount) > Number(maximum_discount)) discount = Number(maximum_discount);
            } amount = Number(nominal) - Number(discount);
            if (Number(amount) < 0) amount = 0;
        } if (Number(amount) < 0) amount = 0;

        if (discount_type==='cashback'){
            // code...
        }

        return amount
    }

    incrementCount = async (item, index) => {
        const id_login = await AsyncStorage.getItem('@id_login')

        if (id_login === null) {
            const guest = await AsyncStorage.getItem('id_login_guest')

            axios.get(API_URL+'/main/shopping/cart/'+item.produk_id+'/add', {params: 
                {id_login: guest, jumlah: 1}
            })
            .then(res => {
                if (res.data.status == 'stok habis') {
                    Alert.alert(
                        "",
                        "Persediaan stok telah habis",
                        [{text: "Ok"}]
                    );
                } else {
                    this.componentDidMount()
                    ToastAndroid.show('menambahkan ke keranjang belanja', ToastAndroid.SHORT)
                }
            })
            .catch(err => {
                console.log('add_to_cart: '+err)
            })
        } else {
            axios.get(API_URL+'/main/shopping/cart/'+item.produk_id+'/add', {params: {
                id_login: id_login,
                jumlah: 1
            }})
            .then(res => {
                if (res.data.status == 'stok habis') {
                    Alert.alert(
                        "",
                        "Persediaan stok telah habis",
                        [{text: "Ok"}]
                    );
                } else {
                    this.componentDidMount()
                    ToastAndroid.show('menambahkan ke keranjang belanja', ToastAndroid.SHORT)
                }
            })
            .catch(err => {
                console.log('add_to_cart: '+err)
            })
        }
    }

    decrementCount = async (item) => {
        if (item.jumlah === '1') {
            Alert.alert(
                "",
                "Hapus dari keranjang belanja?",
                [
                  {
                    text: 'Tidak', onPress: () => console.log('Cancel Pressed'), style: "cancel"
                  },
                  { text: 'Ya', onPress: () => {
                    axios.get(API_URL+'/main/shopping/cart/'+item.id+'/decrease')
                    .then(res => {
                        this.componentDidMount()
                        ToastAndroid.show('Dikeluarkan dari keranjang belanja', ToastAndroid.SHORT)
                    })
                  }}
                ],  
                { cancelable: false }
            );
        } else {
            axios.get(API_URL+'/main/shopping/cart/'+item.id+'/decrease')
            .then(res => {
                this.componentDidMount()
                ToastAndroid.show('mengurangi dari keranjang belanja', ToastAndroid.SHORT)
            })
        }

        /* else if (id_login === null) {
            const guest = await AsyncStorage.getItem('id_login_guest')

            axios.get(API_URL+'/main/shopping/cart/'+item.produk_id+'/add', {params: {
                id_login: guest,
                jumlah: -1
            }})
            .then(resp => {
                // console.log(resp.data.data)
                // console.log('Jumlah: '+jumlah)
                this.componentDidMount()
                ToastAndroid.show('Reduce Your Product '+item.nama+'', ToastAndroid.SHORT)
            })
            .catch(err => {
                console.log('add_to_cart: '+err)
            })
        } else {
            axios.get(API_URL+'/main/shopping/cart/'+item.produk_id+'/add', {params: {
                id_login: id_login,
                jumlah: -1
            }})
            .then(resp => {
                // console.log(resp.data.data)
                // console.log('Jumlah: '+jumlah)
                this.componentDidMount()
                ToastAndroid.show('Reduce Your Product '+item.nama+'', ToastAndroid.SHORT)
            })
            .catch(err => {
                console.log('add_to_cart: '+err)
            })
        }*/
    }

    removeProduct(item) {
        Alert.alert(
            "",
            "Apakah Anda yakin ingin menghapus?",
            [
                {text: 'Tidak', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                {text: 'Ya', onPress: () => {
                    this.setState({ loading: true })
                    axios.get(API_URL+'/main/shopping/cart/'+item.id+'/remove')
                    .then(res => {
                        // console.log(res.data.status)
                        this.componentDidMount()
                        if (res.data.status === 'true') {
                            ToastAndroid.show('Your order has been deleted', ToastAndroid.SHORT)
                        }
                    })
                }}
            ],
            {cancelable: false}
        );
        return true;
    }

    async nextScreen(totalPrice) {
        const id_login = await AsyncStorage.getItem('@id_login')

        if (id_login !== null) {
            this.props.navigation.push('Payment', {price: totalPrice})
        } else {
            ToastAndroid.show('You must login first before continuing to the next page', ToastAndroid.SHORT)
            this.props.navigation.push('CreateAccount')
        }
    }

    format(number) {
        if (number) {
          var rupiah = "";
          var numberrev = number
            .toString()
            .split("")
            .reverse()
            .join("");
          for (var i = 0; i < numberrev.length; i++)
            if (i % 3 == 0) rupiah += numberrev.substr(i, 3) + ".";
          return (
            "Rp " +
            rupiah
              .split("", rupiah.length - 1)
              .reverse()
              .join("")
          );
        } else {
            return (
                "Rp "+number
            );
        }
    }

    render() {
        const { cart } = this.state;
        let totalQuantity = 0;
        let totalPrice = 0;
        cart.forEach((item) => {
            totalQuantity += item.jumlah;
            totalPrice += item.jumlah * item.harga/* - 10000*/;
        })
        return (
            <>
                <NavigationEvents onDidFocus={() => this.componentDidMount()} />
                <View style={{ width: '100%', height: '100%', backgroundColor: '#121212' }}>
                    <View style={{ width: '100%', flexDirection: 'row', alignItems: 'center', marginTop: wp('4%'), marginBottom: wp('4%') }}>
                        <View style={{ width: wp('20%') }}>
                            <TouchableOpacity onPress={() => this.props.navigation.pop()} style={{ marginLeft: wp('4%') }}>
                                <FastImage source={require('../asset/left.png')} style={{ width: wp('8%'), height: wp('8%') }} />
                            </TouchableOpacity>
                        </View>
                        <View style={[ p.center, { width: wp('60%') }]}>
                            <Text style={[ c.light, f._18, { fontFamily: 'Roboto-Bold' }]}>Keranjang</Text>
                        </View>
                        <View style={{ width: wp('20%') }}></View>
                    </View>
                    <ScrollView showsVerticalScrollIndicator={false} style={{ backgroundColor: '#181B22' }} refreshControl={<RefreshControl onRefresh={() => this.componentDidMount()} refreshing={this.state.loading} />}>
                        <View style={{ width: '100%', alignItems: 'center' }}>
                            <View style={{ width: wp('90%'), marginTop: wp('5%') }}>
                                <FlatList
                                    data={this.state.cart}
                                    keyExtractor={item => item.id}
                                    renderItem={({item, index}) => (
                                        <>
                                            <View style={{ width: '100%', flexDirection: 'row', alignItems: 'center' }}>
                                                <View style={{ width: wp('25%') }}>
                                                    <FastImage source={{ uri: API_URL+'/public/product/'+item.gambar.split(",",1) }} style={{ width: wp('22%'), height: wp('22%') }} />
                                                </View>
                                                <View style={{ width: wp('35%') }}>
                                                    <View>
                                                        <Text style={[ c.light, { fontFamily: 'Roboto-Regular' }]} numberOfLines={2}>{item.nama}</Text>
                                                    </View>
                                                    <View style={[ b.mt2, { flexDirection: 'row', alignItems: 'center', height: wp('7%') }]}>
                                                        <TouchableHighlight underlayColor={blue} onPress={() => this.decrementCount(item)} style={[ p.center, { width: wp('7%'), height: '100%', borderWidth: 1, borderColor: blue, borderRadius: wp('2%') }]}>
                                                            <Image source={require('../asset/minus.png')} style={{ width: xs(12.5), height: xs(12.5), tintColor: light }} />
                                                        </TouchableHighlight>
                                                        <View style={[ p.center, { width: wp('8%') }]}>
                                                            <Text style={[ c.light, { fontFamily: 'Roboto-Regular' }]}>{item.jumlah}</Text>
                                                        </View>
                                                        <TouchableHighlight underlayColor={blue} onPress={() => this.incrementCount(item, index)} style={[ p.center, { width: wp('7%'), height: '100%', borderWidth: 1, borderColor: blue, borderRadius: wp('2%') }]}>
                                                            <Image source={require('../asset/plus.png')} style={{ width: xs(12.5), height: xs(12.5), tintColor: light }} />
                                                        </TouchableHighlight>
                                                    </View>
                                                </View>
                                                <View style={{ width: wp('30%'), alignItems: 'flex-end' }}>
                                                    <TouchableOpacity onPress={() => this.removeProduct(item)}>
                                                        <Image source={require('../asset/trash.png')} style={{ width: wp('6%'), height: wp('6%'), tintColor: blue }} />
                                                    </TouchableOpacity>
                                                    <Text style={[ c.light, { marginTop: wp('5%'), fontFamily: 'Roboto-Regular' }]} numberOfLines={1}>{this.format(item.jumlah * item.harga)}</Text>
                                                </View>
                                            </View>
                                            <View style={{ borderBottomWidth: 1, borderBottomColor: blue, marginTop: wp('3%'), marginBottom: wp('4%') }} />
                                        </>
                                    )}
                                />
                                {
                                    this.state.count !== 0 ?
                                    <>
                                        <View style={{ width: wp('90%'), flexDirection: 'row', alignItems: 'center' }}>
                                            <View style={{ width: wp('45%') }}>
                                                <Text style={[ b.ml4, c.light, { fontFamily: 'Roboto-Bold' }]}>Total harga</Text>
                                            </View>
                                            <View style={{ width: wp('45%'), alignItems: 'flex-end' }}>
                                                <Text style={[ c.light, { fontFamily: 'Roboto-Bold' }]}>{this.format(totalPrice)}</Text>
                                            </View>
                                        </View>
                                        <TouchableOpacity onPress={() => this.props.navigation.push('PromoCode')} style={{ width: wp('90%'), height: wp('10%'), backgroundColor: '#121212', flexDirection: 'row', alignItems: 'center', borderWidth: 1, borderColor: blue, borderRadius: xs(5), marginTop: wp('4%'), marginBottom: wp('4%') }}>
                                            <View style={{ width: wp('50%'), flexDirection: 'row', alignItems: 'center' }}>
                                                <Image source={require('../asset/promocodekecil.png')} style={[ b.ml2, { width: xs(30), height: xs(16), tintColor: light }]} />
                                                <Text style={[ b.ml2, c.light, { fontFamily: 'Roboto-Bold' }]}>{this.state.voucherCode===null?'Masukan Promo':this.state.voucherCode}</Text>
                                            </View>
                                            <View style={{ width: wp('40%'), alignItems: 'flex-end' }}>
                                                <FastImage source={require('../asset/promocodebesar.png')} style={{ width: xs(80), height: xs(28), marginRight: xs(5) }}>
                                                    <Text style={[ c.light, { fontFamily: 'Roboto-Bold', marginTop: xs(4), marginLeft: xs(16) }]}>
                                                    {
                                                        this.state.voucherCode===null?
                                                        'Gunakan'
                                                        :
                                                        (
                                                            this.state.discount_type === 'percentage' ?
                                                            '-'+this.state.discount_amount+'%'
                                                            :
                                                            '-'+this.format(this.state.discount_amount)
                                                        )
                                                    }
                                                    </Text>
                                                </FastImage>
                                            </View>
                                        </TouchableOpacity>
                                            {
                                                this.state.voucherCode===null?
                                                null
                                                :
                                                <View style={{ width: wp('90%'), flexDirection: 'row', alignItems: 'center', marginBottom: wp('4%') }}>
                                                    <View style={{ width: wp('45%') }}>
                                                        <Text style={[ b.ml4, c.light, { fontFamily: 'Roboto-Bold' }]}>Voucher promo</Text>
                                                    </View>
                                                    <View style={{ width: wp('45%'), alignItems: 'flex-end' }}>
                                                        <Text style={[ c.light, { fontFamily: 'Roboto-Bold' }]}>-{this.format(10000)}</Text>
                                                    </View>
                                                </View>
                                            }
                                            </>
                                            :
                                            <View style={{ width: '100%', alignItems: 'center' }}>
                                                <View style={{ width: wp('90%') }}>
                                                    <Text style={[ c.light, { textAlign: 'center' }]}>Keranjang Belanja Kosong</Text>
                                                </View>
                                            </View>
                                            }
                                    </View>
                                </View>
                        </ScrollView>
                    <View style={{ width: '100%', height: hp('19%'), position: 'relative', bottom: 0, backgroundColor: '#121212', alignItems: 'center' }}>
                        <View style={{ width: wp('90%'), flexDirection: 'row', alignItems: 'center', marginTop: wp('5%') }}>
                            <View style={{ width: wp('45%') }}>
                                <Text style={[ c.light, { fontFamily: 'Roboto-Regular' }]}>Jumlah produk ({this.state.count})</Text>
                            </View>
                            <View style={{ width: wp('45%'), alignItems: 'flex-end' }}>
                                <Text style={[ c.light, { fontFamily: 'Roboto-Bold' }]}>{this.format(totalPrice)}</Text>
                            </View>
                        </View>
                        <TouchableOpacity onPress={() => this.nextScreen(totalPrice)} style={[ p.center, { width: wp('90%'), height: hp('8%'), backgroundColor: this.state.count !== 0 && this.state.voucherStatus === 'voucher valid' ? blue : '#CCC', borderRadius: wp('2%'), marginTop: wp('5%') }]} disabled={this.state.count === 0 || this.state.voucherStatus !== 'voucher valid' }>
                            <Text style={[ c.light, f._18, { fontFamily: 'Roboto-Bold' }]}>Pilih Metode Pembayaran</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </>
        )
    }
}
