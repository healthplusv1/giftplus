import React, { Component } from 'react'
import { Text, View, Image, ScrollView, TouchableOpacity, RefreshControl, FlatList, ToastAndroid, TouchableHighlight, Alert } from 'react-native'
import { blue, light, softGrey } from '../utils/Color'
import { xs, vs } from '../utils/Responsive'
import { f, c, b, p } from '../utils/StyleHelper'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
import FastImage from 'react-native-fast-image'
import ImageSlider from 'react-native-image-slider'
import Swiper from 'react-native-swiper'
import AsyncStorage from '@react-native-async-storage/async-storage'
import axios from 'axios'
import { API_LOCAL, API_URL } from '@env'
export default class ProductDetail extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            count: '',
            jumlah_product: '',
            harga: 'loading...',
            nama: 'loading...',
            stok: 'loading...',
            nama_toko: 'loading...',
            kota: 'loading...',
            expandable: false,
            limited_desc: '',
            expanded:false,
            liked:false,
            image: require('../asset/love.png'),
            ulasan: 0,
            avg: 0,
            status_rating: '',
            score: 0,
            kandungan_aktif: 'loading...',
            review: undefined,
            banner1: null,
            banner2: null,
            banner3: null,
            banner4: null
        }
    }

    async componentDidMount() {

        const id_login = await AsyncStorage.getItem('@id_login')

        this.setState({ loading: true })
        console.log('current produk id ==> ', this.props.navigation.getParam('id_product'))
        axios.get(API_URL+'/main/shopping/produk/details', {params: {
            id: this.props.navigation.getParam('id_product'),
            id_login: id_login
        }})
        .then(res => {


            /*
            if string length is more than 500 :
              - (x) explode by "." and get 1 index string length, that's the limited description
              - limit the string to 100 string length
            */if (res.data.data.deskripsi !== null) {
                if (res.data.data.deskripsi.length > 500) {
                    const limited_desc = res.data.data.deskripsi.slice(0,100)
                    this.setState({
                        expandable: true,
                        limited_desc: limited_desc
                    })
                }
            } else
                this.setState({expandable: false})

            this.setState({
                harga: res.data.data.harga,
                nama: res.data.data.nama,
                stok: res.data.data.stok,
                liked: res.data.data.liked,
                sold: res.data.data.sold,
                nama_toko: res.data.data.seller.nama_toko,
                ikon_toko: res.data.data.seller.ikon_toko,
                kota: res.data.data.seller.kota,
                kandungan_aktif: res.data.data.deskripsi,
                banner1: res.data.data.gambar.split(",")[0],
                banner2: res.data.data.gambar.split(",")[1],
                banner3: res.data.data.gambar.split(",")[2],
                banner4: res.data.data.gambar.split(",")[3],
                loading: false
            })

            if (res.data.data.liked) {
                this.setState({
                    liked: true,
                    image: require('../asset/loved.png')
                })
            } else {
                this.setState({
                    liked: false,
                    image: require('../asset/love.png')
                })
            }
        })

        axios.get(API_URL+'/main/shopping/produk/ratings', {params: {
            id: this.props.navigation.getParam('id_product'),
            limited: 'true'
        }})
        .then(resp => {
            console.log(resp.data.data.avg)
            if (resp.data.status != 'belum pernah dirating') {
                this.setState({
                    review: resp.data.data.ratings,
                    ulasan: resp.data.data.length,
                    avg: resp.data.data.avg,
                    loading: false
                });
            } else {
                this.setState({
                    status_rating: resp.data.data.status
                })
            }
        })

        if (id_login === null) {
            const guest = await AsyncStorage.getItem('id_login_guest')

            axios.get(API_URL+'/main/shopping/cart', {params: {
                id_login: guest
            }})
            .then(res => {
                this.setState({
                    count: res.data.data.length,
                    loading: false
                })
            })

            axios.get(API_URL+'/main/shopping/cart/one', {params: {
                id_login: guest,
                produk_id: this.props.navigation.getParam('id_product')
            }})
            .then(res => {
                this.setState({loading: false})
                if (res.data.status != 'produk tidak ada dikeranjang') {
                    this.setState({jumlah_product: res.data.data.jumlah, cart_id:res.data.data.id})
                } else {
                    this.setState({jumlah_product:''})
                }
            })
        } else {
            axios.get(API_URL+'/main/shopping/cart', {params: {
                id_login: id_login
            }})
            .then(res => {
                this.setState({
                    loading: false,
                    count: res.data.data.length
                })
            })
            
            axios.get(API_URL+'/main/shopping/cart/one', {params: {
                id_login: id_login,
                produk_id: this.props.navigation.getParam('id_product')
            }})
            .then(res => {
                this.setState({loading: false})
                if (res.data.status != 'produk tidak ada dikeranjang') {
                    this.setState({jumlah_product: res.data.data.jumlah, cart_id:res.data.data.id})
                } else {
                    this.setState({jumlah_product:''})
                }
            })
        }
    }

    // async likeProduct() {
    //     const id_login = await AsyncStorage.getItem('@id_login')
    //     this.setState({disable_click_like: true})

    //     if (this.state.liked === false) {
    //         axios.post(API_URL+'/main/shopping/favorit', {id_login: id_login, produk_id: this.props.navigation.getParam('id_product')})
    //         .then(res => {
    //             ToastAndroid.show(res.data.status, ToastAndroid.SHORT)
    //             this.setState({disable_click_like: false})
    //             this.setState({
    //                 liked: true,
    //                 image: require('../asset/loved.png')
    //             })
    //         })
    //     } else {
    //         axios.get(API_URL+'/main/shopping/favorit/remove/'+this.props.navigation.getParam('id_product'), {params: {id_login: id_login}})
    //         .then(res => {
    //             ToastAndroid.show(res.data.status, ToastAndroid.SHORT)
    //             this.setState({disable_click_like: false})
    //             this.setState({
    //                 liked: false,
    //                 image: require('../asset/love.png')
    //             })
    //         })
    //     }
    // }

    incrementCount = async () => {
        const id_login = await AsyncStorage.getItem('@id_login')

        if (id_login === null) {
            const guest = await AsyncStorage.getItem('id_login_guest')

            axios.get(API_URL+'/main/shopping/cart/'+this.props.navigation.getParam('id_product')+'/add', {params: 
                {id_login: guest}
            })
            .then(res => {
                if (res.data.status == 'stok habis') {
                    Alert.alert(
                        "",
                        "Persediaan stok telah habis",
                        [{text: "Ok"}]
                    );
                } else {
                    // console.log(resp.data.buyer)
                    this.componentDidMount()
                    ToastAndroid.show('menambahkan ke keranjang belanja', ToastAndroid.SHORT)
                }
            })
            .catch(err => {
                console.log('add_to_cart: '+err)
            })
        } else {

            axios.get(API_URL+'/main/shopping/cart/'+this.props.navigation.getParam('id_product')+'/add', {params: 
                {id_login: id_login}
            })
            .then(res => {
                if (res.data.status == 'stok habis') {
                    Alert.alert(
                        "",
                        "Persediaan stok telah habis",
                        [{text: "Ok"}]
                    );
                } else {
                    // console.log(resp.data.buyer)
                    this.componentDidMount()
                    ToastAndroid.show('menambahkan ke keranjang belanja', ToastAndroid.SHORT)
                }
            })
            .catch(err => {
                console.log('add_to_cart: '+err)
            })
        }
    }

    decrementCount = async () => {
        if (this.state.jumlah_product === '1') {
            Alert.alert(
                "",
                "Hapus dari keranjang belanja?",
                [
                  {
                    text: 'Tidak', onPress: () => console.log('Cancel Pressed'), style: "cancel"
                  },
                  { text: 'Ya', onPress: () => {
                    axios.get(API_URL+'/main/shopping/cart/'+this.state.cart_id+'/remove')
                    .then(res => {
                        this.componentDidMount()
                        ToastAndroid.show('Dikeluarkan dari keranjang belanja', ToastAndroid.SHORT)
                    })
                  }}
                ],  
                { cancelable: false }
            );
        } else {
            axios.get(API_URL+'/main/shopping/cart/'+this.state.cart_id+'/decrease')
            .then(res => {
                this.componentDidMount()
                ToastAndroid.show('mengurangi dari keranjang belanja', ToastAndroid.SHORT)
            });
        }
    }

    async addToCart() {
        const id_login = await AsyncStorage.getItem('@id_login')

        if (id_login === null) {
            const guest = await AsyncStorage.getItem('id_login_guest')

            axios.get(API_URL+'/main/shopping/cart/'+this.props.navigation.getParam('id_product')+'/add', {params: 
                {id_login: guest}
            })
            .then(resp => {
                if (resp.data.status == 'stok habis') {
                    Alert.alert(
                        "",
                        "Persediaan stok telah habis",
                        [{text: "Ok"}]
                    );
                } else {
                    this.componentDidMount()
                    ToastAndroid.show('menambahkan ke keranjang belanja', ToastAndroid.SHORT)
                }
            })
            .catch(err => {
                console.log('add_to_cart: '+err)
            })
        } else {
            axios.get(API_URL+'/main/shopping/cart/'+this.props.navigation.getParam('id_product')+'/add', {params: 
                {id_login: id_login}
            })
            .then(resp => {
                if (resp.data.status == 'stok habis') {
                    Alert.alert(
                        "",
                        "Persediaan stok telah habis",
                        [{text: "Ok"}]
                    );
                } else if (resp.data.status === 'Anda hanya bisa memasukan produk dari toko yang sama dalam satu keranjang') {
                    Alert.alert(
                        "",
                        resp.data.status,
                        [{text: "Ok"}]
                    );
                } else {
                    this.componentDidMount()
                    ToastAndroid.show('menambahkan ke keranjang belanja', ToastAndroid.SHORT)
                }
            })
            .catch(err => {
                console.log('add_to_cart: '+err)
            })
        }
    }

    hitung_hari(from_date) {
		var date1 = new Date(from_date); 
		var date2 = new Date(new Date().getFullYear(), new Date().getMonth() , new Date().getDate()); 
		// console.log(date1);
		// console.log(date2);
		  
		// To calculate the time difference of two dates 
		var Difference_In_Time = date2.getTime() - date1.getTime();
		  
		// To calculate the no. of days between two dates 
		var Difference_In_Days = Difference_In_Time / (1000 * 3600 * 24);
		if (typeof Difference_In_Days === 'number') {
			if(Number(Difference_In_Days.toString().replace('-', '')).toFixed() == 0){
				return 'Today';
			}else{
				return Number(Difference_In_Days.toString().replace('-', '')).toFixed()+' Days ago';
			}
		}
	}

    async likeProduct() {

        const id_login = await AsyncStorage.getItem('@id_login')
        this.setState({disable_click_like: true})
        if (this.state.liked === false) {
            axios.post(API_URL+'/main/shopping/favorit', {id_login: id_login, produk_id: this.props.navigation.getParam('id_product')})
            .then(res => {
                ToastAndroid.show(res.data.status, ToastAndroid.SHORT)
                this.setState({disable_click_like: false})
                this.setState({
                    liked: true,
                    image: require('../asset/loved.png')
                })
            })
        } else {
            axios.get(API_URL+'/main/shopping/favorit/remove/'+this.props.navigation.getParam('id_product'), {params: {id_login: id_login}})
            .then(res => {
                ToastAndroid.show(res.data.status, ToastAndroid.SHORT)
                this.setState({disable_click_like: false})
                this.setState({
                    liked: false,
                    image: require('../asset/love.png')
                })
            })
        }
    }

    format(number) {
        if (number) {
			var rupiah = "";
			var numberrev = number
				.toString()
				.split("")
				.reverse()
				.join("");
			for (var i = 0; i < numberrev.length; i++)
				if (i % 3 == 0) rupiah += numberrev.substr(i, 3) + ".";
			return (
				"Rp " +
				rupiah
				.split("", rupiah.length - 1)
				.reverse()
				.join("")
			);
        } else {
            return (
                "Rp "+number
            );
        }
    }
    
    render() {
        return (
            <View style={{ width: '100%', height: '100%', backgroundColor: '#121212' }}>
                <View style={{ width: '100%', alignItems: 'center' }}>
                    <View style={[ b.mt4, b.mb4, { width: wp('90%'), flexDirection: 'row', alignItems: 'center' }]}>
                        <View style={{ width: wp('15%') }}>
                            <TouchableOpacity onPress={() => this.props.navigation.pop()}>
                                <FastImage source={require('../asset/left.png')} style={{ width: wp('8%'), height: wp('8%') }} />
                            </TouchableOpacity>
                        </View>
                        <View style={{ width: wp('60%'), alignItems: 'center' }}>
                            <Text style={[ c.light, f.bold, f._16 ]}>Detail Produk</Text>
                        </View>
                        <View style={{ width: wp('15%'), alignItems: 'flex-end' }}>
                            <TouchableOpacity onPress={() => this.props.navigation.push('Cart')}>
                				<Image source={require('../asset/cart.png')} style={{ width: xs(19), height: xs(20), tintColor: light }} />
                                {this.state.count !== 0 && (
                    			    <View style={{ width: xs(12), height: xs(12), borderRadius: xs(12), backgroundColor: 'darkorange', position: 'absolute', right: 0, alignItems: 'center', justifyContent: 'center' }}>
                                        <Text style={[ c.light, f._10 ]}>{this.state.count}</Text>
                                    </View>
                                )}
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>

                <ScrollView showsVerticalScrollIndicator={false} refreshControl={<RefreshControl onRefresh={() => this.componentDidMount()} refreshing={this.state.loading} />}>
                    <Swiper
                        height={350}
                        dotStyle={{ marginBottom: xs(-30) }}
                        activeDotStyle={{ marginBottom: xs(-30) }}
                        dotColor={'#CCC'}
                    >
                        <FastImage source={{ uri: API_URL+'/public/product/'+this.state.banner1 }} style={{ width: '100%', height: '100%' }} />
                        <FastImage source={{ uri: API_URL+'/public/product/'+this.state.banner2 }} style={{ width: '100%', height: '100%' }} />
                        <FastImage source={{ uri: API_URL+'/public/product/'+this.state.banner3 }} style={{ width: '100%', height: '100%' }} />
                        <FastImage source={{ uri: API_URL+'/public/product/'+this.state.banner4 }} style={{ width: '100%', height: '100%' }} />
                    </Swiper>

                    <View style={{ width: '100%', backgroundColor: '#181B22' }}>
                        <Text style={[ f.bold, f._16, c.light, b.mt2, b.ml2 ]}>{this.state.loading === true ? (this.state.harga) : (this.format(this.state.harga))}</Text>
                        <Text style={[ b.mt1, b.ml2, c.light ]}>{this.state.nama}</Text>
                        <View style={[ b.mt1, b.ml2, { width: wp('94%'), flexDirection: 'row', alignItems: 'center' }]}>
                            <View style={{ width: wp('47%') }}>
                                <Text style={[ c.light ]}>Stok {this.state.stok}</Text>
                            </View>
                            <View style={{ width: wp('47%'), alignItems: 'flex-end' }}>
                                {this.state.jumlah_product !== '' && (
                                    <View style={{ flexDirection: 'row-reverse' }}>
                                        <TouchableOpacity onPress={() => this.likeProduct()}>
                                            <Image source={this.state.image} style={{ width: xs(25), height: xs(25), tintColor: blue }} />
                                        </TouchableOpacity>

                                        <View style={[ b.mr2, { flexDirection: 'row', alignItems: 'center', borderRadius: wp('1%'), height: wp('7%') }]}>
                                            <TouchableHighlight underlayColor={blue} onPress={() => this.decrementCount()} style={[ p.center, { width: wp('7%'), height: '100%', borderWidth: 1, borderColor: blue, borderRadius: wp('2%') }]}>
                                                <Image source={require('../asset/minus.png')} style={{ width: xs(15), height: xs(15), tintColor: light }} />
                                            </TouchableHighlight>
                                            <View style={[ p.center, { width: wp('8%') }]}>
                                                <Text style={[ c.light ]}>{this.state.jumlah_product}</Text>
                                            </View>
                                            <TouchableHighlight underlayColor={blue} onPress={() => this.incrementCount()} style={[ p.center, { width: wp('7%'), height: '100%', borderWidth: 1, borderColor: blue, borderRadius: wp('2%') }]}>
                                                <Image source={require('../asset/plus.png')} style={{ width: xs(15), height: xs(15), tintColor: light }} />
                                            </TouchableHighlight>
                                        </View>
                                    </View>
                                )}
                                {this.state.jumlah_product === '' && (
                                    <View style={{ flexDirection: 'row-reverse' }}>
                                        <TouchableOpacity onPress={() => this.likeProduct()}>
                                            <Image source={this.state.image} style={{ width: xs(25), height: xs(25), tintColor: blue }} />
                                        </TouchableOpacity>

                                        <TouchableOpacity onPress={() => this.addToCart()} style={[ b.mr2, b.roundedLow, { height: xs(25), backgroundColor: blue, alignItems: 'center', justifyContent: 'center', flexDirection: 'row' }]}>
                                            <Image source={require('../asset/plus.png')} style={[ b.ml4, b.mr1, { width: xs(15), height: xs(15), tintColor: light }]} />
                                            <Image source={require('../asset/cart.png')} style={[ b.mr4, { width: xs(15), height: xs(16), tintColor: light }]} />
                                        </TouchableOpacity>
                                    </View>
                                )}
                            </View>
                        </View>
                        <View style={[ b.mt1, b.mb2, b.ml2, { flexDirection: 'row', alignItems: 'center' }]}>
                            <View style={{ width: wp('60%'), flexDirection: 'row', alignItems: 'center' }}>
                                <Image source={require('../asset/star.png')} style={{ width: xs(15), height: xs(15), tintColor: '#FE8829' }} />
                                <Text style={[ f._12, b.ml1, { color: '#FE8829' }]}>{this.state.avg}.0</Text>
                                <View style={{ width: xs(10), borderBottomWidth: 1, borderBottomColor: '#FE8829', transform: [{ rotate: '90deg' }] }} />
                                <Text style={[ f._12, { color: '#FE8829' }]}>{this.state.ulasan} Ulasan</Text>
                            </View>
                            <View style={[ b.ml4, { width: wp('30%') }]}>
                                <Text style={[ c.light, { fontFamily: 'Roboto-Regular' }]}>Terjual {this.state.sold}</Text>
                            </View>
                        </View>
                    </View>

                    <View style={[ b.mt4, { width: '100%', backgroundColor: '#181B22' }]}>
                        <View style={[ b.mt2, b.ml2, { flexDirection: 'row' }]}>
                            <View style={{ width: xs(40), height: xs(40), backgroundColor: light, borderRadius: xs(40), alignItems: 'center', justifyContent: 'center' }}>
                                <FastImage source={
                                    this.state.ikon_toko!=='undefined' ? {uri: API_URL+'/public/user/'+this.state.ikon_toko} : require('../asset/logo_apotek.png')
                                } style={{ width: xs(25), height: xs(25) }} />
                            </View>
                            <View style={[ b.ml2, { width: xs(170) }]}>
                                <Text onPress={() => this.props.navigation.push('BrandPilihan', {nama_brand: 'xiaomi'})} style={[ c.light, { fontFamily: 'Roboto-Bold' }]} numberOfLines={1}>{this.state.nama_toko}</Text>
                                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                    <Image source={require('../asset/tokolokasi_icon.png')} style={{ width: xs(14), height: xs(14) }} />
                                    <Text style={[ b.ml1, c.light ]}>{this.state.kota}</Text>
                                </View>
                            </View>
                            <View style={[ b.ml2, { width: xs(98), alignItems: 'flex-end' }]}>
                                <TouchableOpacity onPress={() => this.props.navigation.push('ChatDetail')}>
                                    <Image source={require('../asset/chatBubble.png')} style={{ width: xs(25), height: xs(25), transform: [{ rotateY: '180deg' }] }} />
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View style={[ b.mt1, b.mb2, b.ml2, { flexDirection: 'row', alignItems: 'center' }]}>
                            <Image source={require('../asset/star.png')} style={{ width: xs(15), height: xs(15), tintColor: '#FE8829' }} />
                            <Text style={[ f._12, b.ml1, { color: '#FE8829' }]}>4.8 rata-rata ulasan</Text>
                        </View>
                    </View>

                    <View style={[ b.mt4, { width: '100%', backgroundColor: '#181B22' }]}>
                        <Text style={[ b.mt2, b.ml2, f.bold, f._16, c.light ]}>Informasi Produk</Text>
                        {
                            this.state.expandable ?
                            (
                                this.state.expanded ?
                                <>
                                <Text style={[ b.mt4, b.ml2, b.mb2, c.light ]}>{
                                    this.state.kandungan_aktif === null || this.state.kandungan_aktif === undefined ?
                                    <View style={{ width: '100%', alignItems: 'center' }}>
                                        <View style={{ width: wp('90%') }}>
                                            <Text style={[ c.light, { textAlign: 'center' }]}>Tidak ada informasi</Text>
                                        </View>
                                    </View>
                                    : this.state.kandungan_aktif
                                }</Text>
                                <TouchableOpacity onPress={() => this.setState({expanded:false})}>
                                <Text style={[ f.bold, b.ml2, b.mb2, c.blue ]}>Sembunyikan</Text>
                                </TouchableOpacity>
                                </>
                                :
                                <>
                                <Text style={[ b.mt4, b.ml2, b.mb2, c.light ]}>{this.state.limited_desc}...</Text>
                                <TouchableOpacity onPress={() => this.setState({expanded:true})}>
                                <Text style={[ f.bold, b.ml2, b.mb2, c.blue ]}>Lihat Lainnya</Text>
                                </TouchableOpacity>
                                </>
                            )
                            :
                            <Text style={[ b.mt4, b.ml2, b.mb2, c.light ]}>{
                                    this.state.kandungan_aktif === null || this.state.kandungan_aktif === undefined ?
                                    <Text style={[ c.light, { textAlign: 'center' }]}>Tidak ada informasi</Text>
                                    : this.state.kandungan_aktif
                                }</Text>
                        }
                    </View>

                    <View style={[ b.mt4, { width: '100%', backgroundColor: '#181B22' }]}>
                        <View style={[ b.mt2, { width: '100%', flexDirection: 'row', alignItems: 'center' }]}>
                            <View style={{ width: wp('50%') }}>
                                <Text style={[ c.light, f.bold, f._16, b.ml2 ]}>Ulasan Pembeli</Text>
                            </View>
                            <View style={{ width: wp('50%'), alignItems: 'flex-end' }}>
                                <TouchableOpacity onPress={() => this.props.navigation.push('AllRatings')} style={[ b.mr2 ]}>
                                    <Text style={[ c.blue ]}>Lihat Semua</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                        {this.state.status_rating === 'belum pernah dirating' ?
                            <View style={[ b.mt4, b.mb4, { width: '100%', alignItems: 'center' }]}>
                                <Text style={[ f._16, c.light, { fontFamily: 'Roboto-Regular' }]}>Tidak ada ulasan</Text>
                            </View>
                            :
                            <View style={[ b.mt1, b.mb2, b.ml2, { flexDirection: 'row', alignItems: 'center' }]}>
                                <Image source={require('../asset/star.png')} style={{ width: xs(20), height: xs(20), tintColor: '#FE8829' }} />
                                <Text style={[ f._16, b.ml1, { color: '#FE8829' }]}> {this.state.avg} .0</Text>
                                <Text style={[ f._12, b.ml1, { color: '#FE8829', marginTop: xs(2) }]}>dari {this.state.ulasan} Ulasan</Text>
                            </View>
                        }
                        <View style={{ width: '100%', alignItems: 'center' }}>
                            <View style={{ width: wp('95%'), marginTop: wp('4%') }}>
                                {/* <View style={{ width: '100%', alignItems: 'center', position: 'absolute', zIndex: 1, bottom: wp('4%') }}>
                                    <TouchableHighlight onPress={() => this.props.navigation.push('AddReview')} style={{ width: wp('30%'), height: hp('6%'), alignItems: 'center', justifyContent: 'center', backgroundColor: blue, borderRadius: wp('2%') }} underlayColor='#3399FF'>
                                        <Text style={[ c.light, { fontFamily: 'lineto-circular-pro-bold' }]}>Tulis Ulasan</Text>
                                    </TouchableHighlight>
                                </View> */}
                                <FlatList
                                    data={this.state.review}
                                    keyExtractor={item => item.id}
                                    renderItem={({item}) => (
                                        <>
                                            <View style={{ width: '100%', height: wp('15%'), flexDirection: 'row' }}>
                                                <View style={{ width: wp('15%'), height: '100%' }}>
                                                    <Image source={require('../asset/10.png')} style={{ width: '100%', height: '100%' }} />
                                                </View>
                                                <View style={{ width: wp('50%'), height: '100%' }}>
                                                    <Text style={[ c.light, { fontFamily: 'lineto-circular-pro-bold', marginLeft: wp('4%'), marginTop: wp('1%') }]}>{item.nama}</Text>
                                                    <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: wp('1%'), marginLeft: wp('4%') }}>
                                                        {item.bintang === '5' && (
                                                            <>
                                                                <Image source={require('../asset/star.png')} style={{ width: wp('5%'), height: wp('5%'), tintColor: '#FE8829' }} />
                                                                <Image source={require('../asset/star.png')} style={{ width: wp('5%'), height: wp('5%'), tintColor: '#FE8829' }} />
                                                                <Image source={require('../asset/star.png')} style={{ width: wp('5%'), height: wp('5%'), tintColor: '#FE8829' }} />
                                                                <Image source={require('../asset/star.png')} style={{ width: wp('5%'), height: wp('5%'), tintColor: '#FE8829' }} />
                                                                <Image source={require('../asset/star.png')} style={{ width: wp('5%'), height: wp('5%'), tintColor: '#FE8829' }} />
                                                            </>
                                                        )}
                                                        {item.bintang === '4' && (
                                                            <>
                                                                <Image source={require('../asset/star.png')} style={{ width: wp('5%'), height: wp('5%'), tintColor: '#FE8829' }} />
                                                                <Image source={require('../asset/star.png')} style={{ width: wp('5%'), height: wp('5%'), tintColor: '#FE8829' }} />
                                                                <Image source={require('../asset/star.png')} style={{ width: wp('5%'), height: wp('5%'), tintColor: '#FE8829' }} />
                                                                <Image source={require('../asset/star.png')} style={{ width: wp('5%'), height: wp('5%'), tintColor: '#FE8829' }} />
                                                                <Image source={require('../asset/star.png')} style={{ width: wp('5%'), height: wp('5%') }} />
                                                            </>
                                                        )}
                                                        {item.bintang === '3' && (
                                                            <>
                                                                <Image source={require('../asset/star.png')} style={{ width: wp('5%'), height: wp('5%'), tintColor: '#FE8829' }} />
                                                                <Image source={require('../asset/star.png')} style={{ width: wp('5%'), height: wp('5%'), tintColor: '#FE8829' }} />
                                                                <Image source={require('../asset/star.png')} style={{ width: wp('5%'), height: wp('5%'), tintColor: '#FE8829' }} />
                                                                <Image source={require('../asset/star.png')} style={{ width: wp('5%'), height: wp('5%') }} />
                                                                <Image source={require('../asset/star.png')} style={{ width: wp('5%'), height: wp('5%') }} />
                                                            </>
                                                        )}
                                                        {item.bintang === '2' && (
                                                            <>
                                                                <Image source={require('../asset/star.png')} style={{ width: wp('5%'), height: wp('5%'), tintColor: '#FE8829' }} />
                                                                <Image source={require('../asset/star.png')} style={{ width: wp('5%'), height: wp('5%'), tintColor: '#FE8829' }} />
                                                                <Image source={require('../asset/star.png')} style={{ width: wp('5%'), height: wp('5%') }} />
                                                                <Image source={require('../asset/star.png')} style={{ width: wp('5%'), height: wp('5%') }} />
                                                                <Image source={require('../asset/star.png')} style={{ width: wp('5%'), height: wp('5%') }} />
                                                            </>
                                                        )}
                                                        {item.bintang === '1' && (
                                                            <>
                                                                <Image source={require('../asset/star.png')} style={{ width: wp('5%'), height: wp('5%'), tintColor: '#FE8829' }} />
                                                                <Image source={require('../asset/star.png')} style={{ width: wp('5%'), height: wp('5%') }} />
                                                                <Image source={require('../asset/star.png')} style={{ width: wp('5%'), height: wp('5%') }} />
                                                                <Image source={require('../asset/star.png')} style={{ width: wp('5%'), height: wp('5%') }} />
                                                                <Image source={require('../asset/star.png')} style={{ width: wp('5%'), height: wp('5%') }} />
                                                            </>
                                                        )}
                                                        <Text style={[ c.light, { marginLeft: wp('2%') }]}>{item.bintang}.0</Text>
                                                    </View>
                                                </View>
                                                <View style={{ width: wp('25%'), alignItems: 'flex-end' }}>
                                                    <Text style={[ c.light, f._12, { marginTop: wp('7.3%') }]}>{this.hitung_hari(item.created_at)}</Text>
                                                </View>
                                            </View>

                                            <View style={{ width: '100%', marginTop: wp('2%'), marginBottom: wp('4%') }}>
                                                <Text style={[ c.light, { textAlign: 'justify' }]}>{item.ulasan}</Text>
                                            </View>
                                        </>
                                    )}
                                />
                            </View>
                        </View>
                    </View>
                </ScrollView>
            </View>
        )
    }
}
