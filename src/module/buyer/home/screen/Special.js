import React, { Component } from 'react'
import { Text, View, Image, ScrollView, TouchableOpacity, FlatList } from 'react-native'
import { blue, light } from '../utils/Color'
import { xs, vs } from '../utils/Responsive'
import { f, c, b, p } from '../utils/StyleHelper'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
import FastImage from 'react-native-fast-image'
import LinearGradient from 'react-native-linear-gradient'
import AsyncStorage from '@react-native-async-storage/async-storage'
import axios from 'axios'
import { API_LOCAL } from '@env'

export default class Special extends Component {
    constructor(props) {
        super(props);
        this.state = {
        	list_burger: [
        		{id: 1, nama: 'Burger Kentang Es teh', gambar: require('../asset/burger.png'), lokasi: 'Semarang', bintang: '5', ulasan: '10', harga_awal: '100000', diskon: '40%', harga_diskon: '60000'},
        		{id: 2, nama: 'Burger Kentang Es teh', gambar: require('../asset/burger.png'), lokasi: 'Semarang', bintang: '5', ulasan: '10', harga_awal: '100000', diskon: '40%', harga_diskon: '60000'},
        		{id: 3, nama: 'Burger Kentang Es teh', gambar: require('../asset/burger.png'), lokasi: 'Semarang', bintang: '5', ulasan: '10', harga_awal: '100000', diskon: '40%', harga_diskon: '60000'}
        	],
        	list_pizza: [
        		{id: 1, gambar: require('../asset/pizza.png'), harga_awal: '400000', diskon: '50%', harga_diskon: '200000'},
        		{id: 2, gambar: require('../asset/pizza.png'), harga_awal: '400000', diskon: '50%', harga_diskon: '200000'},
        		{id: 3, gambar: require('../asset/pizza.png'), harga_awal: '400000', diskon: '50%', harga_diskon: '200000'}
        	],
        	kupon: [
        		{id: 1, background: require('../asset/kupon_mock.png'), diskon: '10%OFF', des: 'Min. Belanja Rp 100K', tgl_berlaku: '31 Jul - 31 Agu, 2021'},
        		{id: 2, background: require('../asset/kupon_mock.png'), diskon: '10%OFF', des: 'Min. Belanja Rp 100K', tgl_berlaku: '31 Jul - 31 Agu, 2021'}
        	],
        	logo_brand_makanan: [
        		{id: 1, gambar: require('../asset/logo_makanan.png')}
        	],
        	promo: [
        		{id: 1, background: require('../asset/box_tax_makanan.png'), nama: 'Promo Hari ini'},
        		{id: 2, background: require('../asset/box_tax_makanan.png'), nama: 'Sedang Populer'},
        		{id: 3, background: require('../asset/box_tax_makanan.png'), nama: 'Brand Pilihan'},
        		{id: 4, background: require('../asset/box_tax_makanan.png'), nama: 'Diskon Spesial'}
        	]
        }
    }

    format(number) {
        if (number) {
			var rupiah = "";
			var numberrev = number
				.toString()
				.split("")
				.reverse()
				.join("");
			for (var i = 0; i < numberrev.length; i++)
				if (i % 3 == 0) rupiah += numberrev.substr(i, 3) + ".";
			return (
				"Rp " +
				rupiah
				.split("", rupiah.length - 1)
				.reverse()
				.join("")
			);
        } else {
            return (
                "Rp "+number
            );
        }
    }

    render() {
        return (
            <ScrollView style={{ backgroundColor: '#181B22' }} showsVerticalScrollIndicator={false}>
            	<LinearGradient
					start={{x: 0.0, y: 0.25}} end={{x: 0.4, y: 2.0}}
					locations={[0,0.4,0]}
					colors={[ '#121212', '#181B22', '#181B22' ]}
					style={{ width: '100%', alignItems: 'center' }}
            	>
            		<View style={[ b.mt4, b.mb4, { width: wp('90%'), flexDirection: 'row', alignItems: 'center' }]}>
            			<View style={{ width: wp('15%') }}>
            				<TouchableOpacity onPress={() => this.props.navigation.pop()}>
            					<FastImage source={require('../asset/left.png')} style={{ width: xs(25), height: xs(25) }} />
            				</TouchableOpacity>
            			</View>
            			<View style={{ width: wp('60%'), alignItems: 'center' }}>
            				<Text style={[ c.light, f._16, { fontFamily: 'Roboto-Bold' }]}>Spesial Minggu ini</Text>
            			</View>
            			<View style={{ width: wp('15%') }} />
            		</View>
            	</LinearGradient>

            	<FastImage source={require('../asset/bannersample1.png')} style={{ width: '100%', height: xs(120) }} />

            	<View style={[ b.mt4, { width: '100%', flexDirection: 'row' }]}>
            		<View style={{ width: wp('20%'), backgroundColor: '#FF8B12', alignItems: 'center', borderTopRightRadius: xs(5) }}>
            			<View style={[ b.roundedLow, { width: wp('15%'), backgroundColor: '#168336', alignItems: 'center', marginTop: xs(8), marginBottom: xs(8) }]}>
            				<Text style={[ c.light, f._18, { fontFamily: 'Roboto-Bold' }]}>40%</Text>
            				<Text style={[ f._18, { fontFamily: 'Roboto-Bold', color: '#ECF50E', marginTop: xs(-5) }]}>OFF</Text>
            			</View>
            		</View>

            		<View style={{ width: wp('80%'), backgroundColor: '#FF8B12', borderTopRightRadius: xs(5), marginTop: xs(5), justifyContent: 'center' }}>
        				<Text style={[ c.light, f._18, { fontFamily: 'Roboto-Bold' }]}>Diskon Spesial</Text>
        				<Text style={[ f._14, c.light, { fontFamily: 'Roboto-Regular' }]}>Hari ini</Text>
        			</View>
            	</View>
            	<View style={[ b.mt3, { width: '100%', marginLeft: wp('5%') }]}>
	            	<FlatList
	            		horizontal={true}
	            		showsHorizontalScrollIndicator={false}
	            		data={this.state.list_burger}
	            		keyExtractor={item => item.id}
	            		renderItem={({ item }) => (
	            			<View style={[ b.roundedLow, b.mr2, { width: wp('28%'), borderWidth: 1, borderColor: blue, backgroundColor: '#121212', alignItems: 'center' }]}>
	            				<View style={{ width: '100%', height: xs(80), backgroundColor: light, borderTopRightRadius: xs(5), borderTopLeftRadius: xs(5), alignItems: 'center', justifyContent: 'center' }}>
	            					<FastImage source={item.gambar} style={{ width: '100%', height: xs(65) }} />
	            				</View>

	            				<Text style={[ c.light, { fontFamily: 'Roboto-Bold', marginTop: xs(3) }]}>{this.format(item.harga_diskon)}</Text>
	            				<View style={[ b.mb2, b.mt1, { flexDirection: 'row', alignItems: 'center' }]}>
	            					<View style={{ backgroundColor: '#FF8B12', borderRadius: xs(3) }}>
	            						<Text style={[ c.light, f._10, { fontFamily: 'Roboto-Bold', marginTop: xs(1), marginBottom: xs(1), marginLeft: xs(2), marginRight: xs(2) }]}>{item.diskon}</Text>
	            					</View>
	            					<Text style={[ f._10, { fontFamily: 'Roboto-Regular', color: '#CCC', textDecorationLine: 'line-through', marginLeft: xs(3) }]}>{this.format(item.harga_awal)}</Text>
	            				</View>
	            			</View>
            			)}
	            	/>

	            	<Text style={[ c.light, b.mt3, b.mb3, { fontFamily: 'Roboto-Bold' }]}>Promo Diskon hari ini</Text>

	            	<FlatList
	            		horizontal={true}
	            		showsHorizontalScrollIndicator={false}
	            		data={this.state.kupon}
	            		keyExtractor={item => item.id}
	            		renderItem={({ item }) => (
	            			<FastImage source={item.background} style={[ b.mr2, { width: wp('43%'), height: xs(72), flexDirection: 'row' }]}>
	            				<View style={{ width: xs(44), alignItems: 'center', justifyContent: 'center' }}>
	            					<View style={{ width: xs(30), height: xs(30), borderRadius: xs(30), backgroundColor: light, alignItems: 'center', justifyContent: 'center' }}>
	            						<Image source={require('../asset/tokolokasi_icon.png')} style={{ width: xs(15), height: xs(15), tintColor: blue }} />
	            					</View>
            						<Text style={[ c.light, f._12, { fontFamily: 'Roboto-Regular', marginTop: xs(2) }]}>Toko</Text>
	            				</View>

	            				<View style={{ width: xs(104), alignItems: 'center', justifyContent: 'center', marginLeft: xs(2) }}>
	            					<Text style={[ c.blue, f._16, { fontFamily: 'Roboto-Bold' }]}>{item.diskon}</Text>
	            					<Text style={[ c.blue, f._10, { fontFamily: 'Roboto-Bold', marginTop: xs(-1) }]}>{item.des}</Text>
	            					<Text style={[ c.blue, f._10, { fontFamily: 'Roboto-Bold', marginTop: xs(-2) }]}>{item.tgl_berlaku}</Text>

	            					<TouchableOpacity style={{ width: xs(55), height: xs(22), backgroundColor: blue, borderRadius: xs(50), alignItems: 'center', justifyContent: 'center', marginTop: xs(1), marginBottom: xs(2) }}>
	            						<Text style={[ c.light, f._12, { fontFamily: 'Roboto-Regular' }]}>Ambil</Text>
	            					</TouchableOpacity>
	            				</View>
	            			</FastImage>
	            		)}
	            	/>
	            </View>

            	<View style={[ b.mt4, { width: wp('65%'), height: hp('6%'), backgroundColor: '#FF8B12', justifyContent: 'center', borderTopRightRadius: xs(25), borderBottomRightRadius: xs(25) }]}>
            		<Text style={[ c.light, { fontFamily: 'Roboto-Bold', marginLeft: wp('5%') }]}>Penawaran Brand hari ini</Text>
            	</View>
            	<View style={[ b.mt3, { width: '100%', alignItems: 'center' }]}>
            		<Image source={require('../asset/penawaran_brand_makanan.png')} style={{ width: wp('90%'), height: xs(116) }} />
            	</View>
        		<View style={[ b.mt3, { width: '100%', marginLeft: wp('5%') }]}>
            		<FlatList
	            		horizontal={true}
	            		showsHorizontalScrollIndicator={false}
	            		data={this.state.list_pizza}
	            		keyExtractor={item => item.id}
	            		renderItem={({ item }) => (
	            			<View style={[ b.roundedLow, b.mr2, { width: wp('28%'), borderWidth: 1, borderColor: blue, backgroundColor: '#121212', alignItems: 'center' }]}>
	            				<View style={{ width: '100%', backgroundColor: light, borderTopRightRadius: xs(5), borderTopLeftRadius: xs(5), alignItems: 'center', justifyContent: 'center' }}>
	            					<FastImage source={item.gambar} style={[ b.mt1, b.mb1, { width: '100%', height: xs(85) }]} />
	            				</View>

	            				<Text style={[ c.light, { fontFamily: 'Roboto-Bold', marginTop: xs(3) }]}>{this.format(item.harga_diskon)}</Text>
	            				<View style={[ b.mb2, b.mt1, { flexDirection: 'row', alignItems: 'center' }]}>
	            					<View style={{ backgroundColor: '#FF8B12', borderRadius: xs(3) }}>
	            						<Text style={[ c.light, f._10, { fontFamily: 'Roboto-Bold', marginTop: xs(1), marginBottom: xs(1), marginLeft: xs(2), marginRight: xs(2) }]}>{item.diskon}</Text>
	            					</View>
	            					<Text style={[ f._10, { fontFamily: 'Roboto-Regular', color: '#CCC', textDecorationLine: 'line-through', marginLeft: xs(3) }]}>{this.format(item.harga_awal)}</Text>
	            				</View>
	            			</View>
            			)}
	            	/>
	            </View>

            	<View style={[ b.mt4, b.mb3, { width: wp('65%'), height: hp('6%'), backgroundColor: '#FF8B12', justifyContent: 'center', borderTopRightRadius: xs(25), borderBottomRightRadius: xs(25) }]}>
            		<Text style={[ c.light, { fontFamily: 'Roboto-Bold', marginLeft: wp('5%') }]}>Beli langsung dari Brandnya</Text>
            	</View>
            	<FlatList
            		horizontal={true}
            		showsHorizontalScrollIndicator={false}
            		data={this.state.logo_brand_makanan}
            		keyExtractor={item => item.id}
            		renderItem={({ item }) => (
		        		<FastImage source={require('../asset/logo_makanan.png')} style={{ width: xs(373), height: xs(150), marginLeft: xs(18), marginRight: xs(18) }} />
            		)}
		        />
		        <View style={[ b.mt3, { marginLeft: wp('5%') }]}>
			        <FlatList
	            		horizontal={true}
	            		showsHorizontalScrollIndicator={false}
	            		data={this.state.promo}
	            		keyExtractor={item => item.id}
	            		renderItem={({ item }) => (
			        		<FastImage source={item.background} style={{ width: xs(85), height: xs(40), marginRight: wp('5%'), justifyContent: 'center' }}>
			        			<Text style={[ c.light, b.ml1, { fontFamily: 'Roboto-Bold' }]}>{item.nama}</Text>
			        		</FastImage>
	            		)}
			        />
		        </View>
		        <View style={[ b.mt3, { alignItems: 'center', marginLeft: wp('5%') }]}>
		        	<FlatList
	            		numColumns={2}
	            		data={this.state.list_burger}
	            		keyExtractor={item => item.id}
	            		renderItem={({ item }) => (
	            			<View style={[ b.roundedLow, { width: wp('42%'), borderWidth: 1, borderColor: blue, backgroundColor: '#121212', marginRight: wp('5%'), marginBottom: wp('5%') }]}>
	            				<View style={{ width: '100%', backgroundColor: light, borderTopRightRadius: xs(5), borderTopLeftRadius: xs(5), alignItems: 'center', justifyContent: 'center' }}>
	            					<FastImage source={item.gambar} style={[ b.mt2, b.mb2, { width: '100%', height: xs(90) }]} />
	            				</View>

	            				<Text style={[ c.light, b.ml1, { fontFamily: 'Roboto-Regular', marginTop: xs(3) }]}>{item.nama}</Text>
	            				<Text style={[ c.light, b.ml1, { fontFamily: 'Roboto-Bold' }]}>{this.format(item.harga_diskon)}</Text>
	            				<View style={[ b.mb1, b.mt1, b.ml1, { flexDirection: 'row', alignItems: 'center' }]}>
	            					<View style={{ backgroundColor: '#FF8B12', borderRadius: xs(3) }}>
	            						<Text style={[ c.light, f._10, { fontFamily: 'Roboto-Bold', marginTop: xs(1), marginBottom: xs(1), marginLeft: xs(2), marginRight: xs(2) }]}>{item.diskon}</Text>
	            					</View>
	            					<Text style={[ f._10, { fontFamily: 'Roboto-Regular', color: '#CCC', textDecorationLine: 'line-through', marginLeft: xs(3) }]}>{this.format(item.harga_awal)}</Text>
	            				</View>
	            				<View style={[ b.mb1, b.ml1, { flexDirection: 'row', alignItems: 'center' }]}>
	            					<FastImage source={require('../asset/tokolokasi_icon.png')} style={{ width: xs(20), height: xs(17) }} />
	            					<Text style={[ f._12, { fontFamily: 'Roboto-Regular', color: '#CCC', marginLeft: xs(3) }]}>{item.lokasi}</Text>
	            				</View>
	            				<View style={[ b.mb2, b.ml1, { flexDirection: 'row', alignItems: 'center' }]}>
	            					<Image source={require('../asset/star.png')} style={{ width: xs(15), height: xs(15), tintColor: 'darkorange' }} />
	            					<Text style={[ f._12, { fontFamily: 'Roboto-Regular', color: 'darkorange', marginLeft: xs(3) }]}>{item.bintang}.0</Text>
	            					<View style={{ width: xs(12), borderBottomWidth: 1, borderBottomColor: 'darkorange', transform: [{ rotate: '90deg' }] }} />
	            					<Text style={[ f._12, { fontFamily: 'Roboto-Regular', color: 'darkorange' }]}>{item.ulasan} Ulasan</Text>
	            				</View>
	            			</View>
            			)}
	            	/>
		        </View>
            </ScrollView>
        )
    }
}
