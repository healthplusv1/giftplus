import React, { Component } from 'react'
import { View, Text, Image, TouchableOpacity, ScrollView } from 'react-native'
import { blue, light } from '../utils/Color'
import { xs, vs } from '../utils/Responsive'
import { f, c, b, p } from '../utils/StyleHelper'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
import FastImage from 'react-native-fast-image'
import AsyncStorage from '@react-native-async-storage/async-storage'
import axios from 'axios'
import { API_LOCAL, API_URL } from '@env'

export default class DetailPromoCode extends Component {
	constructor(props) {
		super(props);
		this.state = {
		}
	}

	render() {
		return (
			<View style={{ width: '100%', height: '100%', backgroundColor: '#121212' }}>
				<View style={{ width: '100%', alignItems: 'center' }}>
					<View style={[ b.mt4, b.mb4, { width: wp('90%'), flexDirection: 'row', alignItems: 'center' }]}>
						<View style={{ width: wp('15%') }}>
							<TouchableOpacity onPress={() => this.props.navigation.pop()}>
								<Image source={require('../asset/left.png')} style={{ width: wp('8%'), height: wp('8%') }} />
							</TouchableOpacity>
						</View>
						<View style={{ width: wp('60%'), alignItems: 'center' }}>
							<Text style={[ c.light, f._16, { fontFamily: 'Roboto-Bold' }]}>Syarat dan Ketentuan</Text>
						</View>
						<View style={{ width: wp('15%') }}>
						</View>
					</View>
				</View>

				<ScrollView showsVerticalScrollIndicator={false} style={{ backgroundColor: '#181B22' }}>
					<View style={[ b.mt4, { width: '100%', alignItems: 'center' }]}>
						<View style={{ width: wp('90%'), flexDirection: 'row' }}>
							<View style={{ width: wp('5%') }}>
								<Text style={[ c.light, { fontFamily: 'Roboto-Regular' }]}>1.</Text>
							</View>
							<View style={{ width: wp('85%') }}>
								<Text style={[ c.light, { fontFamily: 'Roboto-Regular' }]}>Gratis ongkir hanya berlaku untuk pengguna baru.</Text>
							</View>
						</View>
						<View style={[ b.mt2, { width: wp('90%'), flexDirection: 'row' }]}>
							<View style={{ width: wp('5%') }}>
								<Text style={[ c.light, { fontFamily: 'Roboto-Regular' }]}>2.</Text>
							</View>
							<View style={{ width: wp('85%') }}>
								<Text style={[ c.light, { fontFamily: 'Roboto-Regular' }]}>Masa berlaku promo hinggal 31 Desember 2021.</Text>
							</View>
						</View>
						<View style={[ b.mt2, { width: wp('90%'), flexDirection: 'row' }]}>
							<View style={{ width: wp('5%') }}>
								<Text style={[ c.light, { fontFamily: 'Roboto-Regular' }]}>3.</Text>
							</View>
							<View style={{ width: wp('85%') }}>
								<Text style={[ c.light, { fontFamily: 'Roboto-Regular' }]}>Promo bisa digunakan untuk semua produk.</Text>
							</View>
						</View>
					</View>
				</ScrollView>
			</View>
		)
	}
}