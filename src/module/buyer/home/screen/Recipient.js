import React, { Component } from 'react'
import { Text, View, Image, TouchableOpacity, TextInput, ToastAndroid } from 'react-native'
import { blue, light } from '../utils/Color'
import { xs, vs } from '../utils/Responsive'
import { f, c, b, p } from '../utils/StyleHelper'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
import FastImage from 'react-native-fast-image'
import CheckBox from '@react-native-community/checkbox'
import DateTimePicker from '@react-native-community/datetimepicker'
import Share from 'react-native-share'

export default class Recipient extends Component {
    constructor(props) {
        super(props);
        this.state = {
            checked: false,
            startDate: new Date().setDate(new Date().getDate()),
            startTime: new Date().setDate(new Date().getDate()),
            startDateActive: false,
            startTimeActive: false,
            image_url: ''
        }
    }

    componentDidMount() {
        this.setState({
            image_url: this.props.navigation.getParam('imageURI')
        })
    }

    agreement() {
        if (this.state.checked === false) {
            this.setState({
                checked: true
            })
        }else{
            this.setState({
                checked: false
            })
        }
    }

    shareWhatsApp() {
        Share.shareSingle({
            title: 'Share via',
            message: 'some message',
            url: this.state.image_url,
            social: Share.Social.WHATSAPP,
        })
    }

    shareMessenger() {
        Share.shareSingle({
            title: 'Share via',
            message: 'some message',
            url: this.state.image_url,
            social: Share.Social.MESSENGER,
        })
    }

    shareTelegram() {
        Share.shareSingle({
            title: 'Share via',
            message: 'some message',
            url: this.state.image_url,
            social: Share.Social.TELEGRAM,
        })
    }

    shareEmail() {
        Share.shareSingle({
            title: 'Share via',
            message: 'some message',
            url: this.state.image_url,
            social: Share.Social.EMAIL,
        })
    }

    sharePesan() {
        Share.shareSingle({
            title: 'Share via',
            message: 'some message',
            url: this.state.image_url,
            social: Share.Social.SMS,
        })
    }

    render() {
        return (
            <View style={{ width: '100%', height: '100%', backgroundColor: '#181B22' }}>
                <View style={{ width: '100%', alignItems: 'center', backgroundColor: '#121212' }}>
                    <View style={[ b.mt4, b.mb4, { width: wp('90%'), flexDirection: 'row', alignItems: 'center' }]}>
                        <View style={{ width: wp('15%') }}>
                            <TouchableOpacity onPress={() => this.props.navigation.pop()}>
                                <FastImage source={require('../asset/left.png')} style={{ width: xs(25), height: xs(25) }} />
                            </TouchableOpacity>
                        </View>
                        <View style={{ width: wp('60%') }}>
                            <Text style={[ c.light, f._16, { fontFamily: 'Roboto-Bold' }]}>Kirim Giftcard</Text>
                        </View>
                        <View style={{ width: wp('15%') }}></View>
                    </View>
                </View>

                <View style={[ b.mb1, { width: '100%', backgroundColor: '#343E47', alignItems: 'center' }]}>
                    <View style={[ b.mt2, b.mb2, { width: wp('90%'), flexDirection: 'row', alignItems: 'center' }]}>
                        <View style={{ width: wp('45%') }}>
                            <Text style={[ c.light, { fontFamily: 'Roboto-Regular' }]}>Kirim Nanti</Text>
                        </View>
                        <View style={{ width: wp('45%'), alignItems: 'flex-end' }}>
                            <CheckBox
                                style={{ zIndex: 1 }}
                                tintColors={{ false: light, true: blue }}
                                value={this.state.checked}
                                onChange={() => this.agreement()}
                            />
                            {this.state.checked === true && (
                                <View style={{ width: xs(13), height: xs(13), backgroundColor: light, position: 'absolute', top: xs(9), right: xs(9) }} />
                            )}
                        </View>
                    </View>
                </View>
                {this.state.checked === true && (
                    <View style={[ b.mb4, { width: '100%', alignItems: 'center' }]}>
                        <View style={[ b.mt3, { width: wp('90%'), flexDirection: 'row', alignItems: 'center' }]}>
                            <TouchableOpacity onPress={() => this.setState({ startDateActive: true })} style={{ height: hp('5%'), backgroundColor: '#121212', borderWidth: 1, borderColor: blue, borderRadius: xs(5), flexDirection: 'row', alignItems: 'center' }}>
                                <Text style={[ c.light, b.ml1, { fontFamily: 'Roboto-Regular' }]}>{typeof this.state.startDate === 'object' ? new Date(Date.parse(this.state.startDate)).getDate()+'/'+(new Date(Date.parse(this.state.startDate)).getMonth()+1)+'/'+new Date(Date.parse(this.state.startDate)).getFullYear() : new Date(this.state.startDate).getDate()+'/'+(new Date(this.state.startDate).getMonth()+1)+'/'+new Date(this.state.startDate).getFullYear()}</Text>
                                <Image source={require('../asset/tanggal.png')} style={[ b.ml1, b.mr1, { width: xs(18), height: xs(18), tintColor: blue }]} />
                            </TouchableOpacity>
                            {this.state.startDateActive === true && (
                                <DateTimePicker
                                    mode="date"
                                    value={new Date()}
                                    minimumDate={new Date()}
                                    onChange={data => {
                                        if (data.type === 'dismissed') {
                                            this.setState({
                                                startDateActive: false
                                            })
                                        } else {
                                            // console.log(data.nativeEvent.timestamp)
                                            this.setState({
                                                startDate: data.nativeEvent.timestamp,
                                                startDateActive: false
                                            })
                                        }
                                    }}
                                />
                            )}

                            <Text style={[ c.light, b.ml2, b.mr2, { fontFamily: 'Roboto-Regular' }]}>-</Text>

                            <TouchableOpacity onPress={() => this.setState({ startTimeActive: true })} style={{ height: hp('5%'), backgroundColor: '#121212', borderWidth: 1, borderColor: blue, borderRadius: xs(5), flexDirection: 'row', alignItems: 'center' }}>
                                <Text style={[ c.light, b.ml1, { fontFamily: 'Roboto-Regular' }]}>{typeof this.state.startTime === 'object' ? new Date(Date.parse(this.state.startTime)).getHours()+':'+new Date(Date.parse(this.state.startTime)).getMinutes()+':'+new Date(Date.parse(this.state.startTime)).getSeconds() : new Date(this.state.startTime).getHours()+':'+new Date(this.state.startTime).getMinutes()+':'+new Date(this.state.startTime).getSeconds()}</Text>
                                <Image source={require('../asset/estimated_icon.png')} style={[ b.ml2, b.mr1, { width: xs(18), height: xs(18), tintColor: blue }]} />
                            </TouchableOpacity>
                            {this.state.startTimeActive === true && (
                                <DateTimePicker
                                    mode="time"
                                    value={new Date()}
                                    onChange={data => {
                                        if (data.type === 'dismissed') {
                                            this.setState({
                                                startTimeActive: false
                                            })
                                        } else {
                                            this.setState({
                                                startTime: data.nativeEvent.timestamp,
                                                startTimeActive: false
                                            })
                                        }
                                    }}
                                />
                            )}
                        </View>
                    </View>
                )}

                <View style={{ width: '100%', backgroundColor: '#343E47', alignItems: 'center' }}>
                    <View style={[ b.mt2, b.mb2, { width: wp('90%') }]}>
                        <Text style={[ c.light, { fontFamily: 'Roboto-Regular' }]}>Kirim dengan</Text>
                    </View>
                </View>
                <View style={[ b.mb4, { width: '100%', alignItems: 'center' }]}>
                    <View style={[ b.mt4, { width: wp('90%'), flexDirection: 'row' }]}>
                        <TouchableOpacity onPress={() => this.shareWhatsApp()} style={{ width: wp('22.5%'), alignItems: 'center' }}>
                            <FastImage source={require('../asset/whatsapp.png')} style={{ width: xs(40), height: xs(40) }} />
                            <Text style={[ p.textCenter, c.light, b.mt1 ]}>WhatsApp</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => ToastAndroid.show('the package still has problem to send by Facebook Messenger', ToastAndroid.SHORT)} style={{ width: wp('22.5%'), alignItems: 'center' }}>
                            <FastImage source={require('../asset/facebookmessager.png')} style={{ width: xs(40), height: xs(40) }} />
                            <Text style={[ p.textCenter, c.light, b.mt1 ]}>Facebook Messenger</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.shareTelegram()} style={{ width: wp('22.5%'), alignItems: 'center' }}>
                            <FastImage source={require('../asset/telegram.png')} style={{ width: xs(40), height: xs(40) }} />
                            <Text style={[ p.textCenter, c.light, b.mt1 ]}>Telegram</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.shareEmail()} style={{ width: wp('22.5%'), alignItems: 'center' }}>
                            <FastImage source={require('../asset/email.png')} style={{ width: xs(40), height: xs(40) }} />
                            <Text style={[ p.textCenter, c.light, b.mt1 ]}>Email</Text>
                        </TouchableOpacity>
                    </View>

                    <View style={[ b.mt3, { width: wp('90%'), flexDirection: 'row' }]}>
                        <TouchableOpacity onPress={() => ToastAndroid.show('the package still has problem to send by SMS', ToastAndroid.SHORT)} style={{ width: wp('22.5%'), alignItems: 'center' }}>
                            <FastImage source={require('../asset/pesan.png')} style={{ width: xs(40), height: xs(40) }} />
                            <Text style={[ p.textCenter, c.light, b.mt1 ]}>Pesan</Text>
                        </TouchableOpacity>
                    </View>
                </View>

                {/*<TouchableOpacity style={[ b.roundedLow, { width: wp('90%'), height: hp('7.5%'), backgroundColor: blue, alignItems: 'center', justifyContent: 'center', position: 'absolute', zIndex: 1, bottom: wp('5%'), left: wp('5%') }]}>
                    <Text style={[ c.light, f._16 ]}>Kirim</Text>
                </TouchableOpacity>*/}
            </View>
        )
    }
}
