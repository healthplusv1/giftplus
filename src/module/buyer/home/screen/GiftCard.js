import React, { Component } from 'react'
import { Text, View, Image, ScrollView, TouchableOpacity, TextInput, FlatList, RefreshControl, Animated } from 'react-native'
import { blue, light } from '../utils/Color'
import { xs, vs } from '../utils/Responsive'
import { f, c, b, p } from '../utils/StyleHelper'
import FastImage from 'react-native-fast-image'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
import AsyncStorage from '@react-native-async-storage/async-storage'
import axios from 'axios'
import { API_LOCAL, API_URL } from '@env'

var isHidden = true;

export default class Gift extends Component {
	constructor(props) {
		super(props);
		this.state = {
            loading: false,
			count: '',
			giftCard: [
				{id: 1, image: require('../asset/giftcardpreview1.png'), title: 'Selamat ulang tahun'},
				{id: 2, image: require('../asset/giftcardpreview1.png'), title: 'Selamat ulang tahun'},
				{id: 3, image: require('../asset/giftcardpreview1.png'), title: 'Selamat ulang tahun'}
			],
            template: [
                {id: 1, image: require('../asset/giftcard1.png')},
                {id: 2, image: require('../asset/giftcard2.png')},
                {id: 3, image: require('../asset/giftcard3.png')}
            ],
            hidden: true,
            value: new Animated.Value(100)
		}
	}

	async componentDidMount() {
        this.setState({ loading: true })
		const id_login = await AsyncStorage.getItem('@id_login')

        if (id_login === null) {
            const guest = await AsyncStorage.getItem('id_login_guest')

            axios.get(API_URL+'/main/shopping/cart', {params: {
                id_login: guest
            }})
            .then(res => {
                this.setState({
                    count: res.data.data.length,
                    loading: false
                })
            })
        } else {
    		axios.get(API_URL+'/main/shopping/cart', {params: {
    			id_login: id_login
    		}})
    		.then(res => {
    			this.setState({
    				count: res.data.data.length,
                    loading: false
    			})
    		})
        }
	}

    toogleShow() {
        var toValue = 100;

        if (isHidden) {
            toValue = 0
        }

        Animated.spring(
            this.state.value,
            {
                toValue: toValue,
                velocity: 3,
                tension: 2,
                friction: 8
            }
        ).start();

        isHidden = !isHidden;
    }

    render() {
        return (
            <View style={{ width: '100%', height: '100%', backgroundColor: '#121212' }}>
            	<TouchableOpacity onPress={() => this.setState({ hidden: false })} style={{ width: xs(50), height: xs(50), backgroundColor: blue, borderRadius: xs(50), position: 'absolute', right: xs(20), bottom: xs(20), zIndex: 1, alignItems: 'center', justifyContent: 'center' }}>
            		<Image source={require('../asset/plus.png')} style={{ width: xs(25), height: xs(25), tintColor: light }} />
            	</TouchableOpacity>

                {this.state.hidden === false && (
                    <View style={{ width: '100%', height: hp('80%'), alignItems: 'center', position: 'absolute', backgroundColor: '#343E47', zIndex: 2, marginTop: xs(115) }}>
                        <TouchableOpacity onPress={() => console.log('fanfnjfdkafnadsadkasldasdsad')} style={[ b.mt3, { width: xs(35), height: xs(5), backgroundColor: light }]} />
                        <View style={[ b.mt4, { width: wp('90%') }]}>
                            <Text style={[ c.light, f._16, b.mb4, { fontFamily: 'Roboto-Bold' }]}>Template Giftcard</Text>

                            <FlatList
                                numColumns={2}
                                data={this.state.template}
                                keyExtractor={item => item.id}
                                renderItem={({ item }) => (
                                    <TouchableOpacity onPress={() => this.props.navigation.push('DesignGiftCard', {id_gift: item.id})} style={[ b.mr2, b.mb2 ]}>
                                        <Image source={item.image} style={[ b.roundedLow, { width: wp('43%'), height: xs(100) }]} />
                                    </TouchableOpacity>
                                )}
                            />
                        </View>
                    </View>
                )}

                <View style={{ width: '100%', alignItems: 'center' }}>
                	<View style={[ b.mt4, b.mb4, p.row, { width: wp('90%') }]}>
                		<View style={{ width: wp('45%') }}>
                			<FastImage source={require('../asset/giftplus.png')} style={{ width: xs(120), height: xs(32) }} />
                		</View>
                		<View style={{ width: wp('45%'), flexDirection: 'row-reverse' }}>
                			<TouchableOpacity onPress={() => this.props.navigation.push('Drawer')}>
                				<Image source={require('../asset/menu.png')} style={{ width: xs(22), height: xs(22), tintColor: light }} />
                			</TouchableOpacity>
                			<TouchableOpacity onPress={() => this.props.navigation.push('Notification')} style={[ b.mr2 ]}>
                				<Image source={require('../asset/notification.png')} style={{ width: xs(22), height: xs(22), tintColor: light }} />
                			</TouchableOpacity>
                			<TouchableOpacity onPress={() => this.props.navigation.push('Cart')} style={[ b.mr2 ]}>
                				<Image source={require('../asset/cart.png')} style={{ width: xs(19), height: xs(20), tintColor: light }} />
                                {this.state.count !== 0 && (
    								<View style={{ width: xs(12), height: xs(12), borderRadius: xs(12), backgroundColor: 'darkorange', position: 'absolute', right: 0, alignItems: 'center', justifyContent: 'center' }}>
                                        <Text style={[ c.light, f._10 ]}>{this.state.count}</Text>
                                    </View>
                                )}
                			</TouchableOpacity>
                		</View>
                	</View>

                	<View style={[ b.mb4, { width: wp('90%') }]}>
                		<Text style={[ c.light, f.bold, f._16 ]}>Pilih giftcard</Text>
                	</View>
                </View>

                <ScrollView showsVerticalScrollIndicator={false} refreshControl={<RefreshControl onRefresh={() => this.componentDidMount()} refreshing={this.state.loading} />}>
                	<View style={{ width: '100%', alignItems: 'center' }}>
                		<FlatList
                			numColumns={2}
                			data={this.state.giftCard}
                			keyExtractor={item => item.id}
                			renderItem={({ item }) => (
                				<TouchableOpacity onPress={() => this.props.navigation.push('Recipient')} style={{ width: wp('42.5%'), marginRight: wp('5%'), marginBottom: wp('5%'), backgroundColor: '#181B22' }}>
                					<View style={[ b.mt1, b.mb1, { width: '100%', flexDirection: 'row', alignItems: 'center' }]}>
                						<View style={{ width: wp('38%'), alignItems: 'flex-end' }}>
                							<Text style={[ c.light, f._12, b.mr1 ]}>{item.title}</Text>
                						</View>
                						<View style={{ width: wp('4.5%'), alignItems: 'center' }}>
                							<TouchableOpacity>
                								<Image source={require('../asset/menuVertical.png')} style={{ width: xs(15), height: xs(15), tintColor: blue }} />
                							</TouchableOpacity>
                						</View>
                					</View>
                					<FastImage source={item.image} style={[ b.roundedLow, { width: '100%', height: xs(101) }]} />
                				</TouchableOpacity>
            				)}
                		/>
                	</View>
                </ScrollView>
            </View>
        )
    }
}
