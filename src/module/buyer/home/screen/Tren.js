import React, { Component } from 'react'
import { View, Text, Image, TouchableOpacity } from 'react-native'
import { blue, light } from '../utils/Color'
import { xs, vs } from '../utils/Responsive'
import { f, c, b, p } from '../utils/StyleHelper'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
import FastImage from 'react-native-fast-image'
import LinearGradient from 'react-native-linear-gradient'
import Swiper from 'react-native-swiper'
import AsyncStorage from '@react-native-async-storage/async-storage'
import axios from 'axios'
import { API_LOCAL, API_URL } from '@env'

export default class Tren extends Component {
	constructor(props) {
		super(props);
		this.state = {
		}
	}

	render() {
		return (
			<LinearGradient start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }} colors={[ '#181C21', '#252F38', '#252F38' ]} style={{ width: '100%', height: '100%' }}>
				<View style={{ width: '100%', backgroundColor: '#121212', alignItems: 'center' }}>
					<View style={{ width: wp('90%'), height: hp('10%'), flexDirection: 'row', alignItems: 'center' }}>
						<View style={{ width: wp('15%') }}>
							<TouchableOpacity onPress={() => this.props.navigation.pop()}>
								<FastImage source={require('../asset/left.png')} style={{ width: xs(25), height: xs(25) }} />
							</TouchableOpacity>
						</View>
						<View style={{ width: wp('60%'), alignItems: 'center' }}>
							<Text style={[ c.light, f._16, { fontFamily: 'Roboto-Bold' }]}>PEMBERITAHUAN PENJUAL</Text>
						</View>
						<View style={{ width: wp('15%') }}>
						</View>
					</View>
				</View>

				<View style={{ width: '100%', alignItems: 'center' }}>
					<View style={[ b.rounded, { width: xs(320), height: xs(130), backgroundColor: '#323232', marginTop: vs(25), flexDirection: 'row', alignItems: 'center' }]}>
						<FastImage source={require('../asset/edu4.png')} style={[ b.ml2, { width: xs(90), height: xs(75) }]} />
						<View style={[ b.ml2, { width: xs(180) }]}>
							<Text style={[ c.light, p.textCenter, f._18, { fontFamily: 'Roboto-Regular' }]}>Memperkirakan tren</Text>
							<Text style={[ c.light, p.textCenter, f._18, b.mt4, { fontFamily: 'Roboto-Regular' }]}>mengidentifikasi pasar</Text>
						</View>
					</View>
				</View>

				<Swiper
					loop={false}
					dotColor={'#CCC'}
					showsButtons={true}
					buttonWrapperStyle={{ backgroundColor: 'transparent', flexDirection: 'row', position: 'absolute', top: xs(175), left: 0, flex: 1, paddingHorizontal: 10, paddingVertical: 10, justifyContent: 'space-between', alignItems: 'center' }}
					nextButton={<Text style={[ c.blue, f._18, { fontFamily: 'Roboto-Bold' }]}>Next</Text>}
					prevButton={<Text style={[ c.blue, f._18, { fontFamily: 'Roboto-Bold' }]}>Prev</Text>}
				>
					<View style={{ width: '100%', height: '100%', alignItems: 'center' }}>
						<FastImage source={require('../asset/graphic.png')} style={{ width: xs(300), height: xs(300) }} />

						<Text style={[ c.light, f._16, { fontFamily: 'Roboto-Bold', marginTop: vs(-30) }]}>Kriteria Utama</Text>
						<View style={{ width: xs(320) }}>
							<View style={{ width: xs(6), height: xs(6), backgroundColor: light, borderRadius: xs(6), position: 'absolute', left: vs(65), top: vs(9) }} />
							<Text style={[ c.light, p.textCenter, f._16, { fontFamily: 'Roboto-Regular' }]}>Menampilkan grafik untuk</Text>
							<Text style={[ c.light, p.textCenter, f._16, { fontFamily: 'Roboto-Regular' }]}>menyimpulkan performa toko Anda</Text>
						</View>
					</View>

					<View style={{ width: '100%', height: '100%', alignItems: 'center' }}>
						<FastImage source={require('../asset/products.png')} style={[ b.mt4, { width: xs(250), height: xs(250) }]} />

						<View style={{ width: xs(320), marginTop: vs(25) }}>
							<View style={{ width: xs(6), height: xs(6), backgroundColor: light, borderRadius: xs(6), position: 'absolute', left: vs(48), top: vs(9) }} />
							<Text style={[ c.light, p.textCenter, f._16, { fontFamily: 'Roboto-Regular' }]}>Menampilkan tren berdasarkan</Text>
							<Text style={[ c.light, p.textCenter, f._16, { fontFamily: 'Roboto-Regular' }]}>kriteria utama yang dipilih Penjual</Text>
						</View>
					</View>
				</Swiper>
			</LinearGradient>
		)
	}
}