import React, { Component } from 'react'
import { View, Text, Image, TouchableOpacity, FlatList, ScrollView } from 'react-native'
import { blue, light, softBlue } from '../utils/Color'
import { xs, vs } from '../utils/Responsive'
import { f, c, b, p } from '../utils/StyleHelper'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
import FastImage from 'react-native-fast-image'
import QRCodeScanner from 'react-native-qrcode-scanner'
import { NavigationEvents } from 'react-navigation'
import AsyncStorage from '@react-native-async-storage/async-storage'
import axios from 'axios'
import { API_LOCAL, API_URL } from '@env'

export default class ScanQR extends Component {
	constructor(props) {
		super(props);
		this.state = {
			scan: [
                {title: 'Scan QR'},
                {title: 'Riwayat Scan'}
            ],
            clicked: 0,
            scanner: true,
            ScanResult: false,
            result: null
		}
	}

	async swicth(index) {
        await this.setState({
            clicked: index
        })
    }

    onSuccess = (e) => {
        const check = e.data.substring(0, 4);
        console.log('scanned data' + check);
        this.setState({
            result: e,
            scanner: false
        })
        if (check === 'http') {
            // Linking.openURL(e.data).catch(err => console.error('An error occured', err));
            this.props.navigation.push('ScanPreview', {scanView: e.data})
            // console.log(e.data)
        } else {
            this.setState({
                result: e,
                scanner: false
            })
        }
    }

	render() {
		return (
			<>
				<NavigationEvents onDidFocus={() => this.setState({ scanner: true })} />
				<View style={{ width: '100%', height: '100%', backgroundColor: '#181B22' }}>
					<FlatList
	                    horizontal={true}
	                    data={this.state.scan}
	                    keyExtractor={item => item.title}
	                    renderItem={({ item, index }) => (
	                        (this.state.clicked === index ?
	                            <TouchableOpacity onPress={() => this.swicth(index)} style={{ width: wp('50%'), height: hp('8%'), backgroundColor: blue, borderBottomWidth: 4, borderBottomColor: softBlue, alignItems: 'center', justifyContent: 'center' }}>
	                                <Text style={[ c.light, { fontFamily: 'Roboto-Bold' }]}>{item.title}</Text>
	                            </TouchableOpacity>
	                            :
	                            <TouchableOpacity onPress={() => this.swicth(index)} style={{ width: wp('50%'), height: hp('8%'), backgroundColor: '#121212', alignItems: 'center', justifyContent: 'center' }}>
	                                <Text style={[ c.light, { fontFamily: 'Roboto-Bold' }]}>{item.title}</Text>
	                            </TouchableOpacity>
	                        )
	                    )}
	                />

	                {this.state.clicked === 0 && (
	                	<ScrollView>
		                	{this.state.scanner &&
		                        <QRCodeScanner
		                            reactivate={true}
		                            showMarker={true}
		                            ref={(node) => { this.scanner = node }}
		                            onRead={this.onSuccess}
		                        />
		                    }
		                    {/*{this.state.ScanResult &&
		                        <View style={{ width: '100%', height: xs(250), justifyContent: 'center', alignItems: 'center', backgroundColor: light }}>
		                            <Text>Result</Text>
		                            <View style={this.state.ScanResult ? {width: xs(250), backgroundColor: light} : {width: xs(200), backgroundColor: light}}>
		                                <Text>Type : {this.state.result.type}</Text>
		                                <Text>Result : {this.state.result.data}</Text>
		                                <Text numberOfLines={1}>RawData: {this.state.result.rawData}</Text>
		                                <TouchableOpacity onPress={this.scanAgain} style={[ b.mt4, b.roundedLow, { backgroundColor: blue, alignItems: 'center', justifyContent: 'center' }]}>
		                                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
		                                        <Image source={require('../asset/camera.png')} style={{height: 36, width: 36, tintColor: light}}></Image>
		                                        <Text style={[ b.ml2, c.light ]}>Click to scan again</Text>
		                                    </View>
		                                </TouchableOpacity>
		                            </View>
		                        </View>
		                    }*/}
		                    {/*<View style={{ width: '100%', alignItems: 'center' }}>
		                		<View style={{ width: xs(250), height: xs(250), borderWidth: 1, borderColor: light }}>
		                			<View style={{ width: '100%', borderBottomWidth: 1, borderBottomColor: light, marginTop: xs(125) }} />
		                		</View>
		                	</View>*/}
	                	</ScrollView>
	                )}

	                {this.state.clicked === 1 && (
	                	<ScrollView>
		                	<View style={{ width: '100%', alignItems: 'center' }}>
		                		<Image source={require('../asset/activity-history.png')} style={{ width: xs(100), height: xs(100), tintColor: '#FFF' }} />
		                        <Text style={[ c.light, b.mt2 ]}>Tidak ada History</Text>
		                	</View>
	                	</ScrollView>
	                )}
				</View>
			</>
		)
	}
}

