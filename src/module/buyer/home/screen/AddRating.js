import React, { Component } from 'react'
import { Text, View, Image, TouchableOpacity, TextInput, StyleSheet, Easing, ToastAndroid } from 'react-native'
import { xs, vs } from '../utils/Responsive'
import { f, c, b, p } from '../utils/StyleHelper'
import { blue, light } from '../utils/Color'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
import FastImage from 'react-native-fast-image'
import LinearGradient from 'react-native-linear-gradient'
import CustomizedStarRating from 'react-native-customized-star-rating'
import AsyncStorage from '@react-native-async-storage/async-storage'
import axios from 'axios'
import { API_LOCAL, API_URL } from '@env'

export default class AddRating extends Component {
  constructor(props) {
    super(props);
    this.state = {
      filledStar: 0,
    };
  }

  async clickStar(j) {
    await this.setState({ filledStar: j })
    // console.log(this.state.filledStar)
  }

  render() {
    return (
      <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }} colors={[ '#181C21', '#252F38', '#252F38' ]} style={[ b.mb2, { width: '100%', height: '100%' }]}>
        <View style={{ width: '100%', alignItems: 'center', backgroundColor: '#121212' }}>
          <View style={[ b.mt4, b.mb4, { width: wp('90%'), flexDirection: 'row', alignItems: 'center' }]}>
            <View style={{ width: wp('15%') }}>
              <TouchableOpacity onPress={() => this.props.navigation.pop()}>
                <FastImage source={require('../asset/left.png')} style={{ width: xs(25), height: xs(25) }} />
              </TouchableOpacity>
            </View>
            <View style={{ width: wp('60%'), alignItems: 'center' }}>
              <Text style={[ c.light, f._16, { fontFamily: 'Roboto-Bold' }]}>Ulasan</Text>
            </View>
            <View style={{ width: wp('15%') }}>
            </View>
          </View>
        </View>

        <View style={[ b.mt4, { width: '100%', alignItems: 'center' }]}>
          <View style={{ width: xs(80), height: xs(80), borderRadius: xs(80), backgroundColor: light, alignItems: 'center', justifyContent: 'center' }}>
            <Image source={require('../asset/user.png')} style={{ width: xs(60), height: xs(60), tintColor: '#CCC' }} />
          </View>
          <View style={[ b.mt3, b.mb4, { width: wp('60%') }]}>
            <Text style={[ p.textCenter, c.light, f._16, { fontFamily: 'Roboto-Regular' }]}>Bagikan pengalaman anda setelah berbelanja disini</Text>
          </View>

          <CustomizedStarRating
            noOfStars={'5'}
            starRowStyle={styles.starRowStyle}
            starSizeStyle={styles.starSizeStyle}
            selectedStar={this.state.filledStar}
            starAnimationScale={1.15}
            animationDuration={300}
            easingType={Easing.easeInCirc}
            emptyStarImagePath={require('../asset/star.png')}
            filledStarImagePath={require('../asset/staredd.png')}
            onClickFunc={(i) => this.clickStar(i)}
          />
          {this.state.filledStar === 1 && (
            <Text style={[ c.light, { marginTop: wp('1%') }]}>OK</Text>
          )}
          {this.state.filledStar === 2 && (
            <Text style={[ c.light, { marginTop: wp('1%') }]}>Hmm...</Text>
          )}
          {this.state.filledStar === 3 && (
            <Text style={[ c.light, { marginTop: wp('1%') }]}>Good</Text>
          )}
          {this.state.filledStar === 4 && (
            <Text style={[ c.light, { marginTop: wp('1%') }]}>Very Good</Text>
          )}
          {this.state.filledStar === 5 && (
            <Text style={[ c.light, { marginTop: wp('1%') }]}>Amazing</Text>
          )}

          <View style={{ width: wp('90%'), marginTop: wp('10%'), flexDirection: 'row', alignItems: 'center' }}>
              <View style={{ width: wp('45%') }}>
                  <Text style={[ c.light, { fontFamily: 'Roboto-Bold' }]}>Tulis Ulasan</Text>
              </View>
              <View style={{ width: wp('45%'), alignItems: 'center' }}></View>
          </View>
          <View style={{ width: wp('90%'), height: hp('20%'), borderRadius: xs(3), backgroundColor: light, marginTop: wp('2%'), alignItems: 'center' }}>
            <TextInput
              placeholder='Input Text here'
              placeholderTextColor='#121212'
              style={{ width: wp('85%') }}
              multiline={true}
              onChangeText={(text) => this.setState({ ulasan: text })}
            />
          </View>

          <TouchableOpacity onPress={() => ToastAndroid.show('still not implemented', ToastAndroid.SHORT)} style={{ width: wp('35%'), height: hp('6%'), backgroundColor: blue, alignItems: 'center', justifyContent: 'center', marginTop: wp('10%'), borderRadius: wp('2%') }}>
              <Text style={[ c.light, { fontFamily: 'Roboto-Bold' }]}>Kirim Ulasan</Text>
          </TouchableOpacity>
        </View>
      </LinearGradient>
    )
  }
}

const styles = StyleSheet.create({
  starRowStyle: {
    flexDirection: 'row'
  },

  starSizeStyle: {
    width: wp('10%'),
    height: wp('10%'),
    marginRight: vs(5)
  }
})