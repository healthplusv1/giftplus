import React, { Component } from 'react'
import { View, Text, Image, TouchableOpacity, ScrollView, TextInput, StyleSheet, ToastAndroid } from 'react-native'
import { blue, light } from '../utils/Color'
import { xs, vs } from '../utils/Responsive'
import { f, c, b, p } from '../utils/StyleHelper'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
import FastImage from 'react-native-fast-image'
import LinearGradient from 'react-native-linear-gradient'
import AsyncStorage from '@react-native-async-storage/async-storage'
import axios from 'axios'
import { API_LOCAL, API_URL } from '@env'

export default class TopUp extends Component {
	constructor(props) {
		super(props);
		this.state = {
			amount: ''
		}
	}

	async selectingPayment(payment_method) {
		const id_login = await AsyncStorage.getItem('@id_login')
		console.log(this.state.amount)
		axios.get(API_URL+'/main/duitku/topup/'+id_login+'/'+payment_method+'/60', {params:{
			amount: this.state.amount
		}})
		.then(res => {
			console.log("TOPUP ==> ", res.data.data)
            ToastAndroid.show('memuat permintaan pembayaran', ToastAndroid.SHORT)
            this.props.navigation.push('PaymentMethod', {duitku: res.data.data})
		}).catch(err => {
			console.log("TOPUP FAILURE ==> ", err)
		})
    }

	render() {
		return (
			<LinearGradient start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }} colors={[ '#181C21', '#252F38', '#252F38' ]} style={{ width: '100%', height: '100%' }}>
				<View style={{ width: '100%', alignItems: 'center', backgroundColor: '#121212' }}>
					<View style={[ b.mt4, b.mb4, { width: wp('90%'), flexDirection: 'row', alignItems: 'center' }]}>
						<View style={{ width: wp('15%') }}>
							<TouchableOpacity onPress={() => this.props.navigation.pop()}>
								<FastImage source={require('../asset/left.png')} style={{ width: xs(25), height: xs(25) }} />
							</TouchableOpacity>
						</View>
						<View style={{ width: wp('60%'), alignItems: 'center' }}>
							<Text style={[ c.light, f._16, { fontFamily: 'Roboto-Bold' }]}>Top Up</Text>
						</View>
						<View style={{ width: wp('15%') }}>
						</View>
					</View>
				</View>

				<ScrollView showsVerticalScrollIndicator={false}>
					<View style={{ width: '100%', alignItems: 'center' }}>
						<View style={[ b.mt4, { width: wp('85%') }]}>
							<View style={{ width: wp('85%') }}>
	                            <Text style={[ c.light, f._16, { fontFamily: 'Roboto-Bold' }]}>Jumlah</Text>
	                            <View style={[ b.mt1, b.roundedLow, { width: '100%', backgroundColor: light }]}>
		                            <TextInput
		                            	style={[ b.ml1 ]}
		                            	placeholder='0'
		                            	keyboardType={"phone-pad"}
		                            	value={this.state.amount}
		                            	onChangeText={(text) => this.setState({ amount: text })}
		                            />
	                            </View>

	                            <Text style={[ c.light, f._16, b.mt4, { fontFamily: 'Roboto-Bold' }]}>Metode pembayaran lain</Text>
	                            <View style={[ b.mt2 ]} />
	                            <TouchableOpacity style={[ styles.bankItem, b.roundedLow, b.mb2, p.row ]} onPress={() => this.selectingPayment('VA')}>
	                                <View style={[ p.center, { width: xs(100) }]}>
	                                    <Image source={require('../asset/maybank.png')} style={{ width: xs(80), height: xs(20) }} />
	                                </View>
	                                <View style={[ b.ml2, { width: xs(130), justifyContent: 'center' }]}>
	                                    <Text style={{ fontFamily: 'Roboto-Regular' }}>Maybank</Text>
	                                </View>
	                            </TouchableOpacity>
	                            <TouchableOpacity style={[ styles.bankItem, b.roundedLow, b.mb2, p.row ]} onPress={() => this.selectingPayment('BT')}>
	                                <View style={[ p.center, { width: xs(100) }]}>
	                                    <Image source={require('../asset/permata_bank.png')} style={{ width: xs(80), height: xs(25) }} />
	                                </View>
	                                <View style={[ b.ml2, { width: xs(130), justifyContent: 'center' }]}>
	                                    <Text style={{ fontFamily: 'Roboto-Regular' }}>Permata Bank</Text>
	                                </View>
	                            </TouchableOpacity>
	                            <TouchableOpacity style={[ styles.bankItem, b.roundedLow, b.mb2, p.row ]} onPress={() => this.selectingPayment('B1')}>
	                                <View style={[ p.center, { width: xs(100) }]}>
	                                    <Image source={require('../asset/cimb3x.png')} style={{ width: xs(80), height: xs(20) }} />
	                                </View>
	                                <View style={[ b.ml2, { width: xs(130), justifyContent: 'center' }]}>
	                                    <Text style={{ fontFamily: 'Roboto-Regular' }}>CIMB Niaga</Text>
	                                </View>
	                            </TouchableOpacity>
	                            <TouchableOpacity style={[ styles.bankItem, b.roundedLow, b.mb2, p.row ]} onPress={() => this.selectingPayment('A1')}>
	                                <View style={[ p.center, { width: xs(100) }]}>
	                                    <Image source={require('../asset/atm_bersama.png')} style={{ width: xs(80), height: xs(20) }} />
	                                </View>
	                                <View style={[ b.ml2, { width: xs(130), justifyContent: 'center' }]}>
	                                    <Text style={{ fontFamily: 'Roboto-Regular' }}>ATM Bersama</Text>
	                                </View>
	                            </TouchableOpacity>
	                            <TouchableOpacity style={[ styles.bankItem, b.roundedLow, b.mb2, p.row ]} onPress={() => this.selectingPayment('I1')}>
	                                <View style={[ p.center, { width: xs(100) }]}>
	                                    <Image source={require('../asset/bni.png')} style={{ width: xs(80), height: xs(20) }} />
	                                </View>
	                                <View style={[ b.ml2, { width: xs(130), justifyContent: 'center' }]}>
	                                    <Text style={{ fontFamily: 'Roboto-Regular' }}>BNI</Text>
	                                </View>
	                            </TouchableOpacity>
	                            <TouchableOpacity style={[ styles.bankItem, b.roundedLow, b.mb2, p.row ]} onPress={() => this.selectingPayment('M1')}>
	                                <View style={[ p.center, { width: xs(100) }]}>
	                                    <Image source={require('../asset/mandiri.png')} style={{ width: xs(80), height: xs(30) }} />
	                                </View>
	                                <View style={[ b.ml2, { width: xs(130), justifyContent: 'center' }]}>
	                                    <Text style={{ fontFamily: 'Roboto-Regular' }}>Mandiri</Text>
	                                </View>
	                            </TouchableOpacity>
	                            <TouchableOpacity style={[ styles.bankItem, b.roundedLow, b.mb2, p.row ]} onPress={() => this.selectingPayment('VC')}>
	                                <View style={[ p.center, { width: xs(100) }]}>
	                                    <Image source={require('../asset/VC.png')} style={{ width: xs(80), height: xs(30) }} />
	                                </View>
	                                <View style={[ b.ml2, { width: xs(130), justifyContent: 'center' }]}>
	                                    <Text style={{ fontFamily: 'Roboto-Regular' }}>Credit Card</Text>
	                                </View>
	                            </TouchableOpacity>
	                            <TouchableOpacity style={[ styles.bankItem, b.roundedLow, b.mb2, p.row ]} onPress={() => this.selectingPayment('BK')}>
	                                <View style={[ p.center, { width: xs(100) }]}>
	                                    <Image source={require('../asset/bca_klik_pay.png')} style={{ width: xs(80), height: xs(20) }} />
	                                </View>
	                                <View style={[ b.ml2, { width: xs(130), justifyContent: 'center' }]}>
	                                    <Text style={{ fontFamily: 'Roboto-Regular' }}>BCA Klik Pay</Text>
	                                </View>
	                            </TouchableOpacity>
	                            <TouchableOpacity style={[ styles.bankItem, b.roundedLow, b.mb2, p.row ]} onPress={() => this.selectingPayment('OV')}>
	                                <View style={[ p.center, { width: xs(100) }]}>
	                                    <Image source={require('../asset/ovo.png')} style={{ width: xs(80), height: xs(30) }} />
	                                </View>
	                                <View style={[ b.ml2, { width: xs(130), justifyContent: 'center' }]}>
	                                    <Text style={{ fontFamily: 'Roboto-Regular' }}>OVO</Text>
	                                </View>
	                            </TouchableOpacity>
	                            <TouchableOpacity style={[ styles.bankItem, b.roundedLow, b.mb2, p.row ]} onPress={() => this.selectingPayment('FT')}>
	                                <View style={[ p.center, { width: xs(100) }]}>
	                                    <Image source={require('../asset/retail.png')} style={{ width: xs(80), height: xs(45) }} />
	                                </View>
	                                <View style={[ b.ml2, { width: xs(130), justifyContent: 'center' }]}>
	                                    <Text style={{ fontFamily: 'Roboto-Regular' }}>Retail</Text>
	                                </View>
	                            </TouchableOpacity>
	                        </View>
						</View>
					</View>
				</ScrollView>
			</LinearGradient>
		)
	}
}

const styles = StyleSheet.create({
    bankItem: {
        width: '100%',
        height: xs(50),
        backgroundColor: light
    },

    bankItemSelected: {
        width: '100%',
        height: xs(50),
        backgroundColor: 'lightblue'
    }
})