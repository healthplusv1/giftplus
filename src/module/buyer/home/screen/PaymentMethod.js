import React, { Component } from 'react'
import { View, Text, TouchableOpacity, Alert } from 'react-native'
import { blue, light } from '../utils/Color'
import { xs, vs } from '../utils/Responsive'
import { f, c, b, p } from '../utils/StyleHelper'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
import FastImage from 'react-native-fast-image'
import { WebView } from 'react-native-webview'
import AsyncStorage from '@react-native-async-storage/async-storage'
import axios from 'axios'
import { API_LOCAL, API_URL } from '@env'

import firebase from '@react-native-firebase/app';
import messaging from '@react-native-firebase/messaging';

export default class PaymentMethod extends Component {
	constructor(props) {
		super(props);
		this.state = {
		}
	}

	componentDidMount(){
		messaging().onMessage(async remoteMessage => {
			/* 
			{
			"data": {
			"additionalParam": "", 
			"amount": "4099000", 
			"merchantCode": "D9005", 
			"merchantOrderId": "201", 
			"merchantUserId": "", 
			"paymentCode": "BK", 
			"productDetail": "GiftPlus Payment", 
			"reference": "D900552EMPY0VSDVFL1D", 
			"resultCode": "00", 
			"signature": "703d5afbe8ca4bc172dc0654274ff08f", 
			"spUserHash": ""}, 
			"from": "747415462681", 
			"messageId": "0:1631690222325797%631a7097f9fd7ecd", 
			"sentTime": 1631690222317, 
			"ttl": 2419200
			}
			 */
			console.log("remoteMessage ==> ", remoteMessage.data)

			if (remoteMessage.data.resultCode === '00') {
				this.props.navigation.push('ChooseProduct', {order_id: remoteMessage.data.merchantOrderId});
			} else {
				Alert.alert('','pembayaran terganggu, coba beberapa saat lagi')
			}
		});
	}

	render() {
		return (
			<View style={{ width: '100%', height: '100%' }}>
				<View style={{ width: '100%', alignItems: 'center', backgroundColor: blue }}>
					<View style={[ b.mt4, b.mb4, { width: wp('90%'), flexDirection: 'row' }]}>
						<View style={{ width: wp('15%') }}>
							<TouchableOpacity onPress={() => this.props.navigation.pop()}>
								<FastImage source={require('../asset/left.png')} style={{ width: xs(25), height: xs(25) }} />
							</TouchableOpacity>
						</View>
						<View style={{ width: wp('60%'), alignItems: 'center' }}>
							<Text style={[ c.light, f._16, { fontFamily: 'Roboto-Bold' }]}>Pembayaran</Text>
						</View>
						<View style={{ width: wp('15%') }}>
						</View>
					</View>
				</View>

				<WebView
				source={{ uri: this.props.navigation.getParam('duitku').paymentUrl}}
				/>
			</View>
		)
	}
}