import React, { Component } from 'react'
import { View, Text, Image, TouchableOpacity } from 'react-native'
import { blue, light } from '../utils/Color'
import { xs, vs } from '../utils/Responsive'
import { f, c, b, p } from '../utils/StyleHelper'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
import FastImage from 'react-native-fast-image'
import LinearGradient from 'react-native-linear-gradient'
import Swiper from 'react-native-swiper'
import AsyncStorage from '@react-native-async-storage/async-storage'
import axios from 'axios'
import { API_LOCAL, API_URL } from '@env'

export default class Strategi extends Component {
	constructor(props) {
		super(props);
		this.state = {
		}
	}

	render() {
		return (
			<LinearGradient start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }} colors={[ '#181C21', '#252F38', '#252F38' ]} style={{ width: '100%', height: '100%' }}>
				<View style={{ width: '100%', backgroundColor: '#121212', alignItems: 'center' }}>
					<View style={{ width: wp('90%'), height: hp('10%'), flexDirection: 'row', alignItems: 'center' }}>
						<View style={{ width: wp('15%') }}>
							<TouchableOpacity onPress={() => this.props.navigation.pop()}>
								<FastImage source={require('../asset/left.png')} style={{ width: xs(25), height: xs(25) }} />
							</TouchableOpacity>
						</View>
						<View style={{ width: wp('60%'), alignItems: 'center' }}>
							<Text style={[ c.light, f._16, { fontFamily: 'Roboto-Bold' }]}>PEMBERITAHUAN PENJUAL</Text>
						</View>
						<View style={{ width: wp('15%') }}>
						</View>
					</View>
				</View>

				<View style={{ width: '100%', alignItems: 'center' }}>
					<View style={[ b.rounded, { width: xs(320), height: xs(130), backgroundColor: '#323232', marginTop: vs(25), flexDirection: 'row', alignItems: 'center' }]}>
						<FastImage source={require('../asset/edu3.png')} style={[ b.ml4, { width: xs(80.5), height: xs(80) }]} />
						<View style={[ b.ml2, { width: xs(160) }]}>
							<Text style={[ c.light, p.textCenter, f._18, { fontFamily: 'Roboto-Regular' }]}>Mempelajari lebih dalam untuk penyesuaian strategi</Text>
						</View>
					</View>
				</View>

				<Swiper
					loop={false}
					dotColor={'#CCC'}
					showsButtons={true}
					buttonWrapperStyle={{ backgroundColor: 'transparent', flexDirection: 'row', position: 'absolute', top: xs(175), left: 0, flex: 1, paddingHorizontal: 10, paddingVertical: 10, justifyContent: 'space-between', alignItems: 'center' }}
					nextButton={<Text style={[ c.blue, f._18, { fontFamily: 'Roboto-Bold' }]}>Next</Text>}
					prevButton={<Text style={[ c.blue, f._18, { fontFamily: 'Roboto-Bold' }]}>Prev</Text>}
				>
					<View style={{ width: '100%', height: '100%', alignItems: 'center' }}>
						<View style={[ b.mt4, b.roundedLow, { width: xs(130), height: xs(30), backgroundColor: blue, flexDirection: 'row', alignItems: 'center' }]}>
							<Text style={[ c.light, b.ml2, { fontFamily: 'Roboto-Bold' }]}>A)</Text>
							<Text style={[ c.light, b.ml1, { fontFamily: 'Roboto-Bold' }]}>Produk</Text>
						</View>

						<FastImage source={require('../asset/group_157.png')} style={[ b.mt4, { width: xs(129), height: xs(120) }]} />
						<View style={{ width: xs(230) }}>
							<Text style={[ c.light, p.textCenter, f._18, b.mt2, { fontFamily: 'Roboto-Bold' }]}>Pastikan Foto Produk Berkualitas Tinggi dengan</Text>
							<Text style={[ p.textCenter, f._18, { fontFamily: 'Roboto-Bold', color: 'darkorange' }]}>Rincian Produk Lengkap</Text>
						</View>
					</View>

					<View style={{ width: '100%', height: '100%', alignItems: 'center' }}>
						<View style={[ b.mt4, b.roundedLow, { width: xs(130), height: xs(30), backgroundColor: blue, flexDirection: 'row', alignItems: 'center' }]}>
							<Text style={[ c.light, b.ml2, { fontFamily: 'Roboto-Bold' }]}>B)</Text>
							<Text style={[ c.light, b.ml1, { fontFamily: 'Roboto-Bold' }]}>Harga</Text>
						</View>

						<FastImage source={require('../asset/price.png')} style={[ b.mt4, { width: xs(120), height: xs(120) }]} />
						<View style={{ width: xs(230) }}>
							<Text style={[ c.light, p.textCenter, f._18, b.mt2, { fontFamily: 'Roboto-Bold' }]}>Bandingkan <Text style={[ p.textCenter, f._18, b.mt2, { fontFamily: 'Roboto-Bold', color: 'darkorange' }]}>Harga produk</Text></Text>
							<Text style={[ c.light, p.textCenter, f._18, { fontFamily: 'Roboto-Bold' }]}>dengan <Text style={[ p.textCenter, f._18, b.mt2, { fontFamily: 'Roboto-Bold', color: 'darkorange' }]}>Kompetitor</Text></Text>
						</View>
					</View>

					<View style={{ width: '100%', height: '100%', alignItems: 'center' }}>
						<View style={[ b.mt4, b.roundedLow, { width: xs(130), height: xs(30), backgroundColor: blue, flexDirection: 'row', alignItems: 'center' }]}>
							<Text style={[ c.light, b.ml2, { fontFamily: 'Roboto-Bold' }]}>C)</Text>
							<Text style={[ c.light, b.ml1, { fontFamily: 'Roboto-Bold' }]}>Chat</Text>
						</View>

						<FastImage source={require('../asset/group_154.png')} style={[ b.mt4, { width: xs(107.5), height: xs(120) }]} />
						<View style={{ width: xs(230) }}>
							<Text style={[ p.textCenter, f._18, b.mt2, { fontFamily: 'Roboto-Bold', color: 'darkorange' }]}>Buat Template Pesan <Text style={[ c.light, p.textCenter, f._18, b.mt2, { fontFamily: 'Roboto-Bold' }]}>untuk</Text></Text>
							<Text style={[ c.light, p.textCenter, f._18, { fontFamily: 'Roboto-Bold' }]}>Pertanyaan yang sering ditanyakan</Text>
						</View>
					</View>

					<View style={{ width: '100%', height: '100%', alignItems: 'center' }}>
						<View style={[ b.mt4, b.roundedLow, { width: xs(130), height: xs(30), backgroundColor: blue, flexDirection: 'row', alignItems: 'center' }]}>
							<Text style={[ c.light, b.ml2, { fontFamily: 'Roboto-Bold' }]}>D)</Text>
							<Text style={[ c.light, b.ml1, { fontFamily: 'Roboto-Bold' }]}>Penilaian</Text>
						</View>

						<FastImage source={require('../asset/group_151.png')} style={[ b.mt4, { width: xs(121), height: xs(120) }]} />
						<View style={{ width: xs(230) }}>
							<Text style={[ c.light, p.textCenter, f._18, b.mt2, { fontFamily: 'Roboto-Bold' }]}>Penilaian yang baik dapat</Text>
							<Text style={[ p.textCenter, f._18, { fontFamily: 'Roboto-Bold', color: 'darkorange' }]}>membangun kepercayaan dan</Text>
							<Text style={[ p.textCenter, f._18, { fontFamily: 'Roboto-Bold', color: 'darkorange' }]}>keyakinan <Text style={[ c.light, p.textCenter, f._18, b.mt2, { fontFamily: 'Roboto-Bold' }]}>Pembeli</Text></Text>
						</View>
					</View>
				</Swiper>
			</LinearGradient>
		)
	}
}