import React, { Component } from 'react'
import { View, Text, Image, TouchableOpacity, ScrollView, TextInput, ToastAndroid } from 'react-native'
import { blue, light } from '../utils/Color'
import { xs, vs } from '../utils/Responsive'
import { f, c, b, p } from '../utils/StyleHelper'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
import FastImage from 'react-native-fast-image'
import LinearGradient from 'react-native-linear-gradient'
import AsyncStorage from '@react-native-async-storage/async-storage'
import axios from 'axios'
import { API_LOCAL, API_URL } from '@env'

export default class ChatDetail extends Component {
	constructor(props) {
		super(props);
		this.state = {
		}
	}

	componentDidMount() {
		ToastAndroid.show('still not implemented', ToastAndroid.SHORT)
	}

	render() {
		return (
			<LinearGradient start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }} colors={[ '#181C21', '#252F38', '#252F38' ]} style={{ width: '100%', height: '100%' }}>
				<View style={{ width: '100%', backgroundColor: blue, alignItems: 'center' }}>
					<View style={[ b.mt2, b.mb2, { width: wp('90%'), flexDirection: 'row', alignItems: 'center' }]}>
						<View style={{ width: wp('15%') }}>
							<TouchableOpacity onPress={() => this.props.navigation.pop()}>
								<FastImage source={require('../asset/left.png')} style={{ width: xs(25), height: xs(25) }} />
							</TouchableOpacity>
						</View>
						<View style={{ width: wp('60%'), flexDirection: 'row', alignItems: 'center' }}>
							<View style={{ width: xs(50), height: xs(50), backgroundColor: light, borderRadius: xs(50), alignItems: 'center', justifyContent: 'center' }}>
								<FastImage source={require('../asset/Canon_logo.png')} style={{ width: xs(35), height: xs(35) }} />
							</View>
							<View style={[ b.ml2, { width: wp('40%') }]}>
								<Text style={[ c.light, { fontFamily: 'Roboto-Regular' }]} numberOfLines={1}>Canon Official</Text>
								<Text style={[ c.light, f._12, { fontFamily: 'Roboto-Regular' }]} numberOfLines={1}>Ruko Mataram 16</Text>
							</View>
						</View>
						<View style={{ width: wp('15%'), alignItems: 'flex-end' }}>
							<Image source={require('../asset/phone.png')} style={{ width: xs(25), height: xs(25), tintColor: light }} />
						</View>
					</View>
				</View>

				<ScrollView>
					<View style={{ width: '100%', alignItems: 'center' }}>
						<View style={[ b.mt4, { width: wp('90%') }]}>
							<View style={[ b.mb4, { width: '100%', alignItems: 'flex-end' }]}>
								<View style={{ maxWidth: wp('65%'), backgroundColor: light, borderRadius: wp('2%') }}>
	                                <Text style={[ b.mt1, b.ml2, b.mr2, { fontFamily: 'Roboto-Regular' }]}>Kak, barang ini adakah?</Text>
	                                <Text style={[ f._12, b.ml2, b.mr2, b.mb2, { textAlign: 'right' }]}>11:00 AM</Text>
	                            </View>
							</View>

                            <View style={[ b.mb4, { width: '100%' }]}>
	                            <View style={{ maxWidth: wp('65%'), backgroundColor: blue, borderRadius: wp('2%') }}>
	                                <Text style={[ c.light, b.mt1, b.ml2, b.mr2, { fontFamily: 'Roboto-Regular' }]}>Selamat datang di toko kami! Terima kasih telah menghubungi kami. Barang Siap dipesan Kak.</Text>
	                                <Text style={[ c.light, f._12, b.ml2, b.mr2, b.mb2, { textAlign: 'right' }]}>11:00 AM</Text>
	                            </View>
	                        </View>
						</View>
					</View>
				</ScrollView>

				<View style={{ width: '100%', alignItems: 'center', position: 'relative', bottom: 0, backgroundColor: light }}>
					<View style={[ b.mt1, b.mb1, { width: wp('90%'), flexDirection: 'row', alignItems: 'center' }]}>
						<View style={{ width: wp('75%') }}>
							<TextInput
								placeholder='Tulis Pesan...'
								multiline={true}
							/>
						</View>
						<View style={{ width: wp('15%'), alignItems: 'flex-end' }}>
							<TouchableOpacity>
								<FastImage source={require('../asset/sendmessage_icon.png')} style={{ width: xs(25), height: xs(25) }} />
							</TouchableOpacity>
						</View>
					</View>
				</View>
			</LinearGradient>
		)
	}
}