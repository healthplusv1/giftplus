import React, { Component } from 'react'
import { View, Text, Image, TouchableOpacity, ScrollView, ActivityIndicator, StyleSheet, ToastAndroid, FlatList, RefreshControl, TextInput, Alert } from 'react-native'
import { blue, light, softBlue } from '../utils/Color'
import { xs, vs } from '../utils/Responsive'
import { f, c, b, p } from '../utils/StyleHelper'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
import FastImage from 'react-native-fast-image'
import LinearGradient from 'react-native-linear-gradient'
import { Tabs, Tab, NativeBaseProvider } from 'native-base'
import AsyncStorage from '@react-native-async-storage/async-storage'
import axios from 'axios'
import { API_URL } from '@env'

export default class Payment extends Component {
    constructor(props) {
        super(props);
        this.state = {
            payment: [
                {title: 'Pembayaran'},
                {title: 'Riwayat'}
            ],
            clicked: 0,
            totalPrice: null,
            saldo: null,
            voucherCode: null,
            detail_payment: null,
            loading: false,
            historyPayment: [
                {id: 1, pembayaran: 'BCA', totalPrice: '100000', tanggal: '15 September 2021', waktu: '14:00', status: 'SUCCESS'},
                {id: 2, pembayaran: 'MANDIRI', totalPrice: '100000', tanggal: '15 September 2021', waktu: '14:00', status: 'PROCESS'},
                {id: 3, pembayaran: 'BNI', totalPrice: '100000', tanggal: '15 September 2021', waktu: '14:00', status: 'EXPIRED'}
            ]
        }
    }

    async componentDidMount() {
        const id_login = await AsyncStorage.getItem('@id_login')
        let voucher = await AsyncStorage.getItem('@voucher')
        voucher = JSON.parse(voucher)
        if (voucher!==null) {
            this.setState({voucherCode: voucher.code})
        } console.log("THE VOUCHER ===> ", voucher)

        axios.get(API_URL+'/main/wallet',{params:{
            id_login: id_login
        }}).then(res => {
            console.log("SALDO LOG ===> ", res.data)
            this.setState({
                saldo: res.data.saldo,
                totalPrice: this.props.navigation.getParam('price')
            })
        });
    }

    onRefresh() {
        this.setState({ loading: true })
        this.componentDidMount()
        this.setState({ loading: false })
    }

    async selectingPayment(payment_method) {
        const id_login = await AsyncStorage.getItem('@id_login')
        axios.get(API_URL+'/main/shopping/order/checkout', {params: {
            id_login: id_login,
            voucherCode: this.state.voucherCode
        }})
        .then(res => {
            console.log("SAVE ORDER LOG ==> ",res.data)
            if (res.data.status==='true') {
                if (res.data.status==='true') {
                    axios.post(API_URL+"/main/duitku/generate/"+res.data.order_id+"/"+payment_method+"/60",{params:{
                        amount: this.props.navigation.getParam('price')
                    }})
                    .then(res => {
                        console.log("PAYMENT LOG ==> ", res.data)
                        ToastAndroid.show('memuat permintaan pembayaran', ToastAndroid.SHORT)
                        this.props.navigation.push('PaymentMethod', {duitku: res.data.data})
                        // ToastAndroid.show('Halaman Selanjutnya (QR Code GiftCard) masih dalam proses pembangunan', ToastAndroid.LONG)
                    })
                }
            } else if (res.data.status==='keranjang belanja kosong') {
                Alert.alert('', 'Tidak ada barang dikeranjang belanja anda')
            } else if (res.data.status==='stok habis') {
                Alert.alert('', 'Salah satu produk dikeranjang telah kehabisan persediaan ')
            } else if (res.data.status==='voucher tidak ditemukan') {
                Alert.alert('', 'Voucher tidak ditemukan')
            } else if (res.data.status==='minimal pembelian tidak terpenuhi') {
                Alert.alert('', 'Minimal pembelian untuk menggunakan voucher ini belum terpenuhi')
            } else if (res.data.status==='voucher tidak valid') {
                Alert.alert('', 'Voucher sudah tidak berlaku')
            } else if (res.data.status==='voucher sudah pernah dipakai') {
                Alert.alert('', 'Voucher sudah pernah dipakai')
            } else if (res.data.status==='voucher telah mencapai batas pemakaian') {
                Alert.alert('', 'Voucher telah mencapai batas pemakaian')
            } else if (res.data.status==='produk tidak didukung') {
                Alert.alert('', 'Produk tidak didukung untuk menggunakan voucher ini')
            } else {
                Alert.alert('', 'Terjadi kesalahan, mohon coba kembali beberapa saat lagi')
            }
        });
    }

    async checkWallet() {
        const id_login = await AsyncStorage.getItem('@id_login')

        axios.get(API_URL+'/main/wallet', {params: {id_login: id_login}})
        .then(res => {
            if (res.data.status==='true') {
                if (Number(res.data.saldo)>Number(this.props.navigation.getParam('price'))) {
                    this.props.navigation.push('PaymentWallet')
                } else {
                    Alert.alert('', 'Your balance is not sufficient, please top up your balance first')
                }
            } else {
                Alert.alert('', 'Your account was not found')
            }
        });
    }

    async swicth(index) {
        await this.setState({
            clicked: index
        })
    }

    format(number) {
        if (number) {
          var rupiah = "";
          var numberrev = number
            .toString()
            .split("")
            .reverse()
            .join("");
          for (var i = 0; i < numberrev.length; i++)
            if (i % 3 == 0) rupiah += numberrev.substr(i, 3) + ".";
          return (
            "Rp " +
            rupiah
              .split("", rupiah.length - 1)
              .reverse()
              .join("")
          );
        } else {
            return (
                "Rp "+number
            );
        }
    }

    render() {
        return (
            <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }} colors={[ '#181C21', '#252F38', '#252F38' ]} style={{ width: '100%', height: '100%' }}>
                <View style={{ width: '100%', height: hp('8%') }}>
                    <FlatList
                        horizontal={true}
                        data={this.state.payment}
                        keyExtractor={item => item.title}
                        renderItem={({ item, index }) => (
                            (this.state.clicked === index ?
                                <TouchableOpacity onPress={() => this.swicth(index)} style={{ width: wp('50%'), height: '100%', backgroundColor: blue, borderBottomWidth: 4, borderBottomColor: softBlue, alignItems: 'center', justifyContent: 'center' }}>
                                    <Text style={[ c.light, { fontFamily: 'Roboto-Bold' }]}>{item.title}</Text>
                                </TouchableOpacity>
                                :
                                <TouchableOpacity onPress={() => this.swicth(index)} style={{ width: wp('50%'), height: '100%', backgroundColor: '#121212', alignItems: 'center', justifyContent: 'center' }}>
                                    <Text style={[ c.light, { fontFamily: 'Roboto-Bold' }]}>{item.title}</Text>
                                </TouchableOpacity>
                            )
                        )}
                    />
                </View>

                {this.state.clicked === 0 && (
                    <ScrollView showsVerticalScrollIndicator={false} refreshControl={<RefreshControl refreshing={this.state.loading} onRefresh={() => this.onRefresh()} />}>
                        <View style={[ p.center, { width: '100%' }]}>
                            <View style={[ b.mt3, { width: wp('85%'), borderBottomWidth: 1, borderBottomColor: light, flexDirection: 'row', alignItems: 'center' }]}>
                                <View style={[ b.mb1, { width: wp('42.5%') }]}>
                                    <Text style={[ c.light, { fontFamily: 'Roboto-Bold' }]}>Total harga</Text>
                                </View>
                                <View style={[ b.mb1, { width: wp('42.5%'), alignItems: 'flex-end' }]}>
                                    <Text style={[ c.light, { fontFamily: 'Roboto-Bold' }]}>{this.format(this.state.totalPrice)}</Text>
                                </View>
                            </View>
                            <View style={[ b.mt4, { width: wp('85%') }]}>
                                <Text style={[ c.light, f._16, { fontFamily: 'Roboto-Bold' }]}>Saldo</Text>
                                <TouchableOpacity onPress={() => this.checkWallet()} style={[ b.rounded, b.mt2, { width: '100%', alignItems: 'center', backgroundColor: blue }]}>
                                    <View style={[ b.mt4, b.mb4, { width: wp('80%'), flexDirection: 'row' }]}>
                                        <View style={{ width: wp('40%') }}>
                                            <View style={{ flexDirection: 'row' }}>
                                                <Image source={require('../asset/belumbayar_icon.png')} style={{ width: xs(20), height: xs(20), tintColor: light }} />
                                                <View style={[ b.ml1, { width: wp('33%') }]}>
                                                    <Text style={[ c.light, { fontFamily: 'Roboto-Bold' }]}>Saldo Anda</Text>
                                                    <Text style={[ c.light, { fontFamily: 'Roboto-Bold' }]} numberOfLines={1}>{
                                                        this.state.saldo==null? 'loading...' : this.format(this.state.saldo)
                                                    }</Text>
                                                </View>
                                            </View>
                                        </View>
                                        <View style={{ width: wp('40%'), alignItems: 'flex-end' }}>
                                            <TouchableOpacity onPress={() => this.props.navigation.push('TopUp')} style={{ width: wp('25%'), height: hp('5%'), alignItems: 'center', justifyContent: 'center', backgroundColor: light, borderRadius: xs(5) }}>
                                                <Text style={[ c.blue, { fontFamily: 'Roboto-Bold' }]}>Isi Saldo</Text>
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                </TouchableOpacity>

                                <Text style={[ c.light, f._16, b.mt4, { fontFamily: 'Roboto-Bold' }]}>Metode pembayaran lain</Text>
                                <View style={[ b.mt2 ]} />
                                <TouchableOpacity style={[ styles.bankItem, b.roundedLow, b.mb2, p.row ]} onPress={() => this.selectingPayment('VA')}>
                                    <View style={[ p.center, { width: xs(100) }]}>
                                        <Image source={require('../asset/maybank.png')} style={{ width: xs(80), height: xs(20) }} />
                                    </View>
                                    <View style={[ b.ml2, { width: xs(130), justifyContent: 'center' }]}>
                                        <Text style={{ fontFamily: 'Roboto-Regular' }}>Maybank</Text>
                                    </View>
                                </TouchableOpacity>
                                <TouchableOpacity style={[ styles.bankItem, b.roundedLow, b.mb2, p.row ]} onPress={() => this.selectingPayment('BT')}>
                                    <View style={[ p.center, { width: xs(100) }]}>
                                        <Image source={require('../asset/permata_bank.png')} style={{ width: xs(80), height: xs(25) }} />
                                    </View>
                                    <View style={[ b.ml2, { width: xs(130), justifyContent: 'center' }]}>
                                        <Text style={{ fontFamily: 'Roboto-Regular' }}>Permata Bank</Text>
                                    </View>
                                </TouchableOpacity>
                                <TouchableOpacity style={[ styles.bankItem, b.roundedLow, b.mb2, p.row ]} onPress={() => this.selectingPayment('B1')}>
                                    <View style={[ p.center, { width: xs(100) }]}>
                                        <Image source={require('../asset/cimb3x.png')} style={{ width: xs(80), height: xs(20) }} />
                                    </View>
                                    <View style={[ b.ml2, { width: xs(130), justifyContent: 'center' }]}>
                                        <Text style={{ fontFamily: 'Roboto-Regular' }}>CIMB Niaga</Text>
                                    </View>
                                </TouchableOpacity>
                                <TouchableOpacity style={[ styles.bankItem, b.roundedLow, b.mb2, p.row ]} onPress={() => this.selectingPayment('A1')}>
                                    <View style={[ p.center, { width: xs(100) }]}>
                                        <Image source={require('../asset/atm_bersama.png')} style={{ width: xs(80), height: xs(20) }} />
                                    </View>
                                    <View style={[ b.ml2, { width: xs(130), justifyContent: 'center' }]}>
                                        <Text style={{ fontFamily: 'Roboto-Regular' }}>ATM Bersama</Text>
                                    </View>
                                </TouchableOpacity>
                                <TouchableOpacity style={[ styles.bankItem, b.roundedLow, b.mb2, p.row ]} onPress={() => this.selectingPayment('I1')}>
                                    <View style={[ p.center, { width: xs(100) }]}>
                                        <Image source={require('../asset/bni.png')} style={{ width: xs(80), height: xs(20) }} />
                                    </View>
                                    <View style={[ b.ml2, { width: xs(130), justifyContent: 'center' }]}>
                                        <Text style={{ fontFamily: 'Roboto-Regular' }}>BNI</Text>
                                    </View>
                                </TouchableOpacity>
                                <TouchableOpacity style={[ styles.bankItem, b.roundedLow, b.mb2, p.row ]} onPress={() => this.selectingPayment('M1')}>
                                    <View style={[ p.center, { width: xs(100) }]}>
                                        <Image source={require('../asset/mandiri.png')} style={{ width: xs(80), height: xs(30) }} />
                                    </View>
                                    <View style={[ b.ml2, { width: xs(130), justifyContent: 'center' }]}>
                                        <Text style={{ fontFamily: 'Roboto-Regular' }}>Mandiri</Text>
                                    </View>
                                </TouchableOpacity>
                                <TouchableOpacity style={[ styles.bankItem, b.roundedLow, b.mb2, p.row ]} onPress={() => this.selectingPayment('VC')}>
                                    <View style={[ p.center, { width: xs(100) }]}>
                                        <Image source={require('../asset/VC.png')} style={{ width: xs(80), height: xs(30) }} />
                                    </View>
                                    <View style={[ b.ml2, { width: xs(130), justifyContent: 'center' }]}>
                                        <Text style={{ fontFamily: 'Roboto-Regular' }}>Credit Card</Text>
                                    </View>
                                </TouchableOpacity>
                                <TouchableOpacity style={[ styles.bankItem, b.roundedLow, b.mb2, p.row ]} onPress={() => this.selectingPayment('BK')}>
                                    <View style={[ p.center, { width: xs(100) }]}>
                                        <Image source={require('../asset/bca_klik_pay.png')} style={{ width: xs(80), height: xs(20) }} />
                                    </View>
                                    <View style={[ b.ml2, { width: xs(130), justifyContent: 'center' }]}>
                                        <Text style={{ fontFamily: 'Roboto-Regular' }}>BCA Klik Pay</Text>
                                    </View>
                                </TouchableOpacity>
                                <TouchableOpacity style={[ styles.bankItem, b.roundedLow, b.mb2, p.row ]} onPress={() => this.selectingPayment('OV')}>
                                    <View style={[ p.center, { width: xs(100) }]}>
                                        <Image source={require('../asset/ovo.png')} style={{ width: xs(80), height: xs(30) }} />
                                    </View>
                                    <View style={[ b.ml2, { width: xs(130), justifyContent: 'center' }]}>
                                        <Text style={{ fontFamily: 'Roboto-Regular' }}>OVO</Text>
                                    </View>
                                </TouchableOpacity>
                                <TouchableOpacity style={[ styles.bankItem, b.roundedLow, b.mb2, p.row ]} onPress={() => this.selectingPayment('FT')}>
                                    <View style={[ p.center, { width: xs(100) }]}>
                                        <Image source={require('../asset/retail.png')} style={{ width: xs(80), height: xs(45) }} />
                                    </View>
                                    <View style={[ b.ml2, { width: xs(130), justifyContent: 'center' }]}>
                                        <Text style={{ fontFamily: 'Roboto-Regular' }}>Retail</Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </ScrollView>
                )}

                {this.state.clicked === 1 && (
                    <ScrollView showsVerticalScrollIndicator={false} refreshControl={<RefreshControl refreshing={this.state.loading} onRefresh={() => this.onRefresh()} />}>
                        {this.state.historyPayment === undefined ?
                            <View style={[ p.center, { width: '100%', height: wp('160%') }]}>
                                <Image source={require('../asset/activity-history.png')} style={{ width: xs(100), height: xs(100), tintColor: '#FFF' }} />
                                <Text style={[ c.light, b.mt2 ]}>Tidak ada History</Text>
                            </View>
                            :
                            <View style={[ b.mt4, { width: '100%', alignItems: 'center' }]}>
                                <FlatList
                                    data={this.state.historyPayment}
                                    keyExtractor={item => item.id}
                                    renderItem={({ item }) => (
                                        <View style={[ b.mb3, b.roundedLow, { width: wp('95%'), backgroundColor: light, alignItems: 'center' }]}>
                                            <View style={[ b.mt2, b.mb2, { width: wp('90%'), flexDirection: 'row' }]}>
                                                <View style={{ width: wp('45%') }}>
                                                    <Text style={{ fontFamily: 'Roboto-Bold' }}>Pembayaran {item.pembayaran}</Text>
                                                    <Text style={[ f._12, { fontFamily: 'Roboto-Bold' }]}>Bank {item.pembayaran}</Text>
                                                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                                        <Text style={[ f._12, { fontFamily: 'Roboto-Regular' }]}>{item.tanggal}</Text>
                                                        <Text style={[ f._12, b.ml1, { fontFamily: 'Roboto-Regular' }]}>{item.waktu}</Text>
                                                    </View>
                                                </View>
                                                <View style={{ width: wp('45%'), alignItems: 'flex-end' }}>
                                                    <Text style={{ fontFamily: 'Roboto-Bold' }}>{this.format(item.totalPrice)}</Text>
                                                </View>
                                            </View>
                                        </View>
                                    )}
                                />
                            </View>
                        }
                    </ScrollView> 
                )}
            </LinearGradient>
        )
    }
}

const styles = StyleSheet.create({
    bankItem: {
        width: '100%',
        height: xs(50),
        backgroundColor: light
    },

    bankItemSelected: {
        width: '100%',
        height: xs(50),
        backgroundColor: 'lightblue'
    }
})