import React, { Component } from 'react'
import { Text, View, Image, ScrollView, TouchableOpacity, TextInput, FlatList, BackHandler, Alert, RefreshControl, TouchableWithoutFeedback } from 'react-native'
import { blue, light, softGrey } from '../utils/Color'
import { xs, vs } from '../utils/Responsive'
import { f, c, b, p } from '../utils/StyleHelper'
import FastImage from 'react-native-fast-image'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
import ImageSlider from 'react-native-image-slider'
import Swiper from 'react-native-swiper'
import ActionButton from 'react-native-action-button'
import Products from './Products'
import AsyncStorage from '@react-native-async-storage/async-storage'
import axios from 'axios'
import { API_LOCAL, API_URL } from '@env'

export default class Home extends Component {

	_didFocusSubscription;
	_willBlurSubscription;

	constructor(props) {
		super(props);
		this.state = {
			loading: false,
			search: '',
			count: '',
			number: '',
			liked: false,
			image: require('../asset/love.png'),
			all_products: undefined,
			categories: [
				{id: 1, nama: 'Pakaian pria', image: require('../asset/pakaian_icon.png')},
				{id: 2, nama: 'Computer & Laptop', image: require('../asset/pc_icon.png')},
				{id: 3, nama: 'Gaming & Electronic', image: require('../asset/game_icon.png')},
				{id: 4, nama: 'Furniture', image: require('../asset/furniture_icon.png')},
				{id: 5, nama: 'Alat Perkakas', image: require('../asset/perkakas_icon.png')},
			],
			popular: [
				{id: 1, nama: 'Nike Air Max', image: require('../asset/sepatu.jpg'), harga: 'Rp 1.200.000', lokasi: 'Semarang'},
				{id: 2, nama: 'Nike Air Max', image: require('../asset/sepatu.jpg'), harga: 'Rp 1.200.000', lokasi: 'Semarang'},
				{id: 3, nama: 'Nike Air Max', image: require('../asset/sepatu.jpg'), harga: 'Rp 1.200.000', lokasi: 'Semarang'},
			],
			recomendation: [
				{id: 1, nama: 'Sarung Tangan', image: require('../asset/sarung_tangan.jpg'), harga: 'Rp 120.000', lokasi: 'Semarang'},
				{id: 2, nama: 'Sarung Tangan', image: require('../asset/sarung_tangan.jpg'), harga: 'Rp 60.000', lokasi: 'Semarang'}
			],
		};
		this._didFocusSubscription = props.navigation.addListener('didFocus', payload =>
			BackHandler.addEventListener('hardwareBackPress', this.handleBackPress)
		)
	}

	async componentDidMount() {
		this.setState({ loading: true })
		
		this._willBlurSubscription = this.props.navigation.addListener('willBlur', payload =>
			BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress)
		)
		const id_login = await AsyncStorage.getItem('@id_login')

		if (id_login === null) {
			const guest = await AsyncStorage.getItem('id_login_guest')

			axios.get(API_URL+'/main/shopping/cart', {params: {
				id_login: guest
			}})
			.then(res => {
				this.setState({
					count: res.data.data.length,
					loading: false
				})
			})
		} else {
			axios.get(API_URL+'/main/shopping/cart', {params: {
				id_login: id_login
			}})
			.then(res => {
				this.setState({
					count: res.data.data.length,
					loading: false
				})
			})
		}

		axios.get(API_URL+'/main/shopping/produk', {params:{id_login: id_login}})
		.then(result => {
			// console.log(result.data.data[0].gambar.split(",")[1])
			this.setState({
				all_products: result.data.data,
				loading: false
			})
		})
	}

	format(number) {
        if (number) {
			var rupiah = "";
			var numberrev = number
				.toString()
				.split("")
				.reverse()
				.join("");
			for (var i = 0; i < numberrev.length; i++)
				if (i % 3 == 0) rupiah += numberrev.substr(i, 3) + ".";
			return (
				"Rp " +
				rupiah
				.split("", rupiah.length - 1)
				.reverse()
				.join("")
			);
        } else {
            return (
                "Rp "+number
            );
        }
    }

    nextScreen() {
        axios.get(API_URL+'/main/shopping/search', {params: {
            keyword: this.state.search
        }})
        .then(res => {
            // console.log(this.state.search+' = '+JSON.stringify(res.data.data))
            this.props.navigation.push('AllProduct', { keyword: this.state.search })
        })
    }

    bannerNextScreen(number) {
    	if (number === '' || number === 0) {
    		this.props.navigation.push('Special')
    	} else if (number === 1) {
    		this.props.navigation.push('SpecialPromo')
    	} else if (number === 2) {
    		this.props.navigation.push('MonthPromo')
    	}
    }

    likeProduct() {
    	if (this.state.liked === false) {
				this.setState({
					liked: true,
					image: require('../asset/loved.png')
				})
			}
		else {    
			this.setState({
				liked: false,
				image: require('../asset/love.png')
			})
		}
    }

	handleBackPress = () => {
		if (this.props.navigation.isFocused()) {
			Alert.alert(
				'Keluar Aplikasi',
				'Anda yakin ingin menutup aplikasi ini?',
				[
					{text: 'Tidak', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
					{text: 'Ya', onPress: () => BackHandler.exitApp()}
				],
				{cancelable: false}
			)
		}
		return true;
	}

    render() {
        return (
        	<>
        		<ActionButton buttonColor={blue} style={{ zIndex: 1 }}>
					<ActionButton.Item
						buttonColor={blue}
						onPress={() => this.props.navigation.push('ChatList')}
					>
						<Image source={require('../asset/chaticonF.png')} style={{ width: xs(20), height: xs(20), tintColor: light }} />
					</ActionButton.Item>
					<ActionButton.Item
						buttonColor={blue}
						onPress={() => this.props.navigation.push('ScanQR')}
					>
						<Image source={require('../asset/scanqr_icon.png')} style={{ width: xs(35), height: xs(35) }} />
					</ActionButton.Item>
				</ActionButton>
	            <ScrollView showsVerticalScrollIndicator={false} style={{ backgroundColor: '#121212' }} refreshControl={<RefreshControl onRefresh={() => this.componentDidMount()} refreshing={this.state.loading} />}>
	                <View style={{ width: '100%', alignItems: 'center' }}>
	                	<View style={[ b.mt4, b.mb4, p.row, { width: wp('90%') }]}>
	                		<View style={{ width: wp('45%') }}>
	                			<FastImage source={require('../asset/giftplus.png')} style={{ width: xs(120), height: xs(32) }} />
	                		</View>
	                		<View style={{ width: wp('45%'), flexDirection: 'row-reverse' }}>
	                			<TouchableOpacity onPress={() => this.props.navigation.push('Drawer')}>
	                				<Image source={require('../asset/menu.png')} style={{ width: xs(22), height: xs(22), tintColor: light }} />
	                			</TouchableOpacity>
	                			<TouchableOpacity onPress={() => this.props.navigation.push('Notification')} style={[ b.mr2 ]}>
	                				<Image source={require('../asset/notification.png')} style={{ width: xs(22), height: xs(22), tintColor: light }} />
	                			</TouchableOpacity>
	                			<TouchableOpacity onPress={() => this.props.navigation.push('Cart')} style={[ b.mr2 ]}>
	                				<Image source={require('../asset/cart.png')} style={{ width: xs(19), height: xs(20), tintColor: light }} />
									{this.state.count !== 0 && (
										<View style={{ width: xs(12), height: xs(12), borderRadius: xs(12), backgroundColor: 'darkorange', position: 'absolute', right: 0, alignItems: 'center', justifyContent: 'center' }}>
		                                    <Text style={[ c.light, f._10 ]}>{this.state.count}</Text>
		                                </View>
									)}
	                			</TouchableOpacity>
	                		</View>
	                	</View>

						<View style={{ width: wp('90%'), height: hp('6.5%'), borderRadius: wp('5%'), backgroundColor: softGrey, flexDirection: 'row', alignItems: 'center' }}>
							<TextInput
								style={[ c.light, { width: wp('62%'), height: '100%', marginLeft: wp('4%') }]}
								ref={ref => this.textInputRef = ref}
								placeholder='Apa yang kamu cari'
								placeholderTextColor={light}
								returnKeyType='search'
								onChangeText={(text) => this.setState({ search: text })}
								onSubmitEditing={() => this.nextScreen()}
								value={this.state.search}
							/>
							<View style={{ width: wp('20%'), flexDirection: 'row-reverse' }}>
								{this.state.search === '' ?
									<TouchableOpacity onPress={() => this.textInputRef.focus()}>
										<Image source={require('../asset/search.png')} style={{ width: wp('6%'), height: wp('6%'), tintColor: light, marginLeft: wp('4%') }} />
									</TouchableOpacity>
									:
									<TouchableOpacity onPress={() => this.setState({ search: '' })} style={{ marginLeft: wp('4%') }}>
										<Image source={require('../asset/cross.png')} style={{ width: wp('6%'), height: wp('6%'), tintColor: light }} />
									</TouchableOpacity>
								}
								{/* <TouchableOpacity>
									<Image source={require('../asset/camera.png')} style={{ width: wp('6%'), height: wp('6%'), tintColor: light }} />
								</TouchableOpacity> */}
							</View>
						</View>

						<View style={[ b.mt4, { width: '100%' }]}>
							{/*<ImageSlider
								loop={false}
				                autoPlayWithInterval={5000}
				                images={banners}
				                style={{ width: '100%', height: xs(120) }}
				                onPress={({ index }) => {
									if (index === 0) {
										this.props.navigation.push('Special')
									} else if (index == 1) {
										this.props.navigation.push('SpecialPromo')
									} else if (index == 2) {
										this.props.navigation.push('MonthPromo')
									}
				                }}
							/>*/}
							<Swiper
								autoplay={true}
								autoplayTimeout={5}
								height={120}
								dotColor={'#CCC'}
								dotStyle={{ marginBottom: xs(-30) }}
								activeDotStyle={{ marginBottom: xs(-30) }}
								onIndexChanged={(index) => this.setState({ number: index })}
							>
								<TouchableWithoutFeedback onPress={() => this.bannerNextScreen(this.state.number)}>
									<FastImage source={require('../asset/bannersample1.png')} style={{ width: '100%', height: '100%' }} />
								</TouchableWithoutFeedback>
								<TouchableWithoutFeedback onPress={() => this.bannerNextScreen(this.state.number)}>
									<FastImage source={require('../asset/bannersample2.png')} style={{ width: '100%', height: '100%' }} />
								</TouchableWithoutFeedback>
								<TouchableWithoutFeedback onPress={() => this.bannerNextScreen(this.state.number)}>
									<FastImage source={require('../asset/bannersample3.png')} style={{ width: '100%', height: '100%' }} />
								</TouchableWithoutFeedback>
							</Swiper>
						</View>

	                	<View style={[ p.row, b.mt4, b.mb4, { width: wp('90%') }]}>
	                		<View style={{ width: wp('45%') }}>
	                			<Text style={[ c.light, { fontFamily: 'Roboto-Regular' }]}>Kategori trending</Text>
	                		</View>
	                		<View style={{ width: wp('45%'), alignItems: 'flex-end' }}>
	                			<TouchableOpacity onPress={() => this.props.navigation.push('AllProduct')}>
		                			<Text style={[ c.blue, { fontFamily: 'Roboto-Regular' }]}>Lihat Semua</Text>
		                		</TouchableOpacity>
	                		</View>
	                	</View>

	                	<View style={{ width: wp('90%') }}>
	                		<FlatList
	                			horizontal={true}
	                			data={this.state.categories}
	                			keyExtractor={item => item.id}
	                			renderItem={({ item }) => (
	                				<TouchableOpacity style={[ b.mr2, { width: xs(60), alignItems: 'center' }]}>
	                					<FastImage source={item.image} style={{ width: '100%', height: xs(60) }} />
	                					<Text style={[ c.light, b.mt1, p.textCenter, { fontFamily: 'Roboto-Regular' }]} numberOfLines={2}>{item.nama}</Text>
	                				</TouchableOpacity>
	            				)}
	                		/>
	                	</View>

	                	<View style={[ p.row, b.mt4, b.mb4, { width: wp('90%') }]}>
	                		<View style={{ width: wp('45%') }}>
	                			<Text style={[ c.light, { fontFamily: 'Roboto-Regular' }]}>Pembelian popular</Text>
	                		</View>
	                		<View style={{ width: wp('45%'), alignItems: 'flex-end' }}>
	                			<TouchableOpacity onPress={() => this.props.navigation.push('AllProduct')}>
		                			<Text style={[ c.blue, { fontFamily: 'Roboto-Regular' }]}>Lihat Semua</Text>
		                		</TouchableOpacity>
	                		</View>
	                	</View>

	                	<View style={{ width: wp('90%') }}>
	                		<FlatList
	                			horizontal={true}
	                			showsHorizontalScrollIndicator={false}
	                			data={this.state.all_products}
	                			keyExtractor={item => item.id}
	                			renderItem={({ item }) => (
	                				<TouchableOpacity onPress={() => this.props.navigation.push('ProductDetail', {id_product: item.id})} style={[ b.mr2, { width: xs(120), borderColor: blue, borderWidth: 1, borderRadius: xs(5) }]}>
	                					<FastImage source={{ uri: API_URL+'/public/product/'+item.gambar.split(",")[0] }} style={{ width: '100%', height: xs(125), borderTopLeftRadius: xs(5), borderTopRightRadius: xs(5) }} />
	                					<View style={{ width: '100%', height: xs(55) }}>
		                					<Text style={[ c.light, b.mt1, b.ml1, f._12, { fontFamily: 'Roboto-Regular' }]} numberOfLines={2}>{item.nama}</Text>
		                					<Text style={[ c.light, b.ml1, f._12, { fontFamily: 'Roboto-Regular' }]}>{this.format(item.harga)}</Text>
	                					</View>
	                					<View style={[ p.row, b.ml1, b.mt1, b.mb2 ]}>
	                						<Image source={require('../asset/tokolokasi_icon.png')} style={{ width: xs(15), height: xs(15) }} />
	                						<Text style={[ b.ml1, c.light, f._12, { fontFamily: 'Roboto-Regular' }]}>Semarang</Text>
	                					</View>
	                				</TouchableOpacity>
	            				)}
	                		/>
	                	</View>

	                	<View style={[ p.row, b.mt4, b.mb4, { width: wp('90%') }]}>
	                		<View style={{ width: wp('45%') }}>
	                			<Text style={[ c.light, { fontFamily: 'Roboto-Regular' }]}>Rekomendasi</Text>
	                		</View>
	                		<View style={{ width: wp('45%'), alignItems: 'flex-end' }}></View>
	                	</View>
	                	<View style={{ width: wp('90%'), marginBottom: xs(20) }}>
	                		<FlatList
	                			numColumns={2}
	                			data={this.state.all_products}
	                			keyExtractor={item => item.id}
	                			renderItem={({ item }) => (
	                				<Products
	                            		gambar={item.gambar}
	                            		nama={item.nama}
	                            		harga={item.harga}
	                            		liked={item.liked}
	                            		id_product={item.id}
	                            		avg_ratings={item.avg}
	                            		ratings_length={item.length}
	                            		ratings={item.ratings}
	                            		action={() => this.props.navigation.push('ProductDetail', {id_product: item.id})}
	                            	/>
	            				)}
	                		/>
	                	</View>
	                </View>
	            </ScrollView>
        	</>
        )
    }
}
