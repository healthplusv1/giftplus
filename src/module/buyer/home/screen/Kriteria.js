import React, { Component } from 'react'
import { View, Text, Image, TouchableOpacity, ScrollView } from 'react-native'
import { blue, light } from '../utils/Color'
import { xs, vs } from '../utils/Responsive'
import { f, c, b, p } from '../utils/StyleHelper'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
import FastImage from 'react-native-fast-image'
import LinearGradient from 'react-native-linear-gradient'
import AsyncStorage from '@react-native-async-storage/async-storage'
import axios from 'axios'
import { API_LOCAL, API_URL } from '@env'

export default class Kriteria extends Component {
	constructor(props) {
		super(props);
		this.state = {
		}
	}

	render() {
		return (
			<LinearGradient start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }} colors={[ '#181C21', '#252F38', '#252F38' ]} style={{ width: '100%', height: '100%' }}>
				<ScrollView showsVerticalScrollIndicator={false}>
					<View style={{ width: '100%', backgroundColor: '#121212', alignItems: 'center' }}>
						<View style={{ width: wp('90%'), height: hp('10%'), flexDirection: 'row', alignItems: 'center' }}>
							<View style={{ width: wp('15%') }}>
								<TouchableOpacity onPress={() => this.props.navigation.pop()}>
									<FastImage source={require('../asset/left.png')} style={{ width: xs(25), height: xs(25) }} />
								</TouchableOpacity>
							</View>
							<View style={{ width: wp('60%'), alignItems: 'center' }}>
								<Text style={[ c.light, f._16, { fontFamily: 'Roboto-Bold' }]}>PEMBERITAHUAN PENJUAL</Text>
							</View>
							<View style={{ width: wp('15%') }}>
							</View>
						</View>
					</View>

					<View style={{ width: '100%', alignItems: 'center' }}>
						<View style={[ b.rounded, { width: xs(320), height: xs(130), backgroundColor: '#323232', marginTop: vs(25), flexDirection: 'row', alignItems: 'center' }]}>
							<FastImage source={require('../asset/edu2.png')} style={[ b.ml2, { width: xs(100), height: xs(85) }]} />
							<View style={[ b.ml2, { width: xs(180) }]}>
								<Text style={[ c.light, p.textCenter, f._18, { fontFamily: 'Roboto-Regular' }]}>Memahami kriteria penjualan</Text>
							</View>
						</View>
					</View>

					<View style={[ b.mt4, { width: '100%', alignItems: 'center' }]}>
						<View style={[ b.rounded, { width: xs(250) }]}>
							<Text style={[ c.light, p.textCenter, f._18, { fontFamily: 'Roboto-Regular' }]}>3 Kriteria utama yang mempengaruhi penjualan</Text>
						</View>

						<Image source={require('../asset/total.png')} style={[ b.mt3, { width: xs(80), height: xs(80), tintColor: blue }]} />
						<Text style={[ f._16, b.mt1, { color: 'darkorange', fontFamily: 'Roboto-Bold' }]}>Total Penjualan</Text>
						<Image source={require('../asset/path_77.png')} style={[ b.mt2, { width: xs(25), height: xs(25) }]} />

						<Image source={require('../asset/kunjungan.png')} style={[ b.mt3, { width: xs(80), height: xs(80), tintColor: blue }]} />
						<View style={[ b.mt1, { width: wp('90%'), flexDirection: 'row', alignItems: 'center' }]}>
							<View style={{ width: wp('15%'), alignItems: 'flex-end' }}>
								<View style={{ width: xs(22), height: xs(22), backgroundColor: blue, borderRadius: xs(22), borderWidth: 2.5, borderColor: light, alignItems: 'center', justifyContent: 'center' }}>
									<Text style={[ c.light, { fontFamily: 'Roboto-Bold' }]}>1</Text>
								</View>
							</View>
							<View style={{ width: wp('60%'), alignItems: 'center' }}>
								<Text style={[ f._16, { color: 'darkorange', fontFamily: 'Roboto-Bold' }]}>Kunjungan</Text>
							</View>
							<View style={{ width: wp('15%') }}>
							</View>
						</View>
						<View style={{ width: wp('80%') }}>
							<Text style={[ c.light, p.textCenter, f._16, { fontFamily: 'Roboto-BoldItalic' }]}>Total Pengunjung</Text>
						</View>
						
						<Image source={require('../asset/konversi.png')} style={[ b.mt3, { width: xs(80), height: xs(80), tintColor: blue }]} />
						<View style={[ b.mt1, { width: wp('90%'), flexDirection: 'row', alignItems: 'center' }]}>
							<View style={{ width: wp('15%'), alignItems: 'flex-end' }}>
								<View style={{ width: xs(22), height: xs(22), backgroundColor: blue, borderRadius: xs(22), borderWidth: 2.5, borderColor: light, alignItems: 'center', justifyContent: 'center' }}>
									<Text style={[ c.light, { fontFamily: 'Roboto-Bold' }]}>2</Text>
								</View>
							</View>
							<View style={{ width: wp('60%'), alignItems: 'center' }}>
								<Text style={[ f._16, { color: 'darkorange', fontFamily: 'Roboto-Bold' }]}>Tingkat Konversi</Text>
							</View>
							<View style={{ width: wp('15%') }}>
							</View>
						</View>
						<View style={{ width: wp('80%') }}>
							<Text style={[ c.light, p.textCenter, f._16, { fontFamily: 'Roboto-BoldItalic' }]}>Jumlah Pembeli yang telah membayar dibagi total pengunjung</Text>
						</View>
						
						<Image source={require('../asset/penjualan.png')} style={[ b.mt3, { width: xs(80), height: xs(80), tintColor: blue }]} />
						<View style={[ b.mt1, { width: wp('90%'), flexDirection: 'row', alignItems: 'center' }]}>
							<View style={{ width: wp('15%'), alignItems: 'flex-end' }}>
								<View style={{ width: xs(22), height: xs(22), backgroundColor: blue, borderRadius: xs(22), borderWidth: 2.5, borderColor: light, alignItems: 'center', justifyContent: 'center' }}>
									<Text style={[ c.light, { fontFamily: 'Roboto-Bold' }]}>3</Text>
								</View>
							</View>
							<View style={{ width: wp('60%'), alignItems: 'center' }}>
								<Text style={[ f._16, { color: 'darkorange', fontFamily: 'Roboto-Bold' }]}>Penjualan per Pembeli</Text>
							</View>
							<View style={{ width: wp('15%') }}>
							</View>
						</View>
						<View style={[ b.mb4, { width: wp('50%') }]}>
							<Text style={[ c.light, p.textCenter, f._16, { fontFamily: 'Roboto-BoldItalic' }]}>Jumlah Penjualan dibagi Total Pembeli</Text>
						</View>
					</View>
				</ScrollView>
			</LinearGradient>
		)
	}
}