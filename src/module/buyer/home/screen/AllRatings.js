import React, { Component } from 'react'
import { Text, View, Image, TouchableOpacity, ScrollView, FlatList } from 'react-native'
import { xs, vs } from '../utils/Responsive'
import { f, c, b, p } from '../utils/StyleHelper'
import { blue, light } from '../utils/Color'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
import FastImage from 'react-native-fast-image'
import AsyncStorage from '@react-native-async-storage/async-storage'
import axios from 'axios'
import { API_URL } from '@env'

export default class AllRatings extends Component {
	constructor(props) {
		super(props);
		this.state = {
			review: [
				{id: 1, nama: 'Rizki Hadi Pratama', bintang: '5', ulasan: 'barang ini bagus sekali, jangan ragu-ragu lagi deh kalau mau beli', created_at: '2021-08-30'},
				{id: 1, nama: 'Rizqi Maulana', bintang: '5', ulasan: 'barang ini bagus sekali, jangan ragu-ragu lagi deh kalau mau beli', created_at: '2021-08-30'}
			]
		};
	}

	hitung_hari(from_date) {
		var date1 = new Date(from_date); 
		var date2 = new Date(new Date().getFullYear(), new Date().getMonth() , new Date().getDate()); 
		// console.log(date1);
		// console.log(date2);
		  
		// To calculate the time difference of two dates 
		var Difference_In_Time = date2.getTime() - date1.getTime();
		  
		// To calculate the no. of days between two dates 
		var Difference_In_Days = Difference_In_Time / (1000 * 3600 * 24);
		if (typeof Difference_In_Days === 'number') {
			if(Number(Difference_In_Days.toString().replace('-', '')).toFixed() == 0){
				return 'Today';
			}else{
				return Number(Difference_In_Days.toString().replace('-', '')).toFixed()+' Days ago';
			}
		}
	}

    render() {
        return (
        	<ScrollView showsVerticalScrollIndicator={false} style={{ backgroundColor: '#181B22' }}>
        		<View style={{ width: '100%', backgroundColor: '#121212', alignItems: 'center' }}>
        			<View style={[ b.mt4, b.mb4, { width: wp('90%'), flexDirection: 'row', alignItems: 'center' }]}>
        				<View style={{ width: wp('15%') }}>
        					<TouchableOpacity onPress={() => this.props.navigation.pop()}>
        						<FastImage source={require('../asset/left.png')} style={{ width: xs(25), height: xs(25) }} />
        					</TouchableOpacity>
        				</View>
        				<View style={{ width: wp('60%'), alignItems: 'center' }}>
        					<Text style={[ c.light, f._16, { fontFamily: 'Roboto-Bold' }]}>Ulasan</Text>
        				</View>
        				<View style={{ width: wp('15%') }}>
        				</View>
        			</View>

        			<Text style={{ fontFamily: 'Roboto-Bold', color: 'darkorange', fontSize: xs(30) }}>5.0</Text>
        			<View style={[ b.mt1, { flexDirection: 'row', alignItems: 'center' }]}>
        				<Image source={require('../asset/star.png')} style={[ b.mr1, { width: wp('5%'), height: wp('5%'), tintColor: '#FE8829' }]} />
                        <Image source={require('../asset/star.png')} style={[ b.mr1, { width: wp('5%'), height: wp('5%'), tintColor: '#FE8829' }]} />
                        <Image source={require('../asset/star.png')} style={[ b.mr1, { width: wp('5%'), height: wp('5%'), tintColor: '#FE8829' }]} />
                        <Image source={require('../asset/star.png')} style={[ b.mr1, { width: wp('5%'), height: wp('5%'), tintColor: '#FE8829' }]} />
                        <Image source={require('../asset/star.png')} style={{ width: wp('5%'), height: wp('5%'), tintColor: '#FE8829' }} />
        			</View>
        			<Text style={[ b.mt1, { fontFamily: 'Roboto-Regular', color: 'darkorange' }]}>berdasarkan dari 10 ulasan</Text>

        			<View style={[ b.mt4, b.mb4, { width: wp('90%') }]}>
        				<View style={{ flexDirection: 'row', alignItems: 'center' }}>
        					<View style={{ width: wp('15%'), flexDirection: 'row', alignItems: 'center' }}>
	        					<Image source={require('../asset/star.png')} style={{ width: wp('3%'), height: wp('3%'), tintColor: '#FE8829' }} />
		                        <Image source={require('../asset/star.png')} style={{ width: wp('3%'), height: wp('3%'), tintColor: '#FE8829' }} />
		                        <Image source={require('../asset/star.png')} style={{ width: wp('3%'), height: wp('3%'), tintColor: '#FE8829' }} />
		                        <Image source={require('../asset/star.png')} style={{ width: wp('3%'), height: wp('3%'), tintColor: '#FE8829' }} />
		                        <Image source={require('../asset/star.png')} style={{ width: wp('3%'), height: wp('3%'), tintColor: '#FE8829' }} />
		        			</View>
		        			<View style={{ width: wp('55%'), alignItems: 'flex-end' }}>
		        				<View style={{ width: wp('45%'), height: hp('1%'), backgroundColor: 'darkorange', borderRadius: xs(5) }} />
		        			</View>
		        			<View style={{ width: wp('10%'), alignItems: 'flex-end' }}>
		        				<Text style={{ color: 'darkorange', fontFamily: 'Roboto-Regular' }}>10</Text>
		        			</View>
	        			</View>
	        			<View style={{ flexDirection: 'row', alignItems: 'center' }}>
        					<View style={{ width: wp('15%'), flexDirection: 'row', alignItems: 'center' }}>
	        					<Image source={require('../asset/star.png')} style={{ width: wp('3%'), height: wp('3%'), tintColor: '#FE8829' }} />
		                        <Image source={require('../asset/star.png')} style={{ width: wp('3%'), height: wp('3%'), tintColor: '#FE8829' }} />
		                        <Image source={require('../asset/star.png')} style={{ width: wp('3%'), height: wp('3%'), tintColor: '#FE8829' }} />
		                        <Image source={require('../asset/star.png')} style={{ width: wp('3%'), height: wp('3%'), tintColor: '#FE8829' }} />
		                        <Image source={require('../asset/star.png')} style={{ width: wp('3%'), height: wp('3%'), tintColor: light }} />
		        			</View>
		        			<View style={{ width: wp('55%'), alignItems: 'flex-end' }}>
		        				<View style={{ width: wp('45%'), height: hp('1%'), backgroundColor: light, borderRadius: xs(5) }} />
		        			</View>
		        			<View style={{ width: wp('10%'), alignItems: 'flex-end' }}>
		        				<Text style={{ color: 'darkorange', fontFamily: 'Roboto-Regular' }}>0</Text>
		        			</View>
	        			</View>
	        			<View style={{ flexDirection: 'row', alignItems: 'center' }}>
        					<View style={{ width: wp('15%'), flexDirection: 'row', alignItems: 'center' }}>
	        					<Image source={require('../asset/star.png')} style={{ width: wp('3%'), height: wp('3%'), tintColor: '#FE8829' }} />
		                        <Image source={require('../asset/star.png')} style={{ width: wp('3%'), height: wp('3%'), tintColor: '#FE8829' }} />
		                        <Image source={require('../asset/star.png')} style={{ width: wp('3%'), height: wp('3%'), tintColor: '#FE8829' }} />
		                        <Image source={require('../asset/star.png')} style={{ width: wp('3%'), height: wp('3%'), tintColor: light }} />
		                        <Image source={require('../asset/star.png')} style={{ width: wp('3%'), height: wp('3%'), tintColor: light }} />
		        			</View>
		        			<View style={{ width: wp('55%'), alignItems: 'flex-end' }}>
		        				<View style={{ width: wp('45%'), height: hp('1%'), backgroundColor: light, borderRadius: xs(5) }} />
		        			</View>
		        			<View style={{ width: wp('10%'), alignItems: 'flex-end' }}>
		        				<Text style={{ color: 'darkorange', fontFamily: 'Roboto-Regular' }}>0</Text>
		        			</View>
	        			</View>
	        			<View style={{ flexDirection: 'row', alignItems: 'center' }}>
        					<View style={{ width: wp('15%'), flexDirection: 'row', alignItems: 'center' }}>
	        					<Image source={require('../asset/star.png')} style={{ width: wp('3%'), height: wp('3%'), tintColor: '#FE8829' }} />
		                        <Image source={require('../asset/star.png')} style={{ width: wp('3%'), height: wp('3%'), tintColor: '#FE8829' }} />
		                        <Image source={require('../asset/star.png')} style={{ width: wp('3%'), height: wp('3%'), tintColor: light }} />
		                        <Image source={require('../asset/star.png')} style={{ width: wp('3%'), height: wp('3%'), tintColor: light }} />
		                        <Image source={require('../asset/star.png')} style={{ width: wp('3%'), height: wp('3%'), tintColor: light }} />
		        			</View>
		        			<View style={{ width: wp('55%'), alignItems: 'flex-end' }}>
		        				<View style={{ width: wp('45%'), height: hp('1%'), backgroundColor: light, borderRadius: xs(5) }} />
		        			</View>
		        			<View style={{ width: wp('10%'), alignItems: 'flex-end' }}>
		        				<Text style={{ color: 'darkorange', fontFamily: 'Roboto-Regular' }}>0</Text>
		        			</View>
	        			</View>
	        			<View style={{ flexDirection: 'row', alignItems: 'center' }}>
        					<View style={{ width: wp('15%'), flexDirection: 'row', alignItems: 'center' }}>
	        					<Image source={require('../asset/star.png')} style={{ width: wp('3%'), height: wp('3%'), tintColor: '#FE8829' }} />
		                        <Image source={require('../asset/star.png')} style={{ width: wp('3%'), height: wp('3%'), tintColor: light }} />
		                        <Image source={require('../asset/star.png')} style={{ width: wp('3%'), height: wp('3%'), tintColor: light }} />
		                        <Image source={require('../asset/star.png')} style={{ width: wp('3%'), height: wp('3%'), tintColor: light }} />
		                        <Image source={require('../asset/star.png')} style={{ width: wp('3%'), height: wp('3%'), tintColor: light }} />
		        			</View>
		        			<View style={{ width: wp('55%'), alignItems: 'flex-end' }}>
		        				<View style={{ width: wp('45%'), height: hp('1%'), backgroundColor: light, borderRadius: xs(5) }} />
		        			</View>
		        			<View style={{ width: wp('10%'), alignItems: 'flex-end' }}>
		        				<Text style={{ color: 'darkorange', fontFamily: 'Roboto-Regular' }}>0</Text>
		        			</View>
	        			</View>
        			</View>
        		</View>

        		<View style={[ b.mt4, { width: '100%', alignItems: 'center' }]}>
        			<View style={{ width: wp('90%') }}>
        				<FlatList
                            data={this.state.review}
                            keyExtractor={item => item.id}
                            renderItem={({item}) => (
                                <>
                                    <View style={{ width: '100%', height: wp('15%'), flexDirection: 'row' }}>
                                        <View style={{ width: wp('15%'), height: '100%' }}>
                                            <Image source={require('../asset/10.png')} style={{ width: '100%', height: '100%' }} />
                                        </View>
                                        <View style={{ width: wp('50%'), height: '100%' }}>
                                            <Text style={[ c.light, { fontFamily: 'lineto-circular-pro-bold', marginLeft: wp('4%'), marginTop: wp('1%') }]}>{item.nama}</Text>
                                            <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: wp('1%'), marginLeft: wp('4%') }}>
                                                {item.bintang === '5' && (
                                                    <>
                                                        <Image source={require('../asset/star.png')} style={{ width: wp('5%'), height: wp('5%'), tintColor: '#FE8829' }} />
                                                        <Image source={require('../asset/star.png')} style={{ width: wp('5%'), height: wp('5%'), tintColor: '#FE8829' }} />
                                                        <Image source={require('../asset/star.png')} style={{ width: wp('5%'), height: wp('5%'), tintColor: '#FE8829' }} />
                                                        <Image source={require('../asset/star.png')} style={{ width: wp('5%'), height: wp('5%'), tintColor: '#FE8829' }} />
                                                        <Image source={require('../asset/star.png')} style={{ width: wp('5%'), height: wp('5%'), tintColor: '#FE8829' }} />
                                                    </>
                                                )}
                                                {item.bintang === '4' && (
                                                    <>
                                                        <Image source={require('../asset/star.png')} style={{ width: wp('5%'), height: wp('5%'), tintColor: '#FE8829' }} />
                                                        <Image source={require('../asset/star.png')} style={{ width: wp('5%'), height: wp('5%'), tintColor: '#FE8829' }} />
                                                        <Image source={require('../asset/star.png')} style={{ width: wp('5%'), height: wp('5%'), tintColor: '#FE8829' }} />
                                                        <Image source={require('../asset/star.png')} style={{ width: wp('5%'), height: wp('5%'), tintColor: '#FE8829' }} />
                                                        <Image source={require('../asset/star.png')} style={{ width: wp('5%'), height: wp('5%') }} />
                                                    </>
                                                )}
                                                {item.bintang === '3' && (
                                                    <>
                                                        <Image source={require('../asset/star.png')} style={{ width: wp('5%'), height: wp('5%'), tintColor: '#FE8829' }} />
                                                        <Image source={require('../asset/star.png')} style={{ width: wp('5%'), height: wp('5%'), tintColor: '#FE8829' }} />
                                                        <Image source={require('../asset/star.png')} style={{ width: wp('5%'), height: wp('5%'), tintColor: '#FE8829' }} />
                                                        <Image source={require('../asset/star.png')} style={{ width: wp('5%'), height: wp('5%') }} />
                                                        <Image source={require('../asset/star.png')} style={{ width: wp('5%'), height: wp('5%') }} />
                                                    </>
                                                )}
                                                {item.bintang === '2' && (
                                                    <>
                                                        <Image source={require('../asset/star.png')} style={{ width: wp('5%'), height: wp('5%'), tintColor: '#FE8829' }} />
                                                        <Image source={require('../asset/star.png')} style={{ width: wp('5%'), height: wp('5%'), tintColor: '#FE8829' }} />
                                                        <Image source={require('../asset/star.png')} style={{ width: wp('5%'), height: wp('5%') }} />
                                                        <Image source={require('../asset/star.png')} style={{ width: wp('5%'), height: wp('5%') }} />
                                                        <Image source={require('../asset/star.png')} style={{ width: wp('5%'), height: wp('5%') }} />
                                                    </>
                                                )}
                                                {item.bintang === '1' && (
                                                    <>
                                                        <Image source={require('../asset/star.png')} style={{ width: wp('5%'), height: wp('5%'), tintColor: '#FE8829' }} />
                                                        <Image source={require('../asset/star.png')} style={{ width: wp('5%'), height: wp('5%') }} />
                                                        <Image source={require('../asset/star.png')} style={{ width: wp('5%'), height: wp('5%') }} />
                                                        <Image source={require('../asset/star.png')} style={{ width: wp('5%'), height: wp('5%') }} />
                                                        <Image source={require('../asset/star.png')} style={{ width: wp('5%'), height: wp('5%') }} />
                                                    </>
                                                )}
                                                <Text style={[ c.light, { marginLeft: wp('2%') }]}>{item.bintang}.0</Text>
                                            </View>
                                        </View>
                                        <View style={{ width: wp('25%'), alignItems: 'flex-end' }}>
                                            <Text style={[ c.light, f._12, { marginTop: wp('7.3%') }]}>{this.hitung_hari(item.created_at)}</Text>
                                        </View>
                                    </View>

                                    <View style={[ b.mt2, b.mb4, { width: '100%' }]}>
                                        <Text style={[ c.light, { textAlign: 'justify' }]}>{item.ulasan}</Text>
                                    </View>
                                </>
                            )}
                        />
        			</View>
        		</View>
        	</ScrollView>
        )
    }
}
