import React, { Component } from 'react'
import { View, Text, Image, TouchableOpacity, ToastAndroid } from 'react-native'
import { blue, light } from '../utils/Color'
import { xs, vs } from '../utils/Responsive'
import { f, c, b, p } from '../utils/StyleHelper'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
import FastImage from 'react-native-fast-image'
import LinearGradient from 'react-native-linear-gradient'
import AsyncStorage from '@react-native-async-storage/async-storage'
import axios from 'axios'
import { API_LOCAL, API_URL } from '@env'

export default class EdukasiPenjual extends Component {
	constructor(props) {
		super(props);
		this.state = {
		}
	}

	render() {
		return (
			<LinearGradient start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }} colors={[ '#181C21', '#252F38', '#252F38' ]} style={{ width: '100%', height: '100%' }}>
				<View style={{ width: '100%', backgroundColor: '#121212', alignItems: 'center' }}>
					<View style={{ width: wp('90%'), height: hp('10%'), flexDirection: 'row', alignItems: 'center' }}>
						<View style={{ width: wp('15%') }}>
							<TouchableOpacity onPress={() => this.props.navigation.pop()}>
								<FastImage source={require('../asset/left.png')} style={{ width: xs(25), height: xs(25) }} />
							</TouchableOpacity>
						</View>
						<View style={{ width: wp('60%'), alignItems: 'center' }}>
							<Text style={[ c.light, { fontFamily: 'Roboto-Bold' }]}>PEMBERITAHUAN PENJUAL</Text>
						</View>
						<View style={{ width: wp('15%') }}>
						</View>
					</View>
				</View>

				<View style={{ width: '100%', alignItems: 'center' }}>
					<View style={[ b.rounded, { width: xs(320), height: xs(130), backgroundColor: '#323232', marginTop: vs(30), alignItems: 'center', justifyContent: 'center' }]}>
						<Text style={[ c.light, f._24, { fontFamily: 'Roboto-Bold' }]}>Apa itu seller GIFTPLUS?</Text>
						<View style={{ flexDirection: 'row', alignItems: 'center', backgroundColor: 'darkorange', borderTopLeftRadius: xs(20), borderBottomLeftRadius: xs(20), position: 'absolute', right: vs(10), top: 0 }}>
							<View style={{ width: xs(35), height: xs(35), borderRadius: xs(35), borderWidth: 3, borderColor: '#323232', alignItems: 'center', justifyContent: 'center' }}>
								<Image source={require('../asset/icongabungjadiseller.png')} style={{ width: xs(18), height: xs(18), tintColor: light }} />
							</View>
							<View style={{ height: xs(30), alignItems: 'center', justifyContent: 'center' }}>
								<Text style={[ c.light, b.ml1, b.mr2, { fontFamily: 'Roboto-Regular' }]}>Khusus Penjual</Text>
							</View>
						</View>
					</View>

					<View style={[ b.mt4, { width: wp('90%'), flexDirection: 'row' }]}>
						<View style={{ width: wp('45%') }}>
							<TouchableOpacity onPress={() => this.props.navigation.push('Tren')} style={[ b.rounded, { width: wp('43%'), height: wp('43%'), backgroundColor: blue, alignItems: 'center', justifyContent: 'center' }]}>
								<FastImage source={require('../asset/edu4.png')} style={{ width: xs(65), height: xs(50) }} />
								<View style={[ b.ml2, b.mr2, b.mt2 ]}>
									<Text style={[ c.light, p.textCenter, { fontFamily: 'Roboto-Bold' }]}>Memperkirakan tren dan mengidentifikasi pasar</Text>
								</View>
							</TouchableOpacity>
						</View>
						<View style={{ width: wp('45%'), alignItems: 'flex-end' }}>
							<TouchableOpacity onPress={() => this.props.navigation.push('Strategi')} style={[ b.rounded, { width: wp('43%'), height: wp('43%'), backgroundColor: blue, alignItems: 'center', justifyContent: 'center' }]}>
								<FastImage source={require('../asset/edu3.png')} style={{ width: xs(65.5), height: xs(65) }} />
								<View style={[ b.ml2, b.mr2, b.mt2 ]}>
									<Text style={[ c.light, p.textCenter, { fontFamily: 'Roboto-Bold' }]}>Mempelajari lebih dalam untuk penyesuaian strategi</Text>
								</View>
							</TouchableOpacity>
						</View>
					</View>
					<View style={[ b.mt3, { width: wp('90%'), flexDirection: 'row' }]}>
						<View style={{ width: wp('45%') }}>
							<TouchableOpacity onPress={() => this.props.navigation.push('Kriteria')} style={[ b.rounded, { width: wp('43%'), height: wp('43%'), backgroundColor: blue, alignItems: 'center', justifyContent: 'center' }]}>
								<FastImage source={require('../asset/edu2.png')} style={{ width: xs(65), height: xs(56) }} />
								<View style={[ b.ml2, b.mr2, b.mt2 ]}>
									<Text style={[ c.light, p.textCenter, { fontFamily: 'Roboto-Bold' }]}>Memahami kriteria penjualan</Text>
								</View>
							</TouchableOpacity>
						</View>
						<View style={{ width: wp('45%'), alignItems: 'flex-end' }}>
							<TouchableOpacity onPress={() => this.props.navigation.push('Performa')} style={[ b.rounded, { width: wp('43%'), height: wp('43%'), backgroundColor: blue, alignItems: 'center', justifyContent: 'center' }]}>
								<FastImage source={require('../asset/edu1.png')} style={{ width: xs(72), height: xs(50) }} />
								<View style={[ b.ml2, b.mr2, b.mt2 ]}>
									<Text style={[ c.light, p.textCenter, { fontFamily: 'Roboto-Bold' }]}>Memantau performa anda</Text>
								</View>
							</TouchableOpacity>
						</View>
					</View>
				</View>
			</LinearGradient>
		)
	}
}