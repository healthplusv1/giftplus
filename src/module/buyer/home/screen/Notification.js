import React, { Component } from 'react'
import { View, Text, Image, TouchableOpacity, FlatList, ScrollView } from 'react-native'
import { blue, light } from '../utils/Color'
import { xs, vs } from '../utils/Responsive'
import { f, c, b, p } from '../utils/StyleHelper'
import FastImage from 'react-native-fast-image'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
import AsyncStorage from '@react-native-async-storage/async-storage'
import axios from 'axios'
import { API_LOCAL, API_URL } from '@env'

export default class Notification extends Component {
	constructor(props) {
		super(props);
		this.state = {
			notif: ''
		}
	}

	render() {
		return (
			<View style={{ width: '100%', height: '100%', backgroundColor: '#121212' }}>
				<View style={{ width: '100%', alignItems: 'center' }}>
					<View style={[ b.mt4, b.mb4, { width: wp('90%'), flexDirection: 'row', alignItems: 'center' }]}>
						<View style={{ width: wp('15%') }}>
							<TouchableOpacity onPress={() => this.props.navigation.pop()}>
								<FastImage source={require('../asset/left.png')} style={{ width: xs(25), height: xs(25) }} />
							</TouchableOpacity>
						</View>
						<View style={{ width: wp('60%'), alignItems: 'center' }}>
							<Text style={[ c.light, f._16, { fontFamily: 'Roboto-Bold' }]}>Notifikasi</Text>
						</View>
						<View style={{ width: wp('15%') }}>
						</View>
					</View>
				</View>

				<ScrollView showsVerticalScrollIndicator={false} style={{ backgroundColor: '#181B22' }}>
					{this.state.notif === '' ?
						<View style={{ width: '100%', alignItems: 'center', marginTop: xs(200) }}>
							<Image source={require('../asset/notification.png')} style={{ width: xs(25), height: xs(25), tintColor: '#CCC' }} />
							<Text style={{ fontFamily: 'Roboto-Bold', color: '#CCC' }}>Tidak ada notifikasi</Text>
						</View>
						:
						null
					}
				</ScrollView>
			</View>
		)
	}
}