import React, { Component } from 'react'
import { View, Text, Image, TouchableOpacity, ToastAndroid } from 'react-native'
import { blue, light } from '../utils/Color'
import { xs, vs } from '../utils/Responsive'
import { f, c, b, p } from '../utils/StyleHelper'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
import FastImage from 'react-native-fast-image'
import AsyncStorage from '@react-native-async-storage/async-storage'
import axios from 'axios'
import { API_LOCAL, API_URL } from '@env'

export default class GiftCard extends Component {
	constructor(props) {
		super(props);
		this.state = {
			disable_click_like: false,
			liked: false,
			image: require('../asset/love.png')
		}
	}

	componentDidMount() {
		if (this.props.liked) {
			this.setState({
				liked: true,
				image: require('../asset/loved.png')
			})
		} else {
			this.setState({
				liked: false,
				image: require('../asset/love.png')
			})
		}
	}

	async likeProduct() {

		const id_login = await AsyncStorage.getItem('@id_login')
		this.setState({disable_click_like: true})
    	if (this.state.liked === false) {
			axios.post(API_URL+'/main/shopping/favorit', {id_login: id_login, produk_id: this.props.id_product})
			.then(res => {
				ToastAndroid.show(res.data.status, ToastAndroid.SHORT)
				this.setState({disable_click_like: false})
				this.setState({
					liked: true,
					image: require('../asset/loved.png')
				})
			})
		} else {
			axios.get(API_URL+'/main/shopping/favorit/remove/'+this.props.id_product, {params: {id_login: id_login}})
			.then(res => {
				ToastAndroid.show(res.data.status, ToastAndroid.SHORT)
				this.setState({disable_click_like: false})
				this.setState({
					liked: false,
					image: require('../asset/love.png')
				})
			})
		}
    }

    format(number) {
        if (number) {
			var rupiah = "";
			var numberrev = number
				.toString()
				.split("")
				.reverse()
				.join("");
			for (var i = 0; i < numberrev.length; i++)
				if (i % 3 == 0) rupiah += numberrev.substr(i, 3) + ".";
			return (
				"Rp " +
				rupiah
				.split("", rupiah.length - 1)
				.reverse()
				.join("")
			);
        
        } 
        else {
            return (
                "Rp "+number
            );
        }
    }

	render() {
		return (
			<>
				<TouchableOpacity onPress={eval(this.props.action)} style={[ b.mr2, b.mb2, { width: wp('43.5%'), borderColor: blue, borderWidth: 1, borderRadius: xs(5) }]}>
	                <FastImage source={{ uri: API_URL+'/public/product/'+this.props.gambar.split(",")[0] }} style={{ width: '100%', height: xs(150), borderTopLeftRadius: xs(5), borderTopRightRadius: xs(5) }} />
	                <Text style={[ c.light, b.mt1, b.ml1, f._12 ]}>{this.props.nama}</Text>
	                <Text style={[ c.light, b.ml1, f._12 ]}>{this.format(this.props.harga)}</Text>
	                <View style={[ p.row, b.ml1, b.mt1 ]}>
	                    <Image source={require('../asset/tokolokasi_icon.png')} style={{ width: xs(15), height: xs(15) }} />
	                    <Text style={[ b.ml1, c.light, f._12 ]}>Semarang</Text>
	                </View>
	                <View style={[ b.mt1, b.mb2, b.ml1, { flexDirection: 'row', alignItems: 'center' }]}>
	                    <Image source={require('../asset/star.png')} style={{ width: xs(13), height: xs(13), tintColor: '#FE8829' }} />
	                    <Text style={[ f._12, { color: '#FE8829', fontFamily: 'Roboto-Regular', marginLeft: xs(2) }]}>{this.props.avg_ratings}.0</Text>
	                    <View style={{ width: xs(10), borderBottomWidth: 1, borderBottomColor: '#FE8829', transform: [{ rotate: '90deg' }] }} />
	                        <Text style={[ f._12, { color: '#FE8829', fontFamily: 'Roboto-Regular' }]}>{this.props.ratings_length} Ulasan</Text>
	                </View>
	                <TouchableOpacity onPress={() => this.likeProduct()} style={{ position: 'absolute', bottom: xs(9), right: xs(5) }}>
	                	<Image source={this.state.image} style={{ width: xs(20), height: xs(20), tintColor: blue }} />
	                </TouchableOpacity>
	            </TouchableOpacity>
			</>
 		)
	}
}