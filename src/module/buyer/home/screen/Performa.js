import React, { Component } from 'react'
import { View, Text, Image, TouchableOpacity } from 'react-native'
import { blue, light } from '../utils/Color'
import { xs, vs } from '../utils/Responsive'
import { f, c, b, p } from '../utils/StyleHelper'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
import FastImage from 'react-native-fast-image'
import LinearGradient from 'react-native-linear-gradient'
import Swiper from 'react-native-swiper'
import AsyncStorage from '@react-native-async-storage/async-storage'
import axios from 'axios'
import { API_LOCAL, API_URL } from '@env'

export default class Performa extends Component {
	constructor(props) {
		super(props);
		this.state = {
		}
	}

	render() {
		return (
			<LinearGradient start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }} colors={[ '#181C21', '#252F38', '#252F38' ]} style={{ width: '100%', height: '100%' }}>
				<View style={{ width: '100%', backgroundColor: '#121212', alignItems: 'center' }}>
					<View style={{ width: wp('90%'), height: hp('10%'), flexDirection: 'row', alignItems: 'center' }}>
						<View style={{ width: wp('15%') }}>
							<TouchableOpacity onPress={() => this.props.navigation.pop()}>
								<FastImage source={require('../asset/left.png')} style={{ width: xs(25), height: xs(25) }} />
							</TouchableOpacity>
						</View>
						<View style={{ width: wp('60%'), alignItems: 'center' }}>
							<Text style={[ c.light, f._16, { fontFamily: 'Roboto-Bold' }]}>PEMBERITAHUAN PENJUAL</Text>
						</View>
						<View style={{ width: wp('15%') }}>
						</View>
					</View>
				</View>

				<View style={{ width: '100%', alignItems: 'center' }}>
					<View style={[ b.rounded, { width: xs(320), height: xs(130), backgroundColor: '#323232', marginTop: vs(25), flexDirection: 'row', alignItems: 'center' }]}>
						<FastImage source={require('../asset/edu1.png')} style={[ b.ml4, { width: xs(87), height: xs(60) }]} />
						<View style={[ b.ml2, { width: xs(180) }]}>
							<Text style={[ c.light, p.textCenter, f._18, { fontFamily: 'Roboto-Regular' }]}>Memantau performa anda</Text>
						</View>
					</View>
				</View>

				<Swiper
					loop={false}
					dotColor={'#CCC'}
					showsButtons={true}
					buttonWrapperStyle={{ backgroundColor: 'transparent', flexDirection: 'row', position: 'absolute', top: xs(175), left: 0, flex: 1, paddingHorizontal: 10, paddingVertical: 10, justifyContent: 'space-between', alignItems: 'center' }}
					nextButton={<Text style={[ c.blue, f._18, { fontFamily: 'Roboto-Bold' }]}>Next</Text>}
					prevButton={<Text style={[ c.blue, f._18, { fontFamily: 'Roboto-Bold' }]}>Prev</Text>}
				>
					<View style={{ width: '100%', height: '100%', alignItems: 'center' }}>
						<View style={[ b.mt4, { width: wp('90%'), flexDirection: 'row' }]}>
							<View style={{ width: wp('70%') }}>
								<Text style={[ c.light, f._18, { fontFamily: 'Roboto-Regular' }]}>Informasi penjualan:</Text>
								<Text style={[ c.light, f._18, b.mt3, { fontFamily: 'Roboto-Bold' }]}>1. Total Pengunjung</Text>
							</View>
							<View style={{ width: wp('20%'), alignItems: 'flex-end' }}>
								<FastImage source={require('../asset/bulb.png')} style={{ width: xs(48), height: xs(60) }} tintColor={light} />
							</View>
						</View>

						<FastImage source={require('../asset/rectangle_50.png')} style={[ b.mt4, { width: xs(250), height: xs(220) }]}>
							<View style={{ width: xs(20), height: xs(20), backgroundColor: '#ED4E2C', borderRadius: xs(20), alignItems: 'center', justifyContent: 'center', top: vs(70), left: vs(20), zIndex: 1 }}>
								<Text style={[ c.light, { fontFamily: 'Roboto-Bold' }]}>1</Text>
							</View>
							<View style={{ width: xs(135), height: xs(50), borderWidth: 4, borderColor: '#ED4E2C', position: 'absolute', top: vs(80), left: vs(30) }} />
						</FastImage>
					</View>

					<View style={{ width: '100%', height: '100%', alignItems: 'center' }}>
						<View style={[ b.mt4, { width: wp('90%'), flexDirection: 'row' }]}>
							<View style={{ width: wp('70%') }}>
								<Text style={[ c.light, f._18, { fontFamily: 'Roboto-Regular' }]}>Informasi penjualan:</Text>
								<Text style={[ c.light, f._18, b.mt3, { fontFamily: 'Roboto-Bold' }]}>2. Tingkat Konversi</Text>
							</View>
							<View style={{ width: wp('20%'), alignItems: 'flex-end' }}>
								<FastImage source={require('../asset/bulb.png')} style={{ width: xs(48), height: xs(60) }} tintColor={light} />
							</View>
						</View>

						<FastImage source={require('../asset/rectangle_57.png')} style={[ b.mt4, { width: xs(250), height: xs(240) }]}>
							<View style={{ width: xs(20), height: xs(20), backgroundColor: '#ED4E2C', borderRadius: xs(20), alignItems: 'center', justifyContent: 'center', top: vs(35), left: vs(20), zIndex: 1 }}>
								<Text style={[ c.light, { fontFamily: 'Roboto-Bold' }]}>2</Text>
							</View>
							<View style={{ width: xs(225), height: xs(200), borderWidth: 4, borderColor: '#ED4E2C', position: 'absolute', bottom: 0, right: 0 }} />
						</FastImage>
					</View>

					<View style={{ width: '100%', height: '100%', alignItems: 'center' }}>
						<View style={[ b.mt4, { width: wp('90%'), flexDirection: 'row' }]}>
							<View style={{ width: wp('70%') }}>
								<Text style={[ c.light, f._18, { fontFamily: 'Roboto-Regular' }]}>Informasi penjualan:</Text>
								<Text style={[ c.light, f._18, b.mt3, { fontFamily: 'Roboto-Bold' }]}>3. Penjualan per Pembeli</Text>
							</View>
							<View style={{ width: wp('20%'), alignItems: 'flex-end' }}>
								<FastImage source={require('../asset/bulb.png')} style={{ width: xs(48), height: xs(60) }} tintColor={light} />
							</View>
						</View>

						<FastImage source={require('../asset/rectangle_62.png')} style={[ b.mt4, { width: xs(300), height: xs(170) }]}>
							<View style={{ width: xs(20), height: xs(20), backgroundColor: '#ED4E2C', borderRadius: xs(20), alignItems: 'center', justifyContent: 'center', top: vs(35), left: vs(70), zIndex: 1 }}>
								<Text style={[ c.light, { fontFamily: 'Roboto-Bold' }]}>3</Text>
							</View>
							<View style={{ width: xs(200), height: xs(130), borderWidth: 4, borderColor: '#ED4E2C', position: 'absolute', bottom: 0, right: vs(30) }} />
						</FastImage>
					</View>
				</Swiper>
			</LinearGradient>
		)
	}
}