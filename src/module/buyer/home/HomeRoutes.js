import { createStackNavigator } from 'react-navigation-stack'
import HomePage from './screen/Home'
import AllProductPage from './screen/AllProduct'
import ProductDetailPage from './screen/ProductDetail'

export const HomeStack = createStackNavigator({
	Home: {
		screen: HomePage,
		navigationOptions: {
            headerShown: false
        }
	},

	AllProduct: {
		screen: AllProductPage,
		navigationOptions: {
			headerShown: false
		}
	},

    ProductDetail: {
        screen: ProductDetailPage,
        navigationOptions: {
            headerShown: false
        }
    },
})