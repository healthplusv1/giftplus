import React, { Component } from 'react'
import { Text, View, Image, ScrollView, TouchableOpacity, TextInput, FlatList, RefreshControl } from 'react-native'
import { blue, light, softGrey } from '../utils/Color'
import { xs, vs } from '../utils/Responsive'
import { f, c, b, p } from '../utils/StyleHelper'
import FastImage from 'react-native-fast-image'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
import AsyncStorage from '@react-native-async-storage/async-storage'
import axios from 'axios'
import { API_LOCAL, API_URL } from '@env'

export default class AllProduct extends Component {
    constructor(props) {
        super(props);
        this.state = {
            search: '',
			count: '',
			loading: true,
            all_products: undefined,
        }
    }

	async componentDidMount() {
		this.setState({ search: this.props.navigation.getParam('keyword') })

		const id_login = await AsyncStorage.getItem('@id_login')

		if (id_login === null) {
			const guest = await AsyncStorage.getItem('id_login_guest')

			axios.get(API_URL+'/main/shopping/cart', {params: {
				id_login: guest
			}})
			.then(res => {
				this.setState({
					count: res.data.data.length,
					loading: false
				})
			})
		} else {
			axios.get(API_URL+'/main/shopping/cart', {params: {
				id_login: id_login
			}})
			.then(res => {
				this.setState({
					count: res.data.data.length,
					loading: false
				})
			})
		}

		if (this.state.search === undefined || this.state.search === '') {
			axios.get(API_URL+'/main/shopping/produk')
			.then(result => {
				this.setState({
					all_products: result.data.data,
					loading: false
				})
			})
		} else {
			axios.get(API_URL+'/main/shopping/search', {params: {
	            keyword: this.props.navigation.getParam('keyword')
	        }})
	        .then(result => {
	            this.setState({
	                all_products: result.data.data,
	                search: this.props.navigation.getParam('keyword'),
	                loading: false
	            })
	        })
		}
	}

	nextScreen() {
        axios.get(API_URL+'/main/shopping/search', {params: {
            keyword: this.state.search
        }})
        .then(res => {
            // console.log(this.state.search+' = '+JSON.stringify(res.data.data))
            this.props.navigation.setParams({ keyword: this.state.search })
            this.componentDidMount()
        })
    }

	format(number) {
        if (number) {
			var rupiah = "";
			var numberrev = number
				.toString()
				.split("")
				.reverse()
				.join("");
			for (var i = 0; i < numberrev.length; i++)
				if (i % 3 == 0) rupiah += numberrev.substr(i, 3) + ".";
			return (
				"Rp " +
				rupiah
				.split("", rupiah.length - 1)
				.reverse()
				.join("")
			);
        } else {
            return (
                "Rp "+number
            );
        }
    }

    render() {
        return (
            <View style={{ width: '100%', height: '100%', backgroundColor: '#121212' }}>
                <View style={{ width: '100%', alignItems: 'center' }}>
                    <View style={[ b.mt4, b.mb4, p.row, { width: wp('90%') }]}>
                		<View style={{ width: wp('45%') }}>
                			<FastImage source={require('../asset/giftplus.png')} style={{ width: xs(120), height: xs(32) }} />
                		</View>
                		<View style={{ width: wp('45%'), flexDirection: 'row-reverse' }}>
                			<TouchableOpacity onPress={() => this.props.navigation.push('Drawer')}>
                				<Image source={require('../asset/menu.png')} style={{ width: xs(22), height: xs(22), tintColor: light }} />
                			</TouchableOpacity>
                			<TouchableOpacity onPress={() => this.props.navigation.push('Notification')} style={[ b.mr2 ]}>
                				<Image source={require('../asset/notification.png')} style={{ width: xs(22), height: xs(22), tintColor: light }} />
                			</TouchableOpacity>
                			<TouchableOpacity onPress={() => this.props.navigation.push('Cart')} style={[ b.mr2 ]}>
                				<Image source={require('../asset/addCart.png')} style={{ width: xs(22), height: xs(22), tintColor: light }} />
                                {this.state.count !== 0 && (
									<View style={{ width: xs(12), height: xs(12), borderRadius: xs(12), backgroundColor: 'darkorange', position: 'absolute', right: 0, alignItems: 'center', justifyContent: 'center' }}>
	                                    <Text style={[ c.light, f._10 ]}>{this.state.count}</Text>
	                                </View>
                            	)}
							</TouchableOpacity>
                		</View>
                	</View>

                    <View style={{ width: wp('90%'), height: hp('6.5%'), borderRadius: wp('5%'), backgroundColor: softGrey, flexDirection: 'row', alignItems: 'center' }}>
						<TextInput
							style={[ c.light, { width: wp('62%'), height: '100%', marginLeft: wp('4%') }]}
							ref={ref => this.textInputRef = ref}
							placeholder='Apa yang kamu cari'
							placeholderTextColor={light}
							returnKeyType='search'
							onChangeText={(text) => this.setState({ search: text })}
							onSubmitEditing={() => this.nextScreen()}
							value={this.state.search}
						/>
						<View style={{ width: wp('20%'), flexDirection: 'row-reverse' }}>
							{this.state.search === '' || this.state.search === undefined ?
								<TouchableOpacity onPress={() => this.textInputRef.focus()}>
									<Image source={require('../asset/search.png')} style={{ width: wp('6%'), height: wp('6%'), tintColor: light, marginLeft: wp('4%') }} />
								</TouchableOpacity>
								:
								<TouchableOpacity onPress={() => this.setState({ search: '' })} style={{ marginLeft: wp('4%') }}>
									<Image source={require('../asset/cross.png')} style={{ width: wp('6%'), height: wp('6%'), tintColor: light }} />
								</TouchableOpacity>
							}
							{/* <TouchableOpacity>
								<Image source={require('../asset/camera.png')} style={{ width: wp('6%'), height: wp('6%'), tintColor: light }} />
							</TouchableOpacity> */}
						</View>
					</View>
                </View>
                <ScrollView style={[ b.mt4 ]} refreshControl={<RefreshControl onRefresh={() => this.componentDidMount()} refreshing={this.state.loading} />}>
                    <View style={{ width: '100%', alignItems: 'center' }}>
                        <View style={{ width: wp('90%'), marginBottom: xs(10) }}>
                            <FlatList
                                numColumns={2}
                                data={this.state.all_products}
                                keyExtractor={item => item.id}
                                renderItem={({ item }) => (
                                    <TouchableOpacity onPress={() => this.props.navigation.push('ProductDetail', {id_product: item.id})} style={[ b.mr2, b.mb2, { width: wp('43.5%'), borderColor: blue, borderWidth: 1, borderRadius: xs(5) }]}>
                                        <FastImage source={{ uri: API_URL+'/public/product/'+item.gambar.split(",",1) }} style={{ width: '100%', height: xs(150), borderTopLeftRadius: xs(5), borderTopRightRadius: xs(5) }} />
                                        <Text style={[ c.light, b.mt1, b.ml1, f._12 ]}>{item.nama}</Text>
                                        <Text style={[ c.light, b.ml1, f._12 ]}>{this.format(item.harga)}</Text>
                                        <View style={[ p.row, b.ml1, b.mt1, b.mb1 ]}>
                                            <Image source={require('../asset/tokolokasi_icon.png')} style={{ width: xs(15), height: xs(15) }} />
                                            <Text style={[ b.ml1, c.light, f._12 ]}>Semarang</Text>
                                        </View>
                                    </TouchableOpacity>
                                )}
                            />
                        </View>
                    </View>
                </ScrollView>
            </View>
        )
    }
}
