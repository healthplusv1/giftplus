import React, { Component } from 'react'
import { View, Text, Image, TouchableOpacity, FlatList, ScrollView } from 'react-native'
import { blue, light, softBlue } from '../utils/Color'
import { xs, vs } from '../utils/Responsive'
import { f, c, b, p } from '../utils/StyleHelper'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
import FastImage from 'react-native-fast-image'
import LinearGradient from 'react-native-linear-gradient'
import QRCodeScanner from 'react-native-qrcode-scanner'
import Dialog, { DialogContent } from 'react-native-popup-dialog'
import { NavigationEvents } from 'react-navigation'
import AsyncStorage from '@react-native-async-storage/async-storage'
import axios from 'axios'
import { API_LOCAL, API_URL } from '@env'

export default class ScanQRSeller extends Component {
	constructor(props) {
		super(props);
		this.state = {
			scan: [
                {title: 'Scan QR'},
                {title: 'Riwayat Scan'}
            ],
            clicked: 0,
            scanner: true,
            ScanResult: false,
            result: null,
            visible: false,
            detailScan: [
            	{id: 1, no_pesanan: '123456789', tgl_pesan: '2021-09-15', namaPemesan: 'Pratama', items: [{id: 1, nama: 'Fox Premium glove motor Black Sarung Tangan', image: require('../asset/sarung_tangan.png'), stok: 98, harga: '120000', jmlh_produk: 1, total_pesanan: '120000'}, {id: 2, nama: 'Fox Premium glove motor Black Sarung Tangan', image: require('../asset/sarung_tangan.png'), stok: 0, harga: '120000', jmlh_produk: 1, total_pesanan: '120000'}]}
            ],
            historyScan: [
            	{id: 1, no_pesanan: '123456789', jmlh_produk: 1, waktu_scan: '2021-09-30'},
            	{id: 2, no_pesanan: '123456789', jmlh_produk: 1, waktu_scan: '2021-09-29'},
            	{id: 3, no_pesanan: '123456789', jmlh_produk: 1, waktu_scan: '2021-09-28'},
            	{id: 4, no_pesanan: '123456789', jmlh_produk: 1, waktu_scan: '2021-09-27'},
            ],
            products: [
		    	{id: 1, nama: 'Fox Premium glove motor Black Sarung Tangan', image: require('../asset/sarung_tangan.png'), harga: '120000', jml_barang: '1', rating: '5', logo_apotek: require('../asset/logo_apotek.png'), nama_apotek: 'Apotek Sehati', no_pesanan: '123456789', tgl_pesan: '19/08/2021', total_pesanan: '120000'}
		    ],
            historyScanStatus: true,
            detailHistoryScan: false
		}
	}

	async swicth(index) {
        await this.setState({
            clicked: index
        })
    }

    onSuccess = (e) => {
        const check = e.data.substring(0, 4);
        console.log('scanned data' + check);
        this.setState({
            result: e,
            scanner: false
        })
        if (check === 'http') {
            // Linking.openURL(e.data).catch(err => console.error('An error occured', err));
            // console.log(e.data)
            axios.get(e.data)
            .then(res => {
            	console.log(res.data.data)
            })
            this.setState({
                result: e,
                scanner: false,
                ScanResult: true
            })
        } else {
            this.setState({
                result: e,
                scanner: false,
                ScanResult: true
            })
        }
    }

    hitung_hari(from_date) {
		var date1 = new Date(from_date); 
		var date2 = new Date(new Date().getFullYear(),new Date().getMonth() , new Date().getDate()); 
		// console.log(date1);
		// console.log(date2);
		  
		// To calculate the time difference of two dates 
		var Difference_In_Time = date2.getTime() - date1.getTime(); 
		  
		// To calculate the no. of days between two dates 
		var Difference_In_Days = Difference_In_Time / (1000 * 3600 * 24);
		if (typeof Difference_In_Days === 'number') {
			if (Number(Difference_In_Days.toString().replace('-', '')).toFixed() == 0) {
				return 'Today';
			} else if (Number(Difference_In_Days.toString().replace('-', '')).toFixed() <= 1) {
				return 'Kemarin';
			} else {
				return (new Date(from_date).getDate()+'-'+new Date(from_date).getMonth()+'-'+new Date(from_date).getFullYear())
			}
		}
	}

    format(number) {
        if (number) {
            var rupiah = "";
            var numberrev = number
                .toString()
                .split("")
                .reverse()
                .join("");
            for (var i = 0; i < numberrev.length; i++)
                if (i % 3 == 0) rupiah += numberrev.substr(i, 3) + ".";
            return (
                "Rp " +
                rupiah
                .split("", rupiah.length - 1)
                .reverse()
                .join("")
            );
        } else {
            return (
                "Rp "+number
            );
        }
    }

	render() {
		const { products } = this.state;
        let totalQuantity = 0;
        let totalPrice = 0;
        products.forEach((item) => {
            totalQuantity += item.jml_barang;
            totalPrice += item.jml_barang * item.harga/* - 10000*/;
        })
		return (
			<>
				<NavigationEvents onDidFocus={() => this.setState({ scanner: true, historyScanStatus: true })} />

				<Dialog
                    visible={this.state.visible}
                    onTouchOutside={() => this.setState({ visible: false })}
                    dialogStyle={{ backgroundColor: light }}
                >
                    <DialogContent>
                        <View style={{ width: xs(200), alignItems: 'center' }}>
                        	<View style={[ b.mt4, { width: xs(50), height: xs(50), borderRadius: xs(50), borderWidth: 3, borderColor: blue, alignItems: 'center', justifyContent: 'center' }]}>
                        		<Image source={require('../asset/checkmark.png')} style={{ width: xs(30), height: xs(30), tintColor: blue }} />
                        	</View>
                            <View style={[ b.mt2, { width: xs(90), alignItems: 'center' }]}>
                            	<Text style={[ p.textCenter, { fontFamily: 'Roboto-Bold' }]}>Produk Berhasil di scan</Text>
                            </View>

                            <TouchableOpacity onPress={() => this.setState({ visible: false, scanner: true })} style={[ b.roundedLow, b.mt4, { width: xs(90), height: xs(30), backgroundColor: blue, alignItems: 'center', justifyContent: 'center' }]}>
                                <Text style={[ c.light, { fontFamily: 'Roboto-Bold' }]}>Lanjut Scan</Text>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => this.setState({ visible: false })} style={[ b.roundedLow, b.mt1, { width: xs(90), height: xs(30), borderWidth: 1, borderColor: blue, alignItems: 'center', justifyContent: 'center' }]}>
                                <Text style={[ c.blue, { fontFamily: 'Roboto-Bold' }]}>Keluar</Text>
                            </TouchableOpacity>
                        </View>
                    </DialogContent>
                </Dialog>

				<LinearGradient start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }} colors={[ '#181C21', '#252F38', '#252F38' ]} style={{ width: '100%', height: '100%' }}>
					<View style={{ width: '100%', height: hp('8%') }}>
						<TouchableOpacity onPress={() => this.props.navigation.pop()} style={{ position: 'absolute', top: vs(12), left: vs(10), zIndex: 1 }}>
                    		<FastImage source={require('../asset/left.png')} style={{ width: xs(25), height: xs(25) }} />
                    	</TouchableOpacity>
						<FlatList
		                    horizontal={true}
		                    data={this.state.scan}
		                    keyExtractor={item => item.title}
		                    renderItem={({ item, index }) => (
		                        (this.state.clicked === index ?
		                            <TouchableOpacity onPress={() => this.swicth(index)} style={{ width: wp('50%'), height: '100%', backgroundColor: blue, borderBottomWidth: 4, borderBottomColor: softBlue, alignItems: 'center', justifyContent: 'center' }}>
		                                <Text style={[ c.light, { fontFamily: 'Roboto-Bold' }]}>{item.title}</Text>
		                            </TouchableOpacity>
		                            :
		                            <TouchableOpacity onPress={() => this.swicth(index)} style={{ width: wp('50%'), height: '100%', backgroundColor: '#121212', alignItems: 'center', justifyContent: 'center' }}>
		                                <Text style={[ c.light, { fontFamily: 'Roboto-Bold' }]}>{item.title}</Text>
		                            </TouchableOpacity>
		                        )
		                    )}
		                />
					</View>

	                {this.state.clicked === 0 && (
	                	<>
		                	{this.state.scanner &&
		                        <QRCodeScanner
		                            reactivate={true}
		                            showMarker={true}
		                            ref={(node) => { this.scanner = node }}
		                            onRead={this.onSuccess}
		                        />
		                    }

		                    {this.state.ScanResult && (
		                    	<>
		                    		<ScrollView showsVerticalScrollIndicator={false}>
		                    			<FlatList
		                    				data={this.state.detailScan}
		                    				keyExtractor={item => item.id}
		                    				renderItem={({ item }) => (
		                    					<View style={{ width: '100%' }}>
		                    						<View style={{ width: '100%', alignItems: 'center' }}>
			                    						<View style={[ b.mt2, { width: wp('90%'), flexDirection: 'row', alignItems: 'center' }]}>
			                    							<View style={{ width: wp('50%') }}>
			                    								<Text style={[ c.light, { fontFamily: 'Roboto-Regular' }]}>No Pesanan {item.no_pesanan}</Text>
			                    							</View>
			                    							<View style={{ width: wp('40%'), alignItems: 'flex-end' }}>
			                    								<Text style={[ c.light, { fontFamily: 'Roboto-Regular' }]}>Tgl Pesan {new Date(Date.parse(item.tgl_pesan)).getDate()+'/'+(new Date(Date.parse(item.tgl_pesan)).getMonth()+1)+'/'+new Date(Date.parse(item.tgl_pesan)).getFullYear()}</Text>
			                    							</View>
			                    						</View>

			                    						<View style={{ width: '100%', flexDirection: 'row', marginTop: vs(3) }}>
			                    							<View style={{ width: wp('80%'), backgroundColor: '#121212', borderTopWidth: 1, borderBottomWidth: 1, borderTopColor: blue, borderBottomColor: blue, flexDirection: 'row', alignItems: 'center' }}>
			                    								<Text style={[ c.light, b.mt1, b.mb1, { fontFamily: 'Roboto-Regular', marginLeft: wp('5%') }]}>Dipesan oleh </Text>
			                    								<Text style={[ c.light, { fontFamily: 'Roboto-Bold' }]}>{item.namaPemesan}</Text>
			                    							</View>
			                    							<View style={{ width: wp('20%'), backgroundColor: '#121212', borderTopWidth: 1, borderBottomWidth: 1, borderTopColor: blue, borderBottomColor: blue, borderLeftWidth: 1, borderLeftColor: blue, alignItems: 'center' }}>
			                    								<View style={[ b.mt1, b.mb1, { flexDirection: 'row', alignItems: 'center' }]}>
			                    									<Image source={require('../asset/chatBubble.png')} style={{ width: xs(19), height: xs(19), transform: [{rotateY: '180deg'}] }} />
			                    									<Text style={[ c.blue, b.ml1, { fontFamily: 'Roboto-Regular' }]}>Chat</Text>
			                    								</View>
			                    							</View>
			                    						</View>
		                    						</View>

		                    						<FlatList
		                    							data={item.items}
		                    							renderItem={({ item }) => (
		                    								<View style={{ width: '100%', alignItems: 'center' }}>
		                    									<View style={[ b.mt2, { width: wp('90%'), flexDirection: 'row', alignItems: 'center' }]}>
		                    										<View style={{ width: wp('15%') }}>
		                    											<View style={{ width: '100%', height: wp('15%'), backgroundColor: light, borderRadius: xs(3), alignItems: 'center', justifyContent: 'center' }}>
		                    												<FastImage source={item.image} style={{ width: xs(45), height: xs(45) }} />
		                    											</View>
		                    										</View>
		                    										<View style={{ width: wp('45%'), height: wp('15%') }}>
		                    											<Text style={[ b.ml2, c.light, { fontFamily: 'Roboto-Bold' }]} numberOfLines={2}>{item.nama}</Text>
		                    											{item.stok === 0 ?
			                    											<Text style={[ b.ml2, { fontFamily: 'Roboto-Regular', color: 'darkorange', position: 'absolute', bottom: 0 }]}>Stok kosong</Text>
			                    											:
			                    											<Text style={[ b.ml2, c.light, { fontFamily: 'Roboto-Regular', position: 'absolute', bottom: 0 }]}>Stok: {item.stok}</Text>
		                    											}
		                    										</View>
		                    										<View style={{ width: wp('30%'), alignItems: 'flex-end' }}>
		                    											<Text style={{ color: 'darkorange', fontFamily: 'Roboto-Regular' }} numberOfLines={1}>{this.format(item.harga)}</Text>
		                    										</View>
		                    									</View>
		                    									<View style={[ b.mt2, b.mb2, { width: '100%', borderBottomWidth: 1, borderBottomColor: light }]} />
		                    									<View style={{ width: wp('90%'), flexDirection: 'row', alignItems: 'center' }}>
		                    										<View style={{ width: wp('40%') }}>
		                    											<Text style={[ c.light, { fontFamily: 'Roboto-Regular' }]}>{item.jmlh_produk} produk</Text>
		                    										</View>
		                    										<View style={{ width: wp('50%'), alignItems: 'flex-end' }}>
		                    											<Text style={[ c.light, { fontFamily: 'Roboto-Regular' }]}>Total Pesanan: {this.format(item.total_pesanan)}</Text>
		                    										</View>
		                    									</View>
		                    									<View style={[ b.mt2, b.mb2, { width: wp('90%'), borderBottomWidth: 1, borderBottomColor: light }]} />

		                    									<View style={{ width: '100%', height: hp('1%'), backgroundColor: '#121212' }} />
		                    								</View>
		                    							)}
		                    						/>
		                    					</View>
		                    				)}
		                    			/>
		                    		</ScrollView>

				                	<View style={{ width: '100%', alignItems: 'center' }}>
				                		<TouchableOpacity onPress={() => this.setState({ visible: true })} style={[ b.mt4, b.mb4, b.roundedLow, { width: wp('90%'), height: hp('7%'), backgroundColor: blue, alignItems: 'center', justifyContent: 'center' }]}>
					                		<Text style={[ c.light, f._16, { fontFamily: 'Roboto-Bold' }]}>Update</Text>
				                		</TouchableOpacity>
				                	</View>
		                    	</>
		                    )}
	                	</>
	                )}

	                {this.state.clicked === 1 && (
	                	<>
		                	{this.state.historyScanStatus && (
			                	<ScrollView showsVerticalScrollIndicator={false}>
			                		<View style={[ b.mt2, { width: '100%', alignItems: 'center' }]}>
			                			<FlatList
			                				showsVerticalScrollIndicator={false}
			                				data={this.state.historyScan}
			                				keyExtractor={item => item.id}
			                				renderItem={({ item }) => (
			                					<TouchableOpacity onPress={() => this.setState({ detailHistoryScan: true, historyScanStatus: false })} style={[ b.mb2, { width: wp('90%'), backgroundColor: blue, borderRadius: xs(3), flexDirection: 'row', alignItems: 'center' }]}>
			                						<View style={{ width: wp('60%'), height: hp('10%'), justifyContent: 'center' }}>
			                							<Text style={[ c.light, f._16, b.ml2, { fontFamily: 'Roboto-Regular' }]}>Scan {item.no_pesanan}</Text>
			                							<Text style={[ c.light, b.mt1, b.ml2, { fontFamily: 'Roboto-Regular' }]}>{item.jmlh_produk} produk</Text>
			                						</View>
			                						<View style={{ width: wp('30%'), height: hp('10%'), alignItems: 'flex-end' }}>
			                							<Text style={[ c.light, { fontFamily: 'Roboto-Regular', position: 'absolute', bottom: vs(12), right: vs(10) }]}>{this.hitung_hari(item.waktu_scan)}</Text>
			                						</View>
			                					</TouchableOpacity>
			                				)}
			                			/>
			                		</View>
			                	</ScrollView>
		                	)}

		                	{this.state.detailHistoryScan && (
		                		<ScrollView showsVerticalScrollIndicator={false}>
		                			<View style={{ width: '100%' }}>
		                				<View style={{ width: '100%', alignItems: 'center' }}>
		                					<View style={{ width: wp('90%'), height: hp('10%'), flexDirection: 'row', alignItems: 'center' }}>
		                						<View style={{ width: wp('15%') }}>
		                							<TouchableOpacity onPress={() => this.setState({ detailHistoryScan: false, historyScanStatus: true })}>
		                								<FastImage source={require('../asset/left.png')} style={{ width: xs(25), height: xs(25) }} />
		                							</TouchableOpacity>
		                						</View>
		                						<View style={{ width: wp('60%'), alignItems: 'center' }}>
		                							<Text style={[ c.light, f._16, { fontFamily: 'Roboto-Bold' }]}>Detail History</Text>
		                						</View>
		                						<View style={{ width: wp('15%') }}>
		                						</View>
		                					</View>
		                				</View>
		                				<View style={{ width: '100%', alignItems: 'center' }}>
		                					<View style={[ b.mt3, { width: wp('90%'), flexDirection: 'row', alignItems: 'center' }]}>
		                						<View style={{ width: wp('40%') }}>
		                							<Text style={[ c.light, { fontFamily: 'Roboto-Regular' }]}>No Pesanan</Text>
		                						</View>
		                						<View style={{ width: wp('50%'), alignItems: 'flex-end' }}>
		                							<Text style={[ c.light, { fontFamily: 'Roboto-Regular' }]}>12345678</Text>
		                						</View>
		                					</View>
		                					<View style={[ b.mt2, b.mb2, { width: wp('90%'), borderBottomWidth: 1, borderBottomColor: light }]} />
		                					<View style={[ b.mb3, { width: wp('90%'), flexDirection: 'row', alignItems: 'center' }]}>
		                						<View style={{ width: wp('40%') }}>
		                							<Text style={[ c.light, { fontFamily: 'Roboto-Regular' }]}>Waktu</Text>
		                						</View>
		                						<View style={{ width: wp('50%'), alignItems: 'flex-end' }}>
		                							<Text style={[ c.light, { fontFamily: 'Roboto-Regular' }]}>04/09/2021, 14:00 WIB</Text>
		                						</View>
		                					</View>
			                			</View>

			                			<View style={{ width: '100%', height: hp('1.5%'), backgroundColor: '#121212' }} />

			                			<View style={{ width: '100%' }}>
											<FlatList
												style={[ b.mt2 ]}
												data={this.state.products}
												keyExtractor={item => item.id}
												renderItem={({ item }) => (
													<View style={{ width: '100%', alignItems: 'center' }}>
														<View style={{ width: wp('90%'), flexDirection: 'row' }}>
															<View style={{ width: wp('15%') }}>
																<View style={{ width: '100%', height: wp('15%'), backgroundColor: light, borderRadius: xs(3), alignItems: 'center', justifyContent: 'center' }}>
																	<FastImage source={ item.image } style={{ width: xs(50), height: xs(50) }} />
																</View>
															</View>
															<View style={{ width: wp('50%') }}>
																<Text style={[ b.ml2, c.light, { fontFamily: 'Roboto-Bold' }]} numberOfLines={2}>{item.nama}</Text>
															</View>
															<View style={{ width: wp('25%'), alignItems: 'flex-end' }}>
																<Text style={[ c.light, { fontFamily: 'Roboto-Regular', position: 'absolute', bottom: vs(20) }]}>x{item.jml_barang}</Text>
																<Text style={[ c.light, { fontFamily: 'Roboto-Regular', position: 'absolute', bottom: 0 }]} numberOfLines={1}>{this.format(totalPrice)}</Text>
															</View>
														</View>
														<View style={[ b.mt2, b.mb2, { width: '100%', borderBottomWidth: 1, borderBottomColor: light }]} />
														<View style={{ width: wp('90%'), flexDirection: 'row', alignItems: 'center' }}>
															<View style={{ width: wp('30%') }}>
																<Text style={[ c.light ]}>{item.jml_barang} produk</Text>
															</View>
															<View style={{ width: wp('60%'), alignItems: 'flex-end' }}>
																<Text style={[ c.light ]}>Total Pesanan: {this.format(totalPrice)}</Text>
															</View>
														</View>
														<View style={[ b.mt2, b.mb2, { width: wp('90%'), borderBottomWidth: 1, borderBottomColor: light }]} />
													</View>
												)}
											/>
										</View>

										<View style={{ width: '100%', alignItems: 'center' }}>
											<FastImage source={require('../asset/giftcard1.png')} style={ { width: xs(280), height: xs(195), alignItems: 'center', marginTop: vs(50) }}>
												<Text style={[ c.blue, b.mt4, { fontFamily: 'Roboto-Regular' }]}>Hi Michael</Text>
												<Text style={[ c.blue, { fontFamily: 'AlexBrush-Regular', fontSize: xs(25) }]}>Selamat Ulang Tahun</Text>
												<View style={{ width: xs(200) }}>
													<Text style={[ c.blue, p.textCenter, { fontFamily: 'Roboto-Regular' }]}>Semoga panjang umur dan sehat selalu</Text>
												</View>

												<View style={{ width: xs(80), height: xs(80), backgroundColor: light, position: 'absolute', bottom: vs(5) }}>
				                                    <Image source={require('../asset/qrCode.png')} style={{ width: '100%', height: '100%', tintColor: '#000' }} />
				                                </View>
											</FastImage>
										</View>
		                			</View>
		                		</ScrollView>
		                	)}
	                	</>
	                )}
				</LinearGradient>
			</>
		)
	}
}