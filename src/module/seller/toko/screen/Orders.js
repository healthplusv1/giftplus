import React, { Component } from 'react'
import { View, Text, Image, TouchableOpacity, FlatList, ScrollView } from 'react-native'
import { blue, light } from '../utils/Color'
import { xs, vs } from '../utils/Responsive'
import { f, c, b, p } from '../utils/StyleHelper'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
import FastImage from 'react-native-fast-image'
import LinearGradient from 'react-native-linear-gradient'
import AsyncStorage from '@react-native-async-storage/async-storage'
import axios from 'axios'
import { API_LOCAL, API_URL } from '@env'

export default class Orders extends Component {
	constructor(props) {
		super(props);
		this.state = {
			orders: [
				{status: 'Baru'},
				{status: 'Order diproses'},
				{status: 'Selesai'}
			],
			clicked: 0,
			baru: [
				{id: 1, no_pesanan: '123456789', harga: '120000', namaBarang: 'Fox Premium glove motor Black Sarung Tangan', items: [{id: 1, image: require('../asset/sarung_tangan.png')}, {id: 2, image: require('../asset/sarung_tangan.png')}, {id: 3, image: require('../asset/sarung_tangan.png')}]},
				{id: 2, no_pesanan: '123456789', harga: '120000', namaBarang: 'Fox Premium glove motor Black Sarung Tangan', items: [{id: 1, image: require('../asset/sarung_tangan.png')}, {id: 2, image: require('../asset/sarung_tangan.png')}, {id: 3, image: require('../asset/sarung_tangan.png')}]}
			],
			proses: [
				{id: 1, no_pesanan: '123456789', harga: '120000', namaBarang: 'Fox Premium glove motor Black Sarung Tangan', items: [{id: 1, image: require('../asset/sarung_tangan.png'), status: 'Belum Redeem', jumlah: '2/3'}, {id: 2, image: require('../asset/sarung_tangan.png'), status: 'Belum Redeem', jumlah: '2/3'}, {id: 3, image: require('../asset/sarung_tangan.png')}]},
				{id: 2, no_pesanan: '123456789', harga: '120000', namaBarang: 'Fox Premium glove motor Black Sarung Tangan', items: [{id: 1, image: require('../asset/sarung_tangan.png'), status: 'Belum Redeem', jumlah: '2/3'}, {id: 2, image: require('../asset/sarung_tangan.png'), status: 'Belum Redeem', jumlah: '2/3'}, {id: 3, image: require('../asset/sarung_tangan.png')}]}
			],
			selesai: [
				{id: 1, no_pesanan: '123456789', harga: '120000', namaBarang: 'Fox Premium glove motor Black Sarung Tangan', items: [{id: 1, image: require('../asset/sarung_tangan.png')}, {id: 2, image: require('../asset/sarung_tangan.png')}, {id: 3, image: require('../asset/sarung_tangan.png')}]},
				{id: 2, no_pesanan: '123456789', harga: '120000', namaBarang: 'Fox Premium glove motor Black Sarung Tangan', items: [{id: 1, image: require('../asset/sarung_tangan.png')}, {id: 2, image: require('../asset/sarung_tangan.png')}, {id: 3, image: require('../asset/sarung_tangan.png')}]}
			]
		}
	}

	async swicth(index) {
        await this.setState({
            clicked: index
        })
    }

    format(number) {
        if (number) {
            var rupiah = "";
            var numberrev = number
                .toString()
                .split("")
                .reverse()
                .join("");
            for (var i = 0; i < numberrev.length; i++)
                if (i % 3 == 0) rupiah += numberrev.substr(i, 3) + ".";
            return (
                "Rp " +
                rupiah
                .split("", rupiah.length - 1)
                .reverse()
                .join("")
            );
        } else {
            return (
                "Rp "+number
            );
        }
    }

	render() {
		return (
			<LinearGradient start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }} colors={[ '#181C21', '#252F38', '#252F38' ]} style={{ width: '100%', height: '100%' }}>
				<View style={{ width: '100%', backgroundColor: '#121212', alignItems: 'center' }}>
					<View style={{ width: wp('90%'), height: hp('10%'), flexDirection: 'row', alignItems: 'center' }}>
						<View style={{ width: wp('15%') }}>
							<TouchableOpacity onPress={() => this.props.navigation.pop()}>
								<FastImage source={require('../asset/left.png')} style={{ width: xs(25), height: xs(25) }} />
							</TouchableOpacity>
						</View>
						<View style={{ width: wp('60%'), alignItems: 'center' }}>
							<Text style={[ c.light, f._16, { fontFamily: 'Roboto-Bold' }]}>Orders</Text>
						</View>
						<View style={{ width: wp('15%') }}>
						</View>
					</View>
				</View>

				<View style={{ width: '100%', alignItems: 'center' }}>
					<FlatList
						style={[ b.mt3, b.mb3 ]}
						horizontal={true}
						showsHorizontalScrollIndicator={false}
						data={this.state.orders}
						keyExtractor={item => item.status}
						renderItem={({ item, index }) => (
							(this.state.clicked === index ?
								<TouchableOpacity onPress={() => this.swicth(index)} style={[ b.roundedLow, { backgroundColor: blue, marginRight: vs(15) }]}>
									<Text style={[ b.ml4, b.mr4, b.mt1, b.mb1, c.light ]}>{item.status}</Text>
								</TouchableOpacity>
								:
								<TouchableOpacity onPress={() => this.swicth(index)} style={[ b.roundedLow, { backgroundColor: '#121212', borderWidth: 1, borderColor: blue, marginRight: vs(15) }]}>
									<Text style={[ b.ml4, b.mr4, b.mt1, b.mb1, c.light ]}>{item.status}</Text>
								</TouchableOpacity>
							)
						)}
					/>
				</View>

				{this.state.clicked === 0 && (
					<ScrollView showsVerticalScrollIndicator={false}>
						<View style={{ width: '100%', alignItems: 'center' }}>
							<FlatList
								showsVerticalScrollIndicator={false}
								data={this.state.baru}
								keyExtractor={item => item.id}
								renderItem={({ item }) => (
									<View style={[ b.rounded, b.mb3, { width: wp('90%'), backgroundColor: '#121212', borderWidth: 1, borderColor: blue, alignItems: 'center' }]}>
										<View style={[ b.mt2, b.mb1, { width: '100%', flexDirection: 'row', alignItems: 'center' }]}>
											<View style={{ width: wp('45%') }}>
												<Text style={[ c.light, b.ml1, { fontFamily: 'Roboto-Regular' }]}>No. Pesanan {item.no_pesanan}</Text>
											</View>
											<View style={{ width: wp('45%'), alignItems: 'flex-end' }}>
												<Text style={[ b.mr2, { color: 'darkorange', fontFamily: 'Roboto-Regular' }]}>{this.format(item.harga)}</Text>
											</View>
										</View>
										<View style={{ width: '100%', borderBottomWidth: 1, borderBottomColor: blue }} />
										<View style={{ width: wp('86%') }}>
											<Text style={[ c.light, b.mt1, b.mb1, f._16, { fontFamily: 'Roboto-Bold' }]} numberOfLines={1}>{item.namaBarang}</Text>
										</View>

										<FlatList
											style={[ b.mb2, b.mt1 ]}
											horizontal={true}
											data={item.items}
											renderItem={({ item }) => (
												<View style={[ b.mr2, { width: wp('22%'), height: wp('22%'), backgroundColor: light, borderRadius: xs(3), alignItems: 'center', justifyContent: 'center' }]}>
													<FastImage source={item.image} style={{ width: xs(60), height: xs(60) }} />
												</View>
											)}
										/>

										<TouchableOpacity onPress={() => this.props.navigation.push('OrderDetailBaru')} style={{ width: '100%', height: hp('5%'), backgroundColor: blue, flexDirection: 'row', alignItems: 'center', justifyContent: 'center', borderBottomLeftRadius: xs(10), borderBottomRightRadius: xs(10) }}>
											<FastImage source={require('../asset/order_icon.png')} style={{ width: xs(20), height: xs(20) }} />
											<Text style={[ c.light, b.ml2, { fontFamily: 'Roboto-Regular' }]}>Lihat Pesanan</Text>
										</TouchableOpacity>
									</View>
								)}
							/>
						</View>
					</ScrollView>
				)}

				{this.state.clicked === 1 && (
					<ScrollView showsVerticalScrollIndicator={false}>
						<View style={{ width: '100%', alignItems: 'center' }}>
							<FlatList
								showsVerticalScrollIndicator={false}
								data={this.state.proses}
								keyExtractor={item => item.id}
								renderItem={({ item }) => (
									<View style={[ b.rounded, b.mb3, { width: wp('90%'), backgroundColor: '#121212', borderWidth: 1, borderColor: blue, alignItems: 'center' }]}>
										<View style={[ b.mt2, b.mb1, { width: '100%', flexDirection: 'row', alignItems: 'center' }]}>
											<View style={{ width: wp('45%') }}>
												<Text style={[ c.light, b.ml1, { fontFamily: 'Roboto-Regular' }]}>No. Pesanan {item.no_pesanan}</Text>
											</View>
											<View style={{ width: wp('45%'), alignItems: 'flex-end' }}>
												<Text style={[ b.mr2, { color: 'darkorange', fontFamily: 'Roboto-Regular' }]}>{this.format(item.harga)}</Text>
											</View>
										</View>
										<View style={{ width: '100%', borderBottomWidth: 1, borderBottomColor: blue }} />
										<View style={{ width: wp('86%') }}>
											<Text style={[ c.light, b.mt1, b.mb1, f._16, { fontFamily: 'Roboto-Bold' }]} numberOfLines={1}>{item.namaBarang}</Text>
										</View>

										<FlatList
											style={[ b.mb2, b.mt1 ]}
											horizontal={true}
											data={item.items}
											renderItem={({ item }) => (
												<View style={[ b.mr2, { width: wp('22%'), height: wp('22%'), backgroundColor: light, borderRadius: xs(3), alignItems: 'center', justifyContent: 'center' }]}>
													<FastImage source={item.image} style={{ width: xs(60), height: xs(60) }} />
													{item.status ?
														<>
															<View style={{ width: '100%', backgroundColor: 'rgba(248,148,6,0.8)', alignItems: 'center', position: 'absolute', zIndex: 1 }}>
																<Text style={[ c.light, f._12, { fontFamily: 'Roboto-Bold' }]}>{item.status}</Text>
															</View>
															<View style={{ backgroundColor: 'rgba(248,148,6,0.8)', alignItems: 'center', borderRadius: xs(3), position: 'absolute', bottom: 0, right: 0, zIndex: 1 }}>
																<Text style={[ c.light, f._12, { fontFamily: 'Roboto-Bold', marginLeft: vs(2), marginRight: vs(2), marginTop: xs(2), marginBottom: xs(2) }]}>{item.jumlah}</Text>
															</View>
														</>
														:
														<View style={{ width: '100%', height: '100%', backgroundColor: 'rgba(0,0,0,0.3)', position: 'absolute', zIndex: 1 }} />
													}
												</View>
											)}
										/>

										<TouchableOpacity onPress={() => this.props.navigation.push('OrderDetailProses')} style={{ width: '100%', height: hp('5%'), backgroundColor: blue, flexDirection: 'row', alignItems: 'center', justifyContent: 'center', borderBottomLeftRadius: xs(10), borderBottomRightRadius: xs(10) }}>
											<FastImage source={require('../asset/order_icon.png')} style={{ width: xs(20), height: xs(20) }} />
											<Text style={[ c.light, b.ml2, { fontFamily: 'Roboto-Regular' }]}>Lihat Pesanan</Text>
										</TouchableOpacity>
									</View>
								)}
							/>
						</View>
					</ScrollView>
				)}

				{this.state.clicked === 2 && (
					<ScrollView showsVerticalScrollIndicator={false}>
						<View style={{ width: '100%', alignItems: 'center' }}>
							<FlatList
								showsVerticalScrollIndicator={false}
								data={this.state.selesai}
								keyExtractor={item => item.id}
								renderItem={({ item }) => (
									<View style={[ b.rounded, b.mb3, { width: wp('90%'), backgroundColor: '#121212', borderWidth: 1, borderColor: blue, alignItems: 'center' }]}>
										<View style={[ b.mt2, b.mb1, { width: '100%', flexDirection: 'row', alignItems: 'center' }]}>
											<View style={{ width: wp('45%') }}>
												<Text style={[ c.light, b.ml1, { fontFamily: 'Roboto-Regular' }]}>No. Pesanan {item.no_pesanan}</Text>
											</View>
											<View style={{ width: wp('45%'), alignItems: 'flex-end' }}>
												<Text style={[ b.mr2, { color: 'darkorange', fontFamily: 'Roboto-Regular' }]}>{this.format(item.harga)}</Text>
											</View>
										</View>
										<View style={{ width: '100%', borderBottomWidth: 1, borderBottomColor: blue }} />
										<View style={{ width: wp('86%') }}>
											<Text style={[ c.light, b.mt1, b.mb1, f._16, { fontFamily: 'Roboto-Bold' }]} numberOfLines={1}>{item.namaBarang}</Text>
										</View>

										<FlatList
											style={[ b.mb2, b.mt1 ]}
											horizontal={true}
											data={item.items}
											renderItem={({ item }) => (
												<View style={[ b.mr2, { width: wp('22%'), height: wp('22%'), backgroundColor: light, borderRadius: xs(3), alignItems: 'center', justifyContent: 'center' }]}>
													<FastImage source={item.image} style={{ width: xs(60), height: xs(60) }} />
												</View>
											)}
										/>

										<TouchableOpacity onPress={() => this.props.navigation.push('OrderDetailSelesai')} style={{ width: '100%', height: hp('5%'), backgroundColor: blue, flexDirection: 'row', alignItems: 'center', justifyContent: 'center', borderBottomLeftRadius: xs(10), borderBottomRightRadius: xs(10) }}>
											<FastImage source={require('../asset/order_icon.png')} style={{ width: xs(20), height: xs(20) }} />
											<Text style={[ c.light, b.ml2, { fontFamily: 'Roboto-Regular' }]}>Lihat Pesanan</Text>
										</TouchableOpacity>
									</View>
								)}
							/>
						</View>
					</ScrollView>
				)}
			</LinearGradient>
		)
	}
}