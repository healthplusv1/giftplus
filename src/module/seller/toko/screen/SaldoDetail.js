import React, { Component } from 'react'
import { View, Text, Image, TouchableOpacity, ScrollView, FlatList } from 'react-native'
import { blue, light, softGrey } from '../utils/Color'
import { xs, vs } from '../utils/Responsive'
import { f, c, b, p } from '../utils/StyleHelper'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
import FastImage from 'react-native-fast-image'
import LinearGradient from 'react-native-linear-gradient'
import DateTimePicker from '@react-native-community/datetimepicker'
import AsyncStorage from '@react-native-async-storage/async-storage'
import axios from 'axios'
import { API_LOCAL, API_URL } from '@env'

export default class SaldoDetail extends Component {
	constructor(props) {
		super(props);
		this.state = {
			loading: false,
			startDate: new Date().setDate(new Date().getDate()),
			endDate: new Date().setDate(new Date().getDate()+1),
			startDateActive: false,
			endDateActive: false,
			detailSaldo: [
				{status: 'Semua'},
				{status: 'Pendapatan'},
				{status: 'Pengeluaran'}
			],
			clicked: 0,
			semua: [
				{id: 1, no_pesanan: '123456789', tanggal: '2021-09-15', waktu: '14:00', saldo: '100000', status: 'Masuk'},
				{id: 2, no_pesanan: '123456789', tanggal: '2021-09-15', waktu: '14:00', saldo: '100000', status: 'Masuk'},
				{id: 3, no_pesanan: '123456789', tanggal: '2021-09-15', waktu: '14:00', saldo: '100000', status: 'Keluar'},
				{id: 4, no_pesanan: '123456789', tanggal: '2021-09-15', waktu: '14:00', saldo: '200000', status: 'Masuk'}
			],
			pendapatan: [
				{id: 1, no_pesanan: '123456789', tanggal: '2021-09-15', waktu: '14:00', saldo: '100000', status: 'Masuk'},
				{id: 2, no_pesanan: '123456789', tanggal: '2021-09-15', waktu: '14:00', saldo: '100000', status: 'Masuk'},
				{id: 4, no_pesanan: '123456789', tanggal: '2021-09-15', waktu: '14:00', saldo: '200000', status: 'Masuk'}
			],
			pengeluaran: [
				{id: 3, no_pesanan: '123456789', tanggal: '2021-09-15', waktu: '14:00', saldo: '100000', status: 'Keluar'}
			]
		}
	}

	componentDidMount() {
		console.log(new Date())
	}

	async switch(index) {
        await this.setState({
            clicked: index
        })
    }

	format(number) {
        if (number) {
            var rupiah = "";
            var numberrev = number
                .toString()
                .split("")
                .reverse()
                .join("");
            for (var i = 0; i < numberrev.length; i++)
                if (i % 3 == 0) rupiah += numberrev.substr(i, 3) + ".";
            return (
                "Rp " +
                rupiah
                .split("", rupiah.length - 1)
                .reverse()
                .join("")
            );
        } else {
            return (
                "Rp "+number
            );
        }
    }

	render() {
		return (
			<View style={{ width: '100%', height: '100%', backgroundColor: '#121212' }}>
				<View style={{ width: '100%', alignItems: 'center' }}>
					<View style={{ width: wp('90%'), height: hp('10%'), flexDirection: 'row', alignItems: 'center' }}>
						<View style={{ width: wp('15%') }}>
							<TouchableOpacity onPress={() => this.props.navigation.pop()}>
								<FastImage source={require('../asset/left.png')} style={{ width: xs(25), height: xs(25) }} />
							</TouchableOpacity>
						</View>
						<View style={{ width: wp('60%'), alignItems: 'center' }}>
							<Text style={[ c.light, f._16, { fontFamily: 'Roboto-Bold' }]}>Detail Saldo</Text>
						</View>
						<View style={{ width: wp('15%') }}>
						</View>
					</View>
				</View>

				<LinearGradient start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }} colors={[ '#181C21', '#252F38', '#252F38' ]} style={{ width: '100%', alignItems: 'center' }}>
					<View style={[ b.mt4, { width: wp('85%'), alignItems: 'center', backgroundColor: '#343E47', borderRadius: xs(3) }]}>
						<FastImage source={require('../asset/belumbayar_icon.png')} style={[ b.mt2, { width: xs(30), height: xs(30) }]} />
						<Text style={[ b.mt2, b.mb2, f._24, { color: 'darkorange', fontFamily: 'Roboto-Bold' }]}>{this.format(400000)}</Text>
					</View>

					<TouchableOpacity style={[ b.mt3, b.mb4, b.roundedLow, { width: wp('85%'), height: hp('7%'), backgroundColor: blue, alignItems: 'center', justifyContent: 'center' }]}>
						<Text style={[ c.light, f._16, { fontFamily: 'Roboto-Bold' }]}>Tarik Saldo</Text>
					</TouchableOpacity>
				</LinearGradient>

				<View style={{ width: '100%', alignItems: 'center' }}>
					<View style={[ b.mt3, { width: wp('85%') }]}>
						<Text style={[ c.light, { fontFamily: 'Roboto-Regular' }]}>Riwayat Transaksi</Text>
					</View>

					<View style={[ b.mt3, { width: wp('85%'), flexDirection: 'row', alignItems: 'center' }]}>
						<TouchableOpacity onPress={() => this.setState({ startDateActive: true })} style={{ width: wp('39%'), height: hp('5%'), backgroundColor: '#181B22', borderWidth: 1, borderColor: blue, borderRadius: xs(5), flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                            <Text style={[ c.light, { fontFamily: 'Roboto-Regular' }]}>{typeof this.state.startDate === 'object' ? new Date(Date.parse(this.state.startDate)).getDate()+'/'+(new Date(Date.parse(this.state.startDate)).getMonth()+1)+'/'+new Date(Date.parse(this.state.startDate)).getFullYear() : new Date(this.state.startDate).getDate()+'/'+(new Date(this.state.startDate).getMonth()+1)+'/'+new Date(this.state.startDate).getFullYear()}</Text>
                            <Image source={require('../asset/tanggal.png')} style={[ b.ml4, { width: xs(18), height: xs(18), tintColor: blue }]} />
                        </TouchableOpacity>
                        {this.state.startDateActive === true && (
                            <DateTimePicker
                                mode="date"
                                value={new Date()}
                                minimumDate={new Date()}
                                onChange={data => {
                                    if (data.type === 'dismissed') {
                                        this.setState({
                                            startDateActive: false
                                        })
                                    } else {
                                        // console.log(data.nativeEvent.timestamp)
                                        this.setState({
                                            startDate: data.nativeEvent.timestamp,
                                            startDateActive: false
                                        })
                                    }
                                }}
                            />
                        )}

                        <Text style={[ c.light, b.ml2, b.mr2, { fontFamily: 'Roboto-Regular' }]}>-</Text>

                        <TouchableOpacity onPress={() => this.setState({ endDateActive: true })} style={{ width: wp('39%'), height: hp('5%'), backgroundColor: '#181B22', borderWidth: 1, borderColor: blue, borderRadius: xs(5), flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                            <Text style={[ c.light, { fontFamily: 'Roboto-Regular' }]}>{typeof this.state.endDate === 'object' ? new Date(Date.parse(this.state.endDate)).getDate()+'/'+(new Date(Date.parse(this.state.endDate)).getMonth()+1)+'/'+new Date(Date.parse(this.state.endDate)).getFullYear() : new Date(this.state.endDate).getDate()+'/'+(new Date(this.state.endDate).getMonth()+1)+'/'+new Date(this.state.endDate).getFullYear()}</Text>
                            <Image source={require('../asset/tanggal.png')} style={[ b.ml4, { width: xs(18), height: xs(18), tintColor: blue }]} />
                        </TouchableOpacity>
                        {this.state.endDateActive === true && (
                            <DateTimePicker
                                mode="date"
                                value={new Date()}
                                minimumDate={
									(typeof this.state.startDate === undefined ?
										this.state.startDate.setDate(this.state.startDate.getDate()+1)
										:
										new Date(this.state.startDate).setDate(new Date(this.state.startDate).getDate()+1)
									)
								}
                                onChange={data => {
                                    if (data.type === 'dismissed') {
                                        this.setState({
                                            endDateActive: false
                                        })
                                    } else {
                                        // console.log(data.nativeEvent.timestamp)
                                        this.setState({
                                            endDate: data.nativeEvent.timestamp,
                                            endDateActive: false
                                        })
                                    }
                                }}
                            />
                        )}
					</View>

					<FlatList
						style={[ b.mt3, b.ml3 ]}
						horizontal={true}
						data={this.state.detailSaldo}
						keyExtractor={item => item.status}
						renderItem={({ item, index }) => (
							(this.state.clicked === index ?
								<TouchableOpacity onPress={() => this.switch(index)} style={[ b.roundedLow, b.mr3, { backgroundColor: blue }]}>
									<Text style={[ c.light, b.ml3, b.mr3, b.mt1, b.mb1, { fontFamily: 'Roboto-Regular' }]}>{item.status}</Text>
								</TouchableOpacity>
								:
								<TouchableOpacity onPress={() => this.switch(index)} style={[ b.roundedLow, b.mr3, { backgroundColor: '#181B22', borderWidth: 1, borderColor: blue }]}>
									<Text style={[ c.light, b.ml3, b.mr3, b.mt1, b.mb1, { fontFamily: 'Roboto-Regular' }]}>{item.status}</Text>
								</TouchableOpacity>
							)
						)}
					/>

					<View style={[ b.mt3, b.mb2, { width: '100%', borderBottomWidth: 1, borderBottomColor: light }]} />
				</View>
				
				{this.state.clicked === 0 && (
					(this.state.semua === undefined ?
						<View style={{ width: '100%', alignItems: 'center' }}>
							<Text style={{ color: softGrey, fontFamily: 'Roboto-Regular', marginTop: vs(100) }}>Tidak ada riwayat transaksi</Text>
						</View>
						:
						<FlatList
							showsVerticalScrollIndicator={false}
							data={this.state.semua}
							keyExtractor={item => item.id}
							renderItem={({ item }) => (
								<View style={{ width: '100%', alignItems: 'center' }}>
									<View style={{ width: wp('90%'), flexDirection: 'row', alignItems: 'center' }}>
										<View style={{ width: wp('10%') }}>
											<View style={{ width: '100%', height: wp('10%'), backgroundColor: '#343E47', alignItems: 'center', justifyContent: 'center', borderRadius: xs(3) }}>
												{item.status === 'Masuk' ?
													<Image source={require('../asset/up.png')} style={{ width: xs(20), height: xs(20), tintColor: 'green' }} />
													:
													<Image source={require('../asset/up.png')} style={{ width: xs(20), height: xs(20), tintColor: 'red', transform: [{ rotate: '180deg' }] }} />
												}
											</View>
										</View>
										<View style={{ width: wp('50%') }}>
											<View style={{ flexDirection: 'row', alignItems: 'center' }}>
												<Text style={[ f._12, b.ml2, { color: softGrey, fontFamily: 'Roboto-Regular' }]}>{new Date(Date.parse(item.tanggal)).getDate()+'/'+(new Date(Date.parse(item.tanggal)).getMonth()+1)+'/'+new Date(Date.parse(item.tanggal)).getFullYear()}</Text>
												<Text style={[ f._12, b.ml2, { color: softGrey, fontFamily: 'Roboto-Regular' }]}>{item.waktu} WIB</Text>
											</View>
											<Text style={[ c.light, b.ml2, { fontFamily: 'Roboto-Regular' }]} numberOfLines={1}>No Pesanan {item.no_pesanan}</Text>
										</View>
										<View style={{ width: wp('30%'), alignItems: 'flex-end' }}>
											<Text style={{ color: 'darkorange', fontFamily: 'Roboto-Regular' }}>{this.format(item.saldo)}</Text>
										</View>
									</View>

									<View style={[ b.mt2, b.mb2, { width: wp('90%'), borderBottomWidth: 1, borderBottomColor: light }]} />
								</View>
							)}
						/>
					)
				)}

				{this.state.clicked === 1 && (
					(this.state.pendapatan === undefined ?
						<View style={{ width: '100%', alignItems: 'center' }}>
							<Text style={{ color: softGrey, fontFamily: 'Roboto-Regular', marginTop: vs(100) }}>Tidak ada riwayat transaksi</Text>
						</View>
						:
						<FlatList
							showsVerticalScrollIndicator={false}
							data={this.state.pendapatan}
							keyExtractor={item => item.id}
							renderItem={({ item }) => (
								<View style={{ width: '100%', alignItems: 'center' }}>
									<View style={{ width: wp('90%'), flexDirection: 'row', alignItems: 'center' }}>
										<View style={{ width: wp('10%') }}>
											<View style={{ width: '100%', height: wp('10%'), backgroundColor: '#343E47', alignItems: 'center', justifyContent: 'center', borderRadius: xs(3) }}>
												<Image source={require('../asset/up.png')} style={{ width: xs(20), height: xs(20), tintColor: 'green' }} />
											</View>
										</View>
										<View style={{ width: wp('50%') }}>
											<View style={{ flexDirection: 'row', alignItems: 'center' }}>
												<Text style={[ f._12, b.ml2, { color: softGrey, fontFamily: 'Roboto-Regular' }]}>{new Date(Date.parse(item.tanggal)).getDate()+'/'+(new Date(Date.parse(item.tanggal)).getMonth()+1)+'/'+new Date(Date.parse(item.tanggal)).getFullYear()}</Text>
												<Text style={[ f._12, b.ml2, { color: softGrey, fontFamily: 'Roboto-Regular' }]}>{item.waktu} WIB</Text>
											</View>
											<Text style={[ c.light, b.ml2, { fontFamily: 'Roboto-Regular' }]} numberOfLines={1}>No Pesanan {item.no_pesanan}</Text>
										</View>
										<View style={{ width: wp('30%'), alignItems: 'flex-end' }}>
											<Text style={{ color: 'darkorange', fontFamily: 'Roboto-Regular' }}>{this.format(item.saldo)}</Text>
										</View>
									</View>

									<View style={[ b.mt2, b.mb2, { width: wp('90%'), borderBottomWidth: 1, borderBottomColor: light }]} />
								</View>
							)}
						/>
					)
				)}

				{this.state.clicked === 2 && (
					(this.state.pengeluaran === undefined ?
						<View style={{ width: '100%', alignItems: 'center' }}>
							<Text style={{ color: softGrey, fontFamily: 'Roboto-Regular', marginTop: vs(100) }}>Tidak ada riwayat transaksi</Text>
						</View>
						:
						<FlatList
							showsVerticalScrollIndicator={false}
							data={this.state.pengeluaran}
							keyExtractor={item => item.id}
							renderItem={({ item }) => (
								<View style={{ width: '100%', alignItems: 'center' }}>
									<View style={{ width: wp('90%'), flexDirection: 'row', alignItems: 'center' }}>
										<View style={{ width: wp('10%') }}>
											<View style={{ width: '100%', height: wp('10%'), backgroundColor: '#343E47', alignItems: 'center', justifyContent: 'center', borderRadius: xs(3) }}>
												<Image source={require('../asset/up.png')} style={{ width: xs(20), height: xs(20), tintColor: 'red', transform: [{ rotate: '180deg' }] }} />
											</View>
										</View>
										<View style={{ width: wp('50%') }}>
											<View style={{ flexDirection: 'row', alignItems: 'center' }}>
												<Text style={[ f._12, b.ml2, { color: softGrey, fontFamily: 'Roboto-Regular' }]}>{new Date(Date.parse(item.tanggal)).getDate()+'/'+(new Date(Date.parse(item.tanggal)).getMonth()+1)+'/'+new Date(Date.parse(item.tanggal)).getFullYear()}</Text>
												<Text style={[ f._12, b.ml2, { color: softGrey, fontFamily: 'Roboto-Regular' }]}>{item.waktu} WIB</Text>
											</View>
											<Text style={[ c.light, b.ml2, { fontFamily: 'Roboto-Regular' }]} numberOfLines={1}>No Pesanan {item.no_pesanan}</Text>
										</View>
										<View style={{ width: wp('30%'), alignItems: 'flex-end' }}>
											<Text style={{ color: 'darkorange', fontFamily: 'Roboto-Regular' }}>{this.format(item.saldo)}</Text>
										</View>
									</View>

									<View style={[ b.mt2, b.mb2, { width: wp('90%'), borderBottomWidth: 1, borderBottomColor: light }]} />
								</View>
							)}
						/>
					)
				)}
			</View>
		)
	}
}