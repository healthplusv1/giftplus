import React, { Component } from 'react'
import { View, Text, Image, TouchableOpacity, ScrollView, FlatList } from 'react-native'
import { blue, light } from '../utils/Color'
import { xs, vs } from '../utils/Responsive'
import { f, c, b, p } from '../utils/StyleHelper'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
import FastImage from 'react-native-fast-image'
import LinearGradient from 'react-native-linear-gradient'
import AsyncStorage from '@react-native-async-storage/async-storage'
import axios from 'axios'
import { API_LOCAL, API_URL } from '@env'

export default class OrderDetailBaru extends Component {
	constructor(props) {
		super(props);
		this.state = {
			products: [
		    	{id: 1, nama: 'Fox Premium glove motor Black Sarung Tangan', image: require('../asset/sarung_tangan.png'), harga: '40000', jml_barang: 1},
		    	{id: 1, nama: 'Fox Premium glove motor Black Sarung Tangan', image: require('../asset/sarung_tangan.png'), harga: '60000', jml_barang: 1},
		    	{id: 1, nama: 'Fox Premium glove motor Black Sarung Tangan', image: require('../asset/sarung_tangan.png'), harga: '20000', jml_barang: 1}
		    ],
		}
	}

	format(number) {
        if (number) {
            var rupiah = "";
            var numberrev = number
                .toString()
                .split("")
                .reverse()
                .join("");
            for (var i = 0; i < numberrev.length; i++)
                if (i % 3 == 0) rupiah += numberrev.substr(i, 3) + ".";
            return (
                "Rp " +
                rupiah
                .split("", rupiah.length - 1)
                .reverse()
                .join("")
            );
        } else {
            return (
                "Rp "+number
            );
        }
    }

	render() {
		const { products } = this.state;
        let totalQuantity = 0;
        let totalPrice = 0;
        products.forEach((item) => {
            totalQuantity += item.jml_barang;
            totalPrice += item.jml_barang * item.harga;
        })
		return (
			<LinearGradient start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }} colors={[ '#181C21', '#252F38', '#252F38' ]} style={{ width: '100%', height: '100%' }}>
				<View style={{ width: '100%', backgroundColor: '#121212', alignItems: 'center' }}>
					<View style={{ width: wp('90%'), height: hp('10%'), flexDirection: 'row', alignItems: 'center' }}>
						<View style={{ width: wp('15%') }}>
							<TouchableOpacity onPress={() => this.props.navigation.pop()}>
								<FastImage source={require('../asset/left.png')} style={{ width: xs(25), height: xs(25) }} />
							</TouchableOpacity>
						</View>
						<View style={{ width: wp('60%'), alignItems: 'center' }}>
							<Text style={[ c.light, f._16, { fontFamily: 'Roboto-Bold' }]}>Order Baru</Text>
						</View>
						<View style={{ width: wp('15%') }}>
						</View>
					</View>
				</View>

				<ScrollView showsVerticalScrollIndicator={false}>
					<View style={{ width: '100%', alignItems: 'center' }}>
						<View style={[ b.mt2, { width: wp('90%'), flexDirection: 'row', alignItems: 'center' }]}>
							<View style={{ width: wp('40%') }}>
								<Text style={[ c.light, { fontFamily: 'Roboto-Regular' }]}>No. Pesanan</Text>
							</View>
							<View style={{ width: wp('50%'), alignItems: 'flex-end' }}>
								<Text style={[ c.light, { fontFamily: 'Roboto-Regular' }]}>123456789</Text>
							</View>
						</View>

						<View style={[ b.mt2, b.mb4, { width: wp('90%'), borderBottomWidth: 1, borderBottomColor: light }]} />
						<View style={{ width: '100%', height: hp('1%'), backgroundColor: '#121212' }} />

						<FlatList
							style={[ b.mt3 ]}
							data={this.state.products}
							keyExtractor={item => item.id}
							renderItem={({ item }) => (
								<>
									<View style={{ width: wp('90%'), flexDirection: 'row' }}>
										<View style={{ width: wp('15%') }}>
											<View style={{ width: '100%', height: wp('15%'), backgroundColor: light, borderRadius: xs(3), alignItems: 'center', justifyContent: 'center' }}>
												<FastImage source={item.image} style={{ width: xs(50), height: xs(50) }} />
											</View>
										</View>
										<View style={{ width: wp('55%') }}>
											<Text style={[ c.light, b.ml2, { fontFamily: 'Roboto-Bold' }]} numberOfLines={2}>{item.nama}</Text>
											<Text style={[ c.light, b.ml2, { fontFamily: 'Roboto-Regular', position: 'absolute', bottom: 0 }]}>{item.jml_barang} x {this.format(item.harga)}</Text>
										</View>
										<View style={{ width: wp('20%'), alignItems: 'flex-end' }}>
											<Text style={[ c.light, { fontFamily: 'Roboto-Regular', position: 'absolute', bottom: 0 }]}>{this.format(item.jml_barang * item.harga)}</Text>
										</View>
									</View>
									<View style={[ b.mt2, b.mb4, { width: wp('90%'), borderBottomWidth: 1, borderBottomColor: light }]} />
								</>
							)}
						/>
					</View>
				</ScrollView>

				<View style={{ width: '100%', alignItems: 'center', position: 'relative', bottom: 0 }}>
					<View style={{ width: wp('90%'), flexDirection: 'row', alignItems: 'center' }}>
						<View style={{ width: wp('40%') }}>
							<Text style={[ c.light, { fontFamily: 'Roboto-Bold' }]}>Jumlah pesanan</Text>
						</View>
						<View style={{ width: wp('50%'), alignItems: 'flex-end' }}>
							<Text style={[ c.light, { fontFamily: 'Roboto-Bold' }]}>{totalQuantity}</Text>
						</View>
					</View>

					<View style={[ b.mt1, { width: wp('90%'), flexDirection: 'row', alignItems: 'center' }]}>
						<View style={{ width: wp('40%') }}>
							<Text style={[ c.light, { fontFamily: 'Roboto-Bold' }]}>Total Harga pesanan</Text>
						</View>
						<View style={{ width: wp('50%'), alignItems: 'flex-end' }}>
							<Text style={[ c.light, { fontFamily: 'Roboto-Bold' }]}>{this.format(totalPrice)}</Text>
						</View>
					</View>

					<TouchableOpacity style={[ b.roundedLow, b.mt4, b.mb4, { width: wp('90%'), height: hp('7%'), backgroundColor: blue, alignItems: 'center', justifyContent: 'center' }]}>
						<Text style={[ c.light, f._16, { fontFamily: 'Roboto-Bold' }]}>Terima Pesanan</Text>
					</TouchableOpacity>
				</View>
			</LinearGradient>
		)
	}
}