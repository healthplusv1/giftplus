import React, { Component } from 'react'
import { View, Text, Image, TouchableOpacity, ScrollView, FlatList, TextInput } from 'react-native'
import { blue, light, softGrey } from '../utils/Color'
import { xs, vs } from '../utils/Responsive'
import { f, c, b, p } from '../utils/StyleHelper'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
import FastImage from 'react-native-fast-image'
import LinearGradient from 'react-native-linear-gradient'
import AsyncStorage from '@react-native-async-storage/async-storage'
import axios from 'axios'
import { API_LOCAL, API_URL } from '@env'

export default class ProductList extends Component {
	constructor(props) {
		super(props);
		this.state = {
		}
	}

	render() {
		return (
			<LinearGradient start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }} colors={[ '#181C21', '#252F38', '#252F38' ]} style={{ width: '100%', height: '100%' }}>
				<View style={{ width: wp('90%'), height: hp('6.5%'), borderRadius: wp('5%'), backgroundColor: softGrey, flexDirection: 'row', alignItems: 'center' }}>
							<TextInput
								style={[ c.light, { width: wp('62%'), height: '100%', marginLeft: wp('4%') }]}
								ref={ref => this.textInputRef = ref}
								placeholder='Apa yang kamu cari'
								placeholderTextColor={light}
								returnKeyType='search'
								onChangeText={(text) => this.setState({ search: text })}
								// onSubmitEditing={() => this.nextScreen()}
								value={this.state.search}
							/>
							<View style={{ width: wp('20%'), flexDirection: 'row-reverse' }}>
								{this.state.search === '' ?
									<TouchableOpacity onPress={() => this.textInputRef.focus()}>
										<Image source={require('../asset/search.png')} style={{ width: wp('6%'), height: wp('6%'), tintColor: light, marginLeft: wp('4%') }} />
									</TouchableOpacity>
									:
									<TouchableOpacity onPress={() => this.setState({ search: '' })} style={{ marginLeft: wp('4%') }}>
										<Image source={require('../asset/cross.png')} style={{ width: wp('6%'), height: wp('6%'), tintColor: light }} />
									</TouchableOpacity>
								}
								{/* <TouchableOpacity>
									<Image source={require('../asset/camera.png')} style={{ width: wp('6%'), height: wp('6%'), tintColor: light }} />
								</TouchableOpacity> */}
							</View>
						</View>
			</LinearGradient>
		)
	}
}