import React, { Component } from 'react'
import { View, Text, Image, TouchableOpacity, ScrollView, ToastAndroid, BackHandler, Alert, Animated } from 'react-native'
import { blue, light, softGrey, grey } from '../utils/Color'
import { xs, vs } from '../utils/Responsive'
import { f, c, b, p } from '../utils/StyleHelper'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
import FastImage from 'react-native-fast-image'
import LinearGradient from 'react-native-linear-gradient'
import AsyncStorage from '@react-native-async-storage/async-storage'
import axios from 'axios'
import { API_LOCAL, API_URL } from '@env'

var isHidden = true;

export default class Toko extends Component {
	constructor(props) {
		super(props);
		this.state = {
			count: 0,
			image: require('../asset/arrowfloating_ikon.png'),
            bounceValue: new Animated.Value(8)
		};
		this._didFocusSubscription = props.navigation.addListener('didFocus', payload =>
			BackHandler.addEventListener('hardwareBackPress', this.handleBackPress)
		)
	}

	componentDidMount() {
		this._willBlurSubscription = this.props.navigation.addListener('willBlur', payload =>
			BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress)
		)
	}

	changePosition() {
        this.setState({
            image: !isHidden ? require('../asset/arrowfloating_ikon.png') : require('../asset/arrowdownfloating_ikon.png')
        });
        
        var toValue =10;

        if (isHidden) {
            toValue = 0;
        }

        Animated.spring(
            this.state.bounceValue,
            {
                toValue: toValue,
                velocity: 3,
                tension: 2,
                friction: 8
            }
        ).start();

        isHidden = !isHidden;
    }

	format(number) {
        if (number) {
            var rupiah = "";
            var numberrev = number
                .toString()
                .split("")
                .reverse()
                .join("");
            for (var i = 0; i < numberrev.length; i++)
                if (i % 3 == 0) rupiah += numberrev.substr(i, 3) + ".";
            return (
                "Rp " +
                rupiah
                .split("", rupiah.length - 1)
                .reverse()
                .join("")
            );
        } else {
            return (
                "Rp "+number
            );
        }
    }

    handleBackPress = () => {
		if (this.props.navigation.isFocused()) {
			Alert.alert(
				'Keluar Aplikasi',
				'Anda yakin ingin menutup aplikasi ini?',
				[
					{text: 'Tidak', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
					{text: 'Ya', onPress: () => BackHandler.exitApp()}
				],
				{cancelable: false}
			)
		}
		return true;
	}

	render() {
		return (
			<LinearGradient start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }} colors={[ '#181C21', '#252F38', '#252F38' ]} style={{ width: '100%', height: '100%' }}>
				<TouchableOpacity onPress={() => this.props.navigation.push('ScanQRSeller')} style={{ width: xs(50), height: xs(50), backgroundColor: blue, borderRadius: xs(10), borderWidth: 1, borderColor: light, alignItems: 'center', justifyContent: 'center', position: 'absolute', right: wp('5%'), bottom: wp('5%'), zIndex: 2 }}>
                    <FastImage source={require('../asset/scanfloating_ikon.png')} style={{ width: xs(30), height: xs(30) }} />
                </TouchableOpacity>

				<ScrollView showsVerticalScrollIndicator={false}>
					<View style={{ width: '100%', alignItems: 'center', backgroundColor: '#121212' }}>
						<View style={[ b.mt4, b.mb4, { width: wp('90%'), flexDirection: 'row', alignItems: 'center' }]}>
							<View style={{ width: wp('45%') }}>
	                            <FastImage source={require('../asset/giftplus.png')} style={{ width: xs(120), height: xs(32) }} />
	                        </View>
	                        <View style={{ width: wp('45%'), flexDirection: 'row-reverse' }}>
	                            <TouchableOpacity onPress={() => this.props.navigation.push('Drawer')}>
	                                <Image source={require('../asset/menu.png')} style={{ width: xs(22), height: xs(22), tintColor: light }} />
	                            </TouchableOpacity>
	                            <TouchableOpacity onPress={() => this.props.navigation.push('Notification')} style={[ b.mr2 ]}>
	                                <Image source={require('../asset/notification.png')} style={{ width: xs(22), height: xs(22), tintColor: light }} />
	                            </TouchableOpacity>
	                            <TouchableOpacity onPress={() => this.props.navigation.push('Cart')} style={[ b.mr2 ]}>
	                                <Image source={require('../asset/cart.png')} style={{ width: xs(19), height: xs(20), tintColor: light }} />
	                                {this.state.count !== 0 && (
	                                    <View style={{ width: xs(12), height: xs(12), borderRadius: xs(12), backgroundColor: 'darkorange', position: 'absolute', right: 0, alignItems: 'center', justifyContent: 'center' }}>
	                                        <Text style={[ c.light, f._10 ]}>{this.state.count}</Text>
	                                    </View>
	                                )}
	                            </TouchableOpacity>
	                        </View>
						</View>
					</View>

					<View style={{ width: '100%', alignItems: 'center' }}>
						<View style={[ b.mt2, { width: wp('90%'), flexDirection: 'row', alignItems: 'center' }]}>
	                        <View style={{ width: xs(60), height: xs(60), alignItems: 'center', justifyContent: 'center', borderRadius: xs(60), backgroundColor: light }}>
	                            <Image source={require('../asset/logo_apotek.png')} style={{ width: xs(45), height: xs(45) }} />
	                        </View>
	                        <View style={[ b.ml3, b.roundedLow, { width: xs(250), height: xs(85), backgroundColor: grey }]}>
	                            <View style={{ width: '100%', height: xs(80), alignItems: 'center' }}>
	                                <View style={[ b.mt1, { width: xs(220), flexDirection: 'row', alignItems: 'center' }]}>
	                                    <View style={{ width: xs(190) }}>
		                                    <Text style={[ c.light, { fontFamily: 'Roboto-Bold' }]}>Apotek Sehati</Text>
	                                    </View>
	                                </View>
	                                <TouchableOpacity onPress={() => this.props.navigation.push('SaldoDetail')} style={[ b.mt1, { width: '100%', height: xs(58), backgroundColor: blue, borderBottomLeftRadius: xs(5), borderBottomRightRadius: xs(5) }]}>
	                                	<View style={[ b.ml2, b.mt2, { flexDirection: 'row', alignItems: 'center' }]}>
	                                		<Image source={require('../asset/belumbayar_icon.png')} style={{ width: xs(20), height: xs(20) }} />
	                                		<View style={[ b.ml1, { width: xs(200) }]}>
	                                			<Text style={[ c.light, { fontFamily: 'Roboto-Regular' }]} numberOfLines={1}>{this.format(100000)}</Text>
	                                		</View>
	                                	</View>
	                                	<Text style={[ c.light, b.ml2, { fontFamily: 'Roboto-Regular', marginTop: vs(2) }]}>Total saldo saya</Text>
	                                </TouchableOpacity>
	                            </View>
	                        </View>
	                    </View>
					</View>

					<View style={[ b.mt4, { width: '100%', alignItems: 'center' }]}>
						<View style={{ width: wp('90%'), flexDirection: 'row' }}>
							<TouchableOpacity onPress={() => this.props.navigation.push('Orders')} style={{ width: wp('22.5%'), alignItems: 'center' }}>
								<View style={{ width: xs(40), height: xs(40), borderRadius: xs(5), backgroundColor: blue, alignItems: 'center', justifyContent: 'center' }}>
									<Image source={require('../asset/order_icon.png')} style={{ width: xs(25), height: xs(25) }} />
								</View>
								<Text style={[ c.light, p.textCenter, { fontFamily: 'Roboto-Regular', marginTop: vs(3) }]}>Order</Text>
							</TouchableOpacity>
							<TouchableOpacity onPress={() => this.props.navigation.push('ProductList')} style={{ width: wp('22.5%'), alignItems: 'center' }}>
								<View style={{ width: xs(40), height: xs(40), borderRadius: xs(5), backgroundColor: blue, alignItems: 'center', justifyContent: 'center' }}>
									<Image source={require('../asset/produk_icon.png')} style={{ width: xs(25), height: xs(25) }} />
								</View>
								<Text style={[ c.light, p.textCenter, { fontFamily: 'Roboto-Regular', marginTop: vs(3) }]}>Produk</Text>
							</TouchableOpacity>
							<TouchableOpacity onPress={() => ToastAndroid.show('not implemented yet', ToastAndroid.SHORT)} style={{ width: wp('22.5%'), alignItems: 'center' }}>
								<View style={{ width: xs(40), height: xs(40), borderRadius: xs(5), backgroundColor: blue, alignItems: 'center', justifyContent: 'center' }}>
									<Image source={require('../asset/giftcardmenu_icon.png')} style={{ width: xs(25), height: xs(25) }} />
								</View>
								<Text style={[ c.light, p.textCenter, { fontFamily: 'Roboto-Regular', marginTop: vs(3) }]}>Diskon</Text>
							</TouchableOpacity>
							<TouchableOpacity onPress={() => ToastAndroid.show('not implemented yet', ToastAndroid.SHORT)} style={{ width: wp('22.5%'), alignItems: 'center' }}>
								<View style={{ width: xs(40), height: xs(40), borderRadius: xs(5), backgroundColor: blue, alignItems: 'center', justifyContent: 'center' }}>
									<Image source={require('../asset/bonus_icon.png')} style={{ width: xs(25), height: xs(25) }} />
								</View>
								<Text style={[ c.light, p.textCenter, { fontFamily: 'Roboto-Regular', marginTop: vs(3) }]}>Bonus Beli</Text>
							</TouchableOpacity>
						</View>

						<View style={[ b.mt4, { width: wp('90%'), flexDirection: 'row' }]}>
							<TouchableOpacity onPress={() => ToastAndroid.show('not implemented yet', ToastAndroid.SHORT)} style={{ width: wp('22.5%'), alignItems: 'center' }}>
								<View style={{ width: xs(40), height: xs(40), borderRadius: xs(5), backgroundColor: blue, alignItems: 'center', justifyContent: 'center' }}>
									<Image source={require('../asset/prizechallenge_icon.png')} style={{ width: xs(25), height: xs(25) }} />
								</View>
								<Text style={[ c.light, p.textCenter, { fontFamily: 'Roboto-Regular', marginTop: vs(3) }]}>Tantangan Hadiah</Text>
							</TouchableOpacity>
							<TouchableOpacity onPress={() => ToastAndroid.show('not implemented yet', ToastAndroid.SHORT)} style={{ width: wp('22.5%'), alignItems: 'center' }}>
								<View style={{ width: xs(40), height: xs(40), borderRadius: xs(5), backgroundColor: blue, alignItems: 'center', justifyContent: 'center' }}>
									<Image source={require('../asset/pengaturanpromosi_icon.png')} style={{ width: xs(28), height: xs(25) }} />
								</View>
								<Text style={[ c.light, p.textCenter, { fontFamily: 'Roboto-Regular', marginTop: vs(3) }]}>Pengaturan Promosi</Text>
							</TouchableOpacity>
							<TouchableOpacity onPress={() => ToastAndroid.show('not implemented yet', ToastAndroid.SHORT)} style={{ width: wp('22.5%'), alignItems: 'center' }}>
								<View style={{ width: xs(40), height: xs(40), borderRadius: xs(5), backgroundColor: blue, alignItems: 'center', justifyContent: 'center' }}>
									<Image source={require('../asset/tampilantoko_icon.png')} style={{ width: xs(27), height: xs(25) }} />
								</View>
								<Text style={[ c.light, p.textCenter, { fontFamily: 'Roboto-Regular', marginTop: vs(3) }]}>Tampilan Toko</Text>
							</TouchableOpacity>
						</View>
					</View>
				</ScrollView>
			</LinearGradient>
		)
	}
}