import { createStackNavigator } from 'react-navigation-stack'
import TokoPage from './screen/Toko'
import OrdersPage from './screen/Orders'
import OrderDetailBaruPage from './screen/OrderDetailBaru'
import OrderDetailProsesPage from './screen/OrderDetailProses'
import OrderDetailSelesaiPage from './screen/OrderDetailSelesai'

import ProductListPage from './screen/ProductList'

export const TokoStack = createStackNavigator({
	Toko: {
		screen: TokoPage,
		navigationOptions: {
            headerShown: false
        }
	},

    Orders: {
        screen: OrdersPage,
        navigationOptions: {
            headerShown: false
        }
    },

    OrderDetailBaru: {
        screen: OrderDetailBaruPage,
        navigationOptions: {
            headerShown: false
        }
    },

    OrderDetailProses: {
        screen: OrderDetailProsesPage,
        navigationOptions: {
            headerShown: false
        }
    },

    OrderDetailSelesai: {
        screen: OrderDetailSelesaiPage,
        navigationOptions: {
            headerShown: false
        }
    },

    //

    ProductList: {
        screen: ProductListPage,
        navigationOptions: {
            headerShown: false
        }
    },
})