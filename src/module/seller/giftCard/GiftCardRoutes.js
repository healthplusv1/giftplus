import { createStackNavigator } from 'react-navigation-stack'
import GiftCardPage from './screen/GiftCard'

export const GiftCardStack = createStackNavigator({
	GiftCard: {
		screen: GiftCardPage,
		navigationOptions: {
            headerShown: false
        }
	}
})