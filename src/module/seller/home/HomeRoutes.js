import { createStackNavigator } from 'react-navigation-stack'
import HomePage from './screen/Home'

export const HomeSellerStack = createStackNavigator({
	Home: {
		screen: HomePage,
		navigationOptions: {
            headerShown: false
        }
	}
})