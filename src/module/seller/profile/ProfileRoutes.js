import { createStackNavigator } from 'react-navigation-stack'
import ProfilePage from './screen/Profile'

export const ProfileSellerStack = createStackNavigator({
	Profile: {
		screen: ProfilePage,
		navigationOptions: {
            headerShown: false
        }
	}
})