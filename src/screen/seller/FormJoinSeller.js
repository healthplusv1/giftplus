import React, { Component } from 'react'
import { View, Text, Image, ScrollView, TextInput, TouchableOpacity, ToastAndroid } from 'react-native'
import { blue, light } from '../../utils/Color'
import { xs, vs } from '../../utils/Responsive'
import { f, c, b, p } from '../../utils/StyleHelper'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
import FastImage from 'react-native-fast-image'
import AsyncStorage from '@react-native-async-storage/async-storage'
import axios from 'axios'
import { API_LOCAL, API_URL } from '@env'

export default class GiftCard extends Component {
	constructor(props) {
		super(props);
		this.state = {
			loading: false,
			namaLengkap: '',
			namaToko: '',
			noTelpToko: '',
			alamatToko: ''
		}
	}

	async registerSeller() {
        const id_login = await AsyncStorage.getItem('@id_login')

        axios.get(API_URL+'/main/seller/register', {params: {
            id_login: id_login,
            nama_toko: this.state.namaToko,
            alamat_toko: this.state.alamatToko
        }})
        .then(res => {
            // console.log(res.data)
            if (res.data.status === 'true') {
                this.props.navigation.push('Seller')
            } else {
                Alert.alert('', 'You failed to register as a seller, please try again')
            }
        })
    }

	render() {
		return (
			<FastImage source={require('../../asset/background_login.png')} style={{ width: '100%', height: '100%' }}>
				<ScrollView showsVerticalScrollIndicator={false}>
					<View style={{ width: '100%', alignItems: 'center' }}>
						<FastImage source={require('../../asset/giftplus2.png')} style={{ width: xs(60), height: xs(75), marginTop: vs(35) }} />
						<Text style={[ c.light, f._18, { fontFamily: 'Roboto-Bold' }]}>GIFTPLUS</Text>

						<Text style={[ c.light, f._20, { fontFamily: 'Roboto-Bold', marginTop: vs(30) }]}>Buat Akun Penjual</Text>

						<TouchableOpacity style={[ b.mt4 ]}>
							<View style={{ width: xs(60), height: xs(60), borderRadius: xs(60), backgroundColor: light, alignItems: 'center', justifyContent: 'center' }}>
								<Image source={require('../../asset/user.png')} style={{ width: xs(45), height: xs(45), tintColor: '#C9C9C9' }} />
							</View>
						</TouchableOpacity>
						<Text style={[ b.mt1, { fontFamily: 'Roboto-Regular', color: '#C9C9C9' }]}>Foto penjual</Text>

						<View style={[ b.mt4, { width: wp('90%'), borderBottomWidth: 1, borderBottomColor: '#C9C9C9' }]}>
		                    <TextInput
		                        style={[ c.light, { width: '100%', marginBottom: xs(-10) }]}
		                        placeholder='Nama Lengkap Penjual'
		                        placeholderTextColor='#C9C9C9'
		                        onChangeText={(text) => this.setState({ namaLengkap: text })}
		                        value={this.state.namaLengkap}
		                    />
		                </View>
		                <View style={[ b.mt4, { width: wp('90%'), borderBottomWidth: 1, borderBottomColor: '#C9C9C9' }]}>
		                    <TextInput
		                        style={[ c.light, { width: '100%', marginBottom: xs(-10) }]}
		                        placeholder='Nama Toko'
		                        placeholderTextColor='#C9C9C9'
		                        onChangeText={(text) => this.setState({ namaToko: text })}
		                        value={this.state.namaToko}
		                    />
		                </View>
		                <View style={[ b.mt4, { width: wp('90%'), borderBottomWidth: 1, borderBottomColor: '#C9C9C9' }]}>
		                    <TextInput
		                        style={[ c.light, { width: '100%', marginBottom: xs(-10) }]}
		                        placeholder='Nomor Telepon Toko'
		                        placeholderTextColor='#C9C9C9'
		                        keyboardType={"phone-pad"}
		                        onChangeText={(text) => this.setState({ noTelpToko: text })}
		                        value={this.state.noTelpToko}
		                    />
		                </View>
		                <View style={[ b.mt4, { width: wp('90%'), borderBottomWidth: 1, borderBottomColor: '#C9C9C9' }]}>
		                    <TextInput
		                        style={[ c.light, { width: '100%', marginBottom: xs(-10) }]}
		                        placeholder='Alamat Toko'
		                        placeholderTextColor='#C9C9C9'
		                        onChangeText={(text) => this.setState({ alamatToko: text })}
		                        value={this.state.alamatToko}
		                    />
		                </View>
		                <View style={[ b.mt2, { width: wp('90%') }]}>
			                <TouchableOpacity>
			                	<Text style={[ c.blue, { fontFamily: 'Roboto-Regular' }]}>Pilih lewat peta</Text>
			                </TouchableOpacity>
		                </View>

		                <View style={[ b.mt4, { width: wp('90%') }]}>
			                <Text style={[ c.light, f._16, { fontFamily: 'Roboto-Bold' }]}>Tambahkan Foto Toko</Text>
			                <TouchableOpacity style={[ b.mt2, { width: xs(70), height: xs(45), backgroundColor: '#121212', borderWidth: 1, borderColor: blue, borderRadius: xs(5), alignItems: 'center', justifyContent: 'center' }]}>
			                	<FastImage source={require('../../asset/addpicture_icon.png')} style={{ width: xs(20), height: xs(20) }} />
			                </TouchableOpacity>
		                </View>

		                <TouchableOpacity
		                	onPress={() => {
		                		ToastAndroid.show('the backend function is still not complete', ToastAndroid.SHORT)
		                		this.props.navigation.push('VerificationSeller')
		                	}} style={[ b.mb4, { width: wp('90%'), height: hp('7%'), backgroundColor: '#121212', backgroundColor: blue, borderRadius: xs(5), alignItems: 'center', justifyContent: 'center', marginTop: xs(30) }]}>
		                	<Text style={[ c.light, f._16, { fontFamily: 'Roboto-Bold' }]}>Daftar</Text>
		                </TouchableOpacity>
					</View>
				</ScrollView>
			</FastImage>
		)
	}
}