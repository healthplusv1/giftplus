import React, { Component } from 'react'
import { View, Text, Image, TouchableOpacity } from 'react-native'
import { blue, light } from '../../utils/Color'
import { xs, vs } from '../../utils/Responsive'
import { f, c, b, p } from '../../utils/StyleHelper'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
import FastImage from 'react-native-fast-image'
import Swiper from 'react-native-swiper'
import LinearGradient from 'react-native-linear-gradient'
import AsyncStorage from '@react-native-async-storage/async-storage'
import axios from 'axios'
import { API_LOCAL, API_URL } from '@env'

export default class GiftCard extends Component {
	constructor(props) {
		super(props);
		this.state = {
			number: ''
		}
	}

	render() {
		return (
			<LinearGradient start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }} colors={[ '#181C21', '#252F38', '#252F38' ]} style={{ width: '100%', height: '100%' }}>
				<View style={{ width: '100%', height: hp('10%'), alignItems: 'center', justifyContent: 'center', backgroundColor: '#121212' }}>
					<View style={{ width: wp('90%'), flexDirection: 'row', alignItems: 'center' }}>
						<View style={{ width: wp('15%') }}>
							<TouchableOpacity onPress={() => this.props.navigation.pop()}>
								<FastImage source={require('../../asset/left.png')} style={{ width: xs(25), height: xs(25) }} />
							</TouchableOpacity>
						</View>
						<View style={{ width: wp('60%'), alignItems: 'center' }}>
							<Text style={[ c.light, f._16, { fontFamily: 'Roboto-Bold' }]}>Pemberitahuan Penjual</Text>
						</View>
						<View style={{ width: wp('15%') }}>
						</View>
					</View>
				</View>

				<Swiper
					style={{ height: hp('86%') }}
					loop={false}
					onIndexChanged={(index) => this.setState({ number: index })}
					dotStyle={{ marginBottom: xs(50) }}
					activeDotStyle={{ marginBottom: xs(50) }}
					dotColor={light}
					showsButtons={true}
					buttonWrapperStyle={{ backgroundColor: 'transparent', flexDirection: 'row', position: 'absolute', top: xs(250), left: 0, flex: 1, paddingHorizontal: 10, paddingVertical: 10, justifyContent: 'space-between', alignItems: 'center' }}
					nextButton={<Text style={[ c.blue, { fontFamily: 'Roboto-Bold' }]}>Next</Text>}
					prevButton={<Text style={[ c.blue, { fontFamily: 'Roboto-Bold' }]}>{this.state.number === 2 ? '' : 'Prev'}</Text>}
				>
	        		<View style={{ width: '100%', height: '100%', alignItems: 'center' }}>
						<View style={[ b.mt4, { width: '100%', height: xs(125), backgroundColor: '#323232', alignItems: 'center', justifyContent: 'center' }]}>
							<Text style={[ c.light, f._24, { fontFamily: 'Roboto-Bold' }]}>Cara menjadi penjual</Text>
							<View style={{ flexDirection: 'row', alignItems: 'center', backgroundColor: 'darkorange', borderTopLeftRadius: xs(20), borderBottomLeftRadius: xs(20), position: 'absolute', right: 0, top: xs(10) }}>
								<View style={{ width: xs(35), height: xs(35), borderRadius: xs(35), borderWidth: 3, borderColor: '#323232', alignItems: 'center', justifyContent: 'center' }}>
									<Image source={require('../../asset/icongabungjadiseller.png')} style={{ width: xs(18), height: xs(18), tintColor: light }} />
								</View>
								<View style={{ height: xs(30), alignItems: 'center', justifyContent: 'center' }}>
									<Text style={[ c.light, b.ml1, b.mr2, { fontFamily: 'Roboto-Regular' }]}>Khusus Penjual</Text>
								</View>
							</View>
						</View>
						<FastImage source={require('../../asset/Asset_4.png')} style={{ width: xs(200), height: xs(134), marginTop: vs(50) }} />
						<View style={{ width: wp('65%'), marginTop: xs(20) }}>
							<Text style={[ c.light, p.textCenter, { fontFamily: 'Roboto-Bold' }]}>Buat akun pembeli terlebih dahulu, lalu buat akun penjual.</Text>
						</View>
						<View style={[ b.mt1, { width: wp('60%') }]}>
							<Text style={[ p.textCenter, { fontFamily: 'Roboto-Regular', color: '#D4BA08' }]}>Perlindungan Penjual dengan Kebijakan</Text>
						</View>
					</View>

	        		<View style={{ width: '100%', height: '100%', alignItems: 'center' }}>
						<View style={[ b.mt4, { width: '100%', height: xs(125), backgroundColor: '#323232', alignItems: 'center', justifyContent: 'center' }]}>
							<Text style={[ c.light, f._24, { fontFamily: 'Roboto-Bold' }]}>Cara menjadi penjual</Text>
							<View style={{ flexDirection: 'row', alignItems: 'center', backgroundColor: 'darkorange', borderTopLeftRadius: xs(20), borderBottomLeftRadius: xs(20), position: 'absolute', right: 0, top: xs(10) }}>
								<View style={{ width: xs(35), height: xs(35), borderRadius: xs(35), borderWidth: 3, borderColor: '#323232', alignItems: 'center', justifyContent: 'center' }}>
									<Image source={require('../../asset/icongabungjadiseller.png')} style={{ width: xs(18), height: xs(18), tintColor: light }} />
								</View>
								<View style={{ height: xs(30), alignItems: 'center', justifyContent: 'center' }}>
									<Text style={[ c.light, b.ml1, b.mr2, { fontFamily: 'Roboto-Regular' }]}>Khusus Penjual</Text>
								</View>
							</View>
						</View>
						<FastImage source={require('../../asset/Asset_1.png')} style={{ width: xs(200), height: xs(134), marginTop: vs(50) }} />
						<View style={{ width: wp('60%'), marginTop: xs(20) }}>
							<Text style={[ c.light, p.textCenter, { fontFamily: 'Roboto-Bold' }]}>Tentukan nama toko</Text>
							<Text style={[ c.light, p.textCenter, { fontFamily: 'Roboto-Regular' }]}></Text>
						</View>
						<View style={[ b.mt1, { width: wp('60%') }]}>
							<Text style={[ p.textCenter, { fontFamily: 'Roboto-Regular', color: '#D4BA08' }]}>Buatlah nama toko yang menarik</Text>
						</View>
					</View>

	        		<View style={{ width: '100%', height: '100%', alignItems: 'center' }}>
						<View style={[ b.mt4, { width: '100%', height: xs(125), backgroundColor: '#323232', alignItems: 'center', justifyContent: 'center' }]}>
							<Text style={[ c.light, f._24, { fontFamily: 'Roboto-Bold' }]}>Cara menjadi penjual</Text>
							<View style={{ flexDirection: 'row', alignItems: 'center', backgroundColor: 'darkorange', borderTopLeftRadius: xs(20), borderBottomLeftRadius: xs(20), position: 'absolute', right: 0, top: xs(10) }}>
								<View style={{ width: xs(35), height: xs(35), borderRadius: xs(35), borderWidth: 3, borderColor: '#323232', alignItems: 'center', justifyContent: 'center' }}>
									<Image source={require('../../asset/icongabungjadiseller.png')} style={{ width: xs(18), height: xs(18), tintColor: light }} />
								</View>
								<View style={{ height: xs(30), alignItems: 'center', justifyContent: 'center' }}>
									<Text style={[ c.light, b.ml1, b.mr2, { fontFamily: 'Roboto-Regular' }]}>Khusus Penjual</Text>
								</View>
							</View>
						</View>
						<FastImage source={require('../../asset/Asset_2.png')} style={{ width: xs(200), height: xs(155), marginTop: vs(50) }} />
						<View style={{ width: wp('75%'), marginTop: xs(20) }}>
							<Text style={[ c.light, p.textCenter, { fontFamily: 'Roboto-Bold' }]}>Silahkan upload produk yang akan dijual</Text>
							<Text style={[ c.light, p.textCenter, { fontFamily: 'Roboto-Regular' }]}></Text>
						</View>
						<View style={[ b.mt1, { width: wp('60%') }]}>
							<Text style={[ p.textCenter, { fontFamily: 'Roboto-Regular', color: '#D4BA08' }]}>Tampilkan foto terbaik</Text>
						</View>

						<TouchableOpacity onPress={() => this.props.navigation.push('JoinSeller')} style={{ position: 'absolute', bottom: xs(9), right: xs(10) }}>
							<Text style={[ c.blue, { fontFamily: 'Roboto-Bold' }]}>Next</Text>
						</TouchableOpacity>
					</View>
	        	</Swiper>
			</LinearGradient>
		)
	}
}