import React, { Component } from 'react'
import { View, Text, TouchableOpacity } from 'react-native'
import { blue, light } from '../../utils/Color'
import { xs, vs } from '../../utils/Responsive'
import { f, c, b, p } from '../../utils/StyleHelper'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
import FastImage from 'react-native-fast-image'
import LinearGradient from 'react-native-linear-gradient'
import AsyncStorage from '@react-native-async-storage/async-storage'
import axios from 'axios'
import { API_LOCAL, API_URL } from '@env'

export default class GiftCard extends Component {
	constructor(props) {
		super(props);
		this.state = {
		}
	}

	render() {
		return (
			<LinearGradient start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }} colors={[ '#181C21', '#252F38', '#252F38' ]} style={{ width: '100%', height: '100%', alignItems: 'center' }}>
				<FastImage source={require('../../asset/seller_verifikasi.png')} style={{ width: xs(295), height: xs(200), marginTop: xs(80) }} />
				<Text style={[ c.light, f._20, b.mt4, { fontFamily: 'Roboto-Bold' }]}>Proses Verifikasi</Text>

				<View style={[ b.mt1, { width: wp('80%') }]}>
					<Text style={[ c.light, f._18, p.textCenter, { fontFamily: 'Roboto-Regular' }]}>Pendaftaran anda sedang dalam proses verifikasi.</Text>
					<Text style={[ c.light, f._18, p.textCenter, { fontFamily: 'Roboto-Regular' }]}>Mohon tunggu 1x24 jam.</Text>
				</View>

				<TouchableOpacity onPress={() => this.props.navigation.push('Seller')} style={[ b.roundedLow, { width: wp('80%'), height: hp('7%'), backgroundColor: blue, alignItems: 'center', justifyContent: 'center', marginTop: xs(50) }]}>
					<Text style={[ c.light, { fontFamily: 'Roboto-Bold' }]}>Lanjut ke seller</Text>
				</TouchableOpacity>

				<TouchableOpacity onPress={() => this.props.navigation.push('Tabs')} style={[ b.roundedLow, b.mt4, { width: wp('80%'), height: hp('7%'), backgroundColor: blue, alignItems: 'center', justifyContent: 'center' }]}>
					<Text style={[ c.light, { fontFamily: 'Roboto-Bold' }]}>Kembali ke beranda</Text>
				</TouchableOpacity>
			</LinearGradient>
		)
	}
}