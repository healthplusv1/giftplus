import React, { Component } from 'react'
import { Text, View, TextInput, TouchableOpacity, Alert } from 'react-native'
import { blue, light } from '../../utils/Color'
import { xs, vs } from '../../utils/Responsive'
import { f, c, b, p } from '../../utils/StyleHelper'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
import FastImage from 'react-native-fast-image'
import LinearGradient from 'react-native-linear-gradient'
import Dialog, { DialogContent } from 'react-native-popup-dialog'
import AsyncStorage from '@react-native-async-storage/async-storage'
import axios from 'axios'
import { API_LOCAL, API_URL } from '@env'

export default class JoinSeller extends Component {
    constructor(props) {
        super(props);
        this.state = {
            namaToko: '',
            alamatToko: '',
            visible: false
        }
    }

    nextScreen() {
        this.setState({ visible: false })
        this.props.navigation.push('Tabs')
    }

    render() {
        return (
            <>
                <Dialog
                    visible={this.state.visible}
                    onTouchOutside={() => this.setState({ visible: false })}
                    dialogStyle={{ backgroundColor: light }}
                >
                    <DialogContent>
                        <View style={{ width: xs(200), alignItems: 'center' }}>
                            <Text style={[ p.textCenter, b.mt4, { fontFamily: 'Roboto-Bold' }]}>Kami akan menghubungi anda jika ada info baru untuk menjadi penjual.</Text>

                            <TouchableOpacity onPress={() => this.nextScreen()} style={[ b.roundedLow, b.mt4, { width: wp('60%'), height: hp('6%'), backgroundColor: blue, alignItems: 'center', justifyContent: 'center' }]}>
                                <Text style={[ c.light, { fontFamily: 'Roboto-Bold' }]}>Kembali ke beranda</Text>
                            </TouchableOpacity>
                        </View>
                    </DialogContent>
                </Dialog>

                <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }} colors={[ '#181C21', '#252F38', '#252F38' ]} style={{ width: '100%', height: '100%', alignItems: 'center' }}>
                    <FastImage source={require('../../asset/seller_ask.png')} style={{ width: xs(250), height: xs(170), marginTop: xs(80) }} />

                    <Text style={[ c.light, f._18, b.mt4, { fontFamily: 'Roboto-Bold' }]}>Gabung jadi Penjual</Text>
                    <View style={[ b.mt1, { width: wp('60%'), alignItems: 'center' }]}>
                        <Text style={[ c.light, f._16, p.textCenter, { fontFamily: 'Roboto-Regular' }]}>Apakah anda mempunyai Toko fisik?</Text>
                    </View>

                    <TouchableOpacity onPress={() => this.props.navigation.push('FormJoinSeller')} style={[ b.roundedLow, { width: wp('80%'), height: hp('7.5%'), backgroundColor: blue, alignItems: 'center', justifyContent: 'center', marginTop: xs(120) }]}>
                        <Text style={[ c.light, { fontFamily: 'Roboto-Bold' }]}>Saya punya toko fisik</Text>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={() => this.setState({ visible: true })} style={[ b.roundedLow, b.mt2, { width: wp('80%'), height: hp('7.5%'), backgroundColor: light, alignItems: 'center', justifyContent: 'center' }]}>
                        <Text style={[ c.blue, { fontFamily: 'Roboto-Bold' }]}>Saya tertarik</Text>
                    </TouchableOpacity>
                </LinearGradient>
            </>
        )
    }
}
