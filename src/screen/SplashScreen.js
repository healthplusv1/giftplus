import React, { Component } from 'react'
import { Text, View, Image } from 'react-native'
import FastImage from 'react-native-fast-image'
import { xs, vs } from '../utils/Responsive'
import { f, c, b, p } from '../utils/StyleHelper'
import AsyncStorage from '@react-native-async-storage/async-storage'
import axios from 'axios'
import { API_URL } from '@env'

import firebase from '@react-native-firebase/app'
import iid from '@react-native-firebase/iid';

export default class SplashScreen extends Component {
	constructor(props) {
		super(props);
		this.state = {
		}
	}

	async componentDidMount() {
		const check_login = await AsyncStorage.getItem('@id_login')
		const check_guest = await AsyncStorage.getItem('id_login_guest')
		
		if (check_login !== null) {
			/* renew id_notification in server, reducing logic failure */
			const id_notification = await firebase.iid().getToken();
			console.log("ID_NOTIFICATION ==> ", id_notification)
			axios.get(API_URL+'/main/update_id_notification', {params: {id_login: check_login, id_notification: id_notification}})
			/* end renew id_notification */

			axios.get(API_URL+'/main/get_user_by_id_login', {params: {
				id_login: check_login
			}})
			.then(res => {
				if (res.data.status === 'true') {
					setTimeout(() => {
						this.props.navigation.push('Tabs')
					}, 2000);
				}
			})
			.catch(err => {
				console.log(err)
			})
		} else if (check_guest !== null) {
			setTimeout(() => {
				this.props.navigation.push('Tabs')
			}, 2000);
		} else {
			setTimeout(() => {
				this.props.navigation.push('Intro')
			}, 2000);
		}
	}

    render() {
        return (
            <FastImage source={require('../asset/splash.png')} style={{ width: '100%', height: '100%', alignItems: 'center', justifyContent: 'center' }}>
            	<FastImage source={require('../asset/giftplus2.png')} style={{ width: xs(75), height: xs(95) }} />
				<Text style={[ f._20, { color: '#FFF', marginTop: vs(2), fontFamily: 'Roboto-Bold' }]}>GIFTPLUS</Text>
            </FastImage>
        )
    }
}
