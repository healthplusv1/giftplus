import React, { Component } from 'react'
import { Text, View, TouchableOpacity, BackHandler } from 'react-native'
import { xs, vs } from '../../utils/Responsive'
import { f, c, b, p } from '../../utils/StyleHelper'
import { blue, light } from '../../utils/Color'
import FastImage from 'react-native-fast-image'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
import Swiper from 'react-native-swiper'
import AsyncStorage from '@react-native-async-storage/async-storage'

export default class Intro extends Component {

	_didFocusSubscription;
    _willBlurSubscription;

	constructor(props) {
		super(props);
		this.state = {
			number: '',
			texts: 'Prev',
			text: '',
			id_login_guest: ''
		};
		this._didFocusSubscription = props.navigation.addListener('didFocus', payload =>
			BackHandler.addEventListener('hardwareBackPress', this.handleBackPress)
        );
	}

	componentWillUnmount(){
        this._didFocusSubscription && this._didFocusSubscription.remove();
        this._willBlurSubscription && this._willBlurSubscription.remove();
    }

    componentDidMount(){
        this._willBlurSubscription = this.props.navigation.addListener('willBlur', payload =>
			BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress)
        );
    }

    nextScreen() {
    	var date = new Date().getDate();
		var month = new Date().getMonth();
		var year = new Date().getFullYear();
		var hours = new Date().getHours();
		var min = new Date().getMinutes();
		var sec = new Date().getSeconds();
		var guest = year +''+ month +''+ date +''+ hours +''+ min +''+ sec

    	AsyncStorage.setItem('id_login_guest', guest)
    	this.props.navigation.push('Tabs')
    }

	handleBackPress = () => {
        BackHandler.exitApp();
        return true;
    }

    render() {
        return (
        	<Swiper
				loop={false}
				onIndexChanged={(index) => this.setState({ number: index })}
				dotStyle={{ marginBottom: xs(125) }}
				activeDotStyle={{ marginBottom: xs(125) }}
				dotColor={light}
				showsButtons={true}
				buttonWrapperStyle={{ backgroundColor: 'transparent', flexDirection: 'row', position: 'absolute', top: xs(280), left: 0, flex: 1, paddingHorizontal: 10, paddingVertical: 10, justifyContent: 'space-between', alignItems: 'center' }}
				nextButton={<Text style={[ c.blue, { fontFamily: 'Roboto-Bold' }]}>Next</Text>}
				prevButton={<Text style={[ c.blue, { fontFamily: 'Roboto-Bold' }]}>{this.state.number === 2 ? this.state.text: this.state.texts}</Text>}
			>
        		<FastImage source={require('../../asset/background_landing.png')} style={{ width: '100%', height: '100%', alignItems: 'center' }}>
					<FastImage source={require('../../asset/land1.png')} style={{ width: xs(300), height: xs(203), marginTop: vs(100) }} />
					<Text style={[ c.light, f._24, { marginTop: vs(20), fontFamily: 'Roboto-Bold' }]}>Cari Hadiah</Text>
					<Text style={[ f._16, c.light, { marginTop: vs(10), fontFamily: 'Roboto-Regular' }]}>Cari dan pilih Hadiamu</Text>
				</FastImage>

        		<FastImage source={require('../../asset/background_landing.png')} style={{ width: '100%', height: '100%', alignItems: 'center' }}>
					<FastImage source={require('../../asset/land2.png')} style={{ width: xs(300), height: xs(203), marginTop: vs(100) }} />
					<Text style={[ c.light, f._24, { marginTop: vs(20), fontFamily: 'Roboto-Bold' }]}>Bayar</Text>
					<View style={{ width: wp('70%'), alignItems: 'center' }}>
						<Text style={[ f._16, c.light, p.textCenter, { marginTop: vs(10), fontFamily: 'Roboto-Regular' }]}>Bayar dengan aman lewat berbagai metode pembayaran</Text>
					</View>
				</FastImage>

        		<FastImage source={require('../../asset/background_landing.png')} style={{ width: '100%', height: '100%', alignItems: 'center' }}>
					<FastImage source={require('../../asset/land3.png')} style={{ width: xs(300), height: xs(222), marginTop: vs(100) }} />
					<Text style={[ c.light, f._24, { marginTop: vs(20), fontFamily: 'Roboto-Bold' }]}>Kirim dan Redeem</Text>
					<Text style={[ f._16, c.light, p.textCenter, { marginTop: vs(10), fontFamily: 'Roboto-Regular' }]}>Personalisasi hadiah,</Text>
					<Text style={[ f._16, c.light, p.textCenter, { fontFamily: 'Roboto-Regular' }]}> kirim hadiah dan Redeem</Text>

					<TouchableOpacity onPress={() => this.nextScreen()} style={[ b.roundedLow, { width: wp('80%'), height: hp('7.5%'), backgroundColor: blue, alignItems: 'center', justifyContent: 'center', marginTop: xs(125) }]}>
						<Text style={[ c.light, { fontFamily: 'Roboto-Bold' }]}>Mulai memberi Hadiah</Text>
					</TouchableOpacity>
				</FastImage>
        	</Swiper>
        )
    }
}
