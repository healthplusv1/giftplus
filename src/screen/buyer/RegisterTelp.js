import React, { Component } from 'react'
import { Text, View, TextInput, TouchableOpacity, ToastAndroid } from 'react-native'
import { xs, vs } from '../../utils/Responsive'
import { f, c, b, p } from '../../utils/StyleHelper'
import { blue, light } from '../../utils/Color'
import FastImage from 'react-native-fast-image'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
import auth from '@react-native-firebase/auth'
import app from '@react-native-firebase/app'
import iid from '@react-native-firebase/iid'
import AsyncStorage from '@react-native-async-storage/async-storage'
import axios from 'axios'
import { API_URL } from '@env'

export default class RegisterTelp extends Component {
	constructor(props) {
		super(props);
		this.state = {
			no_hp: '',
			code: '',
			confirm: null,
			timer: 60
		}
	}

	async componentDidMount() {
		auth().onAuthStateChanged(async user => {
            if (user) {
                console.log("logged in");
                // console.log("phone => ", await AsyncStorage.getItem('@phone'))
                if (await AsyncStorage.getItem('@phone') !== null) {
                    this.props.navigation.navigate('InputName')
                } else {
                	console.log('error')
                }
            } else {
                console.log("not logged in");
            }
        })
	}

	registerPhone = async (phoneNumber) => {
        this.setState({ timer: 120 })
        axios.get(API_URL+'/main/register', {params: {
        	method: 'phone',
            no_hp: this.state.no_hp
        }})
        .then(async (resp) => {
            // console.log(resp.data)
            if (resp.data.status === 'sudah terdaftar') {
                ToastAndroid.show("Nomor telepon ini sudah terdaftar", ToastAndroid.SHORT)
            } else {
                AsyncStorage.setItem('@phone', this.state.no_hp)
                try {
                    const confirmation = await auth().signInWithPhoneNumber(phoneNumber)
                    console.log("confirm => ", confirmation)
                    this.setState({ confirm: confirmation })
                    let timer_interval = setInterval(() => {
                        if(this.state.timer === 0){
                            clearInterval(timer_interval)
                        } else {
                            this.setState({
                                timer: this.state.timer - 1
                            })
                        }
                    }, 1000)
                } catch (error) {
                    console.log("Invalid => ", error)
                    if (error.code === 'auth/unknown') {
                        ToastAndroid.show("Too many otp request, please try again later", ToastAndroid.SHORT)
                    }
                }
            }
        })
    }

    confirmCode = async () => {
        try {
            await this.state.confirm.confirm(this.state.code)
            ToastAndroid.show("Success", ToastAndroid.SHORT)
            this.props.navigation.push('InputName')
        } catch (error) {
            if (error.code === 'auth/session-expired') {
                ToastAndroid.show("Kode sms sudah tidak berlaku. Harap kirim ulang kode verifikasi untuk mencoba lagi", ToastAndroid.SHORT)
            } else if (error.code === 'auth/invalid-verification-code') {
                ToastAndroid.show("Kode verifikasi salah", ToastAndroid.SHORT)
            }
            console.log('Invalid Code => '+error)
        }
    }

    render() {
        return (
            <FastImage source={require('../../asset/background_login.png')} style={{ width: '100%', height: '100%', alignItems: 'center' }}>
                <FastImage source={require('../../asset/giftplus2.png')} style={{ width: xs(60), height: xs(75), marginTop: xs(30) }} />
                <Text style={[ c.light, f._16, { fontFamily: 'Roboto-Bold' }]}>GIFTPLUS</Text>
                {!this.state.confirm ?
                	<>
		                <Text style={[ c.light, f._18, { fontFamily: 'Roboto-Bold', marginTop: xs(30) }]}>Buat Akun</Text>
		                <View style={[ b.mt3, { width: wp('80%'), borderBottomWidth: 1, borderBottomColor: '#CCC' }]}>
		                	<TextInput
		                		placeholder='Nomor Handphone'
		                		placeholderTextColor='#CCC'
		                		keyboardType={"phone-pad"}
		                		style={[ c.light, { marginBottom: xs(-10), fontFamily: 'Roboto-Regular' }]}
		                		onChangeText={(text) => this.setState({ no_hp: text })}
		                		value={this.state.no_hp}
		                	/>
		                </View>
		                <View style={[ b.mt1, { width: wp('80%') }]}>
                            <Text style={[ f._10, c.light, b.ml1, { fontFamily: 'Roboto-Regular' }]}>Contoh: +6285222334444</Text>
                        </View>

		                <TouchableOpacity onPress={() => this.registerPhone(this.state.no_hp)} style={[ b.roundedLow, { width: wp('80%'), height: hp('7%'), backgroundColor: blue, marginTop: xs(50), alignItems: 'center', justifyContent: 'center' }]}>
		                	<Text style={[ c.light, f._16, { fontFamily: 'Roboto-Bold' }]}>Daftar</Text>
		                </TouchableOpacity>

		                <View style={{ marginTop: xs(10), flexDirection: 'row' }}>
		                	<Text style={{ color: '#CCC', fontFamily: 'Roboto-Regular' }}>Sudah punya akun?</Text>
		                	<Text style={[ c.blue, b.ml1, { fontFamily: 'Roboto-Bold' }]} onPress={() => this.props.navigation.push('LoginTelp')}>Masuk</Text>
		                </View>
                	</>
	                :
	                <>
		                <Text style={[ c.light, f._18, { fontFamily: 'Roboto-Bold' }]}>Masukan kode Verifikasi</Text>
		                <Text style={[ f._12, b.mt1, { color: '#CCC', fontFamily: 'Roboto-Regular' }]}>Kode verifikasi (OTP) telah dikirim ke</Text>
		                <Text style={[ f.bold, b.mt1, c.light ]}>{this.state.no_hp}</Text>

		                <View style={{ width: wp('80%'), marginTop: xs(50), flexDirection: 'row' }}>
		                	<TextInput
		                		placeholder='Masukkan Kode'
		                		placeholderTextColor='#CCC'
		                		keyboardType={"number-pad"}
		                		style={[ c.light, { marginBottom: xs(-10), fontFamily: 'Roboto-Regular' }]}
		                		onChangeText={(text) => this.setState({ code: text })}
		                		value={this.state.code}
		                	/>
		                </View>

		                <View style={{ marginTop: xs(20), flexDirection: 'row' }}>
		                	{this.state.timer === 0 ?
                                <Text onPress={() => this.registerPhone(this.state.no_hp)} style={[ b.mt1 ]}>Kirim Ulang Kode</Text>
                                :
                                <>
				                	<Text style={{ color: '#CCC', fontFamily: 'Roboto-Regular' }}>Tidak menerima kode?</Text>
				                	<Text style={[ c.blue, b.ml1, { fontFamily: 'Roboto-Bold' }]}>Kirim Ulang Kode ({this.state.timer})</Text>
                                </>
                            }
		                </View>

		                <TouchableOpacity onPress={this.confirmCode} style={[ b.roundedLow, { width: wp('80%'), height: hp('7%'), backgroundColor: blue, marginTop: xs(50), alignItems: 'center', justifyContent: 'center' }]}>
		                	<Text style={[ c.light, f._16, { fontFamily: 'Roboto-Bold' }]}>Konfirmasi Verifikasi Kode</Text>
		                </TouchableOpacity>
	                </>
                }
            </FastImage>
        )
    }
}
