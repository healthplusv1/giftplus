import React, { Component } from 'react'
import { Text, View, TextInput, TouchableOpacity, RefreshControl, Alert } from 'react-native'
import { xs, vs } from '../../utils/Responsive'
import { f, c, b, p } from '../../utils/StyleHelper'
import { blue, light } from '../../utils/Color'
import FastImage from 'react-native-fast-image'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
import AsyncStorage from '@react-native-async-storage/async-storage'
import axios from 'axios'
import { API_LOCAL, API_URL } from '@env'

import firebase from '@react-native-firebase/app'
import iid from '@react-native-firebase/iid';

export default class LoginEmail extends Component {
	constructor(props) {
		super(props);
		this.state = {
            loading: true,
			email: '',
			password: '',
            email_valid: ''
		}
	}

    loginUser = async () => {
        if (this.state.email === '' && this.state.password === '') {
            Alert.alert('Enter details to signup!')
        } else {
            this.setState({ loading: true })

            const guest = await AsyncStorage.getItem('id_login_guest')

            axios.get(API_URL+'/main/login', {params: {
                method: 'native',
                email: this.state.email,
                password: this.state.password,
                id_login_guest: guest
            }})
            .then(async res => {
                this.setState({ loading: false })
                if (res.data.status === 'true') {
                    AsyncStorage.removeItem('id_login_guest')
                    AsyncStorage.setItem('@id_login', res.data.data.id_login)

                    /* renew id_notification in server, reducing logic failure */
                    const id_notification = await firebase.iid().getToken();
                    console.log("ID_NOTIFICATION ==> ", id_notification)
                    axios.get(API_URL+'/main/update_id_notification', {params: {id_login: res.data.data.id_login, id_notification: id_notification}})
                    /* end renew id_notification */

                    this.props.navigation.push('Tabs')
                } else if (res.data.status === 'kredensial salah') {
                    Alert.alert('', 'Your email or password is wrong')
                } else {
                    Alert.alert('', 'Account not registered, please register first!')
                }
            })
        }
    }

    validate = (text) => {
        console.log(text);
        let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if (reg.test(text) === false) {
            // console.log("Email is Not Correct");
            this.setState({ email: text, email_valid: 'Email is Not Correct' })
            return true;
        }
        else {
            this.setState({ email: text, email_valid: 'Email is Correct' })
            // console.log("Email is Correct");
        }
    }

    render() {
        return (
            <>
                {this.state.loading === true ?
                    <RefreshControl onRefresh={() => this.loginUser()} refreshing={this.state.loading} />
                    :
                    null
                }
                <FastImage source={require('../../asset/background_login.png')} style={{ width: '100%', height: '100%', alignItems: 'center' }}>
                	<FastImage source={require('../../asset/giftplus2.png')} style={{ width: xs(60), height: xs(75), marginTop: xs(30) }} />
                    <Text style={[ c.light, f._16, { fontFamily: 'Roboto-Bold' }]}>GIFTPLUS</Text>
                    <Text style={[ c.light, f._18, { fontFamily: 'Roboto-Bold', marginTop: xs(30) }]}>Masuk</Text>

                    <View style={[ b.mt3, { width: wp('80%'), borderBottomWidth: 1, borderBottomColor: '#CCC' }]}>
                    	<TextInput
                    		placeholder='Email'
                            placeholderTextColor='#CCC'
                    		style={[ c.light, { marginBottom: xs(-10) }]}
                    		onChangeText={(text) => this.validate(text)}
                    		value={this.state.email}
                    	/>
                    </View>
                    {this.state.email_valid === 'Email is Not Correct' && this.state.email !== '' && (
                        <View style={{ width: wp('80%'), marginTop: vs(2) }}>
                            <Text style={{ color: 'red', marginLeft: vs(4) }}>Email is Not Correct</Text>
                        </View>
                    )}
                    {this.state.email_valid === 'Email is Not Correct' && this.state.email === '' && (
                        <View style={{ width: wp('80%'), marginTop: vs(2) }} />
                    )}
                    <View style={[ b.mt2, { width: wp('80%') }]}>
                        <TouchableOpacity onPress={() => this.props.navigation.push('LoginTelp')}>
                            <Text style={[ c.blue, { fontFamily: 'Roboto-Bold' }]}>Masuk dengan no Handphone</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={[ b.mt3, { width: wp('80%'), borderBottomWidth: 1, borderBottomColor: '#CCC' }]}>
                    	<TextInput
                    		placeholder='Password'
                            placeholderTextColor='#CCC'
                            secureTextEntry= {true}
                    		style={[ c.light, { marginBottom: xs(-10) }]}
                    		onChangeText={(text) => this.setState({ password: text })}
                    		value={this.state.password}
                    	/>
                    </View>
                    <View style={[ b.mt3, { width: wp('80%') }]}>
                        <TouchableOpacity>
                            <Text style={[ c.blue, { fontFamily: 'Roboto-Bold' }]}>Lupa password</Text>
                        </TouchableOpacity>
                    </View>

                    <TouchableOpacity onPress={() => this.loginUser()} style={[ b.roundedLow, { width: wp('80%'), height: hp('7%'), marginTop: xs(50), backgroundColor: blue, alignItems: 'center', justifyContent: 'center' }]}>
                    	<Text style={[ c.light, f._16, { fontFamily: 'Roboto-Bold' }]}>Masuk</Text>
                    </TouchableOpacity>

                    <View style={{ marginTop: xs(10), flexDirection: 'row' }}>
                    	<Text style={{ color: '#CCC', fontFamily: 'Roboto-Regular' }}>Belum punya akun?</Text>
                    	<Text style={[ b.ml1, c.blue, { fontFamily: 'Roboto-Bold' }]} onPress={() => this.props.navigation.pop()}>Daftar</Text>
                    </View>
                </FastImage>
            </>
        )
    }
}
