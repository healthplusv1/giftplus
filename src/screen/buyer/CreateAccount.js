import React, { Component } from 'react'
import { Text, View, TouchableOpacity, Alert } from 'react-native'
import { xs, vs } from '../../utils/Responsive'
import { f, c, b, p } from '../../utils/StyleHelper'
import { blue, light } from '../../utils/Color'
import FastImage from 'react-native-fast-image'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
import { AccessToken, GraphRequest, GraphRequestManager, LoginManager } from 'react-native-fbsdk'
import { GoogleSignin, statusCodes } from '@react-native-google-signin/google-signin'
import AsyncStorage from '@react-native-async-storage/async-storage'
import axios from 'axios'
import { API_LOCAL, API_URL } from '@env'

export default class Intro extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            email: '',
            id_fb: '',
            id_email: '',
            userInfo: {}
        }
    }

    googleSignIn = async () => {
        try {
            GoogleSignin.configure({
                // scopes: ['https://www.googleapis.com/auth/drive.readonly'], // what API you want to access on behalf of the user, default is email and profile
                webClientId: '747415462681-334depgede6hhpolhjl907c5h38bn0kn.apps.googleusercontent.com', // client ID of type WEB for your server (needed to verify user ID and offline access)
                // offlineAccess: true, // if you want to access Google API on behalf of the user FROM YOUR SERVER
                hostedDomain: '', // specifies a hosted domain restriction
                loginHint: '', // [iOS] The user's ID, or email address, to be prefilled in the authentication UI if possible. [See docs here](https://developers.google.com/identity/sign-in/ios/api/interface_g_i_d_sign_in.html#a0a68c7504c31ab0b728432565f6e33fd)
                forceConsentPrompt: true, // [Android] if you want to show the authorization prompt at each login.
                // accountName: '', // [Android] specifies an account name on the device that should be used
                // iosClientId: '<FROM DEVELOPER CONSOLE>', // [iOS] optional, if you want to specify the client ID of type iOS (otherwise, it is taken from GoogleService-Info.plist)
            });
            await GoogleSignin.hasPlayServices();
            const userInfo = await GoogleSignin.signIn();
            // console.log(userInfo)
            this.setState({
                name: userInfo.user.name,
                email: userInfo.user.email,
                id_email: userInfo.user.id
            })

            const guest = await AsyncStorage.getItem('id_login_guest')
            
            axios.get(API_URL+'/main/login', {params: {
                method: 'google',
                nama: this.state.name,
                email: this.state.email,
                id_login: this.state.id_email,
                id_login_guest: guest
            }})
            .then(res => {
                AsyncStorage.removeItem('id_login_guest')
                AsyncStorage.setItem('@id_login', this.state.id_email)
                this.props.navigation.push('Tabs')
            })
        } catch (error) {
            if (error.code === statusCodes.SIGN_IN_CANCELLED) {
                // user cancelled the login flow
            } else if (error.code === statusCodes.IN_PROGRESS) {
                // operation (e.g. sign in) is in progress already
            } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
                // play services not available or outdated
            } else {
                // some other error happened
            }
            console.log(error)
        }
    }

    getInfoFromToken = token => {
        const PROFILE_REQUEST_PARAMS = {
            fields: {
                string: 'id, name, first_name, last_name'
            }
        };
        const profileRequest = new GraphRequest(
            '/me',
            {token, parameters: PROFILE_REQUEST_PARAMS},
            async (error, user) => {
                if (error) {
                    Alert.alert('Login info has error: '+error);
                    Alert.alert('Error fetching data: '+error.toString());
                } else {
                    this.setState({ userInfo: user })

                    const guest = await AsyncStorage.getItem('id_login_guest')

                    axios.get(API_URL+'/main/register', {params: {
                        method: 'facebook',
                        nama: this.state.userInfo.name,
                        id_login: this.state.userInfo.id,
                        id_login_guest: guest
                    }})
                    .then(res => {
                        AsyncStorage.removeItem('id_login_guest')
                        AsyncStorage.setItem('@id_login', this.state.userInfo.id)
                        this.props.navigation.push('Tabs')
                    })
                }
            }
        )
        new GraphRequestManager().addRequest(profileRequest).start();
    }

    facebookSignIn = () => {
        LoginManager.logInWithPermissions(['public_profile'])
        .then(login => {
            if (login.isCancelled) {
                console.log('Login cancelled')
            } else {
                AccessToken.getCurrentAccessToken()
                .then(data => {
                    const accessToken = data.accessToken.toString();
                    this.getInfoFromToken(accessToken)
                })
            }
        }, error => {
            console.log('Login failed with error: '+error)
        })
    }

    render() {
        return (
            <FastImage source={require('../../asset/background_login.png')} style={{ width: '100%', height: '100%', alignItems: 'center' }}>
            	<FastImage source={require('../../asset/giftplus2.png')} style={{ width: xs(60), height: xs(75), marginTop: xs(30) }} />
                <Text style={[ f._16, c.light, { fontFamily: 'Roboto-Bold' }]}>GIFTPLUS</Text>
                <Text style={[ f._18, c.light, { fontFamily: 'Roboto-Bold', marginTop: xs(20) }]}>DAFTAR</Text>
                <Text style={[ b.mt2, c.light, { fontFamily: 'Roboto-Regular' }]}>Buat akun dengan memilih pilihan dibawah:</Text>

                <TouchableOpacity onPress={() => this.facebookSignIn()} style={[ b.mt3, b.roundedLow, { width: wp('80%'), height: hp('7%'), backgroundColor: light, borderWidth: 1, borderColor: blue, flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }]}>
                	<FastImage source={require('../../asset/facebook_logo.png')} style={{ width: xs(25), height: xs(25) }} />
                	<Text style={[ b.ml2, { fontFamily: 'Roboto-Regular' }]}>Daftar dengan Facebook</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.googleSignIn()} style={[ b.mt3, b.roundedLow, { width: wp('80%'), height: hp('7%'), backgroundColor: light, borderWidth: 1, borderColor: blue, flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }]}>
                	<FastImage source={require('../../asset/google_logo.png')} style={{ width: xs(25), height: xs(25) }} />
                	<Text style={[ b.ml2, { fontFamily: 'Roboto-Regular' }]}>Daftar dengan Google</Text>
                </TouchableOpacity>

                <View style={[ b.mt3, b.roundedLow, { width: wp('80%'), flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }]}>
                	<View style={{ width: wp('33%'), borderBottomWidth: 1, borderBottomColor: '#CCC' }} />
                	<Text style={[ b.ml2, b.mr2, { color: '#CCC', fontFamily: 'Roboto-Regular' }]}>Atau</Text>
                	<View style={{ width: wp('33%'), borderBottomWidth: 1, borderBottomColor: '#CCC' }} />
                </View>

                <TouchableOpacity onPress={() => this.props.navigation.push('RegisterEmail')} style={[ b.mt3, b.roundedLow, { width: wp('80%'), height: hp('7%'), backgroundColor: blue, alignItems: 'center', justifyContent: 'center' }]}>
                	<Text style={[ b.ml2, c.light, { fontFamily: 'Roboto-Regular' }]}>Daftar dengan Email</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.props.navigation.push('RegisterTelp')} style={[ b.mt3, b.roundedLow, { width: wp('80%'), height: hp('7%'), backgroundColor: blue, alignItems: 'center', justifyContent: 'center' }]}>
                	<Text style={[ b.ml2, c.light, { fontFamily: 'Roboto-Regular' }]}>Daftar dengan Nomor Handphone</Text>
                </TouchableOpacity>
            </FastImage>
        )
    }
}
