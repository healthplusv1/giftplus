import React from 'react'
import { Image, Text } from 'react-native'
import { createAppContainer, createSwitchNavigator } from 'react-navigation'
import { createStackNavigator } from 'react-navigation-stack'
import { createBottomTabNavigator } from 'react-navigation-tabs'
import { blue, light } from '../utils/Color'
import { xs, vs } from '../utils/Responsive'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'

const Home = require('../asset/icons/home.png')
const Gift = require('../asset/icons/giftcardmenu_icon.png')
const Mall = require('../asset/icons/mall1_ikon.png')
const MallActive = require('../asset/icons/mall2_ikon.png')
const Toko = require('../asset/icons/tokolokasi_icon.png')
const Profile = require('../asset/icons/profilebutton.png')

//================= impor stack ================
import { HomeStack } from '../module/buyer/home/HomeRoutes'
import { GiftStack } from '../module/buyer/gift/GiftRoutes'
import { MallBuyerStack } from '../module/buyer/mall/MallRoutes'
import { ProfileStack } from '../module/buyer/profile/ProfileRoutes'

//================= impor stack seller ================
import { HomeSellerStack } from '../module/seller/home/HomeRoutes'
import { GiftCardStack } from '../module/seller/giftCard/GiftCardRoutes'
import { MallStack } from '../module/seller/mall/MallRoutes'
import { TokoStack } from '../module/seller/toko/TokoRoutes'
import { ProfileSellerStack } from '../module/seller/profile/ProfileRoutes'

//start buyer
//================= impor screen ================
import SplashScreenPage from '../screen/SplashScreen'
import IntroPage from '../screen/buyer/Intro'
import CreateAccountPage from '../screen/buyer/CreateAccount'
import RegisterEmailPage from '../screen/buyer/RegisterEmail'
import LoginEmailPage from '../screen/buyer/LoginEmail'
import RegisterTelpPage from '../screen/buyer/RegisterTelp'
import LoginTelpPage from '../screen/buyer/LoginTelp'

//================= impor home ================
import SpecialPage from '../module/buyer/home/screen/Special'
import SpecialPromoPage from '../module/buyer/home/screen/SpecialPromo'
import MonthPromoPage from '../module/buyer/home/screen/MonthPromo'
import AllRatingsPage from '../module/buyer/home/screen/AllRatings'
import AddRatingPage from '../module/buyer/home/screen/AddRating'
import CartPage from '../module/buyer/home/screen/Cart'
import PromoCodePage from '../module/buyer/home/screen/PromoCode'
import DetailPromoCodePage from '../module/buyer/home/screen/DetailPromoCode'
import PaymentPage from '../module/buyer/home/screen/Payment'
import TopUpPage from '../module/buyer/home/screen/TopUp'
import PaymentWalletPage from '../module/buyer/home/screen/PaymentWallet'
import PaymentMethodPage from '../module/buyer/home/screen/PaymentMethod'
import PaymentDetailPage from '../module/buyer/home/screen/PaymentDetail'
import GiftCardPage from '../module/buyer/home/screen/GiftCard'
import RecipientPage from '../module/buyer/home/screen/Recipient'
import NotificationPage from '../module/buyer/home/screen/Notification'
import DrawerPage from '../module/buyer/home/screen/Drawer'
import ChatListPage from '../module/buyer/home/screen/ChatList'
import ChatDetailPage from '../module/buyer/home/screen/ChatDetail'
import ScanQRPage from '../module/buyer/home/screen/ScanQR'
import ScanPreviewPage from '../module/buyer/home/screen/ScanPreview'
import ChooseProductPage from '../module/buyer/home/screen/ChooseProduct'
import EdukasiPenjualPage from '../module/buyer/home/screen/EdukasiPenjual'
import TrenPage from '../module/buyer/home/screen/Tren'
import KriteriaPage from '../module/buyer/home/screen/Kriteria'
import PerformaPage from '../module/buyer/home/screen/Performa'
import StrategiPage from '../module/buyer/home/screen/Strategi'

//================= impor gift ================
import DesignGiftCardPage from '../module/buyer/gift/screen/DesignGiftCard'
import GiftCardSchedulePage from '../module/buyer/gift/screen/GiftCardSchedule'

//================= impor mall ================
import DaftarBrandPage from '../module/buyer/mall/screen/DaftarBrand'

//================= impor profile ================
import OrderDetailPage from '../module/buyer/profile/screen/OrderDetail'
import HelpPage from '../module/buyer/profile/screen/Help'
import LaporanPage from '../module/buyer/profile/screen/Laporan'
//end buyer

//start seller
//================= impor screen ================
import SellerNotifPage from '../screen/seller/SellerNotif'
import JoinSellerPage from '../screen/seller/JoinSeller'
import FormJoinSellerPage from '../screen/seller/FormJoinSeller'
import VerificationSellerPage from '../screen/seller/VerificationSeller'

//================= impor toko ================
import SaldoDetailPage from '../module/seller/toko/screen/SaldoDetail'
import ScanQRSellerPage from '../module/seller/toko/screen/ScanQRSeller'

const MainTabs = createBottomTabNavigator({
    ModulHome: {
        screen: HomeStack,
        navigationOptions: {
            tabBarLabel: "Beranda",
            tabBarIcon: ({ focused }) => (
                <Image source={Home} style={{ width: wp('6.5%'), height: wp('6.5%'), tintColor: focused ? blue : light }} />
            )
        }
    },

    ModulGift: {
        screen: GiftStack,
        navigationOptions: {
            tabBarLabel: "Gift card",
            tabBarIcon: ({ focused }) => (
                <Image source={Gift} style={{ width: wp('6.5%'), height: wp('6.5%'), tintColor: focused ? blue : light }} />
            )
        }
    },

    ModulMall: {
        screen: MallBuyerStack,
        navigationOptions: {
            tabBarLabel: "Mall",
            tabBarIcon: ({ focused }) => (
                <Image source={focused ? MallActive : Mall} style={{ width: wp('5.5%'), height: wp('6.5%'), tintColor: focused ? blue : light }} />
            )
        }
    },

    ModulProfile: {
        screen: ProfileStack,
        navigationOptions: {
            tabBarLabel: "Profile",
            tabBarIcon: ({ focused }) => (
                <Image source={Profile} style={{ width: wp('6.5%'), height: wp('6.5%'), tintColor: focused ? blue : light }} />
            )
        }
    }
}, {
    tabBarOptions: {
        activeTintColor: blue,
        inactiveTintColor: '#FFF',
        showLabel: true,
        style:{
            backgroundColor: '#2A2A2A',
            height: xs(50),
            borderTopColor: '#2A2A2A'
        },
        labelStyle:{
            marginTop: vs(-10),
            marginBottom: vs(8)
        }
    },
    initialRouteName: "ModulHome"
})

const SellerTabs = createBottomTabNavigator({
    // ModulHome: {
    //     screen: HomeSellerStack,
    //     navigationOptions: {
    //         tabBarLabel: "Beranda",
    //         tabBarIcon: ({ focused }) => (
    //             <Image source={Home} style={{ width: wp('6.5%'), height: wp('6.5%'), tintColor: focused ? blue : light }} />
    //         )
    //     }
    // },

    ModulGift: {
        screen: GiftCardStack,
        navigationOptions: {
            tabBarLabel: "Gift card",
            tabBarIcon: ({ focused }) => (
                <Image source={Gift} style={{ width: wp('6.5%'), height: wp('6.5%'), tintColor: focused ? blue : light }} />
            )
        }
    },

    ModulMall: {
        screen: MallStack,
        navigationOptions: {
            tabBarLabel: "Mall",
            tabBarIcon: ({ focused }) => (
                <Image source={Gift} style={{ width: wp('6.5%'), height: wp('6.5%'), tintColor: focused ? blue : light }} />
            )
        }
    },

    ModulToko: {
        screen: TokoStack,
        navigationOptions: {
            tabBarLabel: "Toko",
            tabBarIcon: ({ focused }) => (
                <Image source={Toko} style={{ width: wp('6.5%'), height: wp('6.5%'), tintColor: focused ? blue : light }} />
            )
        }
    },

    ModulProfile: {
        screen: ProfileSellerStack,
        navigationOptions: {
            tabBarLabel: "Profile",
            tabBarIcon: ({ focused }) => (
                <Image source={Profile} style={{ width: wp('6.5%'), height: wp('6.5%'), tintColor: focused ? blue : light }} />
            )
        }
    }
}, {
    tabBarOptions: {
        activeTintColor: blue,
        inactiveTintColor: '#FFF',
        showLabel: true,
        style:{
            backgroundColor: '#2A2A2A',
            height: xs(50),
            borderTopColor: '#2A2A2A'
        },
        labelStyle:{
            marginTop: vs(-10),
            marginBottom: vs(8)
        }
    },
    initialRouteName: "ModulToko"
})

const App = createStackNavigator({
    SplashScreen: {
        screen: SplashScreenPage,
        navigationOptions: {
            headerShown: false
        }
    },

    //buyer
    Intro: {
        screen: IntroPage,
        navigationOptions: {
            headerShown: false
        }
    },

    CreateAccount: {
        screen: CreateAccountPage,
        navigationOptions: {
            headerShown: false
        }
    },

    RegisterEmail: {
        screen: RegisterEmailPage,
        navigationOptions: {
            headerShown: false
        }
    },

    LoginEmail: {
        screen: LoginEmailPage,
        navigationOptions: {
            headerShown: false
        }
    },

    RegisterTelp: {
        screen: RegisterTelpPage,
        navigationOptions: {
            headerShown: false
        }
    },

    LoginTelp: {
        screen: LoginTelpPage,
        navigationOptions: {
            headerShown: false
        }
    },

    //Seller
    SellerNotif: {
        screen: SellerNotifPage,
        navigationOptions: {
            headerShown: false
        }
    },

    JoinSeller: {
        screen: JoinSellerPage,
        navigationOptions: {
            headerShown: false
        }
    },

    FormJoinSeller: {
        screen: FormJoinSellerPage,
        navigationOptions: {
            headerShown: false
        }
    },

    VerificationSeller: {
        screen: VerificationSellerPage,
        navigationOptions: {
            headerShown: false
        }
    },

    Tabs: {
        screen: MainTabs,
        navigationOptions: {
            headerShown: false
        }
    },

    //Home
    Special: {
        screen: SpecialPage,
        navigationOptions: {
            headerShown: false
        }
    },

    SpecialPromo: {
        screen: SpecialPromoPage,
        navigationOptions: {
            headerShown: false
        }
    },

    MonthPromo: {
        screen: MonthPromoPage,
        navigationOptions: {
            headerShown: false
        }
    },

    AllRatings: {
        screen: AllRatingsPage,
        navigationOptions: {
            headerShown: false
        }
    },

    AddRating: {
        screen: AddRatingPage,
        navigationOptions: {
            headerShown: false
        }
    },

    Cart: {
        screen: CartPage,
        navigationOptions: {
            headerShown: false
        }
    },

    PromoCode: {
        screen: PromoCodePage,
        navigationOptions: {
            headerShown: false
        }
    },

    DetailPromoCode: {
        screen: DetailPromoCodePage,
        navigationOptions: {
            headerShown: false
        }
    },

    Payment: {
        screen: PaymentPage,
        navigationOptions: {
            headerShown: false
        }
    },

    TopUp: {
        screen: TopUpPage,
        navigationOptions: {
            headerShown: false
        }
    },

    PaymentWallet: {
        screen: PaymentWalletPage,
        navigationOptions: {
            headerShown: false
        }
    },

    PaymentMethod: {
        screen: PaymentMethodPage,
        navigationOptions: {
            headerShown: false
        }
    },

    PaymentDetail: {
        screen: PaymentDetailPage,
        navigationOptions: {
            headerShown: false
        }
    },

    GiftCard: {
        screen: GiftCardPage,
        navigationOptions: {
            headerShown: false
        }
    },

    Recipient: {
        screen: RecipientPage,
        navigationOptions: {
            headerShown: false
        }
    },

    Notification: {
        screen: NotificationPage,
        navigationOptions: {
            headerShown: false
        }
    },

    Drawer: {
        screen: DrawerPage,
        navigationOptions: {
            headerShown: false
        }
    },

    ChatList: {
        screen: ChatListPage,
        navigationOptions: {
            headerShown: false
        }
    },

    ChatDetail: {
        screen: ChatDetailPage,
        navigationOptions: {
            headerShown: false
        }
    },

    ScanQR: {
        screen: ScanQRPage,
        navigationOptions: {
            headerShown: false
        }
    },

    ScanPreview: {
        screen: ScanPreviewPage,
        navigationOptions: {
            headerShown: false
        }
    },

    ChooseProduct: {
        screen: ChooseProductPage,
        navigationOptions: {
            headerShown: false
        }
    },

    EdukasiPenjual: {
        screen: EdukasiPenjualPage,
        navigationOptions: {
            headerShown: false
        }
    },

    Tren: {
        screen: TrenPage,
        navigationOptions: {
            headerShown: false
        }
    },

    Kriteria: {
        screen: KriteriaPage,
        navigationOptions: {
            headerShown: false
        }
    },

    Performa: {
        screen: PerformaPage,
        navigationOptions: {
            headerShown: false
        }
    },

    Strategi: {
        screen: StrategiPage,
        navigationOptions: {
            headerShown: false
        }
    },

    //Gift
    DesignGiftCard: {
        screen: DesignGiftCardPage,
        navigationOptions: {
            headerShown: false
        }
    },

    GiftCardSchedule: {
        screen: GiftCardSchedulePage,
        navigationOptions: {
            headerShown: false
        }
    },

    //Mall
    DaftarBrand: {
        screen: DaftarBrandPage,
        navigationOptions: {
            headerShown: false
        }
    },

    //Profile
    OrderDetail: {
        screen: OrderDetailPage,
        navigationOptions: {
            headerShown: false
        }
    },

    Help: {
        screen: HelpPage,
        navigationOptions: {
            headerShown: false
        }
    },

    Laporan: {
        screen: LaporanPage,
        navigationOptions: {
            headerShown: false
        }
    },
    //end buyer

    //Seller
    Seller: {
        screen: SellerTabs,
        navigationOptions: {
            headerShown: false
        }
    },

    //Toko
    SaldoDetail: {
        screen: SaldoDetailPage,
        navigationOptions: {
            headerShown: false
        }
    },

    ScanQRSeller: {
        screen: ScanQRSellerPage,
        navigationOptions: {
            headerShown: false
        }
    },

    initialRouteName: 'SplashScreen'
})

export default createAppContainer(App)